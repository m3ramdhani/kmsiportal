﻿@using System.Web.Optimization
@using System.IO
@using System.Reflection
@using System.Text
@using System.Diagnostics
@using WebHelpers.Mvc5.Enum
@using WebHelpers.Mvc5
@using KMSIPartsPortal_System.Models
@{
    Layout = null;
}

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>
        @if (!string.IsNullOrWhiteSpace(ViewBag.Title))
        {
            @ViewBag.Title
        }
    </title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="~/Content/css/bootstrap.min.css">
    <!-- jQuery UI -->
    <link rel="stylesheet" href="~/Content/css/jquery-ui.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="~/Content/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="~/Content/components/Ionicons/css/ionicons.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="~/Content/components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <!-- DataTables Button -->
    <link rel="stylesheet" href="~/Content/components/datatables.net/css/button.dataTables.min.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="~/Content/components/bootstrap-daterangepicker/daterangepicker.css" />
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="~/Content/css/bootstrap-datepicker3.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="~/Content/css/icheck/blue.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="~/Content/css/AdminLTE.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="~/Content/css/skins/_all-skins.css">
    <!-- Owned style -->
    <link rel="stylesheet" href="~/Content/css/style.css">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="skin-default sidebar-mini" style="background:url(/Content/img/bg-dashboard.jpg);">
    <div id="overlay"></div>
    <div class="wrapper" style="background:transparent;">
        <header class="main-header">
            <a class="logo" style="width: 365px;">
                @* Mini logo for sidebar 50x50 pixels *@
                <span class="logo-mini"><b>A</b>LT</span>
                @* Logo for regular state and mobile devices *@
                @*<img style="height: 100%;" src="~/Content/img/logo_new.png">*@
                <div class="head-top-logo">K-PINTAR PARTS PORTAL SYSTEM</div>
            </a>
            @* END Logo *@
            @* BEGIN Header *@
            <nav class="navbar navbar-static-top" role="navigation" style="margin-left: 350px;">
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <li class="dropdown messages-menu">
                            <a class="dropdown-toggle" data-toggle="dropdown">
                                <i data-toggle="tooltip" class="fa fa-envelope" title="Notification"></i>
                                <span class="label label-danger">1</span>
                            </a>
                        </li>
                        <li class="dropdown messages-menu">
                            <a class="dropdown-toggle" data-toggle="dropdown" role="button" onclick="popContact()">
                                <i data-toggle="tooltip" class="fa fa-user" title="Contact Admin"></i>
                            </a>
                            <div class="box box-success box-contact" style="display:none;">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Contact Admin</h3>
                                    <!-- /.box-tools -->
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <table class="tbl-contact">
                                        <tr>
                                            <td>Phone</td>
                                            <td>:</td>
                                            <td>-</td>
                                        </tr>
                                        <tr>
                                            <td>Email</td>
                                            <td>:</td>
                                            <td>-</td>
                                        </tr>
                                    </table>
                                    <br>
                                    <br>
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </li>

                        @if (Session["RoleType"] == null || Session["RoleType"].ToString() == "Internal")
                        {
                            <li class="dropdown user user-menu">
                                <a href="@Url.Action("Internal", "Dashboard")">
                                    <span class="hidden-xs">
                                        <i data-toggle="tooltip" class="fa fa-home" title="Home"></i>
                                    </span>
                                </a>
                            </li>
                        }

                        @if (Session["RoleId"] == null)
                        {
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cogs"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li>@Html.ActionLink("User", "Index", "Account")</li>
                                    <li>@Html.ActionLink("User Level", "Index", "Roles")</li>
                                    <li>@Html.ActionLink("Module", "Index", "Modules")</li>
                                    <li>@Html.ActionLink("Main Menu", "Index", "MainMenus")</li>
                                    <li>@Html.ActionLink("Sub Menu", "Index", "SubMenus")</li>
                                    <li>@Html.ActionLink("Master Parts", "Index", "Parts")</li>
                                    <li>@Html.ActionLink("Slider", "Index", "Slider")</li>
                                    <li>@Html.ActionLink("Request Access", "Index", "ApprovalRequest")</li>
                                </ul>
                            </li>
                        }
                        @if (Convert.ToInt32(Session["SupplierId"]) != 0)
                        {
                            <li class="dropdown user user-menu">
                                <a href="@Url.Action("Details", "Suppliers", new { id = Convert.ToInt32(Session["SupplierId"]) })">
                                    <span class="hidden-xs">Company Profile</span>
                                </a>
                            </li>
                        }
                        @* BEGIN User Account Menu *@
                        <li class="dropdown user user-menu">
                            <a href="@Url.Action("Logout", "Account")">
                                <span class="hidden-xs">Sign out</span>
                            </a>
                        </li>
                        @* END User Account Menu *@
                    </ul>
                </div>
            </nav>
        </header>

        @if (TempData["ActionMessage"] != null)
        {
            <div class="callout callout-warning">
                @TempData["ActionMessage"].ToString()
            </div>
        }

            <div class="grid-menu-dashboard">
                <div class="headDashboard">
                    <div class="col-md-2 pull-right">
                        <marquee><i> @DateTime.Now.ToString("dddd, yyyy-MM-dd") </i></marquee>
                    </div>
                    <div style="margin-left: 18%;font-size: 20px;" class="hide">Welcome To,</div>
                    <div class="col-md-12" style="font-size: 32px;text-align:center;">WELCOME TO KOMATSU PINTAR PARTS PORTAL</div>
                    <div class="devideApp col-md-12"> Application List </div>
                </div>
                <section class="content">
                    <div class="container boxIconDashboard">
                        @* BEGIN Menu *@
                        <div class="">
                            @{
                                string bg = "bg-aqua";
                                string icon = "fa-book";
                                foreach (var item in ViewBag.ModuleInternal)
                                {

                                    if (item.ModuleName == "FMC")
                                    {
                                        icon = "fa-file";
                                        bg = "bg-aqua";
                                    }
                                    else if (item.ModuleName == "Procurement")
                                    {
                                        icon = "fa-shopping-cart";
                                        bg = "bg-yellow";
                                    }
                                    else if (item.ModuleName == "Helpdesk")
                                    {
                                        icon = "fa-user-circle";
                                        bg = "bg-green";
                                    }
                                    else if (item.ModuleName == "Vendor Stock")
                                    {
                                        icon = "fa-archive";
                                        bg = "bg-red";
                                        item.ModuleName = "Vendor Stock";
                                    }
                                    else if (item.ModuleName == "Surcharge")
                                    {
                                        icon = "fa-file-text";
                                        bg = "bg-orange";
                                    }
                                    else if (item.ModuleName == "Inventory")
                                    {
                                        icon = "fa-cubes";
                                        bg = "bg-navy";
                                    }
                                    else if (item.ModuleName == "Forwarder")
                                    {
                                        icon = "fa-truck";
                                        bg = "bg-olive";
                                    }
                                    else if (item.ModuleName == "Part Master 360")
                                    {
                                        icon = "fa-book";
                                        bg = "bg-maroon";
                                    }
                                    else if (item.ModuleName == "Market Size")
                                    {
                                        icon = "fa-pie-chart";
                                        bg = "bg-purple";
                                    }
                                    else if (item.ModuleName == "Daily Report")
                                    {
                                        icon = "fa-line-chart";
                                        bg = "bg-aqua";
                                    }

                                    <div class="col-md-2 item-internal">
                                        <a href="/Dashboard/ToDashboard/@item.ID">
                                            <div class="small-box @bg">
                                                <div class="icon-module">
                                                    <i class="fa @icon"></i>
                                                </div>
                                                <div class="small-box-footer">@item.ModuleName</div>
                                            </div>
                                        </a>
                                    </div>
                                }
                                <div class="col-md-2 item-internal">
                                    <a href="http://103.122.33.220/kpintar-ench/">
                                        <div class="small-box bg-green">
                                            <div class="icon-module">
                                                <i class="fa fa-map"></i>
                                            </div>
                                            <div class="small-box-footer">K-Pintar (Reman)</div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-2 item-internal">
                                    <a href="#">
                                        <div class="small-box bg-yellow">
                                            <div class="icon-module">
                                                <i class="fa fa-bullhorn"></i>
                                            </div>
                                            <div class="small-box-footer">Other</div>
                                        </div>
                                    </a>
                                </div>
                            }
                        </div>
                        @* END Menu *@
                    </div>
                </section>
            </div>

    </div>

    <script src="@RouteJs.RouteJsHandler.HandlerUrl"></script>
    <script src="@EnumHandler.HandlerUrl"></script>
    <!-- jQuery 3 -->
    <script src="~/Content/js/plugins/jquery/jquery-3.3.1.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="~/Content/js/plugins/bootstrap/bootstrap.min.js"></script>
    <!-- jQuery UI -->
    <script src="~/Content/js/plugins/jquery-ui/jquery-ui.min.js"></script>
    <!-- DataTables -->
    <script src="~/Content/components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="~/Content/components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <!-- jQuery Validator -->
    <script src="~/Content/js/plugins/validator/validator.min.js"></script>
    <!-- date-range-picker -->
    <script src="~/Content/js/plugins/moment/moment.min.js"></script>
    <script src="~/Content/components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap datepicker -->
    <script src="~/Content/js/plugins/datepicker/bootstrap-datepicker.min.js"></script>
    <!-- SlimScroll -->
    <script src="~/Content/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <!-- iCheck -->
    <script src="~/Content/js/plugins/icheck/icheck.min.js"></script>
    <!-- FastClick -->
    <script src="~/Content/js/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="~/Content/js/adminlte.js"></script>
    <!-- Page script -->
    <script src="~/Content/js/init.js"></script>
</body>
</html>