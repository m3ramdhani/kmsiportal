﻿$(function () {
    $(".datepicker").datepicker({
        autoclose: true
    });

    // Fix sidebar white space at bottom of page on resize
    $(window).on("load", function () {
        setTimeout(function () {
            $("body").layout("fix");
            $("body").layout("fixSidebar");
        }, 250);
    });
});

function initFunc() {
    $("body").tooltip({
        selector: '[data-toggle=tooltip]'
    });
}

function pageTransition(url) {
    window.location = url;
}

function onReady(callback) {
    var intervalID = window.setInterval(checkReady, 1000);
    function checkReady() {
        if (document.getElementsByTagName('body')[0] !== undefined) {
            window.clearInterval(intervalID);
            callback.call(this);
        }
    }
}

function initCustomAlert() {
    /* Init Alert*/
    var html = '<div class="modal fade" id="alertContainerCustom" role="dialog">' +
        '<div class="modal-dialog">' +
        '<div class="modal-content" style="margin: 0 auto;width: 70%">' +
        '<div class="modal-header hidden">' +
        '<button type="button" class="close" data-dismiss="modal">&times;</button>' +
        '<h4 class="modal-title">Modal Header</h4>' +
        '</div>' +
        '<div class="modal-body" style="padding:10px; text-align:center">' +
        '<p>[alert_text]</p>' +
        '<div style="height: 40px;">' +
        '<button class="btn bg-navy btn-ok pull-left" data-dismiss="modal" style="width: 100%">Ok</button>' +
        '<button class="btn btn-danger btn-cancel pull-right hidden" data-dismiss="modal" style="width: 49%; margin :0 0 0 2%">Cancel</button>' +
        '</div>' +
        '</div>' +
        '<div class="modal-footer hidden">' +
        '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>';
    $("body").append(html);
}

function customAlert(text, callback, nocallback) {
    console.log("customAlert");
    var uniqueId = new Date().getTime();

    $('#alertContainerCustom').find('p').html(text);
    $("#alertContainerCustom .btn-ok").css("width", "100%");
    $("#alertContainerCustom .btn-ok").addClass("btn-ok-alert-" + uniqueId);
    $("#alertContainerCustom").modal({
        show: true,
        keyboard: false,
        backdrop: 'static'
    });

    $('#alertContainerCustom').on('hidden.bs.modal', function () {
        $('#alertContainerCustom').find('p').html("alert text here");
        $("#alertContainerCustom .btn-ok").removeClass("btn-ok-alert-" + uniqueId);
        if (nocallback != null) nocallback();
    })

    $('#alertContainerCustom').on('click', '.btn-ok-alert-' + uniqueId, function () {
        if (callback != null) callback();
    });
}

function customConfirm(text, callback, nocallback) {
    var uniqueId = new Date().getTime();
    $('#alertContainerCustom').find('p').html(text);
    $("#alertContainerCustom .btn-cancel").removeClass("hidden");
    $("#alertContainerCustom .btn-cancel").addClass("btn-cancel-confirm-" + uniqueId);
    $("#alertContainerCustom .btn-ok").css("width", "49%");
    $("#alertContainerCustom .btn-ok").addClass("btn-ok-confirm-" + uniqueId);

    $("#alertContainerCustom").modal({
        show: true,
        keyboard: false,
        backdrop: 'static'
    });

    $('#alertContainerCustom').on('click', '.btn-ok-confirm-' + uniqueId, function () {
        if (callback != null) callback();
    });

    $('#alertContainerCustom').on('click', '.btn-cancel-confirm-' + uniqueId, function () {
        if (nocallback != null) nocallback();
    });

    $('#alertContainerCustom').on('hidden.bs.modal', function () {
        $('#alertContainerCustom').find('p').html("alert text here");
        $("#alertContainerCustom .btn-cancel").addClass("hidden");
        $("#alertContainerCustom .btn-cancel").removeClass("btn-cancel-confirm-" + uniqueId);
        $("#alertContainerCustom .btn-ok").removeClass("btn-ok-confirm-" + uniqueId);
    })
}

function centeredModalDialog() {
    function centerModal() {
        $(this).css('display', 'block');
        var $dialog = $(this).find(".modal-dialog"),
            offset = ($(window).height() - $dialog.height()) / 2,
            bottomMargin = parseInt($dialog.css('marginBottom'), 10);

        if (offset < bottomMargin) offset = bottomMargin;
        $dialog.css("margin-top", offset);
    }

    $(document).on('show.bs.modal', '.modal', centerModal);
    $(window).on("resize", function () {
        $('.modal:visible').each(centerModal);
    });
}

function show(id, value, timeOut) {
    if (value) {
        $(id).show();
    } else {
        var delay = 0;
        if (timeOut) {
            delay = timeOut;
        }
        setTimeout(function () {
            //your code to be executed after 1 second
            $(id).fadeOut(1000);
        }, delay);
    }
}

function moneyFormat(n, currency) {
    v = parseFloat(n).toFixed(0);
    return currency + " " + v.replace(/./g, function (c, i, a) {
        return i > 0 && c !== "," && (a.length - i) % 3 === 0 ? "." + c : c;
    });
}

function popContact(el) {
    $('.box-contact').fadeToggle("slow", "linear");
}

//untuk set style tiap module di add class di body nya
$(document).ready(function () {
    var str1 = $('.checkDs a').attr('href');
    var str2 = "Fmc";
    if (str1.indexOf(str2) != -1) {
        $("body").addClass(str2 + "-dsh");
    }
    var str2 = "Purchasing";
    if (str1.indexOf(str2) != -1) {
        $("body").addClass(str2 + "-dsh");
    }
    var str2 = "KmsiHelpdesk";
    if (str1.indexOf(str2) != -1) {
        $("body").addClass(str2 + "-dsh");
    }
    var str2 = "KmsiGstock";
    if (str1.indexOf(str2) != -1) {
        $("body").addClass(str2 + "-dsh");
    }

    //set sidebar menu jadi judul page
    var text = $('.treeview-menu .active a').text();
    var title = '<div class="global-title">'+text+'</div>';
    $('.content-wrapper section.content').prepend(title);

    //set overflow tabel karena font size yang di tambah besar nya
    setTimeout(function () {
        $('table').parent().addClass('dataTableOverflow');
    }, 800);
});

function actionAlertGlobal(message) {
    var alert = '<div class="alert alert-success alert-dismissible actionAlertBox">< button type = "button" class="close" data - dismiss="alert" aria - hidden="true" >×</button ><h4><i class="icon fa fa-check"></i> Alert!</h4>' + message +'</div >'
    $('.content-wrapper section.content').prepend(alert);
}

function secondsTodHms(v) {
    v = Number(v);
    var d = Math.floor(v / 86400);
    var h = Math.floor((v % 86400) / 3600);
    var m = Math.floor(((v % 86400) % 3600) / 60);
    var s = Math.floor(((v % 86400) % 3600) % 60);

    var dDisplay = d > 0 ? d + ("d, ") : "";
    var hDisplay = h > 0 ? h < 10 ? "0" + h : h : "00";
    var mDisplay = m > 0 ? m < 10 ? ":0" + m : ":" + m : ":00";
    var sDisplay = s > 0 ? s < 10 ? ":0" + s : ":" + s : ":00";
    return dDisplay + hDisplay + mDisplay + sDisplay;
}