﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Models
{
    [MetadataType(typeof(ModuleMetadata))]
    public partial class Module
    {

    }

    public class ModuleMetadata
    {
        [Display(Name = "Module Name")]
        public string ModuleName { get; set; }
    }
}