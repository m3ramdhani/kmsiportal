﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Models
{
    public class OutstandingPurchaseOrder
    {
        public string SupplierCode { get; set; }
        public string SupplierName { get; set; }
        public DateTime PurchaseDate { get; set; }
        public string PurchaseNumber { get; set; }
        public decimal Amount { get; set; }
        public decimal Aging { get; set; }
        public int ShipQty { get; set; }
        public int ReqQty { get; set; }
        public string Request { get; set; }
        public string ReqComment { get; set; }
        public string Remarks { get; set; }
    }
}