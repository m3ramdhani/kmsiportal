﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Models
{
    [MetadataType(typeof(OutgoingPurchasMetadata))]
    public partial class OutgoingPurchas
    {
        // can add extra/new code in here
    }

    public class OutgoingPurchasMetadata
    {
        [Display(Name = "PO Number")]
        public string PurchaseNumber { get; set; }
        [Display(Name = "Part Name")]
        public string PartName { get; set; }
        [Display(Name = "Supplier Code")]
        public string SupplierNumber { get; set; }
        [Display(Name = "PO Date")]
        public Nullable<System.DateTime> PurchaseDate { get; set; }
        [Display(Name = "Supplier Name")]
        public string SupplierName { get; set; }
        [Display(Name = "Item No")]
        public Nullable<int> ItemNumber { get; set; }
        [Display(Name = "Part Number")]
        public string PartNumber { get; set; }
        [Display(Name = "ORG Part")]
        public string OrgPartNumber { get; set; }
        [Display(Name = "Unit Price")]
        public Nullable<decimal> UnitPrice { get; set; }
        [Display(Name = "Amount (IDR)")]
        public Nullable<decimal> AmountIDR { get; set; }
    }
}