﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Models
{
    [MetadataType(typeof(PurchaseInvoiceDetailMetadata))]
    public partial class PurchaseInvoiceDetail
    {
        // can add extra/new code in here
        public string[] DeliveryOrders { get; set; }
        public int[] ItemNumbers { get; set; }
        public string[] PartNumbersKomatsu { get; set; }
        public decimal[] ShipQties { get; set; }
        public decimal[] UnitPrices { get; set; }
        public string[] CountryOfOrigins { get; set; }

    }
    public class PurchaseInvoiceDetailMetadata
    {
        [DisplayName("Supplier Code")]
        public string SupplierCode { get; set; }
        [DisplayName("Supplier Name")]
        public string SupplierName { get; set; }
        [DisplayName("PO Number")]
        public string PurchaseNumber { get; set; }
        [DisplayName("Invoice Date")]
        public Nullable<System.DateTime> InvoiceDate { get; set; }
        [DisplayName("Invoice Number")]
        public string InvoiceNumber { get; set; }
        [DisplayName("Delivery Order/Packing List")]
        public string DeliveryOrder { get; set; }
        [DisplayName("Item Number")]
        public Nullable<int> ItemNumber { get; set; }
        [DisplayName("Part Number Komatsu")]
        public string PartNumberKomatsu { get; set; }
        [DisplayName("Ship Quantity")]
        public Nullable<decimal> ShipQty { get; set; }
        [DisplayName("Unit Price")]
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public Nullable<decimal> UnitPrice { get; set; }
        [DisplayName("Country Of Origin")]
        public string CountryOfOrigin { get; set; }
    }
}