//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KMSIPartsPortal_System.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class PurchaseDetail
    {
        public int PurchaseDetailId { get; set; }
        public Nullable<int> PurchaseId { get; set; }
        public Nullable<int> PartsId { get; set; }
        public Nullable<int> ItemNumber { get; set; }
        public string Description { get; set; }
        public Nullable<decimal> RequestQuantity { get; set; }
        public Nullable<decimal> ProvidedQuantity { get; set; }
        public Nullable<decimal> Price { get; set; }
        public string Currency { get; set; }
        public Nullable<System.DateTime> EstDelivered { get; set; }
        public string DetailStatus { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public string SupplierCode { get; set; }
        public string SupplierName { get; set; }
        public Nullable<System.DateTime> PurchaseDate { get; set; }
        public string PurchaseNumber { get; set; }
        public string PartNumber { get; set; }
        public string StockPoint { get; set; }
        public Nullable<decimal> ProvidedQuantity_2 { get; set; }
        public Nullable<System.DateTime> EstDelivered_2 { get; set; }
        public Nullable<decimal> ProvidedQuantity_3 { get; set; }
        public Nullable<System.DateTime> EstDelivered_3 { get; set; }
        public Nullable<decimal> ProvidedQuantity_4 { get; set; }
        public Nullable<System.DateTime> EstDelivered_4 { get; set; }
        public Nullable<decimal> ProvidedQuantity_5 { get; set; }
        public Nullable<System.DateTime> EstDelivered_5 { get; set; }
        public string Remarks { get; set; }
    }
}
