﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Models
{
    public class MenuModels
    {
        public string ModuleName { get; set; }
        public int? ModuleId { get; set; }
        public string MainMenuName { get; set; }
        public int? MainMenuId { get; set; }
        public string SubMenuName { get; set; }
        public int? SubMenuId { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public int? RoleId { get; set; }
        public string RoleName { get; set; }
        public Nullable<int> IsInput { get; set; }
        public Nullable<int> IsUpdate { get; set; }
        public Nullable<int> IsDelete { get; set; }
        public Nullable<int> IsView { get; set; }
        public Nullable<int> IsUpload { get; set; }
        public Nullable<int> IsDownload { get; set; }
    }
}