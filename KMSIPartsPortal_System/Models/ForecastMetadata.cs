﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Models
{
    [MetadataType(typeof(ForecastMetadata))]
    public partial class Forecast
    {
        // can add extra/new code in here
    }

    public class ForecastMetadata
    {
        // Apply RequiredAttribute
        [Display(Name = "Supplier Code")]
        public string SupplierCode { get; set; }
        [Display(Name = "Type PO")]
        public string TypePO { get; set; }
        [Display(Name = "Part Number")]
        public string PartNumber { get; set; }
        [Display(Name = "PO Quantity")]
        public Nullable<int> PurchaseQty { get; set; }
        [Display(Name = "Stock Point")]
        public string StockPoint { get; set; }
    }
}