﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Models
{
    [MetadataType(typeof(SalesOrderListMetadata))]
    public partial class SalesOrderList
    {
        // can add extra/new code in here
    }

    public class SalesOrderListMetadata
    {
        [Display(Name = "Customer Purchase No.")]
        public string PurchaseNumber { get; set; }
        [Display(Name = "Item Name")]
        public string ItemName { get; set; }
        [Display(Name = "Order Quantity")]
        public Nullable<int> Quantity { get; set; }
        [Display(Name = "Order Date")]
        public Nullable<System.DateTime> SalesOrderDate { get; set; }
        [Display(Name = "Customer")]
        public string CustomerCode { get; set; }
        [Display(Name = "Customer Item")]
        public string CustomerItem { get; set; }
    }
}