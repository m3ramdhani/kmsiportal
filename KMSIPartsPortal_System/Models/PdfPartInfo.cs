﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Models
{
    public class PdfPartInfo
    {
        public string[] Columns { get; set; }
        public IEnumerable<Kpintar_Part_Info> Data { get; set; }
    }
}