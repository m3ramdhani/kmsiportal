﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Models
{
    public class StockMailModel
    {
        public string PartNumber { get; set; }
        public string QtyReceipt { get; set; }
        public string Position { get; set; }
        public string ConfirmedAt { get; set; }
        public string ConfirmedBy { get; set; }
    }
}