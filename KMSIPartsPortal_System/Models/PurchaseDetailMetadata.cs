﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Models
{
    [MetadataType(typeof(PurchaseDetailMetadata))]
    public partial class PurchaseDetail
    {
        // can add extra/new code in here
        public virtual PurchaseOrder PurchaseOrder { get; set; }
        public virtual Part Part { get; set; }
    }

    public class PurchaseDetailMetadata
    {
        // Apply RequiredAttribute
        [DisplayName("Supplier Code")]
        public string SupplierCode { get; set; }
        [DisplayName("Supplier Name")]
        public string SupplierName { get; set; }
        [DisplayName("PO Date")]
        public Nullable<System.DateTime> PurchaseDate { get; set; }
        [DisplayName("PO Number")]
        public string PurchaseNumber { get; set; }
        [DisplayName("Part Number")]
        public string PartNumber { get; set; }
        [DisplayName("Stock Point")]
        public string StockPoint { get; set; }
        [DisplayName("Item Number PO")]
        public Nullable<int> ItemNumber { get; set; }
        [DisplayName("PO Quantity")]
        public decimal RequestQuantity { get; set; }
        [DisplayName("Unit Price")]
        public Nullable<decimal> Price { get; set; }
        [DisplayName("ETD")]
        public Nullable<System.DateTime> EstDelivered { get; set; }
        [DisplayName("Ship Qty")]
        public Nullable<decimal> ProvidedQuantity { get; set; }
        [DisplayName("PO Detail Status")]
        public string DetailStatus { get; set; }

        // don't add any new code here, just match what is in your model classes and add data annotations to them.
    }
}