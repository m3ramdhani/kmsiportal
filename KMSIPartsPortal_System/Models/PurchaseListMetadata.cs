﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Models
{
    [MetadataType(typeof(PurchaseListMetadata))]
    public partial class PurchaseList
    {
        // can add extra/new code in here
        public int[] IDs { get; set; }
        public string[] ItemCodes { get; set; }
        public string[] ItemNames { get; set; }
        public string[] Units { get; set; }
        public int[] Quantities { get; set; }
    }

    public class PurchaseListMetadata
    {
        [Display(Name = "Sales No.")]
        public string PurchaseNumber { get; set; }
        [Display(Name = "Forecast No.")]
        public string ForecastNumber { get; set; }
        [Display(Name = "Item")]
        public string ItemCode { get; set; }
        [Display(Name = "Order Quantity")]
        public Nullable<int> OrderQuantity { get; set; }
        [Display(Name = "Sales Date")]
        public Nullable<System.DateTime> PurchaseDate { get; set; }
        [Display(Name = "User")]
        public Nullable<int> CreatedBy { get; set; }
    }
}