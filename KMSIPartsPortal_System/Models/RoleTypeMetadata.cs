﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Models
{
    [MetadataType(typeof(RoleTypeMetadata))]
    public partial class RoleType
    {

    }

    public class RoleTypeMetadata
    {
        [Display(Name = "User Level Type")]
        public string TypeName { get; set; }
    }
}