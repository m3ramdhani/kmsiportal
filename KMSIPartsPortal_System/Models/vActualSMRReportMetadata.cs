﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Models
{
    [MetadataType(typeof(vActualSMRReportMetadata))]
    public partial class vActualSMRReport
    {
        // can add extra/new code in here
    }

    public class vActualSMRReportMetadata
    {
        [Display(Name = "Date")]
        public Nullable<System.DateTime> InvoiceDate { get; set; }
        [Display(Name = "Unit SN")]
        public string UnitSerialNumber { get; set; }
        [Display(Name = "Unit Code")]
        public string UnitCode { get; set; }
        [Display(Name = "Unit Model")]
        public string UnitModel { get; set; }
        [Display(Name = "Contract No.")]
        public string ContractCode { get; set; }
        [Display(Name = "Customer ID")]
        public string CustomerCode { get; set; }
        [Display(Name = "Customer Name")]
        public string CustomerName { get; set; }
        [Display(Name = "Total SMR Hours")]
        public Nullable<decimal> SMRContract { get; set; }
        [Display(Name = "SMR/Hours (USD)")]
        public Nullable<decimal> USDperHours { get; set; }
        [Display(Name = "SMR Hours Used")]
        public Nullable<decimal> ActualSMR { get; set; }
        [Display(Name = "Remaining SMR")]
        public Nullable<decimal> RemainingSMR { get; set; }
        [Display(Name = "Amount Total In USD")]
        public Nullable<decimal> AmountUSD { get; set; }
        [Display(Name = "Amount Total In IDR")]
        public Nullable<decimal> AmountIDR { get; set; }
    }
}