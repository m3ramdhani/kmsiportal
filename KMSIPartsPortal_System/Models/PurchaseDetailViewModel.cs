﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Models
{
    public class PurchaseDetailViewModel
    {
        public PurchaseOrder PurchaseOrder { get; set; }
        public PurchaseDetail PurchaseDetail { get; set; }
        public Part Part { get; set; }
    }
}