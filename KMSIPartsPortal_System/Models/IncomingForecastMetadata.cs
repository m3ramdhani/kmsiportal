﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Models
{
    [MetadataType(typeof(IncomingForecastMetadata))]
    public partial class IncomingForecast
    {
        // can add extra/new code in here
        [Display(Name = "Amount")]
        public Nullable<decimal> Amount { get; set; }
    }

    public class IncomingForecastMetadata
    {
        [Display(Name = "Supplier Code")]
        public string SupplierCode { get; set; }
        [Display(Name = "Type PO")]
        public string TypePO { get; set; }
        [Display(Name = "Part Number")]
        public string PartNumber { get; set; }
        [Display(Name = "PO Quantity")]
        public Nullable<int> PurchaseQty { get; set; }
        [Display(Name = "Stock Point")]
        public string StockPoint { get; set; }
        [Display(Name = "PO. Number")]
        public string PONumber { get; set; }
        [Display(Name = "PO. Date")]
        public Nullable<System.DateTime> PODate { get; set; }
        [Display(Name = "Item No.")]
        public string ItemNumber { get; set; }
        [Display(Name = "Unit Price")]
        public Nullable<decimal> UnitPrice { get; set; }
    }
}