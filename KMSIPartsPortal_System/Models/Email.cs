﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Models
{
    public class Email
    {
        public string To { get; set; }
        public DateTime Date { get; set; }
        public List<WorkOrder> WorkOrders { get; set; }
    }
}