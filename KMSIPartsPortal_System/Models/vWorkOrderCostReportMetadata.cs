﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Models
{
    [MetadataType(typeof(vWorkOrderCostReportMetadata))]
    public partial class vWorkOrderCostReport
    {
        // can add extra/new code in here
    }

    public class vWorkOrderCostReportMetadata
    {
        [Display(Name = "WO Date")]
        public Nullable<System.DateTime> WorkOrderDate { get; set; }
        [Display(Name = "WO ID")]
        public string WorkOrderCode { get; set; }
        [Display(Name = "Unit SN")]
        public string UnitSerialNumber { get; set; }
        [Display(Name = "Unit Code")]
        public string UnitCode { get; set; }
        [Display(Name = "Unit Model")]
        public string UnitModel { get; set; }
        [Display(Name = "Contract No.")]
        public string ContractCode { get; set; }
        [Display(Name = "Customer ID")]
        public string CustomerCode { get; set; }
        [Display(Name = "Customer Name")]
        public string CustomerName { get; set; }
        [Display(Name = "Request Delivery Date")]
        public Nullable<System.DateTime> RequestDeliveryDate { get; set; }
        [Display(Name = "Total Parts Amount in USD")]
        public Nullable<decimal> TotalParts { get; set; }
    }
}