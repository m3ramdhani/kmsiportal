﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Models
{
    [MetadataType(typeof(PurchasePlanMetadata))]
    public partial class PurchasePlan
    {
        public int month { get; set; }
        public string monthName { get; set; }
        public int? PlanQty { get; set; }
    }

    public class PurchasePlanMetadata
    {
        [Display(Name = "Part Number")]
        public string PartNumber { get; set; }
    }
}