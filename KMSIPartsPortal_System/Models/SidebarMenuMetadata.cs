﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Models
{
    [MetadataType(typeof(SidebarMenuMetadata))]
    public partial class SidebarMenu
    {
        [NotMapped]
        public bool BoolView
        {
            get { return IsView == 1; }
            set { IsView = value ? 1 : 0; }
        }
        [NotMapped]
        public bool BoolInput
        {
            get { return IsInput == 1; }
            set { IsInput = value ? 1 : 0; }
        }
        [NotMapped]
        public bool BoolUpdate
        {
            get { return IsUpdate == 1; }
            set { IsUpdate = value ? 1 : 0; }
        }
        [NotMapped]
        public bool BoolDelete
        {
            get { return IsDelete == 1; }
            set { IsDelete = value ? 1 : 0; }
        }
        [NotMapped]
        public bool BoolUpload
        {
            get { return IsUpload == 1; }
            set { IsUpload = value ? 1 : 0; }
        }
        [NotMapped]
        public bool BoolDownload
        {
            get { return IsDownload == 1; }
            set { IsDownload = value ? 1 : 0; }
        }
    }

    public class SidebarMenuMetadata
    {
        [Display(Name = "Can Input")]
        public Nullable<int> IsInput { get; set; }
        [Display(Name = "Can Update")]
        public Nullable<int> IsUpdate { get; set; }
        [Display(Name = "Can Delete")]
        public Nullable<int> IsDelete { get; set; }
        [Display(Name = "Can View")]
        public Nullable<int> IsView { get; set; }
        [Display(Name = "Can Upload")]
        public Nullable<int> IsUpload { get; set; }
        [Display(Name = "Can Download")]
        public Nullable<int> IsDownload { get; set; }
    }
}