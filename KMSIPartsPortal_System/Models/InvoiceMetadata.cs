﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Models
{
    [MetadataType(typeof(InvoiceMetadata))]
    public partial class Invoice
    {
        // can add extra/new code in here
    }

    public class InvoiceMetadata
    {
        // Apply RequiredAttribute
        [DisplayName("Unit Serial Number")]
        public string UnitSerialNumber { get; set; }
        [DisplayName("Unit Code")]
        public string UnitCode { get; set; }
        [DisplayName("Unit Model")]
        public string UnitModel { get; set; }
        [DisplayName("Agreement No.")]
        public string ContractCode { get; set; }
        [DisplayName("Actual SMR")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:N0}")]
        public Nullable<decimal> ActualSMR { get; set; }
        [DisplayName("Customer ID")]
        public string CustomerCode { get; set; }
        [DisplayName("Customer Name")]
        public string CustomerName { get; set; }
        [DisplayName("TAX Invoice No.")]
        public string TaxInvoiceNumber { get; set; }
        [DisplayName("Kurs")]
        [DisplayFormat(ApplyFormatInEditMode =true, DataFormatString = "IDR {0:N2}")]
        public Nullable<decimal> Kurs { get; set; }
        [DisplayName("Due Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public Nullable<System.DateTime> InvoiceDueDate { get; set; }
        [DisplayName("Amount Total")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "IDR {0:N2}")]
        public Nullable<decimal> AmountTotal { get; set; }
        [DisplayFormat(DataFormatString = "{0:dddd, dd MMMM yyyy}")]
        public Nullable<System.DateTime> InvoiceDate { get; set; }

        // don't add any new code here, just match what is in your model classes and add data annotations to them.
    }
}