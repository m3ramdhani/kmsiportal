﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Models
{
    [MetadataType(typeof(PartMetadata))]
    public partial class Part
    {
        // can add extra/new code in here
    }

    public class PartMetadata
    {
        // Apply RequiredAttribute
        [DisplayName("Part Number")]
        public string PartsNumber { get; set; }

        // don't add any new code here, just match what is in your model classes and add data annotations to them.
    }
}