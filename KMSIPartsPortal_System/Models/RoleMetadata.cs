﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Models
{
    [MetadataType(typeof(RoleMetadata))]
    public partial class Role
    {
        // can add extra/new code in here
        public virtual ICollection<RegisterViewModel> RegisterViewModels { get; set; }
    }

    public class RoleMetadata
    {
        // Apply RequiredAttribute
        [Display(Name = "Role")]
        public string RoleName { get; set; }
        // don't add any new code here, just match what is in your model classes and add data annotations to them.
    }
}