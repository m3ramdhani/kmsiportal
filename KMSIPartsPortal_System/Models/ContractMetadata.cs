﻿using KMSIPartsPortal_System.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Models
{
    [MetadataType(typeof(ContractMetadata))]
    public partial class Contract
    {
        // can add extra/new code in here
        public virtual ICollection<Customer> Customers { get; set; }
        [Display(Name = "Choose File")]
        [Required(ErrorMessage = "Please select file")]
        [FileExt(Allow = ".xls, .xlsx", ErrorMessage = "Only excel file")]
        public HttpPostedFileBase file { get; set; }
    }

    public class ContractMetadata
    {
        // Apply RequiredAttribute
        
        // don't add any new code here, just match what is in your model classes and add data annotations to them.
    }
}