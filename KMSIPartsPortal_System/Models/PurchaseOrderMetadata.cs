﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Models
{
    [MetadataType(typeof(PurchaseOrderMetadata))]
    public partial class PurchaseOrder
    {
        // can add extra/new code in here
        public virtual Supplier Supplier { get; set; }
    }

    public class PurchaseOrderMetadata
    {
        // Apply RequiredAttribute
        [DisplayName("PO Number")]
        public string PurchaseNumber { get; set; }
        [DisplayName("PO Date")]
        public System.DateTime PurchaseDate { get; set; }
        [DisplayName("Aging PO")]
        public string AgingStatus { get; set; }
        [DisplayName("PO Status")]
        public string PurchaseStatus { get; set; }

        // don't add any new code here, just match what is in your model classes and add data annotations to them.
    }
}