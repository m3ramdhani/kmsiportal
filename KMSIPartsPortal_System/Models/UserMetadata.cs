﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Models
{
    [MetadataType(typeof(UserMetadata))]
    public partial class User
    {
        // can add extra/new code in here
        public virtual ICollection<RegisterViewModel> RegisterViewModels { get; set; }
        public virtual ICollection<Role> Roles { get; set; }
        public int? UserRoleId { get; set; }
        public string RoleName { get; set; }
    }

    public class UserMetadata
    {
        // Apply RequiredAttribute
        public int UserId { get; set; }
        [Required(ErrorMessage = "Username required.", AllowEmptyStrings = false)]
        public string Username { get; set; }
        [DataType(DataType.Password)]
        public string Password { get; set; }

        // don't add any new code here, just match what is in your model classes and add data annotations to them.
    }
}