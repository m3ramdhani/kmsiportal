﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Models
{
    [MetadataType(typeof(SubMenuMetadata))]
    public partial class SubMenu
    {
        // can add extra/new code in here
    }

    public class SubMenuMetadata
    {
        [Display(Name = "Sub Menu")]
        public string SubMenuName { get; set; }
    }
}