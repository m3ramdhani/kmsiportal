﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Models
{
    [MetadataType(typeof(StockPOMetadata))]
    public partial class StockPO
    {
        // can add extra/new code in here
        [Display(Name = "Amount")]
        public Nullable<decimal> Amount { get; set; }
    }

    public class StockPOMetadata
    {
        [Display(Name = "PO. Number")]
        public string PONumber { get; set; }
        [Display(Name = "PO. Date")]
        public Nullable<System.DateTime> PODate { get; set; }
        [Display(Name = "Supplier No.")]
        public string SupplierCode { get; set; }
        [Display(Name = "Item No.")]
        public string ItemNumber { get; set; }
        [Display(Name = "Part Number")]
        public string PartNumber { get; set; }
        [Display(Name = "Part Name")]
        public string PartName { get; set; }
        [Display(Name = "Unit Price")]
        public Nullable<decimal> UnitPrice { get; set; }
    }
}