﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Models
{
    [MetadataType(typeof(vFMCInvoiceReportMetadata))]
    public partial class vFMCInvoiceReport
    {
        // can add extra/new code in here
    }

    public class vFMCInvoiceReportMetadata
    {
        [Display(Name = "Inv Date")]
        public Nullable<System.DateTime> InvoiceDate { get; set; }
        [Display(Name = "Unit SN")]
        public string UnitSerialNumber { get; set; }
        [Display(Name = "Unit Code")]
        public string UnitCode { get; set; }
        [Display(Name = "Unit Model")]
        public string UnitModel { get; set; }
        [Display(Name = "Contract No.")]
        public string ContractCode { get; set; }
        [Display(Name = "SMR Hours")]
        public Nullable<decimal> ActualSMR { get; set; }
        [Display(Name = "USD Hours")]
        public Nullable<decimal> USDperHours { get; set; }
        [Display(Name = "Kurs")]
        public Nullable<decimal> Kurs { get; set; }
        [Display(Name = "Amount Total In USD")]
        public Nullable<decimal> AmountTotalUSD { get; set; }
        [Display(Name = "Amount Total In IDR")]
        public Nullable<decimal> AmountTotalIDR { get; set; }
    }
}