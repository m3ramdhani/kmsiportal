﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Models
{
    public class PurchaseInvoiceViewModel
    {
        public OutstandingPurchase PurchaseOrder { get; set; }
        public PurchaseInvoice PurchaseInvoice { get; set; }
        public PurchaseInvoiceDetail PurchaseInvoiceDetail { get; set; }

        public int PurchaseDetailId { get; set; }
        public string PartNumber { get; set; }
        public string ItemNumber { get; set; }
        public decimal? RequestQuantity { get; set; }
        public decimal? ShipQty { get; set; }
        public double? Price { get; set; }
        public string Description { get; set; }
    }
}