﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Models
{
    public class MenuViewModel
    {
        public MainMenu MainMenu { get; set; }
        public MenuList MenuList { get; set; }
        public SubMenu SubMenu { get; set; }
    }
}