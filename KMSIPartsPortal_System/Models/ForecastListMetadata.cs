﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Models
{
    [MetadataType(typeof(ForecastListMetadata))]
    public partial class ForecastList
    {
        // can add extra/new code in here
        public int[] IDs { get; set; }
        public string[] PartNumbers { get; set; }
        public int[] Quantities { get; set; }
        public string Description { get; set; }
        public string Requester { get; set; }
    }

    public class ForecastListMetadata
    {
        [Display(Name = "Forecast No.")]
        public string ForecastNumber { get; set; }
        [Display(Name = "Part Number")]
        public string PartNumber { get; set; }
        [Display(Name = "PO Qty")]
        public Nullable<int> Quantity { get; set; }
        [Display(Name = "Forecast Date")]
        public Nullable<System.DateTime> ForecastDate { get; set; }
        [Display(Name = "User")]
        public Nullable<int> CreatedBy { get; set; }
    }
}