﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Models
{
    public class PurchaseViewModel
    {
        public PurchaseOrder PurchaseOrders { get; set; }
        public PurchaseDetail PurchaseDetail { get; set; }
        public PurchaseInvoice PurchaseInvoice { get; set; }
        public PurchaseInvoiceDetail PurchaseInvoiceDetail { get; set; }
        public Supplier Suppliers { get; set; }
    }
}