﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Models
{
    public class EmailViewModel
    {
        public string PartNumber { get; set; }
        public string Status { get; set; }
    }
}