﻿using KMSIPartsPortal_System.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Models
{
    public class ContractViewModel
    {
        public Contract Contract { get; set; }

        [Display(Name = "Choose File")]
        [Required(ErrorMessage = "Please select file")]
        [FileExt(Allow = ".xls, .xlsx", ErrorMessage = "Only excel file")]
        public HttpPostedFileBase file { get; set; }
    }
}