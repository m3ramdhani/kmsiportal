﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Models
{
    [MetadataType(typeof(MainMenuMetadata))]
    public partial class MainMenu
    {
        // can add extra/new code in here
    }

    public class MainMenuMetadata
    {
        [Display(Name = "Main Menu")]
        public string MenuName { get; set; }
    }
}