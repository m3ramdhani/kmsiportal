﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Models
{
    [MetadataType(typeof(SupplierMetadata))]
    public partial class Supplier
    {
        // can add extra/new code in here
        public HttpPostedFileBase fileUpload { get; set; }
        public HttpPostedFileBase fileUpload1 { get; set; }
        public HttpPostedFileBase fileUpload2 { get; set; }
        public HttpPostedFileBase fileUpload3 { get; set; }
        public HttpPostedFileBase fileUpload4 { get; set; }
        public HttpPostedFileBase fileUpload5 { get; set; }
        public HttpPostedFileBase fileUpload6 { get; set; }
        public HttpPostedFileBase fileUpload7 { get; set; }
        public HttpPostedFileBase fileUpload8 { get; set; }
        public HttpPostedFileBase fileUpload9 { get; set; }
        public HttpPostedFileBase fileUpload10 { get; set; }
        public HttpPostedFileBase fileUpload11 { get; set; }
        public HttpPostedFileBase fileUpload12 { get; set; }
        public HttpPostedFileBase fileUpload13 { get; set; }
        public HttpPostedFileBase fileUpload14 { get; set; }
        public HttpPostedFileBase fileUpload15 { get; set; }
        public HttpPostedFileBase logoUpload { get; set; }
    }

    public class SupplierMetadata
    {
        // Apply RequiredAttribute
        [DisplayName("Supplier Code")]
        public string SupplierCode { get; set; }
        [DisplayName("Supplier Name")]
        public string SupplierName { get; set; }
        [DisplayName("Company Business Address")]
        public string Addr { get; set; }
        [DisplayName("Company Mailing Address")]
        public string MailAddr { get; set; }
        [DisplayName("Telephone")]
        public string Phone { get; set; }
        [DisplayName("Facsimile")]
        public string Fax { get; set; }
        [DisplayName("Email")]
        public string eMail { get; set; }
        [DisplayName("Contact Name")]
        public string Contact { get; set; }
        [DisplayName("Contact Phone")]
        public string ContactPhone { get; set; }
        [DisplayName("Contact Email Address")]
        public string ContactEmail { get; set; }
        [DisplayName("Bank Name")]
        public string BankName { get; set; }
        [DisplayName("Bank Address")]
        public string BankAddr { get; set; }
        [DisplayName("Account No")]
        public string AccountNumber { get; set; }
        [DisplayName("Type Of Transaction")]
        public string TransactionType { get; set; }
        [DisplayName("Term Of Payment")]
        public string PayTerm { get; set; }
        [DisplayName("Memorandum of Association")]
        public string MemoOfAssoc { get; set; }
        [DisplayName("Memorandum of Association - Latest")]
        public string MemoOfAssocLatest { get; set; }
        [DisplayName("Letter of Business Permit")]
        public string BusinessPermit { get; set; }
        [DisplayName("Letter of Domicile")]
        public string LetterOfDomicile { get; set; }
        [DisplayName("Company Registration Cetificate")]
        public string CompRegCert { get; set; }
        [DisplayName("Agreement with KMSI")]
        public string AgreeWithKmsi { get; set; }
        [DisplayName("Tax ID Number")]
        public string TaxID { get; set; }
        [DisplayName("Tax Object Number")]
        public string TaxObjNumber { get; set; }
        [DisplayName("Do you have 'Sertifikasi Klasifikasi Usaha'")]
        public string SertKlasifikasiUsaha { get; set; }
        [DisplayName("Do you have 'Surat Keterangan Bebas Pemotongan'")]
        public string SKBebasPotongan { get; set; }
        [DisplayName("Company Profile")]
        public string fileOfCompanyProfile { get; set; }
        [DisplayName("Company Business Organization Chart")]
        public string fileOfCompanyOrganization { get; set; }
        [DisplayName("Copy of Akta Pendirian Perusahaan")]
        public string fileOfAktaPendirian { get; set; }
        [DisplayName("Bank Confirmation Letter (Original)")]
        public string fileOfBankConfirmation { get; set; }
        [DisplayName("Copy of Letter of Business Permit")]
        public string fileOfBusinessPermit { get; set; }
        [DisplayName("Copy of Letter of Domicile")]
        public string fileOfDomicile { get; set; }
        [DisplayName("Copy of Tax ID Number")]
        public string fileOfTaxID { get; set; }
        [DisplayName("Copy of Taxable Enterprise Confirmation Letter")]
        public string fileOfTaxableConfirmation { get; set; }
        [DisplayName("Copy of Surat Keterangan Terdaftar")]
        public string fileOfSKTerdaftar { get; set; }
        [DisplayName("Statement Letter of Taxable Enterprises")]
        public string fileOfStatTaxableEnterprise { get; set; }
        [DisplayName("Statement Letter of Non NPWP")]
        public string fileOfNonNPWP { get; set; }
        [DisplayName("Statement Letter of Non Taxable Enterprises")]
        public string fileOfNonTaxable { get; set; }
        [DisplayName("Copy of Sertifikat Klasifikasi Usaha (SKU) untuk Jasa Konstruksi")]
        public string fileOfSKUKontsruksi { get; set; }
        [DisplayName("Copy of Surat Keterangan Bebas Pemotongan Pajak Penghasilan")]
        public string fileOfBebasPotonganPajak { get; set; }
        [DisplayName("Scan Supplier Data Form (Signed & Stamped)")]
        public string scanOfDataForm { get; set; }

        // don't add any new code here, just match what is in your model classes and data annotations to them.
    }
}