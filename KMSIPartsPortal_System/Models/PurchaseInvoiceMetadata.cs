﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Models
{
    [MetadataType(typeof(PurchaseInvoiceMetadata))]
    public partial class PurchaseInvoice
    {
        // can add extra/new code in here
        public HttpPostedFileBase INVFile { get; set; }
        public HttpPostedFileBase FPFile { get; set; }
    }

    public class PurchaseInvoiceMetadata
    {
        [DisplayName("Supplier Code")]
        public string SupplierCode { get; set; }
        [DisplayName("Supplier Name")]
        public string SupplierName { get; set; }
        [DisplayName("Invoice Date")]
        public Nullable<System.DateTime> InvoiceDate { get; set; }
        [DisplayName("Invoice Number")]
        public string InvoiceNumber { get; set; }
        [DisplayName("Payment Due Date")]
        public Nullable<System.DateTime> InvoiceDueDate { get; set; }
        [DisplayName("Description")]
        public string Description { get; set; }
        [DisplayName("Currency")]
        public string Currency { get; set; }
        [DisplayName("Principal Amount")]
        [DisplayFormat(DataFormatString = "IDR {0:N2}", ApplyFormatInEditMode = true)]
        public string PrincipalAmount { get; set; }
        [DisplayName("Principal Tax")]
        [DisplayFormat(DataFormatString = "IDR {0:N2}", ApplyFormatInEditMode = true)]
        public string PrincipalTax { get; set; }
        [DisplayName("Total Amount")]
        [DisplayFormat(DataFormatString = "IDR {0:N2}", ApplyFormatInEditMode = true)]
        public string TotalAmount { get; set; }
        public string Request { get; set; }
        public string ReqComment { get; set; }
        [DisplayName("PDF Invoice")]
        public string fileINV { get; set; }
        [DisplayName("PDF Tax Number")]
        public string fileFP { get; set; }
        [DisplayName("PO Number")]
        public string PurchaseNumber { get; set; }
    }
}