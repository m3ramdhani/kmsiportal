﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Models
{
    [MetadataType(typeof(OutgoingReceiptMetadata))]
    public partial class OutgoingReceipt
    {
        // can add extra/new code in here
    }

    public class OutgoingReceiptMetadata
    {
        [Display(Name = "Delivery Order No.")]
        public string DONO { get; set; }
        [Display(Name = "Part No.")]
        public string PartNumber { get; set; }
        [Display(Name = "Qty")]
        public Nullable<int> InboundQuantity { get; set; }
        [Display(Name = "Purchase No.")]
        public string PurchaseNumber { get; set; }
    }
}