﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Models
{
    public class PdfStockInfo
    {
        public string[] Columns { get; set; }
        public string[] ColumnsQty { get; set; }
        public string[] Filter { get; set; }
        public IEnumerable<vStockInfo> Data { get; set; }
        public string imgPath { get; set; }
    }
}