﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Models
{
    public class IncomingListDocuments
    {
        public string DONO { get; set; }
        public List<DeliveryOrder> IncomingList { get; set; }
    }
}