﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Models
{
    [MetadataType(typeof(IncomingListMetadata))]
    public partial class IncomingList
    {
        // can add extra/new code in here
        public int[] IDs { get; set; }
        public int[] Actuals { get; set; }
    }

    public class IncomingListMetadata
    {
        [Display(Name = "Incoming List No.")]
        public string DONO { get; set; }
        [Display(Name = "Sales No.")]
        public string PurchaseNumber { get; set; }
        [Display(Name = "Part No.")]
        public string PartNumber { get; set; }
        [Display(Name = "Incoming Date")]
        public Nullable<System.DateTime> IncomingDate { get; set; }
        [Display(Name = "Qty")]
        public Nullable<int> Quantity { get; set; }
        [Display(Name = "C/NO.")]
        public string CNO { get; set; }
        [Display(Name = "Qty Receipt")]
        public int ActualQty { get; set; }
    }
}