//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KMSIPartsPortal_System.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class PurchaseList
    {
        public int ID { get; set; }
        public string PurchaseNumber { get; set; }
        public string ForecastNumber { get; set; }
        public string ItemCode { get; set; }
        public Nullable<int> OrderQuantity { get; set; }
        public Nullable<System.DateTime> PurchaseDate { get; set; }
        public string ItemName { get; set; }
        public string Unit { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
    }
}
