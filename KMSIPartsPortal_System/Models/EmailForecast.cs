﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Models
{
    public class EmailForecast
    {
        public string ForecastNo { get; set; }
        public string Date { get; set; }
        public string Remarks { get; set; }
        public List<ForecastList> ForecastLists { get; set; }
        public string Requester { get; set; }
        public string BaseUrl { get; set; }
    }
}