﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Models
{
    public class EmailTicket
    {
        public string TicketNumber { get; set; }
        public string Requester { get; set; }
        public string SolvedBy { get; set; }
        public string AssignBy { get; set; }
        public string BaseUrl { get; set; }
    }
}