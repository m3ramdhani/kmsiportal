﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Models
{
    [MetadataType(typeof(UnitMetadata))]
    public partial class Unit
    {
        // can add extra/new code in here
    }

    public class UnitMetadata
    {
        // Apply RequiredAttribute
        public int UnitId { get; set; }
        public string UnitCode { get; set; }
        public string UnitSerialNumber { get; set; }
        public string UnitModel { get; set; }
        public string UnitType { get; set; }

        // don't add any new code here, just match what is in your model classes and add data annotations to them.
    }
}