﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Models
{
    [MetadataType(typeof(WorkOrderMetadata))]
    public partial class WorkOrder
    {
        // can add extra/new code in here
    }

    public class WorkOrderMetadata
    {
        [DisplayName("WO Code")]
        public string WorkOrderCode { get; set; }
        [DisplayName("WO Date")]
        public System.DateTime WorkOrderDate { get; set; }
        [DisplayName("Unit SN")]
        public string UnitSerialNumber { get; set; }
        [DisplayName("Unit Code")]
        public string UnitCode { get; set; }
        [DisplayName("Unit Model")]
        public string UnitModel { get; set; }
        [DisplayName("Agreement No.")]
        public string AgreementNumber { get; set; }
        [DisplayName("Customer ID")]
        public string CustomerCode { get; set; }
        [DisplayName("Customer Name")]
        public string CustomerName { get; set; }
        [Required]
        [DisplayName("Req.Delivery Date")]
        public System.DateTime RequestDeliveryDate { get; set; }
        [DisplayName("Type Of Problem")]
        public string TypeOfProblem { get; set; }
        [DisplayName("Parts Number")]
        public string PartsNumber { get; set; }
    }
}