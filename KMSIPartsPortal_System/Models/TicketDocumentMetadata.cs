﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KMSIPartsPortal_System.Models
{
    [MetadataType(typeof(TicketDocumentMetadata))]
    public partial class TicketDocument
    {
        // can add extra/new code in here
        public HttpPostedFileBase[] TicketImgUpload { get; set; }
        public HttpPostedFileBase[] TicketDocUpload { get; set; }
        public HttpPostedFileBase[] FeedbackImgUpload { get; set; }
        public HttpPostedFileBase[] FeedbackDocUpload { get; set; }

        [DisplayName("Images Attachment")]
        public string ImageAttachment { get; set; }
        [DisplayName("Docs Attachment")]
        public string DocAttachment { get; set; }
    }

    public class TicketDocumentMetadata
    {
        // Apply RequiredAttribute
        [DisplayName("Ticket No.")]
        public string TicketNumber { get; set; }
        [DisplayFormat(DataFormatString = "{0:d-MMMM-yyyy}")]
        public System.DateTime TicketDate { get; set; }
        [DisplayName("Information By")]
        public string InformationBy { get; set; }
        [DisplayName("Problem Category")]
        public string ProblemCategory { get; set; }
        [DisplayName("Problem Type")]
        public string ProblemType { get; set; }
        [DisplayName("Pickup Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:ddd, d MMMM yyyy; HH:mm:ss}")]
        public Nullable<System.DateTime> PickupDate { get; set; }
        [DisplayName("Pickup By")]
        public int PickupBy { get; set; }
        [DisplayName("Created Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public Nullable<System.DateTime> SubmitDate { get; set; }
        [DisplayName("Submit By")]
        public int SubmitBy { get; set; }
        [DisplayName("Assign To")]
        public Nullable<int> AssignTo1 { get; set; }
        [DisplayName("Assign To")]
        public Nullable<int> AssignTo2 { get; set; }
        [DisplayName("Solved Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public Nullable<System.DateTime> SolvedDate { get; set; }
        [DisplayName("Closed Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public Nullable<System.DateTime> ClosedDate { get; set; }
        [DisplayName("PIC Distributor")]
        public int DistributorPIC { get; set; }

        // don't add any new code here, just match what is in your model classes and add data annotations to them.
    }
}