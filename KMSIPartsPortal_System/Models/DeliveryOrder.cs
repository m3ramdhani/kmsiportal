//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KMSIPartsPortal_System.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class DeliveryOrder
    {
        public int ID { get; set; }
        public string DONO { get; set; }
        public string PurchaseNumber { get; set; }
        public string PartNumber { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<System.DateTime> OrderDate { get; set; }
        public Nullable<System.DateTime> DODate { get; set; }
        public string SalesName { get; set; }
        public string CustomerOrderBy { get; set; }
        public string AttnOrderBy { get; set; }
        public string CompanyNameOrderBy { get; set; }
        public string AddressOrderBy { get; set; }
        public string PhoneOrderBy { get; set; }
        public string EmailOrderBy { get; set; }
        public string CustomerDeliverTo { get; set; }
        public string AttnDeliverTo { get; set; }
        public string CompanyNameDeliverTo { get; set; }
        public string AddressDeliverTo { get; set; }
        public string PhoneDeliverTo { get; set; }
        public string EmailDeliverTo { get; set; }
        public Nullable<int> Number { get; set; }
        public string Description { get; set; }
        public string Unit { get; set; }
        public string Remarks { get; set; }
        public string CNO { get; set; }
        public string StockPoint { get; set; }
        public Nullable<int> IssuedBy { get; set; }
        public Nullable<int> ReceivedBy { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<int> ActualQty { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedAt { get; set; }
        public Nullable<int> IsValid { get; set; }
        public Nullable<int> StatusId { get; set; }
    
        public virtual DeliveryStatus DeliveryStatus { get; set; }
    }
}
