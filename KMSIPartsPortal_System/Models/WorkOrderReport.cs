//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KMSIPartsPortal_System.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class WorkOrderReport
    {
        public int WOReportID { get; set; }
        public string WOReportCode { get; set; }
        public string WorkOrderCode { get; set; }
        public Nullable<System.DateTime> WorkOrderDate { get; set; }
        public string ContractCode { get; set; }
        public string CustomerCode { get; set; }
        public string PartsNumber { get; set; }
        public string UnitSerialNumber { get; set; }
        public Nullable<System.DateTime> WOReportDate { get; set; }
        public string UnitCode { get; set; }
        public string UnitModel { get; set; }
        public string Description { get; set; }
        public string CustomerName { get; set; }
        public Nullable<System.DateTime> RequestDeliveryDate { get; set; }
        public string Site { get; set; }
        public string Status { get; set; }
        public Nullable<int> Quantity { get; set; }
        public string Comments { get; set; }
    }
}
