﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Models
{
    [MetadataType(typeof(DeliveryOrderMetadata))]
    public partial class DeliveryOrder
    {
        // can add extra/new code in here
        public int[] IDs { get; set; }
        public int[] Actuals { get; set; }
    }

    public class DeliveryOrderMetadata
    {
        [Display(Name = "Delivery Order")]
        public string DONO { get; set; }
        [Display(Name = "PO No.")]
        public string PurchaseNumber { get; set; }
        [Display(Name = "Part No.")]
        public string PartNumber { get; set; }
        [Display(Name = "Order Date")]
        public Nullable<System.DateTime> OrderDate { get; set; }
        [Display(Name = "Delivery Date")]
        public Nullable<System.DateTime> DODate { get; set; }
        [Display(Name = "Sales Name")]
        public string SalesName { get; set; }
        [Display(Name = "Ordered By")]
        public string CustomerOrderBy { get; set; }
        [Display(Name = "Attn")]
        public string AttnOrderBy { get; set; }
        [Display(Name = "Company Name")]
        public string CompanyNameOrderBy { get; set; }
        [Display(Name = "Address")]
        public string AddressOrderBy { get; set; }
        [Display(Name = "Telp.")]
        public string PhoneOrderBy { get; set; }
        [Display(Name = "Email Address")]
        public string EmailOrderBy { get; set; }
        [Display(Name = "Deliver To")]
        public string CustomerDeliverTo { get; set; }
        [Display(Name = "Attn")]
        public string AttnDeliverTo { get; set; }
        [Display(Name = "Department")]
        public string CompanyNameDeliverTo { get; set; }
        [Display(Name = "Address")]
        public string AddressDeliverTo { get; set; }
        [Display(Name = "Telp.")]
        public string PhoneDeliverTo { get; set; }
        [Display(Name = "Email Address")]
        public string EmailDeliverTo { get; set; }
        [Display(Name = "C/NO.")]
        public string CNO { get; set; }
        [Display(Name = "Stock Point")]
        public string StockPoint { get; set; }
        [Display(Name = "Issued by")]
        public Nullable<int> IssuedBy { get; set; }
        [Display(Name = "Received by")]
        public Nullable<int> ReceivedBy { get; set; }
        [Display(Name = "Qty Receipt")]
        public Nullable<int> ActualQty { get; set; }
    }
}