//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KMSIPartsPortal_System.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Kpintar_Stock_Information
    {
        public int ID { get; set; }
        public string PART_NUMBER { get; set; }
        public string PART_NAME { get; set; }
        public Nullable<int> FOO { get; set; }
        public Nullable<int> FOH { get; set; }
        public Nullable<int> EO_RSV { get; set; }
        public Nullable<int> EO_RSV_DEPO { get; set; }
        public Nullable<int> FREE_SHIPPED_QTY { get; set; }
        public Nullable<int> FREE_ARRIVED_QTY { get; set; }
        public string STOCK_POINT { get; set; }
        public System.DateTime PullDate { get; set; }
        public string ITC_PART_NUMBER { get; set; }
        public Nullable<int> ITC_CODE { get; set; }
    }
}
