﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Models
{
    public class EmailIncomingList
    {
        public string UploadDate { get; set; }
        public List<List<DeliveryOrder>> Documents { get; set; }
        public string BaseUrl { get; set; }
        public string DownloadUrl { get; set; }
    }
}