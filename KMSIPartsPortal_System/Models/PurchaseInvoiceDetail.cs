//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KMSIPartsPortal_System.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class PurchaseInvoiceDetail
    {
        public int PurchaseInvoiceDetailId { get; set; }
        public string SupplierCode { get; set; }
        public string SupplierName { get; set; }
        public string PurchaseNumber { get; set; }
        public Nullable<System.DateTime> InvoiceDate { get; set; }
        public string InvoiceNumber { get; set; }
        public string DeliveryOrder { get; set; }
        public Nullable<int> ItemNumber { get; set; }
        public string PartNumberKomatsu { get; set; }
        public Nullable<decimal> ShipQty { get; set; }
        public Nullable<decimal> UnitPrice { get; set; }
        public string CountryOfOrigin { get; set; }
    }
}
