﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Models
{
    public class EmailAccount
    {
        public string Name { get; set; }
        public string Company { get; set; }
        public string Position { get; set; }
        public string RequestDt { get; set; }
        public string BaseUrl { get; set; }

        public string ApprovedDt { get; set; }
        public string ApproverName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}