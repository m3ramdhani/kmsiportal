USE [kmsi_portal]
GO
/****** Object:  Table [dbo].[ActualSMRCostReports]    Script Date: 02/05/2019 10:00:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ActualSMRCostReports](
	[ActualSMRCostReportID] [int] IDENTITY(1,1) NOT NULL,
	[ActualSMRCostReportCode] [varchar](255) NULL,
	[ContractCode] [varchar](255) NULL,
	[CustomerCode] [varchar](255) NULL,
	[PartsNumber] [varchar](255) NULL,
	[UnitSerialNumber] [varchar](255) NULL,
	[ActualSMRCostReportDate] [datetime] NULL,
	[UnitCode] [varchar](255) NULL,
	[UnitModel] [varchar](255) NULL,
	[TotalSMRHours] [decimal](18, 0) NULL,
	[RemainingSMR] [decimal](18, 0) NULL,
	[SMRHoursUsed] [decimal](18, 0) NULL,
	[SMRHoursUSD] [decimal](18, 0) NULL,
	[Kurs] [decimal](18, 0) NULL,
	[AmountTotalUSD] [decimal](18, 0) NULL,
	[AmountTotalIDR] [decimal](18, 0) NULL,
	[Comment] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[ActualSMRCostReportID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Contract]    Script Date: 02/05/2019 10:00:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Contract](
	[ContractId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [varchar](max) NULL,
	[ContractCode] [varchar](max) NULL,
	[UnitSerialNumber] [varchar](max) NULL,
	[UnitModel] [varchar](max) NULL,
	[UnitCode] [varchar](max) NULL,
	[ContractStartDate] [date] NULL,
	[ContractEndDate] [date] NULL,
	[SMRContract] [decimal](18, 0) NULL,
	[USDperHours] [decimal](18, 0) NULL,
	[AmountContract] [decimal](18, 0) NULL,
 CONSTRAINT [PK__Contract__F5B3FBEA1CB3042F] PRIMARY KEY CLUSTERED 
(
	[ContractId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 02/05/2019 10:00:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Customer](
	[CustomerId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerCode] [varchar](255) NULL,
	[CustomerName] [varchar](255) NULL,
	[Address] [varchar](255) NULL,
	[Site] [varchar](255) NULL,
	[PhoneNumber] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Invoice]    Script Date: 02/05/2019 10:00:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Invoice](
	[InvoiceCode] [varchar](255) NULL,
	[ContractCode] [varchar](255) NULL,
	[CustomerCode] [varchar](255) NULL,
	[PartsNumber] [varchar](255) NULL,
	[UnitSerialNumber] [varchar](255) NULL,
	[InvoiceDate] [datetime] NULL,
	[UnitCode] [varchar](255) NULL,
	[UnitModel] [varchar](255) NULL,
	[CustomerName] [varchar](255) NULL,
	[RequestDeliveryDate] [datetime] NULL,
	[ActualSMR] [decimal](18, 0) NULL,
	[AmountTotal] [decimal](18, 0) NULL,
	[Kurs] [decimal](18, 0) NULL,
	[InvoiceDueDate] [datetime] NULL,
	[TaxInvoiceNumber] [varchar](max) NULL,
	[InvoiceID] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK__Invoice__0D9D7FF2B5D050FA] PRIMARY KEY CLUSTERED 
(
	[InvoiceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[InvoiceReports]    Script Date: 02/05/2019 10:00:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[InvoiceReports](
	[InvoiceReportCode] [varchar](255) NULL,
	[InvoiceCode] [varchar](255) NULL,
	[InvoiceDate] [datetime] NULL,
	[ContractCode] [varchar](255) NULL,
	[CustomerCode] [varchar](255) NULL,
	[UnitSerialNumber] [varchar](255) NULL,
	[InvoiceReportDate] [datetime] NULL,
	[UnitCode] [varchar](255) NULL,
	[UnitModel] [varchar](255) NULL,
	[Description] [varchar](255) NULL,
	[CustomerName] [varchar](255) NULL,
	[SMRHoursUsed] [decimal](18, 0) NULL,
	[SMRperHoursUSD] [decimal](18, 0) NULL,
	[Site] [varchar](255) NULL,
	[Kurs] [decimal](18, 0) NULL,
	[AmountTotalUSD] [decimal](18, 0) NULL,
	[AmountTotalIDR] [decimal](18, 0) NULL,
	[Comment] [varchar](255) NULL,
	[InvoiceReportID] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK__InvoiceR__99187C39FEC5ED67] PRIMARY KEY CLUSTERED 
(
	[InvoiceReportID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Parts]    Script Date: 02/05/2019 10:00:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Parts](
	[PartsId] [int] IDENTITY(1,1) NOT NULL,
	[PartsNumber] [varchar](255) NULL,
	[PartsName] [varchar](255) NULL,
	[Price] [decimal](18, 0) NULL,
	[Currency] [varchar](255) NULL,
	[UnitType] [varchar](255) NULL,
	[UnitModel] [varchar](255) NULL,
	[PartsCode] [varchar](255) NULL,
 CONSTRAINT [PK__Parts__1038D90220E512F4] PRIMARY KEY CLUSTERED 
(
	[PartsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PurchaseDetails]    Script Date: 02/05/2019 10:00:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PurchaseDetails](
	[PurchaseDetailId] [int] IDENTITY(1,1) NOT NULL,
	[PurchaseId] [int] NULL,
	[PartsId] [int] NULL,
	[ItemNumber] [int] NULL,
	[Description] [varchar](255) NULL,
	[RequestQuantity] [decimal](18, 0) NULL,
	[ProvidedQuantity] [decimal](18, 0) NULL,
	[Price] [decimal](18, 0) NULL,
	[Currency] [varchar](255) NULL,
	[EstDelivered] [datetime] NULL,
	[DetailStatus] [varchar](255) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[SupplierCode] [varchar](max) NULL,
	[SupplierName] [varchar](max) NULL,
	[PurchaseDate] [date] NULL,
	[PurchaseNumber] [varchar](max) NULL,
	[StockPoint] [varchar](max) NULL,
	[PartNumber] [varchar](max) NULL,
	[ProvidedQuantity_2] [decimal](18, 0) NULL,
	[EstDelivered_2] [date] NULL,
	[ProvidedQuantity_3] [decimal](18, 0) NULL,
	[EstDelivered_3] [date] NULL,
	[ProvidedQuantity_4] [decimal](18, 0) NULL,
	[EstDelivered_4] [date] NULL,
	[ProvidedQuantity_5] [decimal](18, 0) NULL,
	[EstDelivered_5] [date] NULL,
 CONSTRAINT [PK__Purchase__88C328B529C4B9A8] PRIMARY KEY CLUSTERED 
(
	[PurchaseDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PurchaseInvoice]    Script Date: 02/05/2019 10:00:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PurchaseInvoice](
	[InvoiceID] [int] IDENTITY(1,1) NOT NULL,
	[PurchaseID] [int] NULL,
	[SupplierID] [int] NULL,
	[SupplierCode] [varchar](max) NULL,
	[SupplierName] [varchar](max) NULL,
	[InvoiceDate] [datetime] NULL,
	[InvoiceNumber] [varchar](255) NULL,
	[InvoiceDueDate] [datetime] NULL,
	[Description] [varchar](max) NULL,
	[Currency] [varchar](max) NULL,
	[PrincipalAmount] [varchar](max) NULL,
	[PrincipalTax] [varchar](max) NULL,
	[TotalAmount] [varchar](max) NULL,
	[Request] [varchar](50) NULL,
	[ReqComment] [varchar](max) NULL,
	[fileINV] [varchar](max) NULL,
	[fileFP] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[InvoiceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PurchaseInvoiceDetails]    Script Date: 02/05/2019 10:00:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PurchaseInvoiceDetails](
	[PurchaseInvoiceDetailId] [int] IDENTITY(1,1) NOT NULL,
	[SupplierCode] [varchar](max) NULL,
	[SupplierName] [varchar](max) NULL,
	[PurchaseNumber] [varchar](max) NULL,
	[InvoiceDate] [date] NULL,
	[InvoiceNumber] [varchar](max) NULL,
	[DeliveryOrder] [varchar](max) NULL,
	[ItemNumber] [int] NULL,
	[PartNumberKomatsu] [varchar](max) NULL,
	[ShipQty] [decimal](18, 0) NULL,
	[UnitPrice] [decimal](18, 0) NULL,
	[CountryOfOrigin] [varchar](max) NULL,
 CONSTRAINT [PK_PurchaseInvoiceDetails] PRIMARY KEY CLUSTERED 
(
	[PurchaseInvoiceDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PurchaseOrders]    Script Date: 02/05/2019 10:00:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PurchaseOrders](
	[PurchaseId] [int] IDENTITY(1,1) NOT NULL,
	[SupplierId] [int] NULL,
	[PurchaseDate] [datetime] NULL,
	[PurchaseNumber] [varchar](255) NULL,
	[Amount] [decimal](18, 0) NULL,
	[AgingStatus] [varchar](255) NULL,
	[PurchaseStatus] [varchar](255) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[Request] [varchar](50) NULL,
	[ReqComment] [varchar](255) NULL,
	[SupplierCode] [varchar](max) NULL,
 CONSTRAINT [PK__Purchase__6B0A6BBEC32FFB79] PRIMARY KEY CLUSTERED 
(
	[PurchaseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 02/05/2019 10:00:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Roles](
	[RoleId] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [varchar](255) NULL,
 CONSTRAINT [PK_dbo.Roles] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Suppliers]    Script Date: 02/05/2019 10:00:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Suppliers](
	[SupplierId] [int] IDENTITY(1,1) NOT NULL,
	[SupplierCode] [varchar](255) NULL,
	[SupplierName] [varchar](255) NULL,
 CONSTRAINT [PK__Supplier__4BE666B496938E3A] PRIMARY KEY CLUSTERED 
(
	[SupplierId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Unit]    Script Date: 02/05/2019 10:00:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Unit](
	[UnitId] [int] IDENTITY(1,1) NOT NULL,
	[UnitCode] [varchar](255) NULL,
	[UnitSerialNumber] [varchar](255) NULL,
	[UnitModel] [varchar](255) NULL,
	[UnitType] [varchar](255) NULL,
 CONSTRAINT [PK__Unit__C2A0507B771B5542] PRIMARY KEY CLUSTERED 
(
	[UnitId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserRoles]    Script Date: 02/05/2019 10:00:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRoles](
	[UserId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
 CONSTRAINT [PK_dbo.UserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Users]    Script Date: 02/05/2019 10:00:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Users](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[Username] [varchar](255) NOT NULL,
	[FirstName] [varchar](255) NULL,
	[LastName] [varchar](255) NULL,
	[Email] [varchar](255) NULL,
	[Password] [varchar](255) NULL,
	[IsActive] [bit] NOT NULL,
	[SupplierId] [int] NOT NULL,
	[UserRole] [int] NULL,
 CONSTRAINT [PK_dbo.Users] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[WorkOrder]    Script Date: 02/05/2019 10:00:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[WorkOrder](
	[WorkOrderID] [int] IDENTITY(1,1) NOT NULL,
	[WorkOrderCode] [varchar](255) NULL,
	[ContractID] [int] NULL,
	[ContractCode] [varchar](255) NULL,
	[CustomerCode] [varchar](255) NULL,
	[PartsNumber] [varchar](255) NULL,
	[UnitSerialNumber] [varchar](255) NULL,
	[WorkOrderDate] [datetime] NULL,
	[UnitCode] [varchar](255) NULL,
	[UnitModel] [varchar](255) NULL,
	[TypeOfProblem] [varchar](255) NULL,
	[CustomerName] [varchar](255) NULL,
	[RequestDeliveryDate] [datetime] NULL,
	[Site] [varchar](255) NULL,
	[Status] [varchar](255) NULL,
	[Quantity] [varchar](255) NULL,
	[Description] [varchar](max) NULL,
	[Remarks] [varchar](max) NULL,
	[Comments] [varchar](255) NULL,
	[AgreementNumber] [varchar](max) NULL,
	[ApprovalStatus] [int] NULL,
 CONSTRAINT [PK__WorkOrde__AE755175E048876F] PRIMARY KEY CLUSTERED 
(
	[WorkOrderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[WorkOrderReports]    Script Date: 02/05/2019 10:00:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[WorkOrderReports](
	[WOReportID] [int] IDENTITY(1,1) NOT NULL,
	[WOReportCode] [varchar](255) NULL,
	[WorkOrderCode] [varchar](255) NULL,
	[WorkOrderDate] [datetime] NULL,
	[ContractCode] [varchar](255) NULL,
	[CustomerCode] [varchar](255) NULL,
	[PartsNumber] [varchar](255) NULL,
	[UnitSerialNumber] [varchar](255) NULL,
	[WOReportDate] [datetime] NULL,
	[UnitCode] [varchar](255) NULL,
	[UnitModel] [varchar](255) NULL,
	[Description] [varchar](255) NULL,
	[CustomerName] [varchar](255) NULL,
	[RequestDeliveryDate] [datetime] NULL,
	[Site] [varchar](255) NULL,
	[Status] [varchar](255) NULL,
	[Quantity] [int] NULL,
	[Comments] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[WOReportID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[vWorkOrderReport]    Script Date: 02/05/2019 10:00:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vWorkOrderReport] AS 
SELECT
	WorkOrderID,
	WorkOrderCode,
	CONVERT (VARCHAR, WorkOrderDate, 3) AS WorkOrderDate,
	ContractCode,
	UnitSerialNumber,
	UnitModel,
	UnitCode,
	CustomerCode,
	CustomerName,
	Site,
	CONVERT (
		VARCHAR,
		RequestDeliveryDate,
		3
	) AS RequestDeliveryDate,
	ApprovalStatus
FROM
	WorkOrder
GO
