﻿CREATE TABLE [dbo].[DeliveryOrders](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DONO] [varchar](max) NULL,
	[PurchaseNumber] [varchar](max) NULL,
	[PartNumber] [varchar](max) NULL,
	[Quantity] [int] NULL,
	[OrderDate] [date] NULL,
	[DODate] [date] NULL,
	[SalesName] [varchar](max) NULL,
	[CustomerOrderBy] [varchar](max) NULL,
	[AttnOrderBy] [varchar](max) NULL,
	[CompanyNameOrderBy] [varchar](max) NULL,
	[AddressOrderBy] [varchar](max) NULL,
	[PhoneOrderBy] [varchar](max) NULL,
	[EmailOrderBy] [varchar](max) NULL,
	[CustomerDeliverTo] [varchar](max) NULL,
	[AttnDeliverTo] [varchar](max) NULL,
	[CompanyNameDeliverTo] [varchar](max) NULL,
	[AddressDeliverTo] [varchar](max) NULL,
	[PhoneDeliverTo] [varchar](max) NULL,
	[EmailDeliverTo] [varchar](max) NULL,
	[Number] [int] NULL,
	[Description] [varchar](max) NULL,
	[Unit] [varchar](max) NULL,
	[Remarks] [varchar](max) NULL,
	[CNO] [varchar](max) NULL,
	[StockPoint] [varchar](max) NULL,
	[IssuedBy] [int] NULL,
	[ReceivedBy] [int] NULL,
	[CreatedAt] [datetime] NULL
 CONSTRAINT [PK_DeliveryOrders] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]