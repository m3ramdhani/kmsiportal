ALTER TABLE dbo.PurchaseDetails ADD SupplierCode VARCHAR (MAX),
 SupplierName VARCHAR (MAX),
 PurchaseDate Date,
 PurchaseNumber VARCHAR (MAX),
 PartNumber VARCHAR (MAX),
 StockPoint VARCHAR (MAX);