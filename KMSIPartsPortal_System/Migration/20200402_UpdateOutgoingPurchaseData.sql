﻿UPDATE OutgoingPurchases
SET DeliveryStatus = 1
FROM
	OutgoingPurchases op
INNER JOIN (
	SELECT
		PurchaseNumber,
		PartNumber,
		SUM (Quantity) AS Quantity
	FROM
		DeliveryOrders
	GROUP BY
		PurchaseNumber,
		PartNumber
) do ON op.PurchaseNumber = do.PurchaseNumber
AND op.PartNumber = do.PartNumber
AND op.Quantity >= do.Quantity;

UPDATE OutgoingPurchases
SET InvoiceStatus = 1
FROM
	OutgoingPurchases op
INNER JOIN (
	SELECT
		PurchaseNumber,
		PartNumber,
		SUM (Quantity) AS Quantity
	FROM
		OutgoingInvoices
	GROUP BY
		PurchaseNumber,
		PartNumber
) inv ON op.PurchaseNumber = inv.PurchaseNumber
AND op.PartNumber = inv.PartNumber
AND op.Quantity >= inv.Quantity;

UPDATE OutgoingPurchases
SET DeliveryStatus = 2
FROM
	OutgoingPurchases op
INNER JOIN (
	SELECT
		PurchaseNumber,
		PartNumber,
		SUM (Quantity) AS Quantity
	FROM
		DeliveryOrders
	GROUP BY
		PurchaseNumber,
		PartNumber
) do ON op.PurchaseNumber = do.PurchaseNumber
AND op.PartNumber = do.PartNumber
AND op.Quantity < do.Quantity;

UPDATE OutgoingPurchases
SET InvoiceStatus = 2
FROM
	OutgoingPurchases op
INNER JOIN (
	SELECT
		PurchaseNumber,
		PartNumber,
		SUM (Quantity) AS Quantity
	FROM
		OutgoingInvoices
	GROUP BY
		PurchaseNumber,
		PartNumber
) inv ON op.PurchaseNumber = inv.PurchaseNumber
AND op.PartNumber = inv.PartNumber
AND op.Quantity < inv.Quantity;
