﻿CREATE TABLE dbo.TicketAttachments(
	ID int IDENTITY(1,1) NOT NULL,
	[FileName] varchar(max),
	[Description] varchar(max),
	FileType varchar(max),
	FileMode varchar(max),
	TicketID int
 CONSTRAINT PK_TicketAttachment PRIMARY KEY CLUSTERED
(
	ID ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE dbo.TicketAttachments  WITH CHECK ADD  CONSTRAINT FK_Attachments_Ticket FOREIGN KEY(TicketID)
REFERENCES dbo.TicketDocuments (TicketID)

ALTER TABLE dbo.TicketAttachments CHECK CONSTRAINT FK_Attachments_Ticket