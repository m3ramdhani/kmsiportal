﻿CREATE TABLE [dbo].[MenuLists](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Menu Name] [varchar](max) NULL,
	[Controller] [varchar](max) NULL,
	[Action] [varchar](max) NULL,
	[MainMenuID] [int] NOT NULL,
 CONSTRAINT [PK_MenuLists] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

ALTER TABLE [dbo].[MenuLists]  WITH CHECK ADD  CONSTRAINT [FK_MenuLists_MainMenu] FOREIGN KEY([MainMenuID])
REFERENCES [dbo].[MainMenu] ([MenuID])

ALTER TABLE [dbo].[MenuLists] CHECK CONSTRAINT [FK_MenuLists_MainMenu]