﻿UPDATE IncomingStatuses
SET	[Name] = 'HUB-3 Waiting Goods', [Value] = 'HUB3W'
WHERE [Value] = 'HUB2W';

UPDATE IncomingStatuses
SET	[Name] = 'Received By HUB-3', [Value] = 'RHUB3'
WHERE [Value] = 'RHUB2';