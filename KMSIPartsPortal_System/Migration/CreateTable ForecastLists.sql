﻿CREATE TABLE [dbo].[ForecastLists](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ForecastNumber] [varchar](max) NULL,
	[PartNumber] [varchar](max) NULL,
	[Quantity] [int] NULL,
	[ForecastDate] [date] NULL,
	[Remarks] [varchar](max) NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL
 CONSTRAINT [PK_ForecastLists] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]