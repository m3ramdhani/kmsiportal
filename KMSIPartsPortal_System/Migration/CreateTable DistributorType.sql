﻿CREATE TABLE DistributorType(
	ID int IDENTITY(1,1) NOT NULL,
	[Name] varchar(50) NULL,
	[Value] varchar(50) NULL
CONSTRAINT [PK_DistributorType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

INSERT INTO DistributorType([Name], [Value]) VALUES('UT', 'UT');
INSERT INTO DistributorType([Name], [Value]) VALUES('UTR', 'UTR');
INSERT INTO DistributorType([Name], [Value]) VALUES('BP', 'BP');
INSERT INTO DistributorType([Name], [Value]) VALUES('KG', 'KG');

ALTER TABLE [dbo].[Users]
  ADD [DistributorType] varchar(50);