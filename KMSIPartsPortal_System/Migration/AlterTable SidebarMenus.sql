﻿ALTER TABLE dbo.SidebarMenus
	ADD IsInput tinyint,
		IsUpdate tinyint,
		IsDelete tinyint,
		IsView tinyint,
		IsUpload tinyint,
		IsDownload tinyint;