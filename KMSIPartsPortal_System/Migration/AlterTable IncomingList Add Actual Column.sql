﻿ALTER TABLE dbo.IncomingLists
  ADD ActualQty int,
	  UpdatedBy int,
	  UpdatedAt datetime,
	  IsValid int;