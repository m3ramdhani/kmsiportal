﻿ALTER TABLE SidebarMenus ALTER COLUMN IsInput int;
ALTER TABLE SidebarMenus ALTER COLUMN IsUpdate int;
ALTER TABLE SidebarMenus ALTER COLUMN IsDelete int;
ALTER TABLE SidebarMenus ALTER COLUMN IsView int;
ALTER TABLE SidebarMenus ALTER COLUMN IsUpload int;
ALTER TABLE SidebarMenus ALTER COLUMN IsDownload int;