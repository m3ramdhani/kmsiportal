﻿CREATE VIEW vStockInfo_Total
AS
SELECT
	ISNULL(ROW_NUMBER() OVER (ORDER BY PART_NUMBER), 0) AS ID,
	PART_NUMBER,
	PART_NAME,
	SUM(FOH) AS FOH_TOTAL,
	SUM(FOO) AS FOO_TOTAL,
	SUM(FREE_SHIPPED_QTY) AS FS_TOTAL,
	SUM(FREE_ARRIVED_QTY) AS FA_TOTAL,
	SUM(EO_RSV_DEPO) AS EO_RSV_DEPO_TOTAL 
FROM
	Kpintar_Stock_Information 
GROUP BY
	PART_NUMBER,
	PART_NAME