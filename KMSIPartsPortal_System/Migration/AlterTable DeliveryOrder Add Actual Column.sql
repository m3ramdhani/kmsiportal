﻿ALTER TABLE dbo.DeliveryOrders
	ADD ActualQty int,
	  UpdatedBy int,
	  UpdatedAt datetime,
	  IsValid int;