CREATE TABLE [dbo].[PurchaseInvoiceDetails](
	[PurchaseInvoiceDetailId] [int] IDENTITY(1,1) NOT NULL,
	[SupplierCode] [varchar](max) NULL,
	[SupplierName] [varchar](max) NULL,
	[PurchaseNumber] [varchar](max) NULL,
	[InvoiceDate] [date] NULL,
	[InvoiceNumber] [varchar](max) NULL,
	[DeliveryOrder] [varchar](max) NULL,
	[ItemNumber] [int] NULL,
	[PartNumberKomatsu] [varchar](max) NULL,
	[ShipQty] [decimal](18, 0) NULL,
	[UnitPrice] [decimal](18, 0) NULL,
	[CountryOfOrigin] [varchar](max) NULL,
 CONSTRAINT [PK_PurchaseInvoiceDetails] PRIMARY KEY CLUSTERED 
(
	[PurchaseInvoiceDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]