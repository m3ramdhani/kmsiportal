﻿CREATE TABLE [dbo].[OutgoingInvoices](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[InvoiceNumber] [varchar](max) NULL,
	[InvoiceDate] [date] NULL,
	[PurchaseNumber] [varchar](max) NULL,
	[PartNumber] [varchar](max) NULL,
	[PartName] [varchar](max) NULL,
	[Quantity] [int] NULL,
	[Unit] [varchar](max) NULL,
	[UnitPrice] [decimal](18,0) NULL,
	[Amount] [decimal](18,0) NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL
 CONSTRAINT [PK_OutgoingInvoices] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]