﻿CREATE VIEW vFMCInvoiceReport
AS
SELECT i.InvoiceID, i.InvoiceDate, i.InvoiceCode, i.UnitSerialNumber, i.UnitCode, i.UnitModel, i.ContractCode, i.CustomerCode, i.CustomerName, cu.Site, i.ActualSMR, c.USDperHours, i.Kurs, 
       i.ActualSMR * c.USDperHours AS AmountTotalUSD, i.ActualSMR * c.USDperHours * i.Kurs AS AmountTotalIDR
FROM   dbo.Invoice AS i LEFT OUTER JOIN
       dbo.Contract AS c ON c.ContractCode = i.ContractCode LEFT OUTER JOIN
       dbo.Customer AS cu ON cu.CustomerCode = i.CustomerCode