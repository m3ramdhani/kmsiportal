﻿CREATE FUNCTION dbo.fGetStockInfoRecordCount()
RETURNS INT
AS 
BEGIN 
    DECLARE @returnvalue INT;

    SELECT @returnvalue = COUNT(*) 
    FROM dbo.vStockInfo

    RETURN(@returnvalue);
END