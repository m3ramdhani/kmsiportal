﻿CREATE VIEW vActualSMRReport
AS
SELECT
	i.InvoiceID,
	i.InvoiceDate,
	i.UnitSerialNumber,
	i.UnitCode,
	i.UnitModel,
	i.ContractCode,
	i.CustomerCode,
	i.CustomerName,
	cu.Site,
	c.SMRContract,
	c.USDperHours,
	i.ActualSMR,
	(c.SMRContract - i.ActualSMR) AS RemainingSMR,
	(c.USDperHours * i.ActualSMR) AS AmountUSD,
	(c.USDperHours * i.ActualSMR * i.Kurs) AS AmountIDR
FROM
	Invoice i
LEFT OUTER JOIN Customer cu ON cu.CustomerCode = i.CustomerCode
LEFT OUTER JOIN Contract c ON c.ContractCode = i.ContractCode AND c.UnitSerialNumber = i.UnitSerialNumber