﻿CREATE TABLE [dbo].[OutgoingPurchases](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PurchaseNumber] [varchar](max) NULL,
	[PartName] [varchar](max) NULL,
	[Quantity] [int] NULL,
	[SupplierNumber] [varchar](max) NULL,
	[PurchaseDate] [date] NULL,
	[MarkingCode] [varchar](max) NULL,
	[PaymentTerms] [varchar](max) NULL,
	[DeliveryTerms] [varchar](max) NULL,
	[PackingCode] [varchar](max) NULL,
	[Currency] [varchar](max) NULL,
	[Remarks] [varchar](max) NULL,
	[SupplierName] [varchar](max) NULL,
	[Address] [varchar](max) NULL,
	[DeliverTo] [varchar](max) NULL,
	[ItemNumber] [int] NULL,
	[PartNumber] [varchar](max) NULL,
	[OrgPartNumber] [varchar](max) NULL,
	[UnitPrice] [decimal](18,0) NULL,
	[AmountIDR] [decimal](18,0) NULL,
	[AcknowledgeBy] [int] NULL,
	[ApprovedBy] [int] NULL,
	[CreatedAt] [datetime] NULL
 CONSTRAINT [PK_OutgoingPurchases] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]