﻿CREATE TABLE [dbo].[IncomingGoods](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DONumber] [varchar](max) NULL,
	[DODate] [date] NULL,
	[PONumber] [varchar](max) NULL,
	[PartNumber] [varchar](max) NULL,
	[PackingQty] [int] NULL,
	[ReceiveQty] [int] NULL,
	[StockPoint] [varchar](max) NULL,
	[Remark] [varchar](max) NULL,
	[Description] [varchar](max) NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedAt] [datetime] NULL,
 CONSTRAINT [PK_IncomingGood] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]