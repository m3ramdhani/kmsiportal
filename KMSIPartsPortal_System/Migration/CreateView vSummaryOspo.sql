﻿CREATE VIEW vSummaryOspo
AS
SELECT
	ISNULL( ROW_NUMBER () OVER ( ORDER BY PO_NUMBER ), 0 ) AS ID,
	OSPO.SUPPLIER_CODE AS SupplierCode,
	(SELECT SupplierName FROM Suppliers WHERE SupplierCode = OSPO.SUPPLIER_CODE) AS SupplierName,
	OSPO.PO_DATE AS PurchaseDate,
	OSPO.PO_NUMBER AS PurchaseNumber,
	OSPO.PO_AMOUNT AS Amount,
	OSPO.CURRENCY_CODE AS Currency,
	OSPO.AGING AS Aging,
	OSPO.REMARK AS Remarks,
	ISNULL((SELECT SUM(ShipQty) FROM PurchaseInvoiceDetails WHERE PurchaseNumber = OSPO.PO_NUMBER), 0) AS ShipQty,
	OSPO.PO_QTY AS ReqQty
FROM
	(
	SELECT
		SUPPLIER_CODE,
		PO_DATE,
		PO_NUMBER,
		SUM ( PO_AMOUNT ) AS PO_AMOUNT,
		CURRENCY_CODE,
		SUM ( AGING ) AS AGING,
		REMARK,
		SUM ( PO_QTY ) AS PO_QTY 
	FROM
		[dbo].[OutstandingPurchase] 
	GROUP BY
		SUPPLIER_CODE,
		PO_DATE,
		PO_NUMBER,
		CURRENCY_CODE,
	REMARK 
	) AS OSPO