﻿ALTER TABLE dbo.OutgoingPurchases
	ADD DeliveryStatus int NOT NULL DEFAULT 0,
		InvoiceStatus int NOT NULL DEFAULT 0;