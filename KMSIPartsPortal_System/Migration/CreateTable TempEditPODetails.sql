﻿CREATE TABLE dbo.TempEditPODetails(
	ID int IDENTITY(1,1) NOT NULL,
	DetailID int,
	ItemNumber int,
	PartNumber varchar(max),
	Descript varchar(max),
	ReqQty decimal(18,0),
	Price decimal(18,0),
	StockPoint varchar(max),
 CONSTRAINT PK_TempEditPODetails PRIMARY KEY CLUSTERED
(
	ID ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]