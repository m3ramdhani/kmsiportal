﻿CREATE TABLE [dbo].[SalesOrderLists](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SalesOrderNumber] [varchar](max) NULL,
	[PurchaseNumber] [varchar](max) NULL,
	[ItemName] [varchar](max) NULL,
	[Quantity] [int] NULL,
	[SalesOrderDate] [date] NULL,
	[CustomerCode] [varchar](max) NULL,
	[CustomerItem] [varchar](max) NULL,
	[Unit] [varchar](max) NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL
 CONSTRAINT [PK_SalesOrderLists] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]