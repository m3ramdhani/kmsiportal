﻿CREATE TABLE [dbo].[TempImportPart](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PartNumber] [varchar](max) NULL,
	[Description] [varchar](max) NULL,
	[Quantity] [int] NULL,
	[Remark] [varchar](max) NULL,
 CONSTRAINT [PK_TempImportPart] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]