﻿CREATE TABLE [dbo].[PurchaseLists](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PurchaseNumber] [varchar](max) NULL,
	[ForecastNumber] [varchar](max) NULL,
	[ItemCode] [varchar](max) NULL,
	[OrderQuantity] [int] NULL,
	[PurchaseDate] [date] NULL,
	[ItemName] [varchar](max) NULL,
	[Unit] [varchar](max) NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL
 CONSTRAINT [PK_PurchaseLists] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]