﻿ALTER TABLE dbo.TicketDocuments
  ADD SolvedDate datetime,
	  SolvedBy int,
	  ClosedDate datetime,
	  ClosedBy int;