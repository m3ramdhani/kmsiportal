﻿CREATE TABLE [dbo].[DeliveryStatuses](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](max) NULL,
	[Value] [varchar](max) NULL
 CONSTRAINT [PK_DeliveryStatuses] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

INSERT INTO [dbo].[DeliveryStatuses]
			([Name]
			,[Value])
	VALUES
			('Waiting Goods',
			'WAIT')
INSERT INTO [dbo].[DeliveryStatuses]
			([Name]
			,[Value])
	VALUES
			('Received Goods',
			'RECEIVE')
INSERT INTO [dbo].[DeliveryStatuses]
			([Name]
			,[Value])
	VALUES
			('Finished QC',
			'FQC')
INSERT INTO [dbo].[DeliveryStatuses]
			([Name]
			,[Value])
	VALUES
			('KMSI On Hand',
			'KMSI')
GO

ALTER TABLE [dbo].DeliveryOrders
  ADD [StatusId] [int];

ALTER TABLE [dbo].DeliveryOrders WITH CHECK ADD CONSTRAINT [FK_DeliveryStatus] FOREIGN KEY ([StatusId])
REFERENCES [dbo].[DeliveryStatuses] ([ID])
GO

ALTER TABLE [dbo].DeliveryOrders CHECK CONSTRAINT [FK_DeliveryStatus]
GO