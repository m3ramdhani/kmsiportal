﻿CREATE VIEW vUnitCostReport
AS
SELECT
	wo.WorkOrderID,
	wo.UnitSerialNumber,
	wo.UnitCode,
	wo.UnitModel,
	wo.ContractCode,
	wo.CustomerCode,
	wo.CustomerName,
	CONVERT(date, wo.RequestDeliveryDate) AS RequestDeliveryDate,
	wo.Site,
	COALESCE(SUM(wo.Quantity * p.Price), 0) AS TotalParts
FROM
	WorkOrder wo
LEFT OUTER JOIN Parts p ON p.PartsNumber = wo.PartsNumber
GROUP BY
	wo.WorkOrderID,
	wo.UnitSerialNumber,
	wo.UnitCode,
	wo.UnitModel,
	wo.ContractCode,
	wo.CustomerCode,
	wo.CustomerName,
	CONVERT(date, wo.RequestDeliveryDate),
	wo.Site