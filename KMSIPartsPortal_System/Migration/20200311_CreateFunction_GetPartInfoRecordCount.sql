﻿CREATE FUNCTION dbo.fGetPartInfoRecordCount()
RETURNS INT
AS 
BEGIN 
    DECLARE @returnvalue INT;

    SELECT @returnvalue = COUNT(*) 
    FROM dbo.Kpintar_Part_Info

    RETURN(@returnvalue);
END