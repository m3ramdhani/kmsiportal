﻿ALTER VIEW vInquiryStock
AS
SELECT
	ISNULL(ROW_NUMBER() OVER (ORDER BY Part_Number), 0) AS ID,
	STOCK.Part_Number,
	STOCK.Part_Name,
	STOCK.Stock_Point,
	STOCK.FreeStock_Quantity
FROM
	(
	SELECT
		Part_Number,
		Part_Name,
		Stock_Point,
		SUM(FreeStock_Quantity) AS FreeStock_Quantity
	FROM
		ST9848
	GROUP BY
		Part_Number,
		Part_Name,
		Stock_Point
	) AS STOCK