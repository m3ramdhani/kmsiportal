﻿ALTER TABLE dbo.PurchaseOrders
  ADD Remarks VARCHAR(MAX) NULL;

ALTER TABLE dbo.PurchaseDetails
  ADD Remarks VARCHAR(MAX) NULL;