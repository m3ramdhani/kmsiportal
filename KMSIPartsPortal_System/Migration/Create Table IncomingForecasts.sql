﻿CREATE TABLE [dbo].[IncomingForecasts](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ForecastNumber] [varchar](max) NULL,
	[SupplierCode] [varchar](max) NULL,
	[TypePO] [varchar](max) NULL,
	[PONumber] [varchar](max) NULL,
	[PODate] [date] NULL,
	[ItemNumber] [varchar](max) NULL,
	[PartNumber] [varchar](max) NULL,
	[ForecastQty] [int] NULL,
	[PurchaseQty] [int] NULL,
	[UnitPrice] [decimal](18,0) NULL,
	[StockPoint] [varchar](max) NULL,
	[Remark] [varchar](max) NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedAt] [datetime] NULL,
 CONSTRAINT [PK_IncomingForecast] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]