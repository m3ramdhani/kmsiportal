﻿CREATE TABLE [dbo].[OutgoingReceipts](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ReceiptNumber] [varchar](max) NULL,
	[DONO] [varchar](max) NULL,
	[PartNumber] [varchar](max) NULL,
	[InboundQuantity] [int] NULL,
	[ReceiptDate] [date] NULL,
	[PurchaseNumber] [varchar](max) NULL,
	[Description] [varchar](max) NULL,
	[Unit] [varchar](max) NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL
 CONSTRAINT [PK_OutgoingReceipts] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]