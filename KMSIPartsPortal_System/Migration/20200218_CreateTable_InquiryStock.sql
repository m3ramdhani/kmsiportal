﻿CREATE TABLE [dbo].[ST9848](
	[ID] int IDENTITY(1,1) NOT NULL,
	[Date] [date] NULL,
	[Part_Number] [varchar](50) NULL,
	[Part_Name] [varchar](50) NULL,
	[Stock_Point] [varchar](50) NULL,
	[Stock_Point_First_2letter] [varchar](50) NULL,
	[Cost_Price] [float] NULL,
	[Unit_Cost_Price_Reminder] [float] NULL,
	[FreeStock_Quantity] [float] NULL,
	[Stock_Allocated_Qty] [float] NULL,
	[Stock_Allocated_Amount] [float] NULL,
	[Commodity_Group_Code] [varchar](50) NULL,
	[Matrix_Rank] [varchar](50) NULL,
	[Order_Point_Quantity] [varchar](50) NULL,
	[Depot_Mortality] [varchar](50) NULL,
	[Depot_List_Price] [float] NULL,
	[Supplier_Code] [varchar](50) NULL,
	[System_Code] [varchar](50) NULL,
	[Last_outgoing_Date] [varchar](50) NULL,
	[Last_Incoming_Date] [varchar](50) NULL,
	[New_Commodity_Code] [varchar](50) NULL,
	[Stock_Value] [float] NULL,
	[PullDate] [datetime] NOT NULL DEFAULT GETDATE(), 
CONSTRAINT [PK_ST9848] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE ST9848Logs(
	ID int IDENTITY(1,1) NOT NULL,
	Activity varchar(255) NULL,
	TimeLogs datetime NOT NULL DEFAULT GETDATE(),
	AffectedRows int NOT NULL DEFAULT 0
CONSTRAINT [PK_ST9848Log] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]