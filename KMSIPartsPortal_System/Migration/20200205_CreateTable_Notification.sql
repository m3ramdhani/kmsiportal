﻿CREATE TABLE dbo.Notifications(
	ID int IDENTITY(1,1) NOT NULL,
	UserId int,
	[Name] varchar(max),
	Module varchar(max),
	FormNumber varchar(max),
	[Message] varchar(max),
	[Url] varchar(max),
	ReceiveId int,
	IsRead bit NOT NULL DEFAULT 0,
	CreatedAt datetime,
	ReadAt datetime
 CONSTRAINT PK_Notification PRIMARY KEY CLUSTERED
(
	ID ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]