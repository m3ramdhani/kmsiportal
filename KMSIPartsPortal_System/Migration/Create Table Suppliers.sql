﻿ALTER TABLE dbo.Suppliers
	ADD LeadTime varchar(max) NULL,
		Usance varchar(max) NULL,
		AgreementPeriod varchar(max) NULL,
		Notes varchar(max) NULL,
		RegistDate date NULL,
		LastUpdTime datetime NULL,
		LastUser varchar(max) NULL,
		SupplierLogo varchar(max) NULL;