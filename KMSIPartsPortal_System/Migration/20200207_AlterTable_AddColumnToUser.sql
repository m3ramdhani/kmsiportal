﻿ALTER TABLE dbo.Users
  ADD CompanyName varchar(max),
	  Position varchar(max),
	  Phone varchar(max),
	  CreatedAt datetime,
	  ActivatedAt datetime;