﻿CREATE TABLE [dbo].[IncomingStatuses](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](max) NULL,
	[Value] [varchar](max) NULL
 CONSTRAINT [PK_IncomingStatuses] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

INSERT INTO [dbo].[IncomingStatuses]
			([Name]
			,[Value])
	VALUES
			('HUB-2 Waiting Goods',
			'HUB2W')
INSERT INTO [dbo].[IncomingStatuses]
			([Name]
			,[Value])
	VALUES
			('Received By HUB-2',
			'RHUB2')
INSERT INTO [dbo].[IncomingStatuses]
			([Name]
			,[Value])
	VALUES
			('Shipment To BJM',
			'SHIPBJM')
INSERT INTO [dbo].[IncomingStatuses]
			([Name]
			,[Value])
	VALUES
			('Received By BJM',
			'RBJM')
INSERT INTO [dbo].[IncomingStatuses]
			([Name]
			,[Value])
	VALUES
			('Finished QC-Waiting Confirmation',
			'FQCW')
INSERT INTO [dbo].[IncomingStatuses]
			([Name]
			,[Value])
	VALUES
			('GGI On Hand',
			'GGI')
GO

ALTER TABLE [dbo].[IncomingLists]
  ADD [StatusId] [int];

ALTER TABLE [dbo].[IncomingLists] WITH CHECK ADD CONSTRAINT [FK_IncomingStatus] FOREIGN KEY ([StatusId])
REFERENCES [dbo].[IncomingStatuses] ([ID])
GO

ALTER TABLE [dbo].[IncomingLists] CHECK CONSTRAINT [FK_IncomingStatus]
GO