﻿SELECT ROW_NUMBER() OVER (ORDER BY WorkOrderID) AS ID, WorkOrderID, WorkOrderCode, WorkOrderDate, ContractCode, UnitSerialNumber, UnitModel, UnitCode, CustomerCode, CustomerName, Site, RequestDeliveryDate, 
CASE WHEN ApprovalStatus = 1 THEN 'New' WHEN ApprovalStatus = 2 THEN 'Approved' WHEN ApprovalStatus = 3 THEN 'Rejected' ELSE '' END AS ApprovalStatus
FROM dbo.WorkOrder