﻿using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.ScheduledTasks
{
    public class JobScheduler
    {
        public static void Start()
        {
            IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler().Result;
            scheduler.Start();

            IJobDetail job = JobBuilder.Create<OspoJob>().Build();

            /*
             * set the daily scheduler to run at 01:00
             */
            /* ITrigger trigger = TriggerBuilder.Create()
                .WithDailyTimeIntervalSchedule
                (s =>
                    s.WithIntervalInHours(24)
                    .OnEveryDay()
                    .StartingDailyAt(TimeOfDay.HourAndMinuteOfDay(1, 0))
                )
                .Build(); */

            /*
             * set the scheduler to run every 3 hours
             */
            ITrigger trigger = TriggerBuilder.Create()
                .ForJob(job)
                .WithCronSchedule("0 0 0/3 1/1 * ? *", cs => cs.WithMisfireHandlingInstructionDoNothing())
                .WithIdentity("triggerOspo", "groupOspo")
                .StartNow()
                .Build();

            scheduler.ScheduleJob(job, trigger);
        }

        public static void ReopenTicket()
        {
            IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler().Result;
            scheduler.Start();

            IJobDetail job = JobBuilder.Create<TicketJob>().Build();

            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("triggerReopen", "groupReopen")
                .StartNow()
                .WithSimpleSchedule(x => x
                    .WithIntervalInHours(1)
                    .RepeatForever())
                .Build();

            scheduler.ScheduleJob(job, trigger);
        }

        public static void SetDurationTicket()
        {
            IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler().Result;
            scheduler.Start();

            IJobDetail job = JobBuilder.Create<DurationJob>().Build();

            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("triggerDuration", "groupDuration")
                .StartNow()
                .WithSimpleSchedule(x => x
                    .WithIntervalInSeconds(10)
                    .RepeatForever())
                .Build();

            scheduler.ScheduleJob(job, trigger);
        }

        public static void GetStockInfo()
        {
            IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler().Result;
            scheduler.Start();

            IJobDetail job = JobBuilder.Create<InquiryStockJob>()
                .WithIdentity("stockInfoJob", "groupStock")
                .Build();

            ITrigger trigger = TriggerBuilder.Create()
                .ForJob(job)
                .WithCronSchedule("0 */30 * ? * *", cs => cs.WithMisfireHandlingInstructionDoNothing())
                .WithIdentity("triggerStock", "groupStock")
                .StartNow()
                .Build();

            scheduler.ScheduleJob(job, trigger);
        }

        public static void GetPartInfo()
        {
            IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler().Result;
            scheduler.Start();

            IJobDetail job = JobBuilder.Create<InquiryPartJob>().Build();

            ITrigger trigger = TriggerBuilder.Create()
                .WithDailyTimeIntervalSchedule
                (s =>
                    s.WithIntervalInHours(24)
                    .OnEveryDay()
                    .StartingDailyAt(TimeOfDay.HourAndMinuteOfDay(8, 0))
                )
                //.WithIdentity("triggerPart", "groupPart")
                //.StartNow()
                //.WithSimpleSchedule(x => x
                //    .WithIntervalInMinutes(60)
                //    .RepeatForever())
                .Build();

            scheduler.ScheduleJob(job, trigger);
        }

        public static void CloseTicket()
        {
            IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler().Result;
            scheduler.Start();

            IJobDetail job = JobBuilder.Create<CloseTicketJob>().Build();

            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("triggerClose", "groupClose")
                .StartNow()
                .WithSimpleSchedule(x => x
                    .WithIntervalInHours(1)
                    .RepeatForever())
                .Build();

            scheduler.ScheduleJob(job, trigger);
        }
    }
}