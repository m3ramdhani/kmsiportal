﻿using KMSIPartsPortal_System.Models;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace KMSIPartsPortal_System.ScheduledTasks
{
    public class TicketJob : IJob
    {
        private kmsi_portalEntities db = new kmsi_portalEntities();

        public Task Execute(IJobExecutionContext context)
        {
            List<TicketDocument> tickets = db.TicketDocuments.Where(x => x.Status == 2).ToList();
            Double limit = 2880;
            foreach (var t in tickets)
            {
                Double duration = DateTime.Now.Subtract(t.PickupDate.Value).TotalMinutes;
                if (t.UpdatedAt.HasValue && t.PickupDate.HasValue)
                {
                    if (t.UpdatedAt.Value == t.PickupDate.Value && duration > limit)
                    {
                        t.Status = 1;
                        t.PickupBy = (int?)null;
                        db.SaveChanges();
                    }
                }
            }
            
            return Task.Run(() => Execute(context));
        }
    }
}