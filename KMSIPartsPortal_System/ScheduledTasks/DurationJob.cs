﻿using KMSIPartsPortal_System.Models;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace KMSIPartsPortal_System.ScheduledTasks
{
    public class DurationJob : IJob
    {
        private kmsi_portalEntities db = new kmsi_portalEntities();

        public Task Execute(IJobExecutionContext context)
        {
            List<TicketDocument> tickets = db.TicketDocuments.Where(x => x.Status != 4).ToList();
            foreach (var t in tickets)
            {
                TimeZoneInfo serverZone = TimeZoneInfo.FindSystemTimeZoneById("SE Asia Standard Time");
                DateTime currentDateTime = TimeZoneInfo.ConvertTime(DateTime.UtcNow, serverZone);

                Double duration = currentDateTime.Subtract(t.SubmitDate.Value).TotalSeconds;
                t.Duration = Convert.ToDecimal(duration);
                db.SaveChanges();
            }

            return Task.Run(() => Execute(context));
        }
    }
}