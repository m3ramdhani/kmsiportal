﻿using KMSIPartsPortal_System.Helpers;
using KMSIPartsPortal_System.Models;
using Quartz;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace KMSIPartsPortal_System.ScheduledTasks
{
    [PersistJobDataAfterExecution]
    [DisallowConcurrentExecution]
    public class InquiryStockJob : IJob
    {
        private kmsi_portalEntities db = new kmsi_portalEntities();

        public async Task Execute(IJobExecutionContext context)
        {
            string remote = ConfigurationManager.ConnectionStrings["OspoConnectionString"].ConnectionString;
            string local = ConfigurationManager.ConnectionStrings["Constring"].ConnectionString;

            DateTime now = DateTime.Now;
            DateTime lastRun = db.StockInfoLogs.Max(x => (DateTime?)x.TimeLogs) ?? DateTime.Now;
            double interval = now.Subtract(lastRun).TotalMinutes;
            if (interval >= 30 || interval < 0)
            {
                FileImport.ImportStockFromDB(remote, local);
            }

            await Task.Run(() => Execute(context));
        }
    }
}