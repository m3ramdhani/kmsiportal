﻿using KMSIPartsPortal_System.Models;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace KMSIPartsPortal_System.ScheduledTasks
{
    public class CloseTicketJob : IJob
    {
        private kmsi_portalEntities db = new kmsi_portalEntities();

        public Task Execute(IJobExecutionContext context)
        {
            List<TicketDocument> tickets = db.TicketDocuments.Where(x => x.Status == 3).ToList();
            Double limit = 120;
            foreach (var t in tickets)
            {
                if (t.UpdatedAt.HasValue && t.SolvedDate.HasValue)
                {
                    Double duration = DateTime.Now.Subtract(t.SolvedDate.Value).TotalHours;
                    if (t.UpdatedAt.Value == t.SolvedDate.Value && duration > limit)
                    {
                        TimeZoneInfo serverZone = TimeZoneInfo.FindSystemTimeZoneById("SE Asia Standard Time");
                        DateTime currentDateTime = TimeZoneInfo.ConvertTime(DateTime.UtcNow, serverZone);

                        t.Status = 4;
                        t.UpdatedAt = currentDateTime;
                        t.UpdatedBy = 0;
                        db.SaveChanges();
                    }
                }
            }

            return Task.Run(() => Execute(context));
        }
    }
}