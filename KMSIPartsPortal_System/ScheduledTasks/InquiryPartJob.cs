﻿using KMSIPartsPortal_System.Helpers;
using KMSIPartsPortal_System.Models;
using Quartz;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace KMSIPartsPortal_System.ScheduledTasks
{
    public class InquiryPartJob : IJob
    {
        private kmsi_portalEntities db = new kmsi_portalEntities();

        public Task Execute(IJobExecutionContext context)
        {
            DateTime today = DateTime.Now.Date;
            PartInfoLog logs = db.PartInfoLogs.FirstOrDefault(x => DbFunctions.TruncateTime(x.TimeLogs) == today);
            if (logs == null)
            {
                string remote = ConfigurationManager.ConnectionStrings["OspoConnectionString"].ConnectionString;
                string local = ConfigurationManager.ConnectionStrings["Constring"].ConnectionString;
                Boolean fetchData = FileImport.ImportPartInfoFromDB(remote, local);
            }

            return Task.Run(() => Execute(context));
        }
    }
}