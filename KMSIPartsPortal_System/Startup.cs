﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(KMSIPartsPortal_System.Startup))]
namespace KMSIPartsPortal_System
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}