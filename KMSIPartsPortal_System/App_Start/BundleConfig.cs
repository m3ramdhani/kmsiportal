﻿using System.Web.Optimization;
using WebHelpers.Mvc5;

namespace KMSIPartsPortal_System.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Bundles/css"));

            bundles.Add(new ScriptBundle("~/Bundles/js"));

#if DEBUG
            BundleTable.EnableOptimizations = false;
#else
            BundleTable.EnableOptimizations = true;
#endif
        }
    }
}
