﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using iTextSharp.text;
using iTextSharp.text.pdf;
using KMSIPartsPortal_System.Models;

namespace KMSIPartsPortal_System.Controllers
{
    public class WorkOrderReportsController : Controller
    {
        private kmsi_portalEntities db = new kmsi_portalEntities();

        // GET: WorkOrderReports
        public ActionResult Index()
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return HttpNotFound();
            }
        }

        public ActionResult List()
        {
            try
            {
                // Creating instance of DatabaseContext class
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                var start_date = Request.Form.GetValues("start_date").FirstOrDefault().Trim();
                var end_date = Request.Form.GetValues("end_date").FirstOrDefault().Trim();

                // Paging Size
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                // Getting all Work Order Reports data
                var woReportsData = db.vWorkOrderReports.AsEnumerable()
                                .GroupBy(x => x.WorkOrderCode)
                                .Select(group => group.First());

                // Sorting
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    woReportsData = woReportsData.OrderBy(sortColumn + " " + sortColumnDir);
                }
                // Search
                if (!string.IsNullOrEmpty(searchValue))
                {
                    woReportsData = woReportsData.Where(wo => wo.WorkOrderCode.ToString().ToLower().Contains(searchValue.ToLower())
                        || wo.WorkOrderDate.ToString().ToLower().Contains(searchValue.ToLower())
                        || wo.UnitSerialNumber.ToLower().Contains(searchValue.ToLower())
                        || wo.UnitCode.ToLower().Contains(searchValue.ToLower())
                        || wo.UnitModel.ToLower().Contains(searchValue.ToLower())
                        || wo.ContractCode.ToLower().Contains(searchValue.ToLower())
                        || wo.CustomerCode.ToLower().Contains(searchValue.ToLower())
                        || wo.CustomerName.ToLower().Contains(searchValue.ToLower())
                        || wo.RequestDeliveryDate.ToString().ToLower().Contains(searchValue.ToLower())
                        || wo.Site.ToLower().Contains(searchValue.ToLower())
                        || wo.ApprovalStatus.ToLower().Contains(searchValue.ToLower())
                    );
                }

                //Filter date
                if (!(string.IsNullOrEmpty(start_date) && string.IsNullOrEmpty(end_date)))
                {
                    DateTime sDate = DateTime.ParseExact(start_date, "dd/MM/yy", null);
                    DateTime eDate = DateTime.ParseExact(end_date, "dd/MM/yy", null);
                    woReportsData = woReportsData.Where(wo => DateTime.ParseExact(wo.WorkOrderDate.Value.ToString("dd/MM/yy"), "dd/MM/yy", null) >= sDate && DateTime.ParseExact(wo.WorkOrderDate.Value.ToString("dd/MM/yy"), "dd/MM/yy", null) <= eDate);
                }

                // total number of rows count
                recordsTotal = woReportsData.Count();
                //Paging
                var data = woReportsData.Skip(skip).Take(pageSize).ToList();

                // Returning Json Data
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return HttpNotFound();
            }
        }

        public ActionResult PostReportPartial(int[] ids, string period)
        {
            DateTime dateTime = DateTime.Now;
            string strPDFFileName = string.Format("WorkOrderReport" + dateTime.ToString("yyyyMMdd_HHmm") + ".pdf");
            string handle = Guid.NewGuid().ToString();

            using (MemoryStream memoryStream = new MemoryStream())
            {
                Document pdfDoc = new Document(PageSize.A4.Rotate());
                PdfWriter.GetInstance(pdfDoc, memoryStream).CloseStream = false;

                pdfDoc.SetMargins(28f, 28f, 72f, 72f);
                //Create PDF Table

                //file will created in this path
                string strAttachment = Server.MapPath("~/PDFs/" + strPDFFileName);

                pdfDoc.Open();

                Phrase phrase = new Phrase();

                //Table
                PdfPTable table = new PdfPTable(2);
                table.WidthPercentage = 100;

                //Cell no 1
                Image header1 = Image.GetInstance(Server.MapPath("~/Content/img/header-1.png"));
                header1.ScaleAbsolute(154f, 35f);
                header1.Alignment = Image.ALIGN_LEFT;
                PdfPCell cell = new PdfPCell();
                cell.Border = 0;
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.AddElement(header1);
                table.AddCell(cell);

                //Cell no 2
                Image header2 = Image.GetInstance(Server.MapPath("~/Content/img/header-2.png"));
                header2.ScaleAbsolute(110f, 21f);
                header2.Alignment = Image.ALIGN_RIGHT;
                cell = new PdfPCell();
                cell.Border = 0;
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.AddElement(header2);
                table.AddCell(cell);

                //Add table to document
                pdfDoc.Add(table);

                //Horizontal Line
                Paragraph line = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1)));
                pdfDoc.Add(line);

                //Title
                table = new PdfPTable(1);
                table.WidthPercentage = 100;
                table.SpacingAfter = 10f;

                table.AddCell(new PdfPCell(new Phrase("WORK ORDER REPORTS", FontFactory.GetFont("Arial", 20, BaseColor.BLACK)))
                {
                    Border = 0,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    VerticalAlignment = Element.ALIGN_MIDDLE
                });
                pdfDoc.Add(table);

                Paragraph para = new Paragraph();
                phrase = new Phrase("Period Date : " + period, FontFactory.GetFont("Arial", 10, Font.BOLD));
                para.Add(phrase);
                pdfDoc.Add(para);

                var workOrders = from item in db.vWorkOrderReports
                                 where ids.Contains(item.WorkOrderID)
                                 select item;

                //Table
                table = new PdfPTable(11);
                table.WidthPercentage = 100;
                table.SpacingBefore = 10f;

                //Cell content
                //header
                string[] headers = { "No.", "WO Date", "WO ID", "Unit SN", "Unit Code", "Unit Model", "Contract No.", "Customer ID", "Customer Name", "Request Delivery Date", "Site" };
                for (int i = 0; i < headers.Count(); i++)
                {
                    table.AddCell(new PdfPCell(new Phrase(headers[i], FontFactory.GetFont("Arial", 12)))
                    {
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_TOP
                    });
                }

                int counter = 1;
                foreach (var item in workOrders)
                {
                    table.AddCell(new PdfPCell(new Phrase(counter.ToString(), FontFactory.GetFont("Arial", 11)))
                    {
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_TOP,
                        PaddingBottom = 8
                    });
                    table.AddCell(new PdfPCell(new Phrase(item.WorkOrderDate.Value.ToString("dd/MM/yy"), FontFactory.GetFont("Arial", 11)))
                    {
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_TOP,
                        PaddingBottom = 8
                    });
                    table.AddCell(new PdfPCell(new Phrase(item.WorkOrderCode, FontFactory.GetFont("Arial", 11)))
                    {
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_TOP,
                        PaddingBottom = 8
                    });
                    table.AddCell(new PdfPCell(new Phrase(item.UnitSerialNumber, FontFactory.GetFont("Arial", 11)))
                    {
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_TOP,
                        PaddingBottom = 8
                    });
                    table.AddCell(new PdfPCell(new Phrase(item.UnitCode, FontFactory.GetFont("Arial", 11)))
                    {
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_TOP,
                        PaddingBottom = 8
                    });
                    table.AddCell(new PdfPCell(new Phrase(item.UnitModel, FontFactory.GetFont("Arial", 11)))
                    {
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_TOP,
                        PaddingBottom = 8
                    });
                    table.AddCell(new PdfPCell(new Phrase(item.ContractCode, FontFactory.GetFont("Arial", 11)))
                    {
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_TOP,
                        PaddingBottom = 8
                    });
                    table.AddCell(new PdfPCell(new Phrase(item.CustomerCode, FontFactory.GetFont("Arial", 11)))
                    {
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_TOP,
                        PaddingBottom = 8
                    });
                    table.AddCell(new PdfPCell(new Phrase(item.CustomerName, FontFactory.GetFont("Arial", 11)))
                    {
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_TOP,
                        PaddingBottom = 8
                    });
                    table.AddCell(new PdfPCell(new Phrase((item.RequestDeliveryDate != null) ? item.RequestDeliveryDate.Value.ToString("dd/MM/yy") : "", FontFactory.GetFont("Arial", 11)))
                    {
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_TOP,
                        PaddingBottom = 8
                    });
                    table.AddCell(new PdfPCell(new Phrase(item.Site, FontFactory.GetFont("Arial", 11)))
                    {
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_TOP,
                        PaddingBottom = 8
                    });
                    counter++;
                }

                pdfDoc.Add(table);

                // Closing the document
                pdfDoc.Close();

                var bytes = memoryStream.ToArray();
                Session[strPDFFileName] = bytes;
            }

            return Json(new { success = true, strPDFFileName }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual ActionResult DownloadPdf(string fileName)
        {
            try
            {
                var ms = Session[fileName] as byte[];
                if (ms == null)
                    return new EmptyResult();
                Session[fileName] = null;
                return File(ms, "application/octet-stream", fileName);
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return HttpNotFound();
            }
        }
    }
}
