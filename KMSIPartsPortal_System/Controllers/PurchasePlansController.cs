﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.OleDb;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.draw;
using KMSIPartsPortal_System.Helpers;
using KMSIPartsPortal_System.Models;

namespace KMSIPartsPortal_System.Controllers
{
    public class PurchasePlansController : Controller
    {
        private kmsi_portalEntities db = new kmsi_portalEntities();

        // GET: PurchasePlans
        public ActionResult Index()
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    DateTime aDay = DateTime.Now;
                    int aYear = aDay.Year;

                    var years = new List<SelectListItem>()
                    {
                        new SelectListItem { Text = (aYear - 3).ToString(), Value = (aYear - 3).ToString()},
                        new SelectListItem { Text = (aYear - 2).ToString(), Value = (aYear - 2).ToString()},
                        new SelectListItem { Text = (aYear - 1).ToString(), Value = (aYear - 1).ToString()},
                        new SelectListItem { Text = (aYear).ToString(), Value = (aYear).ToString(), Selected = true},
                        new SelectListItem { Text = (aYear + 1).ToString(), Value = (aYear + 1).ToString()},
                        new SelectListItem { Text = (aYear + 2).ToString(), Value = (aYear + 2).ToString()},
                        new SelectListItem { Text = (aYear + 3).ToString(), Value = (aYear - 3).ToString()}
                    };
                    var selected = years.Where(x => x.Value == aYear.ToString()).First();
                    selected.Selected = true;

                    ViewBag.year = years;
                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult List()
        {
            try
            {
                // Creating instance of DatabaseContext class
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                var year = Request.Form.GetValues("year").FirstOrDefault();

                // Paging Size
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                // Getting all data
                DateTime aDay = DateTime.Now;
                int aYear = aDay.Year;
                if (year != "")
                {
                    aYear = Convert.ToInt32(year);
                }
                var planLists = from plan in db.PurchasePlans
                                where plan.Year == aYear
                                group plan by new
                                {
                                    plan.PartNumber
                                }
                                into planlist
                                select new
                                {
                                    planlist.Key.PartNumber,
                                    PlanJan = planlist.Where(x => x.Month == 1 && x.Year == aYear).Select(x => x.Plan),
                                    PlanFeb = planlist.Where(x => x.Month == 2 && x.Year == aYear).Select(x => x.Plan),
                                    PlanMar = planlist.Where(x => x.Month == 3 && x.Year == aYear).Select(x => x.Plan),
                                    PlanApr = planlist.Where(x => x.Month == 4 && x.Year == aYear).Select(x => x.Plan),
                                    PlanMay = planlist.Where(x => x.Month == 5 && x.Year == aYear).Select(x => x.Plan),
                                    PlanJun = planlist.Where(x => x.Month == 6 && x.Year == aYear).Select(x => x.Plan),
                                    PlanJul = planlist.Where(x => x.Month == 7 && x.Year == aYear).Select(x => x.Plan),
                                    PlanAgt = planlist.Where(x => x.Month == 8 && x.Year == aYear).Select(x => x.Plan),
                                    PlanSpt = planlist.Where(x => x.Month == 9 && x.Year == aYear).Select(x => x.Plan),
                                    PlanOct = planlist.Where(x => x.Month == 10 && x.Year == aYear).Select(x => x.Plan),
                                    PlanNov = planlist.Where(x => x.Month == 11 && x.Year == aYear).Select(x => x.Plan),
                                    PlanDec = planlist.Where(x => x.Month == 12 && x.Year == aYear).Select(x => x.Plan),
                                    ActJan = (System.Int32?)
                                                ((from id in db.PurchaseDetails
                                                  where id.PartNumber == planlist.Key.PartNumber && id.PurchaseDate.Value.Month == 1 && id.PurchaseDate.Value.Year == aYear
                                                  group id by new
                                                  {
                                                      id.PartNumber
                                                  } into g
                                                  select new
                                                  {
                                                      Actual = g.Sum(p => p.RequestQuantity)
                                                  }).FirstOrDefault().Actual),
                                    ActFeb = (System.Int32?)
                                                ((from id in db.PurchaseDetails
                                                  where id.PartNumber == planlist.Key.PartNumber && id.PurchaseDate.Value.Month == 2 && id.PurchaseDate.Value.Year == aYear
                                                  group id by new
                                                  {
                                                      id.PartNumber
                                                  } into g
                                                  select new
                                                  {
                                                      Actual = g.Sum(p => p.RequestQuantity)
                                                  }).FirstOrDefault().Actual),
                                    ActMar = (System.Int32?)
                                                ((from id in db.PurchaseDetails
                                                  where id.PartNumber == planlist.Key.PartNumber && id.PurchaseDate.Value.Month == 3 && id.PurchaseDate.Value.Year == aYear
                                                  group id by new
                                                  {
                                                      id.PartNumber
                                                  } into g
                                                  select new
                                                  {
                                                      Actual = g.Sum(p => p.RequestQuantity)
                                                  }).FirstOrDefault().Actual),
                                    ActApr = (System.Int32?)
                                                ((from id in db.PurchaseDetails
                                                  where id.PartNumber == planlist.Key.PartNumber && id.PurchaseDate.Value.Month == 4 && id.PurchaseDate.Value.Year == aYear
                                                  group id by new
                                                  {
                                                      id.PartNumber
                                                  } into g
                                                  select new
                                                  {
                                                      Actual = g.Sum(p => p.RequestQuantity)
                                                  }).FirstOrDefault().Actual),
                                    ActMay = (System.Int32?)
                                                ((from id in db.PurchaseDetails
                                                  where id.PartNumber == planlist.Key.PartNumber && id.PurchaseDate.Value.Month == 5 && id.PurchaseDate.Value.Year == aYear
                                                  group id by new
                                                  {
                                                      id.PartNumber
                                                  } into g
                                                  select new
                                                  {
                                                      Actual = g.Sum(p => p.RequestQuantity)
                                                  }).FirstOrDefault().Actual),
                                    ActJun = (System.Int32?)
                                                ((from id in db.PurchaseDetails
                                                  where id.PartNumber == planlist.Key.PartNumber && id.PurchaseDate.Value.Month == 6 && id.PurchaseDate.Value.Year == aYear
                                                  group id by new
                                                  {
                                                      id.PartNumber
                                                  } into g
                                                  select new
                                                  {
                                                      Actual = g.Sum(p => p.RequestQuantity)
                                                  }).FirstOrDefault().Actual),
                                    ActJul = (System.Int32?)
                                                ((from id in db.PurchaseDetails
                                                  where id.PartNumber == planlist.Key.PartNumber && id.PurchaseDate.Value.Month == 7 && id.PurchaseDate.Value.Year == aYear
                                                  group id by new
                                                  {
                                                      id.PartNumber
                                                  } into g
                                                  select new
                                                  {
                                                      Actual = g.Sum(p => p.RequestQuantity)
                                                  }).FirstOrDefault().Actual),
                                    ActAgt = (System.Int32?)
                                                ((from id in db.PurchaseDetails
                                                  where id.PartNumber == planlist.Key.PartNumber && id.PurchaseDate.Value.Month == 8 && id.PurchaseDate.Value.Year == aYear
                                                  group id by new
                                                  {
                                                      id.PartNumber
                                                  } into g
                                                  select new
                                                  {
                                                      Actual = g.Sum(p => p.RequestQuantity)
                                                  }).FirstOrDefault().Actual),
                                    ActSpt = (System.Int32?)
                                                ((from id in db.PurchaseDetails
                                                  where id.PartNumber == planlist.Key.PartNumber && id.PurchaseDate.Value.Month == 9 && id.PurchaseDate.Value.Year == aYear
                                                  group id by new
                                                  {
                                                      id.PartNumber
                                                  } into g
                                                  select new
                                                  {
                                                      Actual = g.Sum(p => p.RequestQuantity)
                                                  }).FirstOrDefault().Actual),
                                    ActOct = (System.Int32?)
                                                ((from id in db.PurchaseDetails
                                                  where id.PartNumber == planlist.Key.PartNumber && id.PurchaseDate.Value.Month == 10 && id.PurchaseDate.Value.Year == aYear
                                                  group id by new
                                                  {
                                                      id.PartNumber
                                                  } into g
                                                  select new
                                                  {
                                                      Actual = g.Sum(p => p.RequestQuantity)
                                                  }).FirstOrDefault().Actual),
                                    ActNov = (System.Int32?)
                                                ((from id in db.PurchaseDetails
                                                  where id.PartNumber == planlist.Key.PartNumber && id.PurchaseDate.Value.Month == 11 && id.PurchaseDate.Value.Year == aYear
                                                  group id by new
                                                  {
                                                      id.PartNumber
                                                  } into g
                                                  select new
                                                  {
                                                      Actual = g.Sum(p => p.RequestQuantity)
                                                  }).FirstOrDefault().Actual),
                                    ActDec = (System.Int32?)
                                                ((from id in db.PurchaseDetails
                                                  where id.PartNumber == planlist.Key.PartNumber && id.PurchaseDate.Value.Month == 12 && id.PurchaseDate.Value.Year == aYear
                                                  group id by new
                                                  {
                                                      id.PartNumber
                                                  } into g
                                                  select new
                                                  {
                                                      Actual = g.Sum(p => p.RequestQuantity)
                                                  }).FirstOrDefault().Actual)
                                };

                // Sorting
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    planLists = planLists.OrderBy(sortColumn + " " + sortColumnDir);
                }
                // Search
                if (!string.IsNullOrEmpty(searchValue))
                {
                    planLists = planLists.Where(m => m.PartNumber.ToLower().Contains(searchValue.ToLower()));
                }

                // total number of rows count
                recordsTotal = planLists.Count();
                // Paging
                var data = planLists.Skip(skip).Take(pageSize).ToList();

                // Returning Json Data
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        // GET: PurchasePlans/Edit/5
        public ActionResult Edit(string partnumber, string year)
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    // TODO: Add delete logic here
                    int Year = Convert.ToInt32(year);
                    var getDataPlan = (from p in db.PurchasePlans
                                       where p.PartNumber == partnumber && p.Year == Year
                                       select p).ToList();

                    Thread.CurrentThread.CurrentCulture = new CultureInfo("id-ID");
                    string[] monthNames = CultureInfo.CurrentCulture.DateTimeFormat.MonthNames;

                    List<PurchasePlan> plans = new List<PurchasePlan>();

                    int i = 1;
                    foreach (string month in monthNames)
                    {
                        if (month != "")
                        {
                            PurchasePlan plan = new PurchasePlan();
                            plan.month = i;
                            plan.monthName = month;
                            var planQty = getDataPlan.Where(x => x.Month == i).FirstOrDefault();
                            plan.PlanQty = (planQty != null) ? planQty.Plan : 0;
                            plans.Add(plan);
                        }
                        i++;
                    }

                    ViewBag.planList = plans;
                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        // POST: PurchasePlans/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,PartNumber,Year,Month,Plan,Actual")] PurchasePlan purchasePlan)
        {
            if (ModelState.IsValid)
            {
                db.Entry(purchasePlan).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(purchasePlan);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public FileResult DownloadExcel()
        {
            string path = "/Docs/PO_Plan_Format.xlsx";
            return File(path, "application/vnd.ms-excel", "PO_Plan_Format.xlsx");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [HandleError]
        public ActionResult UploadExcel(PurchasePlan plan, HttpPostedFileBase file, FormCollection form)
        {
            try
            {
                int aYear = Convert.ToInt32(form[0]);
                var data = new List<string>();
                if (file != null)
                {
                    if (file.ContentType == "application/vnd.ms-excel" || file.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                    {
                        var fileName = file.FileName;
                        var targetPath = Server.MapPath("~/Uploads/");
                        file.SaveAs(targetPath + fileName);
                        var pathToExcelFile = targetPath + fileName;
                        string FileName = Path.GetFileName(file.FileName);
                        string Extension = Path.GetExtension(file.FileName);
                        DataTable dataTable = FileImport.ImportToGrid(pathToExcelFile, Extension, "Yes");
                        foreach (DataRow item in dataTable.Rows)
                        {
                            try
                            {
                                var partNumber = item["PART NUMBER"].ToString();
                                int Jan = (item["1"].ToString() != "") ? Convert.ToInt32(item["1"]) : 0;
                                int Feb = (item["2"].ToString() != "") ? Convert.ToInt32(item["2"]) : 0;
                                int Mar = (item["3"].ToString() != "") ? Convert.ToInt32(item["3"]) : 0;
                                int Apr = (item["4"].ToString() != "") ? Convert.ToInt32(item["4"]) : 0;
                                int May = (item["5"].ToString() != "") ? Convert.ToInt32(item["5"]) : 0;
                                int Jun = (item["6"].ToString() != "") ? Convert.ToInt32(item["6"]) : 0;
                                int Jul = (item["7"].ToString() != "") ? Convert.ToInt32(item["7"]) : 0;
                                int Agt = (item["8"].ToString() != "") ? Convert.ToInt32(item["8"]) : 0;
                                int Spt = (item["9"].ToString() != "") ? Convert.ToInt32(item["9"]) : 0;
                                int Oct = (item["10"].ToString() != "") ? Convert.ToInt32(item["10"]) : 0;
                                int Nov = (item["11"].ToString() != "") ? Convert.ToInt32(item["11"]) : 0;
                                int Dec = (item["12"].ToString() != "") ? Convert.ToInt32(item["12"]) : 0;

                                var JanExist = db.PurchasePlans.FirstOrDefault(x => x.PartNumber == partNumber && x.Month == 1 && x.Year == aYear);
                                if (JanExist != null)
                                {
                                    JanExist.Plan += Jan;
                                    db.SaveChanges();
                                }
                                else
                                {
                                    plan.PartNumber = partNumber;
                                    plan.Year = aYear;
                                    plan.Month = 1;
                                    plan.Plan = Jan;
                                    db.PurchasePlans.Add(plan);
                                    db.SaveChanges();
                                }

                                var FebExist = db.PurchasePlans.FirstOrDefault(x => x.PartNumber == partNumber && x.Month == 2 && x.Year == aYear);
                                if (FebExist != null)
                                {
                                    FebExist.Plan += Feb;
                                    db.SaveChanges();
                                }
                                else
                                {
                                    plan.PartNumber = partNumber;
                                    plan.Year = aYear;
                                    plan.Month = 2;
                                    plan.Plan = Feb;
                                    db.PurchasePlans.Add(plan);
                                    db.SaveChanges();
                                }

                                var MarExist = db.PurchasePlans.FirstOrDefault(x => x.PartNumber == partNumber && x.Month == 3 && x.Year == aYear);
                                if (MarExist != null){
                                    MarExist.Plan += Mar;
                                    db.SaveChanges();
                                }
                                else
                                {
                                    plan.PartNumber = partNumber;
                                    plan.Year = aYear;
                                    plan.Month = 3;
                                    plan.Plan = Mar;
                                    db.PurchasePlans.Add(plan);
                                    db.SaveChanges();
                                }

                                var AprExist = db.PurchasePlans.FirstOrDefault(x => x.PartNumber == partNumber && x.Month == 4 && x.Year == aYear);
                                if (AprExist != null)
                                {
                                    AprExist.Plan += Apr;
                                    db.SaveChanges();
                                }
                                else
                                {
                                    plan.PartNumber = partNumber;
                                    plan.Year = aYear;
                                    plan.Month = 4;
                                    plan.Plan = Apr;
                                    db.PurchasePlans.Add(plan);
                                    db.SaveChanges();
                                }

                                var MayExist = db.PurchasePlans.FirstOrDefault(x => x.PartNumber == partNumber && x.Month == 5 && x.Year == aYear);
                                if (MayExist != null)
                                {
                                    MayExist.Plan += May;
                                    db.SaveChanges();
                                }
                                else
                                {
                                    plan.PartNumber = partNumber;
                                    plan.Year = aYear;
                                    plan.Month = 5;
                                    plan.Plan = May;
                                    db.PurchasePlans.Add(plan);
                                    db.SaveChanges();
                                }

                                var JunExist = db.PurchasePlans.FirstOrDefault(x => x.PartNumber == partNumber && x.Month == 6 && x.Year == aYear);
                                if (JunExist != null)
                                {
                                    JunExist.Plan += Jun;
                                    db.SaveChanges();
                                }
                                else
                                {
                                    plan.PartNumber = partNumber;
                                    plan.Year = aYear;
                                    plan.Month = 6;
                                    plan.Plan = Jun;
                                    db.PurchasePlans.Add(plan);
                                    db.SaveChanges();
                                }

                                var JulExist = db.PurchasePlans.FirstOrDefault(x => x.PartNumber == partNumber && x.Month == 7 && x.Year == aYear);
                                if (JulExist != null)
                                {
                                    JulExist.Plan += Jul;
                                    db.SaveChanges();
                                }
                                else
                                {
                                    plan.PartNumber = partNumber;
                                    plan.Year = aYear;
                                    plan.Month = 7;
                                    plan.Plan = Jul;
                                    db.PurchasePlans.Add(plan);
                                    db.SaveChanges();
                                }

                                var AgtExist = db.PurchasePlans.FirstOrDefault(x => x.PartNumber == partNumber && x.Month == 8 && x.Year == aYear);
                                if (AgtExist != null)
                                {
                                    AgtExist.Plan += Agt;
                                    db.SaveChanges();
                                }
                                else
                                {
                                    plan.PartNumber = partNumber;
                                    plan.Year = aYear;
                                    plan.Month = 8;
                                    plan.Plan = Agt;
                                    db.PurchasePlans.Add(plan);
                                    db.SaveChanges();
                                }

                                var SptExist = db.PurchasePlans.FirstOrDefault(x => x.PartNumber == partNumber && x.Month == 9 && x.Year == aYear);
                                if (SptExist != null)
                                {
                                    SptExist.Plan += Spt;
                                    db.SaveChanges();
                                }
                                else
                                {
                                    plan.PartNumber = partNumber;
                                    plan.Year = aYear;
                                    plan.Month = 9;
                                    plan.Plan = Spt;
                                    db.PurchasePlans.Add(plan);
                                    db.SaveChanges();
                                }

                                var OctExist = db.PurchasePlans.FirstOrDefault(x => x.PartNumber == partNumber && x.Month == 10 && x.Year == aYear);
                                if (OctExist != null)
                                {
                                    OctExist.Plan += Oct;
                                    db.SaveChanges();
                                }
                                else
                                {
                                    plan.PartNumber = partNumber;
                                    plan.Year = aYear;
                                    plan.Month = 10;
                                    plan.Plan = Oct;
                                    db.PurchasePlans.Add(plan);
                                    db.SaveChanges();
                                }

                                var NovExist = db.PurchasePlans.FirstOrDefault(x => x.PartNumber == partNumber && x.Month == 11 && x.Year == aYear);
                                if (NovExist != null)
                                {
                                    NovExist.Plan += Nov;
                                    db.SaveChanges();
                                }
                                else
                                {
                                    plan.PartNumber = partNumber;
                                    plan.Year = aYear;
                                    plan.Month = 11;
                                    plan.Plan = Nov;
                                    db.PurchasePlans.Add(plan);
                                    db.SaveChanges();
                                }

                                var DecExist = db.PurchasePlans.FirstOrDefault(x => x.PartNumber == partNumber && x.Month == 12 && x.Year == aYear);
                                if (DecExist != null)
                                {
                                    DecExist.Plan += Dec;
                                    db.SaveChanges();
                                }
                                else
                                {
                                    plan.PartNumber = partNumber;
                                    plan.Year = aYear;
                                    plan.Month = 12;
                                    plan.Plan = Dec;
                                    db.PurchasePlans.Add(plan);
                                    db.SaveChanges();
                                }
                            }
                            catch (Exception ex)
                            {
                                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                                var sw = new System.IO.StreamWriter(filename, true);
                                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                                sw.Close();
                            }
                        }
                    }
                }
                else
                {
                    var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                    var sw = new System.IO.StreamWriter(filename, true);
                    sw.WriteLine(DateTime.Now.ToString() + " " + "File Not Selected!");
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();
            }

            return RedirectToAction("Index");
        }

        public ActionResult PostReportPartial(string[] ids, string period)
        {
            try
            {
                DateTime dateTime = DateTime.Now;
                string strPDFFileName = string.Format("PurchasePlan" + dateTime.ToString("yyyyMMdd_HHmm") + ".pdf");
                string handle = Guid.NewGuid().ToString();

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    Document pdfDoc = new Document(PageSize.A4.Rotate());
                    PdfWriter.GetInstance(pdfDoc, memoryStream).CloseStream = false;

                    pdfDoc.SetMargins(28f, 28f, 28f, 72f);
                    //Create PDF Table

                    //file will created in this path
                    string strAttachment = Server.MapPath("~/PDFs/" + strPDFFileName);

                    pdfDoc.Open();

                    Phrase phrase = new Phrase();

                    ////Table
                    //PdfPTable table = new PdfPTable(1);
                    //table.WidthPercentage = 100;

                    ////Cell no 1
                    //Image header1 = Image.GetInstance(Server.MapPath("~/Content/img/header-2.png"));
                    //header1.ScaleAbsolute(110f, 21f);
                    //header1.Alignment = Image.ALIGN_RIGHT;
                    //PdfPCell cell = new PdfPCell();
                    //cell.Border = 0;
                    //cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    //cell.AddElement(header1);
                    //table.AddCell(cell);

                    ////Add table to document
                    //pdfDoc.Add(table);

                    PdfPTable table = new PdfPTable(2);
                    table.WidthPercentage = 100;
                    table.SpacingAfter = 10f;

                    //add a LEFT LABEL 
                    table.AddCell(new PdfPCell(new Phrase(".", FontFactory.GetFont("Calibri", 1, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });

                    // invoke LEFT image
                    iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Content/img/header-2.png"));
                    jpg.ScaleAbsolute(120f, 30f);
                    PdfPCell imageCell = new PdfPCell(jpg);
                    imageCell.Colspan = 1; // either 1 if you need to insert one cell
                    imageCell.Border = 0;
                    imageCell.HorizontalAlignment = Element.ALIGN_LEFT;

                    //add a RIGHT LABEL 
                    table.AddCell(new PdfPCell(new Phrase(".", FontFactory.GetFont("Calibri", 1, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_RIGHT,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });

                    // invoke RIGHT image
                    iTextSharp.text.Image rightJpg = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Content/img/logo_kmsi.png"));
                    rightJpg.ScaleAbsolute(120f, 40f);
                    PdfPCell rightImageCell = new PdfPCell(rightJpg);
                    rightImageCell.Colspan = 1; // either 1 if you need to insert one cell
                    rightImageCell.Border = 0;
                    rightImageCell.HorizontalAlignment = Element.ALIGN_RIGHT;

                    // add a LEFT image to PdfPTables
                    table.AddCell(imageCell);
                    // add a RIGHT image to PdfPTables
                    table.AddCell(rightImageCell);

                    table.AddCell(new PdfPCell(new Phrase(".", FontFactory.GetFont("Calibri", 2, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });
                    pdfDoc.Add(table);

                    //Horizontal Line
                    LineSeparator line = new LineSeparator(0.0F, 100.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1);
                    pdfDoc.Add(line);

                    //Table
                    table = new PdfPTable(1);
                    table.WidthPercentage = 100;
                    table.SpacingAfter = 10f;

                    table.AddCell(new PdfPCell(new Phrase("PURCHASE PLAN", FontFactory.GetFont("Arial", 20, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });
                    pdfDoc.Add(table);

                    Paragraph para = new Paragraph();
                    phrase = new Phrase("Period Year : " + period, FontFactory.GetFont("Arial", 10, Font.BOLD));
                    para.Add(phrase);
                    pdfDoc.Add(para);

                    DateTime aDay = DateTime.Now;
                    int aYear = aDay.Year;
                    if (period != "")
                    {
                        aYear = Convert.ToInt32(period);
                    }
                    var planLists = from plan in db.PurchasePlans
                                    where plan.Year == aYear && ids.Contains(plan.PartNumber)
                                    group plan by new
                                    {
                                        plan.PartNumber
                                    }
                                    into planlist
                                    select new
                                    {
                                        planlist.Key.PartNumber,
                                        PlanJan = planlist.Where(x => x.Month == 1 && x.Year == aYear).Select(x => x.Plan),
                                        PlanFeb = planlist.Where(x => x.Month == 2 && x.Year == aYear).Select(x => x.Plan),
                                        PlanMar = planlist.Where(x => x.Month == 3 && x.Year == aYear).Select(x => x.Plan),
                                        PlanApr = planlist.Where(x => x.Month == 4 && x.Year == aYear).Select(x => x.Plan),
                                        PlanMay = planlist.Where(x => x.Month == 5 && x.Year == aYear).Select(x => x.Plan),
                                        PlanJun = planlist.Where(x => x.Month == 6 && x.Year == aYear).Select(x => x.Plan),
                                        PlanJul = planlist.Where(x => x.Month == 7 && x.Year == aYear).Select(x => x.Plan),
                                        PlanAgt = planlist.Where(x => x.Month == 8 && x.Year == aYear).Select(x => x.Plan),
                                        PlanSpt = planlist.Where(x => x.Month == 9 && x.Year == aYear).Select(x => x.Plan),
                                        PlanOct = planlist.Where(x => x.Month == 10 && x.Year == aYear).Select(x => x.Plan),
                                        PlanNov = planlist.Where(x => x.Month == 11 && x.Year == aYear).Select(x => x.Plan),
                                        PlanDec = planlist.Where(x => x.Month == 12 && x.Year == aYear).Select(x => x.Plan),
                                        ActJan = (System.Int32?)
                                                    ((from id in db.PurchaseDetails
                                                      where id.PartNumber == planlist.Key.PartNumber && id.PurchaseDate.Value.Month == 1 && id.PurchaseDate.Value.Year == aYear
                                                      group id by new
                                                      {
                                                          id.PartNumber
                                                      } into g
                                                      select new
                                                      {
                                                          Actual = g.Sum(p => p.RequestQuantity)
                                                      }).FirstOrDefault().Actual),
                                        ActFeb = (System.Int32?)
                                                    ((from id in db.PurchaseDetails
                                                      where id.PartNumber == planlist.Key.PartNumber && id.PurchaseDate.Value.Month == 2 && id.PurchaseDate.Value.Year == aYear
                                                      group id by new
                                                      {
                                                          id.PartNumber
                                                      } into g
                                                      select new
                                                      {
                                                          Actual = g.Sum(p => p.RequestQuantity)
                                                      }).FirstOrDefault().Actual),
                                        ActMar = (System.Int32?)
                                                    ((from id in db.PurchaseDetails
                                                      where id.PartNumber == planlist.Key.PartNumber && id.PurchaseDate.Value.Month == 3 && id.PurchaseDate.Value.Year == aYear
                                                      group id by new
                                                      {
                                                          id.PartNumber
                                                      } into g
                                                      select new
                                                      {
                                                          Actual = g.Sum(p => p.RequestQuantity)
                                                      }).FirstOrDefault().Actual),
                                        ActApr = (System.Int32?)
                                                    ((from id in db.PurchaseDetails
                                                      where id.PartNumber == planlist.Key.PartNumber && id.PurchaseDate.Value.Month == 4 && id.PurchaseDate.Value.Year == aYear
                                                      group id by new
                                                      {
                                                          id.PartNumber
                                                      } into g
                                                      select new
                                                      {
                                                          Actual = g.Sum(p => p.RequestQuantity)
                                                      }).FirstOrDefault().Actual),
                                        ActMay = (System.Int32?)
                                                    ((from id in db.PurchaseDetails
                                                      where id.PartNumber == planlist.Key.PartNumber && id.PurchaseDate.Value.Month == 5 && id.PurchaseDate.Value.Year == aYear
                                                      group id by new
                                                      {
                                                          id.PartNumber
                                                      } into g
                                                      select new
                                                      {
                                                          Actual = g.Sum(p => p.RequestQuantity)
                                                      }).FirstOrDefault().Actual),
                                        ActJun = (System.Int32?)
                                                    ((from id in db.PurchaseDetails
                                                      where id.PartNumber == planlist.Key.PartNumber && id.PurchaseDate.Value.Month == 6 && id.PurchaseDate.Value.Year == aYear
                                                      group id by new
                                                      {
                                                          id.PartNumber
                                                      } into g
                                                      select new
                                                      {
                                                          Actual = g.Sum(p => p.RequestQuantity)
                                                      }).FirstOrDefault().Actual),
                                        ActJul = (System.Int32?)
                                                    ((from id in db.PurchaseDetails
                                                      where id.PartNumber == planlist.Key.PartNumber && id.PurchaseDate.Value.Month == 7 && id.PurchaseDate.Value.Year == aYear
                                                      group id by new
                                                      {
                                                          id.PartNumber
                                                      } into g
                                                      select new
                                                      {
                                                          Actual = g.Sum(p => p.RequestQuantity)
                                                      }).FirstOrDefault().Actual),
                                        ActAgt = (System.Int32?)
                                                    ((from id in db.PurchaseDetails
                                                      where id.PartNumber == planlist.Key.PartNumber && id.PurchaseDate.Value.Month == 8 && id.PurchaseDate.Value.Year == aYear
                                                      group id by new
                                                      {
                                                          id.PartNumber
                                                      } into g
                                                      select new
                                                      {
                                                          Actual = g.Sum(p => p.RequestQuantity)
                                                      }).FirstOrDefault().Actual),
                                        ActSpt = (System.Int32?)
                                                    ((from id in db.PurchaseDetails
                                                      where id.PartNumber == planlist.Key.PartNumber && id.PurchaseDate.Value.Month == 9 && id.PurchaseDate.Value.Year == aYear
                                                      group id by new
                                                      {
                                                          id.PartNumber
                                                      } into g
                                                      select new
                                                      {
                                                          Actual = g.Sum(p => p.RequestQuantity)
                                                      }).FirstOrDefault().Actual),
                                        ActOct = (System.Int32?)
                                                    ((from id in db.PurchaseDetails
                                                      where id.PartNumber == planlist.Key.PartNumber && id.PurchaseDate.Value.Month == 10 && id.PurchaseDate.Value.Year == aYear
                                                      group id by new
                                                      {
                                                          id.PartNumber
                                                      } into g
                                                      select new
                                                      {
                                                          Actual = g.Sum(p => p.RequestQuantity)
                                                      }).FirstOrDefault().Actual),
                                        ActNov = (System.Int32?)
                                                    ((from id in db.PurchaseDetails
                                                      where id.PartNumber == planlist.Key.PartNumber && id.PurchaseDate.Value.Month == 11 && id.PurchaseDate.Value.Year == aYear
                                                      group id by new
                                                      {
                                                          id.PartNumber
                                                      } into g
                                                      select new
                                                      {
                                                          Actual = g.Sum(p => p.RequestQuantity)
                                                      }).FirstOrDefault().Actual),
                                        ActDec = (System.Int32?)
                                                    ((from id in db.PurchaseDetails
                                                      where id.PartNumber == planlist.Key.PartNumber && id.PurchaseDate.Value.Month == 12 && id.PurchaseDate.Value.Year == aYear
                                                      group id by new
                                                      {
                                                          id.PartNumber
                                                      } into g
                                                      select new
                                                      {
                                                          Actual = g.Sum(p => p.RequestQuantity)
                                                      }).FirstOrDefault().Actual)
                                    };

                    //Table
                    float[] widths = new float[] { 21f, 70f, 25f, 35f, 25f, 35f, 25f, 35f, 25f, 35f, 25f, 35f, 25f, 35f, 25f, 35f, 25f, 35f, 25f, 35f, 25f, 35f, 25f, 35f, 25f, 35f };
                    table = new PdfPTable(26);
                    table.TotalWidth = 811f;
                    table.LockedWidth = true;
                    table.SetWidths(widths);
                    table.SpacingBefore = 5f;
                    table.HeaderRows = 2;

                    //Cell content
                    //header
                    table.AddCell(new PdfPCell(new Phrase("No.", FontFactory.GetFont("Calibri", 8, Font.BOLD)))
                    {
                        BorderWidthLeft = 1,
                        BorderWidthBottom = 1,
                        BorderWidthRight = 1,
                        BorderWidthTop = 1,
                        PaddingTop = 3,
                        PaddingBottom = 8,
                        Rowspan = 2,
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });
                    table.AddCell(new PdfPCell(new Phrase("Part Number", FontFactory.GetFont("Calibri", 8, Font.BOLD)))
                    {
                        BorderWidthLeft = 1,
                        BorderWidthBottom = 1,
                        BorderWidthRight = 1,
                        BorderWidthTop = 1,
                        PaddingTop = 3,
                        PaddingBottom = 8,
                        Rowspan = 2,
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });

                    Thread.CurrentThread.CurrentCulture = new CultureInfo("id-ID");
                    string[] monthNames = CultureInfo.CurrentCulture.DateTimeFormat.MonthNames;
                    foreach (string m in monthNames)
                    {
                        if (m != "")
                        {
                            table.AddCell(new PdfPCell(new Phrase(m, FontFactory.GetFont("Calibri", 8, Font.BOLD)))
                            {
                                Border = 0,
                                BorderWidthBottom = 1,
                                BorderWidthRight = 1,
                                BorderWidthTop = 1,
                                PaddingTop = 3,
                                PaddingBottom = 8,
                                Colspan = 2,
                                HorizontalAlignment = Element.ALIGN_CENTER,
                                VerticalAlignment = Element.ALIGN_MIDDLE
                            });
                        }
                    }

                    for (int i = 1; i < monthNames.Count(); i++)
                    {
                        table.AddCell(new PdfPCell(new Phrase("Plan", FontFactory.GetFont("Calibri", 8, Font.BOLD)))
                        {
                            Border = 0,
                            BorderWidthBottom = 1,
                            BorderWidthRight = 1,
                            PaddingTop = 3,
                            PaddingBottom = 8,
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });

                        table.AddCell(new PdfPCell(new Phrase("Actual", FontFactory.GetFont("Calibri", 8, Font.BOLD)))
                        {
                            Border = 0,
                            BorderWidthBottom = 1,
                            BorderWidthRight = 1,
                            PaddingTop = 3,
                            PaddingBottom = 8,
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                    }

                    int counter = 1;
                    foreach (var item in planLists)
                    {
                        table.AddCell(new PdfPCell(new Phrase(counter.ToString(), FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.PartNumber, FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });

                        table.AddCell(new PdfPCell(new Phrase((item.PlanJan.FirstOrDefault() != null) ? item.PlanJan.FirstOrDefault().ToString() : "0", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.ActJan != null) ? item.ActJan.ToString() : "0", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.PlanFeb.FirstOrDefault() != null) ? item.PlanFeb.FirstOrDefault().ToString() : "0", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.ActFeb != null) ? item.ActFeb.ToString() : "0", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.PlanMar.FirstOrDefault() != null) ? item.PlanMar.FirstOrDefault().ToString() : "0", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.ActMar != null) ? item.ActMar.ToString() : "0", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.PlanApr.FirstOrDefault() != null) ? item.PlanApr.FirstOrDefault().ToString() : "0", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.ActApr != null) ? item.ActApr.ToString() : "0", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.PlanMay.FirstOrDefault() != null) ? item.PlanMay.FirstOrDefault().ToString() : "0", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.ActMay != null) ? item.ActMay.ToString() : "0", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.PlanJun.FirstOrDefault() != null) ? item.PlanJun.FirstOrDefault().ToString() : "0", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.ActJun != null) ? item.ActJun.ToString() : "0", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.PlanJul.FirstOrDefault() != null) ? item.PlanJul.FirstOrDefault().ToString() : "0", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.ActJul != null) ? item.ActJul.ToString() : "0", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.PlanAgt.FirstOrDefault() != null) ? item.PlanAgt.FirstOrDefault().ToString() : "0", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.ActAgt != null) ? item.ActAgt.ToString() : "0", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.PlanSpt.FirstOrDefault() != null) ? item.PlanSpt.FirstOrDefault().ToString() : "0", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.ActSpt != null) ? item.ActSpt.ToString() : "0", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.PlanOct.FirstOrDefault() != null) ? item.PlanOct.FirstOrDefault().ToString() : "0", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.ActOct != null) ? item.ActOct.ToString() : "0", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.PlanNov.FirstOrDefault() != null) ? item.PlanNov.FirstOrDefault().ToString() : "0", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.ActNov != null) ? item.ActNov.ToString() : "0", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.PlanDec.FirstOrDefault() != null) ? item.PlanDec.FirstOrDefault().ToString() : "0", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.ActDec != null) ? item.ActDec.ToString() : "0", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        counter++;
                    }

                    pdfDoc.Add(table);

                    // Closing the document
                    pdfDoc.Close();

                    var bytes = memoryStream.ToArray();
                    Session[strPDFFileName] = bytes;
                }

                return Json(new { success = true, strPDFFileName }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [HttpGet]
        public virtual ActionResult DownloadPdf(string fileName)
        {
            try
            {
                var ms = Session[fileName] as byte[];
                if (ms == null)
                    return new EmptyResult();
                Session[fileName] = null;
                return File(ms, "application/octet-stream", fileName);
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return HttpNotFound();
            }
        }

        public ActionResult DeletePlan(string partnumber, string year)
        {
            try
            {
                // TODO: Add delete logic here
                int Year = Convert.ToInt32(year);
                var planToRemove = (from p in db.PurchasePlans
                                   where p.PartNumber == partnumber && p.Year == Year
                                   select p).ToList();
                db.PurchasePlans.RemoveRange(planToRemove);
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }
    }
}
