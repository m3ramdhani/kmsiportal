﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using iTextSharp.text;
using iTextSharp.text.pdf;
using KMSIPartsPortal_System.Helpers;
using KMSIPartsPortal_System.Models;

namespace KMSIPartsPortal_System.Controllers
{
    public class WorkOrderInvoiceController : Controller
    {
        private kmsi_portalEntities db = new kmsi_portalEntities();

        // GET: WorkOrderInvoice
        public ActionResult Index()
        {
            if (Session["UserId"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        public ActionResult List()
        {
            try
            {
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();

                // Paging Size
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                // Getting all Work Order Invoice data
                var woInvoiceData = (from wo in db.Invoices
                                     orderby wo.InvoiceDate ascending
                                     select new { wo.InvoiceID, wo.InvoiceDate, wo.InvoiceCode,
                                         wo.UnitSerialNumber, wo.UnitModel, wo.UnitCode,
                                         wo.PartsNumber, wo.CustomerCode, wo.ActualSMR, wo.AmountTotal });

                // Sorting
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    woInvoiceData = woInvoiceData.OrderBy(sortColumn + " " + sortColumnDir);
                }
                // Search
                if (!string.IsNullOrEmpty(searchValue))
                {
                    woInvoiceData = woInvoiceData.Where(wo => wo.InvoiceID.ToString().ToLower().Contains(searchValue.ToLower()) ||
                        wo.InvoiceDate.ToString().ToLower().Contains(searchValue.ToLower()) ||
                        wo.InvoiceCode.ToString().ToLower().Contains(searchValue.ToLower()) ||
                        wo.UnitSerialNumber.ToString().Contains(searchValue.ToLower()) ||
                        wo.UnitModel.ToString().ToLower().Contains(searchValue.ToLower()) ||
                        wo.UnitCode.ToString().ToLower().Contains(searchValue.ToLower()) ||
                        wo.PartsNumber.ToString().ToLower().Contains(searchValue.ToLower()) ||
                        wo.CustomerCode.ToString().ToLower().Contains(searchValue.ToLower()) ||
                        wo.ActualSMR.ToString().ToLower().Contains(searchValue.ToLower()) ||
                        wo.AmountTotal.ToString().ToLower().Contains(searchValue.ToLower())
                    );
                }

                // total number of rows count
                recordsTotal = woInvoiceData.Count();
                // Paging
                var data = woInvoiceData.Skip(skip).Take(pageSize).ToList();

                // Returning Json Data
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

        // GET: WorkOrderInvoice/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Invoice invoice = db.Invoices.Find(id);
            if (invoice == null)
            {
                return HttpNotFound();
            }
            return View(invoice);
        }

        // GET: WorkOrderInvoice/Create
        public ActionResult Create()
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    using (kmsi_portalEntities db = new kmsi_portalEntities())
                    {
                        var getDataInvoice = (from i in db.Invoices
                                              select i.InvoiceCode).Max();

                        string dtYear = DateTime.Now.ToString("yy");
                        ViewBag.invoiceCode = (getDataInvoice != null) ? CodeGenerator.Generate("INV", "/", getDataInvoice) : "INV/" + "00" + dtYear + "/00001";
                        ViewBag.today = DateTime.Now.ToString("dddd, dd MMMM yyyy");

                        return View();
                    }
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return View();
            }
        }

        // POST: WorkOrderInvoice/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Invoice invoice)
        {
            try
            {
                using (kmsi_portalEntities db = new kmsi_portalEntities())
                {
                    if (ModelState.IsValid)
                    {
                        Contract contract = db.Contracts.Where(c => c.ContractCode == invoice.ContractCode && c.UnitSerialNumber == invoice.UnitSerialNumber).FirstOrDefault();

                        invoice.AmountTotal = contract.USDperHours * invoice.ActualSMR * invoice.Kurs;
                        invoice.InvoiceDate = DateTime.Now;
                        db.Invoices.Add(invoice);
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                }
            }
            catch (Exception)
            {
                return View(invoice);
            }

            return View(invoice);
        }

        // GET: WorkOrderInvoice/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Invoice invoice = db.Invoices.Find(id);
            if (invoice == null)
            {
                return HttpNotFound();
            }
            return View(invoice);
        }

        // POST: WorkOrderInvoice/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "InvoiceCode,ContractCode,CustomerCode,PartsNumber,UnitSerialNumber,InvoiceDate,UnitCode,UnitModel,CustomerName,RequestDeliveryDate,ActualSMR,AmountTotal,Kurs,InvoiceDueDate,TaxInvoiceNumber,InvoiceID")] Invoice invoice)
        {
            if (ModelState.IsValid)
            {
                db.Entry(invoice).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(invoice);
        }

        // GET: WorkOrderInvoice/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Invoice invoice = db.Invoices.Find(id);
            if (invoice == null)
            {
                return HttpNotFound();
            }
            return View(invoice);
        }

        // POST: WorkOrderInvoice/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Invoice invoice = db.Invoices.Find(id);
            db.Invoices.Remove(invoice);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult Download(int[] ids)
        {
            try
            {
                using (kmsi_portalEntities db = new kmsi_portalEntities())
                {
                    DateTime dateTime = DateTime.Now;
                    string strCSVFileName = string.Format("INV_" + dateTime.ToString("yyyyMMdd_HHmm") + ".csv");
                    string handle = Guid.NewGuid().ToString();

                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        StreamWriter streamWriter = new StreamWriter(memoryStream);
                        streamWriter.WriteLine("\"InvoiceCode\",\"PartsNumber\",\"ActualSMR\"");

                        var listInvoice = db.Invoices.Where(w => ids.Contains(w.InvoiceID)).OrderBy(w => w.InvoiceCode);

                        foreach (var wo in listInvoice)
                        {
                            streamWriter.WriteLine(string.Format("\"{0}\",\"{1}\",\"{2}\"",
                                wo.InvoiceCode, wo.PartsNumber, wo.ActualSMR));
                        }

                        streamWriter.Flush();
                        memoryStream.Seek(0, SeekOrigin.Begin);
                        memoryStream.Position = 0;
                        TempData[handle] = memoryStream.ToArray();
                    }

                    return Json(new { success = true, FileGuid = handle, FileName = strCSVFileName }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return HttpNotFound();
            }
        }

        [HttpGet]
        public virtual ActionResult DownloadCsv(string fileGuid, string fileName)
        {
            try
            {
                if (TempData[fileGuid] != null)
                {
                    byte[] data = TempData[fileGuid] as byte[];
                    return File(data, "text/plain", fileName);
                }
                else
                {
                    return new EmptyResult();
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return HttpNotFound();
            }
        }

        public ActionResult DownloadPdf(string code)
        {
            try
            {
                MemoryStream memoryStream = new MemoryStream();
                StringBuilder status = new StringBuilder("");
                DateTime dateTime = DateTime.Now;
                //file name to be created
                string strPDFFileName = string.Format("FMCInvoice_" + code + "_" + dateTime.ToString("yyyyMMdd_HHmm") + ".pdf");
                Document pdfDoc = new Document(PageSize.A4);
                pdfDoc.SetMargins(36f, 36f, 36f, 36f);
                //Create PDF Table

                //file will created in this path
                string strAttachment = Server.MapPath("~/PDFs/" + strPDFFileName);

                PdfWriter.GetInstance(pdfDoc, memoryStream).CloseStream = false;
                pdfDoc.Open();

                Phrase phrase = new Phrase();

                //Table
                PdfPTable table = new PdfPTable(2);
                table.WidthPercentage = 100;
                table.SpacingBefore = 0f;
                table.SpacingAfter = 0f;

                //Cell no 1
                Image header1 = Image.GetInstance(Server.MapPath("~/Content/img/header-1.png"));
                header1.ScaleAbsolute(154f, 35f);
                header1.Alignment = Image.ALIGN_LEFT;
                PdfPCell cell = new PdfPCell();
                cell.Border = 0;
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.AddElement(header1);
                table.AddCell(cell);

                //Cell no 2
                Image header2 = Image.GetInstance(Server.MapPath("~/Content/img/header-2.png"));
                header2.ScaleAbsolute(110f, 21f);
                header2.Alignment = Image.ALIGN_RIGHT;
                cell = new PdfPCell();
                cell.Border = 0;
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.AddElement(header2);
                table.AddCell(cell);

                //Add table to document
                pdfDoc.Add(table);

                //Horizontal Line
                Paragraph line = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1)));
                pdfDoc.Add(line);

                //Title
                table = new PdfPTable(1);
                table.WidthPercentage = 100;
                table.SpacingBefore = 10f;

                table.AddCell(new PdfPCell(new Phrase("INVOICE", FontFactory.GetFont("Arial", 20, BaseColor.BLACK)))
                {
                    Border = 0,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    VerticalAlignment = Element.ALIGN_MIDDLE
                });
                pdfDoc.Add(table);

                List<Invoice> invoices = db.Invoices.Where(i => i.InvoiceCode == code).ToList();
                
                if (invoices.Count > 0)
                {
                    Invoice invoice = invoices.FirstOrDefault();
                    Customer customer = db.Customers.Where(c => c.CustomerCode == invoice.CustomerCode).FirstOrDefault();
                    Contract contract = db.Contracts.Where(c => c.ContractCode == invoice.ContractCode && c.UnitSerialNumber == invoice.UnitSerialNumber).FirstOrDefault();

                    //Table
                    float[] widths = new float[] { 30f, 195f, 90f, 65f, 100f };
                    table = new PdfPTable(5);
                    table.TotalWidth = 480f;
                    table.LockedWidth = true;
                    table.SetWidths(widths);
                    table.SpacingBefore = 20f;
                    table.SpacingAfter = 0f;

                    //Cell
                    table.AddCell(new PdfPCell(new Phrase("TO : "))
                    {
                        Border = 0,
                        Rowspan = 6,
                        VerticalAlignment = Element.ALIGN_TOP
                    });
                    table.AddCell(new PdfPCell(new Phrase(customer.CustomerName.ToUpper()))
                    {
                        BorderWidthLeft = 1,
                        BorderWidthTop = 1,
                        BorderWidthRight = 1,
                        BorderWidthBottom = 0,
                        VerticalAlignment = Element.ALIGN_TOP
                    });
                    table.AddCell(new PdfPCell()
                    {
                        Border = 0,
                        Rowspan = 6
                    });
                    table.AddCell(new PdfPCell(new Phrase("NO"))
                    {
                        BorderWidthLeft = 1,
                        BorderWidthTop = 1,
                        BorderWidthRight = 0,
                        BorderWidthBottom = 1,
                        VerticalAlignment = Element.ALIGN_TOP
                    });
                    table.AddCell(new PdfPCell(new Phrase(": " + invoice.InvoiceCode))
                    {
                        BorderWidthLeft = 0,
                        BorderWidthTop = 1,
                        BorderWidthRight = 1,
                        BorderWidthBottom = 1,
                        VerticalAlignment = Element.ALIGN_TOP
                    });

                    table.AddCell(new PdfPCell(new Phrase(customer.Address))
                    {
                        Rowspan = 3,
                        BorderWidthLeft = 1,
                        BorderWidthTop = 0,
                        BorderWidthRight = 1,
                        BorderWidthBottom = 0,
                        VerticalAlignment = Element.ALIGN_TOP
                    });
                    table.AddCell(new PdfPCell(new Phrase("DATE"))
                    {
                        BorderWidthLeft = 1,
                        BorderWidthTop = 0,
                        BorderWidthRight = 0,
                        BorderWidthBottom = 1,
                        VerticalAlignment = Element.ALIGN_TOP
                    });
                    table.AddCell(new PdfPCell(new Phrase(": " + invoice.InvoiceDate.Value.ToString("dd-MMM-yy")))
                    {
                        BorderWidthLeft = 0,
                        BorderWidthTop = 0,
                        BorderWidthRight = 1,
                        BorderWidthBottom = 1,
                        VerticalAlignment = Element.ALIGN_TOP
                    });

                    table.AddCell(new PdfPCell(new Phrase("DUE DATE"))
                    {
                        BorderWidthLeft = 1,
                        BorderWidthTop = 0,
                        BorderWidthRight = 0,
                        BorderWidthBottom = 1,
                        VerticalAlignment = Element.ALIGN_TOP
                    });
                    table.AddCell(new PdfPCell(new Phrase(": " + invoice.InvoiceDueDate.Value.ToString("dd-MMM-yy")))
                    {
                        BorderWidthLeft = 0,
                        BorderWidthTop = 0,
                        BorderWidthRight = 1,
                        BorderWidthBottom = 1,
                        VerticalAlignment = Element.ALIGN_TOP
                    });

                    table.AddCell(new PdfPCell(new Phrase(" "))
                    {
                        Border = 0
                    });

                    table.AddCell(new PdfPCell(new Phrase(" "))
                    {
                        Border = 0
                    });

                    table.AddCell(new PdfPCell(new Phrase("FINANCE DEPT"))
                    {
                        BorderWidthLeft = 1,
                        BorderWidthTop = 0,
                        BorderWidthRight = 1,
                        BorderWidthBottom = 1
                    });

                    table.AddCell(new PdfPCell(new Phrase(" "))
                    {
                        Border = 0
                    });

                    table.AddCell(new PdfPCell(new Phrase(" "))
                    {
                        Border = 0
                    });

                    pdfDoc.Add(table);

                    //Table content
                    float[] Twidths = new float[] { 30f, 230f, 65f, 65f, 110f };
                    table = new PdfPTable(5);
                    table.TotalWidth = 500f;
                    table.LockedWidth = true;
                    table.SetWidths(Twidths);
                    table.WidthPercentage = 100;
                    table.SpacingBefore = 5f;

                    //Cell content
                    //header
                    table.AddCell(new PdfPCell(new Phrase("NO.", FontFactory.GetFont("Arial", 10, Font.UNDERLINE)))
                    {
                        BorderWidthLeft = 1,
                        BorderWidthBottom = 1,
                        BorderWidthRight = 1,
                        BorderWidthTop = 1,
                        PaddingTop = 3,
                        PaddingBottom = 8,
                        Rowspan = 2,
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_TOP
                    });
                    table.AddCell(new PdfPCell(new Phrase("DESCRIPTION", FontFactory.GetFont("Arial", 10, Font.UNDERLINE)))
                    {
                        Border = 0,
                        BorderWidthBottom = 1,
                        BorderWidthRight = 1,
                        BorderWidthTop = 1,
                        PaddingTop = 3,
                        PaddingBottom = 8,
                        Rowspan = 2,
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_TOP
                    });
                    table.AddCell(new PdfPCell(new Phrase("AMOUNT", FontFactory.GetFont("Arial", 10, Font.UNDERLINE)))
                    {
                        Border = 0,
                        BorderWidthBottom = 1,
                        BorderWidthRight = 1,
                        BorderWidthTop = 1,
                        PaddingTop = 3,
                        PaddingBottom = 8,
                        Colspan = 3,
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });

                    table.AddCell(new PdfPCell(new Phrase("USD", FontFactory.GetFont("Arial", 10, Font.UNDERLINE)))
                    {
                        Border = 0,
                        BorderWidthBottom = 1,
                        BorderWidthRight = 1,
                        PaddingTop = 3,
                        PaddingBottom = 8,
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });
                    table.AddCell(new PdfPCell(new Phrase("KURS", FontFactory.GetFont("Arial", 10, Font.UNDERLINE)))
                    {
                        Border = 0,
                        BorderWidthBottom = 1,
                        BorderWidthRight = 1,
                        PaddingTop = 3,
                        PaddingBottom = 8,
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });
                    table.AddCell(new PdfPCell(new Phrase("IDR", FontFactory.GetFont("Arial", 10, Font.UNDERLINE)))
                    {
                        Border = 0,
                        BorderWidthBottom = 1,
                        BorderWidthRight = 1,
                        PaddingTop = 3,
                        PaddingBottom = 8,
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });

                    int counter = 1;
                    int size = invoices.Count();
                    decimal? SubTotalAmount = 0;
                    foreach (var item in invoices)
                    {
                        table.AddCell(new PdfPCell(new Phrase(counter.ToString(), FontFactory.GetFont("Arial", 10)))
                        {
                            Border = 0,
                            BorderWidthLeft = 1,
                            PaddingBottom = (counter == size) ? 8 : 0,
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP
                        });
                        decimal? USDperHours = (contract != null) ? contract.USDperHours : 0;
                        string desc = "Unit Serial Number : " + item.UnitSerialNumber
                            + "\nUnit Code : " + item.UnitCode
                            + "\nUnit Model : " + item.UnitModel
                            + "\nAgreement No. : " + item.ContractCode
                            + "\nActual SMR : (" + item.ActualSMR.ToString() + " Hours) * (" + USDperHours.Value.ToString("C") + ")";
                        table.AddCell(new PdfPCell(new Phrase(desc, FontFactory.GetFont("Arial", 10)))
                        {
                            Border = 0,
                            BorderWidthLeft = 1,
                            PaddingBottom = (counter == size) ? 8 : 0,
                            HorizontalAlignment = Element.ALIGN_LEFT,
                            VerticalAlignment = Element.ALIGN_TOP
                        });
                        decimal? AmountUSD = item.ActualSMR * USDperHours;
                        table.AddCell(new PdfPCell(new Phrase(AmountUSD.Value.ToString("C"), FontFactory.GetFont("Arial", 10)))
                        {
                            Border = 0,
                            BorderWidthLeft = 1,
                            PaddingBottom = (counter == size) ? 8 : 0,
                            HorizontalAlignment = Element.ALIGN_RIGHT,
                            VerticalAlignment = Element.ALIGN_BOTTOM
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.Kurs.Value.ToString("C", CultureInfo.CreateSpecificCulture("id-ID")), FontFactory.GetFont("Arial", 10)))
                        {
                            Border = 0,
                            BorderWidthLeft = 1,
                            PaddingBottom = (counter == size) ? 8 : 0,
                            HorizontalAlignment = Element.ALIGN_RIGHT,
                            VerticalAlignment = Element.ALIGN_BOTTOM
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.AmountTotal.Value.ToString("C", CultureInfo.CreateSpecificCulture("id-ID")), FontFactory.GetFont("Arial", 10)))
                        {
                            Border = 0,
                            BorderWidthRight = 1,
                            BorderWidthLeft = 1,
                            PaddingBottom = (counter == size) ? 8 : 0,
                            HorizontalAlignment = Element.ALIGN_RIGHT,
                            VerticalAlignment = Element.ALIGN_BOTTOM
                        });

                        SubTotalAmount += item.AmountTotal;

                        if (counter == size)
                        {
                            table.AddCell(new PdfPCell(new Phrase(" ", FontFactory.GetFont("Arial", 10)))
                            {
                                Border = 0,
                                BorderWidthLeft = 1,
                                BorderWidthBottom = 1,
                                BorderWidthRight = 1,
                                PaddingBottom = (counter == size) ? 8 : 0,
                                HorizontalAlignment = Element.ALIGN_CENTER,
                                VerticalAlignment = Element.ALIGN_TOP
                            });
                            table.AddCell(new PdfPCell(new Phrase("TAX Invoice No. : " + item.TaxInvoiceNumber, FontFactory.GetFont("Arial", 10)))
                            {
                                Border = 0,
                                BorderWidthBottom = 1,
                                BorderWidthRight = 1,
                                PaddingBottom = (counter == size) ? 8 : 0,
                                HorizontalAlignment = Element.ALIGN_LEFT,
                                VerticalAlignment = Element.ALIGN_TOP
                            });
                            table.AddCell(new PdfPCell(new Phrase(" ", FontFactory.GetFont("Arial", 10)))
                            {
                                Border = 0,
                                BorderWidthBottom = 1,
                                BorderWidthRight = 1,
                                PaddingBottom = (counter == size) ? 8 : 0,
                                HorizontalAlignment = Element.ALIGN_CENTER,
                                VerticalAlignment = Element.ALIGN_TOP
                            });
                            table.AddCell(new PdfPCell(new Phrase(" ", FontFactory.GetFont("Arial", 10)))
                            {
                                Border = 0,
                                BorderWidthBottom = 1,
                                BorderWidthRight = 1,
                                PaddingBottom = (counter == size) ? 8 : 0,
                                HorizontalAlignment = Element.ALIGN_CENTER,
                                VerticalAlignment = Element.ALIGN_TOP
                            });
                            table.AddCell(new PdfPCell(new Phrase(" ", FontFactory.GetFont("Arial", 10)))
                            {
                                Border = 0,
                                BorderWidthBottom = 1,
                                BorderWidthRight = 1,
                                PaddingBottom = (counter == size) ? 8 : 0,
                                HorizontalAlignment = Element.ALIGN_CENTER,
                                VerticalAlignment = Element.ALIGN_TOP
                            });

                            table.AddCell(new PdfPCell(new Phrase("Sub Total", FontFactory.GetFont("Arial", 10)))
                            {
                                Border = 0,
                                BorderWidthLeft = 1,
                                BorderWidthBottom = 1,
                                BorderWidthRight = 1,
                                Colspan = 4,
                                PaddingBottom = (counter == size) ? 8 : 0,
                                HorizontalAlignment = Element.ALIGN_RIGHT,
                                VerticalAlignment = Element.ALIGN_TOP
                            });
                            table.AddCell(new PdfPCell(new Phrase(SubTotalAmount.Value.ToString("C", CultureInfo.CreateSpecificCulture("id-ID")), FontFactory.GetFont("Arial", 10)))
                            {
                                Border = 0,
                                BorderWidthBottom = 1,
                                BorderWidthRight = 1,
                                PaddingBottom = (counter == size) ? 8 : 0,
                                HorizontalAlignment = Element.ALIGN_RIGHT,
                                VerticalAlignment = Element.ALIGN_TOP
                            });

                            table.AddCell(new PdfPCell(new Phrase("VAT 10%", FontFactory.GetFont("Arial", 10)))
                            {
                                Border = 0,
                                BorderWidthLeft = 1,
                                BorderWidthBottom = 1,
                                BorderWidthRight = 1,
                                Colspan = 4,
                                PaddingBottom = (counter == size) ? 8 : 0,
                                HorizontalAlignment = Element.ALIGN_RIGHT,
                                VerticalAlignment = Element.ALIGN_TOP
                            });
                            decimal? Tax = (decimal)0.1 * SubTotalAmount;
                            table.AddCell(new PdfPCell(new Phrase(Tax.Value.ToString("C", CultureInfo.CreateSpecificCulture("id-ID")), FontFactory.GetFont("Arial", 10)))
                            {
                                Border = 0,
                                BorderWidthBottom = 1,
                                BorderWidthRight = 1,
                                PaddingBottom = (counter == size) ? 8 : 0,
                                HorizontalAlignment = Element.ALIGN_RIGHT,
                                VerticalAlignment = Element.ALIGN_TOP
                            });

                            table.AddCell(new PdfPCell(new Phrase("TOTAL", FontFactory.GetFont("Arial", 10, Font.BOLD)))
                            {
                                Border = 0,
                                BorderWidthLeft = 1,
                                BorderWidthBottom = 1,
                                BorderWidthRight = 1,
                                Colspan = 4,
                                PaddingBottom = (counter == size) ? 8 : 0,
                                HorizontalAlignment = Element.ALIGN_RIGHT,
                                VerticalAlignment = Element.ALIGN_TOP
                            });
                            decimal? Total = SubTotalAmount + Tax;
                            table.AddCell(new PdfPCell(new Phrase(Total.Value.ToString("C", CultureInfo.CreateSpecificCulture("id-ID")), FontFactory.GetFont("Arial", 10, Font.BOLD)))
                            {
                                Border = 0,
                                BorderWidthBottom = 1,
                                BorderWidthRight = 1,
                                PaddingBottom = (counter == size) ? 8 : 0,
                                HorizontalAlignment = Element.ALIGN_RIGHT,
                                VerticalAlignment = Element.ALIGN_TOP
                            });
                        }

                        counter++;
                    }

                    pdfDoc.Add(table);

                    //Table footer
                    float[] Fwidths = new float[] { 350f, 100f };
                    table = new PdfPTable(2);
                    table.TotalWidth = 450f;
                    table.LockedWidth = true;
                    table.SetWidths(Fwidths);
                    table.WidthPercentage = 100;
                    table.SpacingBefore = 20f;

                    //Cell footer
                    table.AddCell(new PdfPCell(new Phrase("PLEASE TRANSFER THE ABOVE AMOUNT TO OUR ACCOUNT AT :", FontFactory.GetFont("Arial", 10, Font.UNDERLINE)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_LEFT
                    });
                    table.AddCell(new PdfPCell(new Phrase(" "))
                    {
                        Border = 0
                    });

                    table.AddCell(new PdfPCell(new Phrase("PT KOMATSU MARKETING AND SUPPORT INDONESIA", FontFactory.GetFont("Arial", 10, Font.BOLD)))
                    {
                        Border = 0,
                        PaddingBottom = 20,
                        HorizontalAlignment = Element.ALIGN_LEFT
                    });
                    table.AddCell(new PdfPCell(new Phrase("JAKARTA, " + invoice.InvoiceDate.Value.ToString("dd-MMM-yy"), FontFactory.GetFont("Arial", 10)))
                    {
                        Border = 0,
                        PaddingBottom = 20,
                        HorizontalAlignment = Element.ALIGN_CENTER
                    });

                    table.AddCell(new PdfPCell(new Phrase("BANK OF TOKYO MITSUBISHI UFJ", FontFactory.GetFont("Arial", 10, Font.BOLD)))
                    {
                        Border = 0,
                        Colspan = 2
                    });

                    table.AddCell(new PdfPCell(new Phrase("MID PLAZA BRANCH - JAKARTA", FontFactory.GetFont("Arial", 10)))
                    {
                        Border = 0,
                        Colspan = 2
                    });

                    table.AddCell(new PdfPCell(new Phrase("INDONESIA", FontFactory.GetFont("Arial", 10)))
                    {
                        Border = 0,
                        Colspan = 2
                    });

                    table.AddCell(new PdfPCell(new Phrase("A/C # 665-165401 (IDR)", FontFactory.GetFont("Arial", 10, Font.BOLD)))
                    {
                        Border = 0,
                        Colspan = 2
                    });

                    table.AddCell(new PdfPCell(new Phrase("A/C # 665-911025 (USD)", FontFactory.GetFont("Arial", 10, Font.BOLD)))
                    {
                        Border = 0
                    });
                    table.AddCell(new PdfPCell(new Phrase("TURISNA YUDHA", FontFactory.GetFont("Arial", 10, Font.UNDERLINE)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_CENTER
                    });

                    table.AddCell(new PdfPCell(new Phrase("A/C # 665-911039 (JPY)", FontFactory.GetFont("Arial", 10, Font.BOLD)))
                    {
                        Border = 0
                    });
                    table.AddCell(new PdfPCell(new Phrase("PARTS DEPT", FontFactory.GetFont("Arial", 10)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_CENTER
                    });

                    pdfDoc.Add(table);
                }

                // Closing the document
                pdfDoc.Close();

                byte[] byteInfo = memoryStream.ToArray();
                memoryStream.Write(byteInfo, 0, byteInfo.Length);
                memoryStream.Position = 0;

                return File(memoryStream, "application/pdf", strPDFFileName);
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return RedirectToAction("Index");
            }
        }
    }
}
