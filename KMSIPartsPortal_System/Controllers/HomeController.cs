﻿using KMSIPartsPortal_System.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Dynamic;
using System.Web.Mvc;

namespace KMSIPartsPortal_System.Controllers
{
    public class HomeController : Controller
    {
        private kmsi_portalEntities db = new kmsi_portalEntities();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AnotherLink()
        {
            return View("Index");
        }

        public String GetNotif(int receiver)
        {
            try
            {
                var notificationData = db.Notifications.Where(x => x.ReceiveId == receiver).ToList().OrderBy("ID desc");

                ViewBag.Notifications = notificationData;
                return JsonConvert.SerializeObject(ViewBag);
            }
            catch (DbEntityValidationException ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);

                foreach (var eve in ex.EntityValidationErrors)
                {
                    sw.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sw.WriteLine("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                }
                sw.Close();

                TempData["status"] = false;
                TempData["message"] = "Failed get notification";
                return JsonConvert.SerializeObject(TempData);
            }
        }

        public String SetReadNotif(string ArrNotifId)
        {
            try
            {
                List<Int32> DataArrNotifId = JsonConvert.DeserializeObject<List<Int32>>(ArrNotifId);
                foreach (var id in DataArrNotifId)
                {
                    Notification notification = db.Notifications.Find(id);
                    notification.IsRead = true;
                    db.SaveChanges();
                }

                TempData["status"] = true;
                TempData["message"] = "Success set read";
                return JsonConvert.SerializeObject(TempData);
            }
            catch (DbEntityValidationException ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);

                foreach (var eve in ex.EntityValidationErrors)
                {
                    sw.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sw.WriteLine("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                }
                sw.Close();

                TempData["status"] = false;
                TempData["message"] = "Failed set read notification";
                return JsonConvert.SerializeObject(TempData);
            }
        }
    }
}
