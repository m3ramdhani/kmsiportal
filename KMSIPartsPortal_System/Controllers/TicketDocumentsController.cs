﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Web;
using System.Web.Mvc;
using iTextSharp.text;
using iTextSharp.text.pdf;
using KMSIPartsPortal_System.Helpers;
using KMSIPartsPortal_System.Models;
using OfficeOpenXml;

namespace KMSIPartsPortal_System.Controllers
{
    public class TicketDocumentsController : Controller
    {
        private kmsi_portalEntities db = new kmsi_portalEntities();

        public ActionResult Dashboard() {
            try
            {
                if (Session["UserId"] != null)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return RedirectToAction("Index");
            }
        }

        // GET: TicketDocuments
        public ActionResult Index()
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    Int32 UserID = 0;
                    if (!string.IsNullOrEmpty(Session["UserId"].ToString()))
                        UserID = Convert.ToInt32(Session["UserId"]);

                    String RoleType = "Internal";
                    if (!string.IsNullOrEmpty(Session["RoleType"].ToString()))
                        RoleType = Session["RoleType"].ToString();

                    List<int?> roleIds = new List<int?> { 1, 4, 5, 6, 13, 14 };
                    var user = db.Users
                        .Where(x => roleIds.Contains(x.UserRole) && x.UserId != UserID)
                        .Select(u => new
                        {
                            UserId = u.UserId,
                            Value = u.FirstName + " " + u.LastName
                        })
                        .ToList();
                    SelectList selectsUser = new SelectList(user, "UserId", "Value");
                    ViewBag.UserList = selectsUser;

                    List<Module> modules = db.Modules.Where(x => x.ModuleName == "Helpdesk" || x.ModuleName == "Information").ToList();
                    ViewBag.ModuleInternalLeft = modules;

                    List<TicketDocument> ticket = db.TicketDocuments.ToList();
                    Int32 totalTicket = ticket.Where(x => x.Status != 0).Count();
                    Int32 newTicket = ticket.Where(x => x.Status == 1).Count();
                    Int32 processTicket = ticket.Where(x => x.Status == 2).Count();
                    Int32 solveTicket = ticket.Where(x => x.Status == 3).Count();
                    Int32 closeTicket = ticket.Where(x => x.Status == 4).Count();
                    if (RoleType.Equals("Distributor"))
                    {
                        totalTicket = ticket.Where(x => x.Status != 0 && (x.SubmitBy == UserID || x.DistributorPIC == UserID)).Count();
                        newTicket = ticket.Where(x => x.Status == 1 && (x.SubmitBy == UserID || x.DistributorPIC == UserID)).Count();
                        processTicket = ticket.Where(x => x.Status == 2 && (x.SubmitBy == UserID || x.DistributorPIC == UserID)).Count();
                        solveTicket = ticket.Where(x => x.Status == 3 && (x.SubmitBy == UserID || x.DistributorPIC == UserID)).Count();
                        closeTicket = ticket.Where(x => x.Status == 4 && (x.SubmitBy == UserID || x.DistributorPIC == UserID)).Count();
                    }
                    ViewBag.All = totalTicket;
                    ViewBag.New = newTicket;
                    ViewBag.Processed = processTicket;
                    ViewBag.Solved = solveTicket;
                    ViewBag.Closed = closeTicket;

                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return RedirectToAction("Index");
            }
        }

        public ActionResult List()
        {
            try
            {
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                var filterParam = Request.Form["Filter"];

                //Paging Size
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                //Getting all Ticket data
                var ticketData = db.TicketDocuments.AsEnumerable()
                    .GroupJoin(db.Users, t => t.SubmitBy, u => u.UserId, (t, u) => new { t, u }).SelectMany(x => x.u.DefaultIfEmpty(), (x, sub) => new { x.t, sub })
                    .GroupJoin(db.Users, t2 => t2.t.PickupBy, u2 => u2.UserId, (t2, u2) => new { t2, u2 }).SelectMany(x => x.u2.DefaultIfEmpty(), (x, pick) => new { x.t2, pick })
                    .GroupJoin(db.Users, t3 => t3.t2.t.AssignTo1, u3 => u3.UserId, (t3, u3) => new { t3, u3 }).SelectMany(x => x.u3.DefaultIfEmpty(), (x, as1) => new { x.t3, as1 })
                    .GroupJoin(db.Users, t4 => t4.t3.t2.t.AssignTo2, u4 => u4.UserId, (t4, u4) => new { t4, u4 }).SelectMany(x => x.u4.DefaultIfEmpty(), (x, as2) => new { x.t4, as2 })
                    .GroupJoin(db.Users, t5 => t5.t4.t3.t2.t.UpdatedBy, u5 => u5.UserId, (t5, u5) => new { t5, u5 }).SelectMany(x => x.u5.DefaultIfEmpty(), (x, upd) => new { x.t5, upd })
                    .GroupJoin(db.Users, t6 => t6.t5.t4.t3.t2.t.DistributorPIC, u6 => u6.UserId, (t6, u6) => new { t6, u6 }).SelectMany(x => x.u6.DefaultIfEmpty(), (x, pic) => new { x.t6, pic })
                    .Select(x => new
                    {
                        x.t6.t5.t4.t3.t2.t.TicketID,
                        x.t6.t5.t4.t3.t2.t.TicketNumber,
                        x.t6.t5.t4.t3.t2.t.TicketDate,
                        SubmitDate = x.t6.t5.t4.t3.t2.t.SubmitDate.HasValue ? x.t6.t5.t4.t3.t2.t.SubmitDate.Value.ToString("yyyy-MM-dd") : "",
                        x.t6.t5.t4.t3.t2.t.ProblemCategory,
                        x.t6.t5.t4.t3.t2.t.ProblemType,
                        x.t6.t5.t4.t3.t2.t.Description,
                        x.t6.t5.t4.t3.t2.t.Notes,
                        StatusName = x.t6.t5.t4.t3.t2.t.Status == 1 ? "Submitted" : x.t6.t5.t4.t3.t2.t.Status == 2 ? "Solving" : x.t6.t5.t4.t3.t2.t.Status == 3 ? "Solved" : x.t6.t5.t4.t3.t2.t.Status == 4 ? "Closed" : "Deleted",
                        x.t6.t5.t4.t3.t2.t.Duration,
                        x.t6.t5.t4.t3.t2.t.SubmitBy,
                        x.t6.t5.t4.t3.t2.t.PickupBy,
                        x.t6.t5.t4.t3.t2.t.AssignTo1,
                        x.t6.t5.t4.t3.t2.t.AssignTo2,
                        x.t6.t5.t4.t3.t2.t.Status,
                        x.t6.t5.t4.t3.t2.t.DistributorPIC,
                        PickupDate = x.t6.t5.t4.t3.t2.t.PickupDate.HasValue ? x.t6.t5.t4.t3.t2.t.PickupDate.Value.ToString("yyyy-MM-dd") : "",
                        SolvedDate = x.t6.t5.t4.t3.t2.t.SolvedDate.HasValue ? x.t6.t5.t4.t3.t2.t.SolvedDate.Value.ToString("yyyy-MM-dd") : "",
                        ClosedDate = x.t6.t5.t4.t3.t2.t.ClosedDate.HasValue ? x.t6.t5.t4.t3.t2.t.ClosedDate.Value.ToString("yyyy-MM-dd") : "",
                        LatestPIC = (x.t6.t5.t4.t3.t2.t.Status == 2) ? x.t6.t5.t4.t3.t2.t.UpdatedBy.HasValue ? (x.t6.upd != null) ? x.t6.upd.FirstName + " " + x.t6.upd.LastName : "" : "" : (x.t6.t5.t4.t3.t2.t.Status == 3 || x.t6.t5.t4.t3.t2.t.Status == 4) ? x.t6.t5.t4.t3.t2.t.PickupBy.HasValue ? (x.t6.t5.t4.t3.pick != null) ? x.t6.t5.t4.t3.pick.FirstName + " " + x.t6.t5.t4.t3.pick.LastName : "" : "" : "",
                        DistriPIC = x.t6.t5.t4.t3.t2.t.DistributorPIC.HasValue ? (x.pic != null) ? x.pic.FirstName + " " + x.pic.LastName : "" : ""
                    });

                var RoleType = Request.Form.GetValues("RoleType").FirstOrDefault();
                var UserId = Convert.ToInt32(Session["UserId"]);
                if (RoleType.Equals("Distributor"))
                    ticketData = ticketData.Where(t => t.SubmitBy == UserId || t.DistributorPIC == UserId);

                var status = Request.Form.GetValues("Status").FirstOrDefault();
                if (status != "")
                {
                    if (Convert.ToInt32(status) == 1)
                    {
                        int[] stat = { 1, 2, 3 };
                        ticketData = ticketData.Where(x => stat.Contains(x.Status));
                    }
                    else if (Convert.ToInt32(status) == 0)
                    {
                        ticketData = ticketData.Where(x => x.Status == 4);
                    }
                }
                else
                {
                    int[] stat = { 1, 2, 3 };
                    ticketData = ticketData.Where(x => stat.Contains(x.Status));
                }

                if (filterParam != "")
                    ticketData = ticketData.Where(x => x.Status == Convert.ToInt32(filterParam));

                //Sorting
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    if (sortColumn.Equals("SubmitDate"))
                    {
                        ticketData = ticketData.OrderBy("TicketDate " + sortColumnDir);
                    }
                    else
                    {
                        ticketData = ticketData.OrderBy(sortColumn + " " + sortColumnDir);
                    }
                }
                //Search
                if (!string.IsNullOrEmpty(searchValue))
                {
                    ticketData = ticketData.Where(t => t.TicketNumber.ToLower().Contains(searchValue.ToLower().Trim())
                        || t.SubmitDate.ToLower().Contains(searchValue.ToLower().Trim())
                        || t.ProblemCategory.ToLower().Contains(searchValue.ToLower().Trim())
                        || t.ProblemType.ToLower().Contains(searchValue.ToLower().Trim())
                        || t.Description.ToLower().Contains(searchValue.ToLower().Trim())
                        || (t.Notes != null && t.Notes.ToLower().Contains(searchValue.ToLower().Trim()))
                        || t.StatusName.ToLower().Contains(searchValue.ToLower().Trim())
                        || t.PickupDate.ToLower().Contains(searchValue.ToLower().Trim())
                        || t.SolvedDate.ToLower().Contains(searchValue.ToLower().Trim())
                        || t.ClosedDate.ToLower().Contains(searchValue.ToLower().Trim())
                        || t.LatestPIC.ToLower().Contains(searchValue.ToLower().Trim())
                        || t.DistriPIC.ToLower().Contains(searchValue.ToLower().Trim())
                    );
                }

                //total number of rows count
                recordsTotal = ticketData.Count();
                //Paging
                if (Convert.ToInt32(length) < 0)
                {
                    pageSize = recordsTotal;
                }
                var data = ticketData.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        // GET: TicketDocuments/Details/5
        public ActionResult Details(int? id)
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    TicketDocument ticketDocument = db.TicketDocuments.Find(id);

                    List<Module> modules = db.Modules.Where(x => x.ModuleName == "Helpdesk" || x.ModuleName == "Information").ToList();
                    ViewBag.ModuleInternalLeft = modules;

                    return View(ticketDocument);
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult HistoryDetail(int? id)
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    TicketDocument ticketDocument = db.TicketDocuments.Find(id);

                    List<Module> modules = db.Modules.Where(x => x.ModuleName == "Helpdesk" || x.ModuleName == "Information").ToList();
                    ViewBag.ModuleInternalLeft = modules;

                    return View(ticketDocument);
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        // GET: TicketDocuments/Create
        public ActionResult Create()
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    User user = db.Users.Find(Convert.ToInt32(Session["UserId"]));
                    ViewBag.Name = user.FirstName + " " + user.LastName;

                    if (user.Role.RoleType.TypeName.Equals("Distributor"))
                    {
                        String Prefix = "";
                        if (user.DistributorType != null)
                        {
                            switch (user.DistributorType)
                            {
                                case "UT":
                                    Prefix = "UT";
                                    break;

                                case "UTR":
                                    Prefix = "UTR";
                                    break;

                                case "BP":
                                    Prefix = "BP";
                                    break;

                                case "KG":
                                    Prefix = "KG";
                                    break;

                                default:
                                    Prefix = "";
                                    break;
                            }
                        }
                        var getDataTicket = db.TicketDocuments
                                                .Where(x => x.TicketNumber.Contains(Prefix + "-") && x.SubmitDate.Value.Year == DateTime.Now.Year)
                                                .Max(x => x.TicketNumber);
                        string dtYear = DateTime.Now.ToString("yy");
                        ViewBag.ticketNumber = getDataTicket != null ? StringGenerator.GenerateAutoNumber(Prefix, "-", getDataTicket) : Prefix + "-" + "00" + dtYear + "-00001";
                    }

                    var distributor = db.DistributorTypes.ToList();
                    SelectList selectListsDistributor = new SelectList(distributor, "Value", "Name");
                    var operation = db.ProblemCategories.Where(x => x.CategoryType == "1").ToList();
                    SelectList selectListsOperation = new SelectList(operation, "CategoryName", "CategoryName");
                    var information = db.ProblemCategories.Where(x => x.CategoryType == "2").ToList();
                    SelectList selectListsInformation = new SelectList(information, "CategoryName", "CategoryName");

                    ViewBag.today = DateTime.Now.ToString("d-MMMM-yyyy");
                    ViewBag.DistributorTypeList = selectListsDistributor;
                    ViewBag.OperationList = selectListsOperation;
                    ViewBag.InformationList = selectListsInformation;

                    List<Module> modules = db.Modules.Where(x => x.ModuleName == "Helpdesk" || x.ModuleName == "Information").ToList();
                    ViewBag.ModuleInternalLeft = modules;

                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult getUserDistributor(string type)
        {
            try
            {
                var users = db.Users.Where(x => x.DistributorType.Equals(type)).Select(x => new
                {
                    ID = x.UserId,
                    Text = x.FirstName + " " + x.LastName
                });

                var getDataTicket = db.TicketDocuments
                    .Where(x => x.TicketNumber.Contains(type + "-") && x.SubmitDate.Value.Year == DateTime.Now.Year)
                    .Max(x => x.TicketNumber);
                string dtYear = DateTime.Now.ToString("yy");
                string ticketNumber = getDataTicket != null ? StringGenerator.GenerateAutoNumber(type, "-", getDataTicket) : type + "-" + "00" + dtYear + "-00001";

                return Json(new { picData = users, number = ticketNumber }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        // POST: TicketDocuments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TicketDocument ticketDocument)
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    if (ModelState.IsValid)
                    {
                        TimeZoneInfo serverZone = TimeZoneInfo.FindSystemTimeZoneById("SE Asia Standard Time");
                        DateTime currentDateTime = TimeZoneInfo.ConvertTime(DateTime.UtcNow, serverZone);

                        List<TicketDocument> ticket = db.TicketDocuments.Where(x => x.TicketNumber == ticketDocument.TicketNumber).ToList();
                        if (ticket.Count > 0)
                        {
                            db.TicketDocuments.RemoveRange(ticket);
                            db.SaveChanges();

                            List<TicketAttachment> attachment = db.TicketAttachments.Where(x => x.TicketID == ticketDocument.TicketID).ToList();
                            foreach (var file in attachment)
                            {
                                string dir = Server.MapPath("~/Uploads/Ticket/Images/");
                                if (file.FileType.Equals("DOCUMENT"))
                                    dir = Server.MapPath("~/Uploads/Ticket/Docs/");

                                if (System.IO.File.Exists(Path.Combine(dir + file.FileName)))
                                    System.IO.File.Delete(Path.Combine(dir + file.FileName));
                            }
                            db.TicketAttachments.RemoveRange(attachment);
                            db.SaveChanges();
                        }

                        ticketDocument.TicketDate = currentDateTime;
                        ticketDocument.InformationBy = ticketDocument.InformationBy != null ? ticketDocument.InformationBy : "System";
                        ticketDocument.ProblemCategory = ticketDocument.ProblemCategory != null ? ticketDocument.ProblemCategory : "Operational";
                        var OperationType = Request.Form.GetValues("OperationType");
                        var InformationType = Request.Form.GetValues("InformationType");
                        if (OperationType[0] != "")
                        {
                            ticketDocument.ProblemType = OperationType[0];
                        }
                        else if (InformationType[0] != "")
                        {
                            ticketDocument.ProblemType = InformationType[0];
                        }
                        ticketDocument.Status = 1;
                        ticketDocument.SubmitDate = currentDateTime;
                        ticketDocument.SubmitBy = Convert.ToInt32(Session["UserId"]);
                        db.TicketDocuments.Add(ticketDocument);
                        db.SaveChanges();

                        int TicketID = ticketDocument.TicketID;
                        if (TicketID > 0)
                        {
                            foreach (HttpPostedFileBase img in ticketDocument.TicketImgUpload)
                            {
                                if (img != null)
                                {
                                    string path = Server.MapPath("~/Uploads/Ticket/Images/");
                                    String FileName = FileImport.UploadFileToDir(img, path);

                                    if (!String.IsNullOrEmpty(FileName))
                                    {
                                        TicketAttachment attachment = new TicketAttachment();
                                        attachment.FileName = FileName;
                                        attachment.FileType = "IMAGE";
                                        attachment.FileMode = "TICKET";
                                        attachment.TicketID = TicketID;
                                        db.TicketAttachments.Add(attachment);
                                    }
                                }
                            }

                            foreach (HttpPostedFileBase doc in ticketDocument.TicketDocUpload)
                            {
                                if (doc != null)
                                {
                                    string path = Server.MapPath("~/Uploads/Ticket/Docs/");
                                    String FileName = FileImport.UploadFileToDir(doc, path);

                                    if (!String.IsNullOrEmpty(FileName))
                                    {
                                        TicketAttachment attachment = new TicketAttachment();
                                        attachment.FileName = FileName;
                                        attachment.FileType = "DOCUMENT";
                                        attachment.FileMode = "TICKET";
                                        attachment.TicketID = TicketID;
                                        db.TicketAttachments.Add(attachment);
                                    }
                                }
                            }

                            db.SaveChanges();
                        }

                        // preparing email body
                        int userId = Convert.ToInt32(Session["UserId"]);
                        User user = db.Users.Find(userId);
                        List<int?> roleIds = new List<int?> { 5 };
                        List<User> userLists = db.Users.Where(x => roleIds.Contains(x.UserRole)).ToList();

                        string baseUrl = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                        string url = "Account/Login?returnUrl=%2FTicketDocuments%2FDetails%2F" + ticketDocument.TicketID;
                        string body = "";

                        if (ticketDocument.DistributorPIC.HasValue)
                        {
                            User pic = db.Users.Find(ticketDocument.DistributorPIC.Value);
                            string Requester = pic.FirstName + " " + pic.LastName;
                            body = ViewRenderer.RenderRazorViewToString(this, "~/Views/Mail/Submitted_Ticket.cshtml", new EmailTicket()
                            {
                                TicketNumber = ticketDocument.TicketNumber,
                                Requester = Requester,
                                BaseUrl = baseUrl + url,
                                AssignBy = "",
                                SolvedBy = ""
                            });
                            bool EmailPIC = EmailHelper.SendEmail(pic.Email, "New Ticket Submitted", body, "");
                            if (EmailPIC)
                            {
                                Notification notification = new Notification();
                                notification.UserId = user.UserId;
                                notification.Name = user.FirstName + " " + user.LastName;
                                notification.Module = "HELPDESK";
                                notification.FormNumber = ticketDocument.TicketNumber;
                                notification.Message = "New request ticket " + ticketDocument.TicketNumber + " from " + notification.Name + " has been submitted";
                                notification.Url = baseUrl + "TicketDocuments/Details/" + ticketDocument.TicketID;
                                notification.ReceiveId = pic.UserId;
                                notification.CreatedAt = currentDateTime;
                                db.Notifications.Add(notification);
                                db.SaveChanges();
                            }
                        }
                        else
                        {
                            body = ViewRenderer.RenderRazorViewToString(this, "~/Views/Mail/Submitted_Ticket.cshtml", new EmailTicket()
                            {
                                TicketNumber = ticketDocument.TicketNumber,
                                Requester = ticketDocument.Requester,
                                BaseUrl = baseUrl + url,
                                AssignBy = "",
                                SolvedBy = ""
                            });
                            bool EmailRequester = EmailHelper.SendEmail(user.Email, "New Ticket Submitted", body, "");
                            if (EmailRequester)
                            {
                                Notification notification = new Notification();
                                notification.UserId = user.UserId;
                                notification.Name = user.FirstName + " " + user.LastName;
                                notification.Module = "HELPDESK";
                                notification.FormNumber = ticketDocument.TicketNumber;
                                notification.Message = "New request ticket " + ticketDocument.TicketNumber + " from " + notification.Name + " has been submitted";
                                notification.Url = baseUrl + "TicketDocuments/Details/" + ticketDocument.TicketID;
                                notification.ReceiveId = user.UserId;
                                notification.CreatedAt = currentDateTime;
                                db.Notifications.Add(notification);
                                db.SaveChanges();
                            }
                        }

                        foreach (var data in userLists)
                        {
                            bool emailStatus = EmailHelper.SendEmail(data.Email, "New Ticket Submitted", body, "");

                            if (emailStatus)
                            {
                                Notification notification = new Notification();
                                notification.UserId = user.UserId;
                                notification.Name = user.FirstName + " " + user.LastName;
                                notification.Module = "HELPDESK";
                                notification.FormNumber = ticketDocument.TicketNumber;
                                notification.Message = "New request ticket " + ticketDocument.TicketNumber + " from " + notification.Name + " has been submitted";
                                notification.Url = baseUrl + "TicketDocuments/Details/" + ticketDocument.TicketID;
                                notification.ReceiveId = data.UserId;
                                notification.CreatedAt = currentDateTime;
                                db.Notifications.Add(notification);
                                db.SaveChanges();
                            }
                        }

                        return RedirectToAction("Index");
                    }

                    return RedirectToAction("Create");
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return RedirectToAction("Create");
            }
        }

        // GET: TicketDocuments/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TicketDocument ticketDocument = db.TicketDocuments.Find(id);
            if (ticketDocument == null)
            {
                return HttpNotFound();
            }

            var itemList = db.ProblemCategories.ToList();
            if (ticketDocument.ProblemCategory.ToLower() == "operational")
            {
                itemList = itemList.Where(x => x.CategoryType == "1").ToList();
            }
            else if (ticketDocument.ProblemCategory.ToLower() == "information")
            {
                itemList = itemList.Where(x => x.CategoryType == "2").ToList();
            }
            SelectList selectLists = new SelectList(itemList, "CategoryName", "CategoryName", ticketDocument.ProblemType);
            ViewBag.SelectList = selectLists;

            var operation = db.ProblemCategories.Where(x => x.CategoryType == "1").ToList();
            SelectList selectListsOperation = new SelectList(operation, "CategoryName", "CategoryName");
            var information = db.ProblemCategories.Where(x => x.CategoryType == "2").ToList();
            SelectList selectListsInformation = new SelectList(information, "CategoryName", "CategoryName");
            ViewBag.OperationList = selectListsOperation;
            ViewBag.InformationList = selectListsInformation;

            List<Module> modules = db.Modules.Where(x => x.ModuleName == "Helpdesk" || x.ModuleName == "Information").ToList();
            ViewBag.ModuleInternalLeft = modules;

            return View(ticketDocument);
        }

        // POST: TicketDocuments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(TicketDocument ticketDocument)
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    TimeZoneInfo serverZone = TimeZoneInfo.FindSystemTimeZoneById("SE Asia Standard Time");
                    DateTime currentDateTime = TimeZoneInfo.ConvertTime(DateTime.UtcNow, serverZone);

                    TicketDocument ticket = db.TicketDocuments.Find(ticketDocument.TicketID);

                    ticket.InformationBy = ticketDocument.InformationBy != null ? ticketDocument.InformationBy : "System";
                    ticket.ProblemCategory = ticketDocument.ProblemCategory != null ? ticketDocument.ProblemCategory : "Operational";
                    var ProblemType = Request.Form.GetValues("ProblemType");
                    var OperationType = Request.Form.GetValues("OperationType");
                    var InformationType = Request.Form.GetValues("InformationType");
                    if (ProblemType[0] != "")
                    {
                        ticket.ProblemType = ProblemType[0];
                    }
                    else if (OperationType[0] != "")
                    {
                        ticket.ProblemType = OperationType[0];
                    }
                    else if (InformationType[0] != "")
                    {
                        ticket.ProblemType = InformationType[0];
                    }
                    ticket.Description = ticketDocument.Description;
                    ticket.Notes = ticketDocument.Notes;
                    ticket.Feedback = ticketDocument.Feedback;
                    Double duration = currentDateTime.Subtract(ticket.SubmitDate.Value).TotalSeconds;
                    ticket.Duration = Convert.ToDecimal(duration);
                    ticket.UpdatedAt = currentDateTime;
                    ticket.UpdatedBy = Convert.ToInt32(Session["UserId"]);
                    db.SaveChanges();

                    int TicketID = ticketDocument.TicketID;
                    if (TicketID > 0)
                    {
                        if (ticketDocument.TicketImgUpload != null)
                        {
                            foreach (HttpPostedFileBase img in ticketDocument.TicketImgUpload)
                            {
                                if (img != null)
                                {
                                    string path = Server.MapPath("~/Uploads/Ticket/Images/");
                                    String FileName = FileImport.UploadFileToDir(img, path);

                                    if (!String.IsNullOrEmpty(FileName))
                                    {
                                        TicketAttachment attachment = new TicketAttachment();
                                        attachment.FileName = FileName;
                                        attachment.FileType = "IMAGE";
                                        attachment.FileMode = "TICKET";
                                        attachment.TicketID = TicketID;
                                        db.TicketAttachments.Add(attachment);
                                    }
                                }
                            }
                        }
                        
                        if (ticketDocument.TicketDocUpload != null)
                        {
                            foreach (HttpPostedFileBase doc in ticketDocument.TicketDocUpload)
                            {
                                if (doc != null)
                                {
                                    string path = Server.MapPath("~/Uploads/Ticket/Docs/");
                                    String FileName = FileImport.UploadFileToDir(doc, path);

                                    if (!String.IsNullOrEmpty(FileName))
                                    {
                                        TicketAttachment attachment = new TicketAttachment();
                                        attachment.FileName = FileName;
                                        attachment.FileType = "DOCUMENT";
                                        attachment.FileMode = "TICKET";
                                        attachment.TicketID = TicketID;
                                        db.TicketAttachments.Add(attachment);
                                    }
                                }
                            }
                        }
                        
                        if (ticketDocument.FeedbackImgUpload != null)
                        {
                            foreach (HttpPostedFileBase img in ticketDocument.FeedbackImgUpload)
                            {
                                if (img != null)
                                {
                                    string path = Server.MapPath("~/Uploads/TicketFeedback/Images/");
                                    String FileName = FileImport.UploadFileToDir(img, path);

                                    if (!String.IsNullOrEmpty(FileName))
                                    {
                                        TicketAttachment attachment = new TicketAttachment();
                                        attachment.FileName = FileName;
                                        attachment.FileType = "IMAGE";
                                        attachment.FileMode = "FEEDBACK";
                                        attachment.TicketID = TicketID;
                                        db.TicketAttachments.Add(attachment);
                                    }
                                }
                            }
                        }
                        
                        if (ticketDocument.FeedbackDocUpload != null)
                        {
                            foreach (HttpPostedFileBase doc in ticketDocument.FeedbackDocUpload)
                            {
                                if (doc != null)
                                {
                                    string path = Server.MapPath("~/Uploads/TicketFeedback/Docs/");
                                    String FileName = FileImport.UploadFileToDir(doc, path);

                                    if (!String.IsNullOrEmpty(FileName))
                                    {
                                        TicketAttachment attachment = new TicketAttachment();
                                        attachment.FileName = FileName;
                                        attachment.FileType = "DOCUMENT";
                                        attachment.FileMode = "FEEDBACK";
                                        attachment.TicketID = TicketID;
                                        db.TicketAttachments.Add(attachment);
                                    }
                                }
                            }
                        }

                        db.SaveChanges();
                    }

                    // preparing email body
                    int userId = Convert.ToInt32(Session["UserId"]);
                    User user = db.Users.Find(userId);
                    List<int?> roleIds = new List<int?> { 5 };
                    List<User> userLists = db.Users.Where(x => roleIds.Contains(x.UserRole)).ToList();

                    string baseUrl = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                    string url = "Account/Login?returnUrl=%2FTicketDocuments%2FDetails%2F" + ticketDocument.TicketID;
                    string body = ViewRenderer.RenderRazorViewToString(this, "~/Views/Mail/Edit_Ticket.cshtml", new EmailTicket()
                    {
                        TicketNumber = ticket.TicketNumber,
                        Requester = ticket.Requester,
                        SolvedBy = "",
                        BaseUrl = baseUrl + url,
                        AssignBy = ""
                    });

                    foreach (var data in userLists)
                    {
                        bool emailStatus = EmailHelper.SendEmail(data.Email, "Ticket Updated", body, "");

                        if (emailStatus)
                        {
                            Notification notification = new Notification();
                            notification.UserId = user.UserId;
                            notification.Name = user.FirstName + " " + user.LastName;
                            notification.Module = "HELPDESK";
                            notification.FormNumber = ticketDocument.TicketNumber;
                            notification.Message = "Request ticket " + ticketDocument.TicketNumber + " has been updated by " + notification.Name;
                            notification.Url = baseUrl + "TicketDocuments/Details/" + ticketDocument.TicketID;
                            notification.ReceiveId = data.UserId;
                            notification.CreatedAt = currentDateTime;
                            db.Notifications.Add(notification);
                            db.SaveChanges();
                        }
                    }

                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        [ActionName("Delete")]
        public ActionResult Delete(int[] ids)
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    var tickets = from item in db.TicketDocuments
                                  where ids.Contains(item.TicketID)
                                  select item;
                    tickets.ToList();

                    TimeZoneInfo serverZone = TimeZoneInfo.FindSystemTimeZoneById("SE Asia Standard Time");
                    DateTime currentDateTime = TimeZoneInfo.ConvertTime(DateTime.UtcNow, serverZone);
                    foreach (var t in tickets)
                    {
                        t.Status = 0;
                        DateTime? submitDate = t.SubmitDate;
                        Double duration = currentDateTime.Subtract(submitDate.Value).TotalSeconds;
                        t.Duration = Convert.ToDecimal(duration);
                        t.UpdatedAt = currentDateTime;
                        t.UpdatedBy = Convert.ToInt32(Session["UserId"]);
                    }
                    db.SaveChanges();

                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return RedirectToAction("Index");
            }
        }

        public ActionResult Pickup(int? id)
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    TimeZoneInfo serverZone = TimeZoneInfo.FindSystemTimeZoneById("SE Asia Standard Time");
                    DateTime currentDateTime = TimeZoneInfo.ConvertTime(DateTime.UtcNow, serverZone);

                    TicketDocument ticket = db.TicketDocuments.Find(id);
                    DateTime? submitDate = ticket.SubmitDate;
                    Double duration = currentDateTime.Subtract(submitDate.Value).TotalSeconds;
                    ticket.Status = 2;
                    ticket.PickupDate = currentDateTime;
                    ticket.PickupBy = Convert.ToInt32(Session["UserId"]);
                    ticket.Duration = Convert.ToDecimal(duration);
                    ticket.UpdatedAt = currentDateTime;
                    ticket.UpdatedBy = Convert.ToInt32(Session["UserId"]);

                    db.SaveChanges();

                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SolveTicket(int? id, TicketDocument ticketDocument)
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    TimeZoneInfo serverZone = TimeZoneInfo.FindSystemTimeZoneById("SE Asia Standard Time");
                    DateTime currentDateTime = TimeZoneInfo.ConvertTime(DateTime.UtcNow, serverZone);

                    TicketDocument ticket = db.TicketDocuments.Find(id);
                    Double duration = currentDateTime.Subtract(ticket.SubmitDate.Value).TotalSeconds;
                    ticket.Status = 3;
                    ticket.Feedback = ticketDocument.Feedback;
                    ticket.Duration = Convert.ToDecimal(duration);
                    ticket.SolvedDate = currentDateTime;
                    ticket.SolvedBy = Convert.ToInt32(Session["UserId"]);
                    ticket.UpdatedAt = currentDateTime;
                    ticket.UpdatedBy = Convert.ToInt32(Session["UserId"]);
                    db.SaveChanges();

                    int TicketID = ticket.TicketID;
                    if (TicketID > 0)
                    {
                        foreach (HttpPostedFileBase img in ticketDocument.FeedbackImgUpload)
                        {
                            if (img != null)
                            {
                                string path = Server.MapPath("~/Uploads/TicketFeedback/Images/");
                                String FileName = FileImport.UploadFileToDir(img, path);

                                if (!String.IsNullOrEmpty(FileName))
                                {
                                    TicketAttachment attachment = new TicketAttachment();
                                    attachment.FileName = FileName;
                                    attachment.FileType = "IMAGE";
                                    attachment.FileMode = "FEEDBACK";
                                    attachment.TicketID = TicketID;
                                    db.TicketAttachments.Add(attachment);
                                }
                            }
                        }

                        foreach (HttpPostedFileBase doc in ticketDocument.FeedbackDocUpload)
                        {
                            if (doc != null)
                            {
                                string path = Server.MapPath("~/Uploads/TicketFeedback/Docs/");
                                String FileName = FileImport.UploadFileToDir(doc, path);

                                if (!String.IsNullOrEmpty(FileName))
                                {
                                    TicketAttachment attachment = new TicketAttachment();
                                    attachment.FileName = FileName;
                                    attachment.FileType = "DOCUMENT";
                                    attachment.FileMode = "FEEDBACK";
                                    attachment.TicketID = TicketID;
                                    db.TicketAttachments.Add(attachment);
                                }
                            }
                        }

                        db.SaveChanges();
                    }

                    // preparing email body
                    int userId = Convert.ToInt32(Session["UserId"]);
                    User solver = db.Users.Find(userId);
                    User data = db.Users.Find(ticket.SubmitBy);
                    string baseUrl = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                    string url = "Account/Login?returnUrl=%2FTicketDocuments%2FDetails%2F" + ticket.TicketID;
                    string body = ViewRenderer.RenderRazorViewToString(this, "~/Views/Mail/Solved_Ticket.cshtml", new EmailTicket()
                    {
                        TicketNumber = ticket.TicketNumber,
                        Requester = ticket.Requester,
                        SolvedBy = solver.FirstName + " " + solver.LastName,
                        BaseUrl = baseUrl + url,
                        AssignBy = ""
                    });
                    bool emailStatus = EmailHelper.SendEmail(data.Email, "Ticket Solved", body, "");

                    if (emailStatus)
                    {
                        Notification notification = new Notification();
                        notification.UserId = solver.UserId;
                        notification.Name = solver.FirstName + " " + solver.LastName;
                        notification.Module = "HELPDESK";
                        notification.FormNumber = ticketDocument.TicketNumber;
                        notification.Message = "Request ticket " + ticketDocument.TicketNumber + " has been solved by " + notification.Name;
                        notification.Url = baseUrl + "TicketDocuments/Details/" + ticketDocument.TicketID;
                        notification.ReceiveId = data.UserId;
                        notification.CreatedAt = currentDateTime;
                        db.Notifications.Add(notification);
                        db.SaveChanges();
                    }

                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return RedirectToAction("Index");
            }
        }

        public ActionResult Close(int[] ids)
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    var tickets = from item in db.TicketDocuments
                                  where ids.Contains(item.TicketID)
                                  select item;
                    tickets.ToList();

                    TimeZoneInfo serverZone = TimeZoneInfo.FindSystemTimeZoneById("SE Asia Standard Time");
                    DateTime currentDateTime = TimeZoneInfo.ConvertTime(DateTime.UtcNow, serverZone);
                    foreach (var t in tickets)
                    {
                        t.Status = 4;
                        DateTime? submitDate = t.SubmitDate;
                        Double duration = currentDateTime.Subtract(submitDate.Value).TotalSeconds;
                        t.Duration = Convert.ToDecimal(duration);
                        t.ClosedDate = currentDateTime;
                        t.ClosedBy = Convert.ToInt32(Session["UserId"]);
                        t.UpdatedAt = currentDateTime;
                        t.UpdatedBy = Convert.ToInt32(Session["UserId"]);
                    }
                    db.SaveChanges();

                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return RedirectToAction("Index");
            }
        }

        public ActionResult Assign(int? id)
        {
            try
            {
                if (Session["UserId"]!= null)
                {
                    TimeZoneInfo serverZone = TimeZoneInfo.FindSystemTimeZoneById("SE Asia Standard Time");
                    DateTime currentDateTime = TimeZoneInfo.ConvertTime(DateTime.UtcNow, serverZone);

                    TicketDocument ticket = db.TicketDocuments.Find(id);
                    Double duration = currentDateTime.Subtract(ticket.SubmitDate.Value).TotalSeconds;
                    if (ticket.AssignDate1 == null)
                    {
                        var user = Request.Form.GetValues("UserList");
                        ticket.AssignDate1 = currentDateTime;
                        ticket.AssignTo1 = user[0] != "" ? Convert.ToInt32(user[0]) : 0;
                        ticket.Status = 2;
                        ticket.UpdatedAt = currentDateTime;
                        ticket.UpdatedBy = user[0] != "" ? Convert.ToInt32(user[0]) : 0;
                        ticket.Duration = Convert.ToDecimal(duration);

                        // preparing email body
                        int userId = Convert.ToInt32(Session["UserId"]);
                        User assigner = db.Users.Find(userId);
                        User data = db.Users.Find(ticket.AssignTo1);
                        string baseUrl = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                        string url = "Account/Login?returnUrl=%2FTicketDocuments%2FDetails%2F" + ticket.TicketID;
                        string body = ViewRenderer.RenderRazorViewToString(this, "~/Views/Mail/Assigned_Ticket.cshtml", new EmailTicket()
                        {
                            TicketNumber = ticket.TicketNumber,
                            AssignBy = assigner.FirstName + " " + assigner.LastName,
                            BaseUrl = baseUrl + url,
                            Requester = "",
                            SolvedBy = ""
                        });
                        bool emailStatus = EmailHelper.SendEmail(data.Email, "Assign Ticket", body, "");

                        if (emailStatus)
                        {
                            Notification notification = new Notification();
                            notification.UserId = assigner.UserId;
                            notification.Name = assigner.FirstName + " " + assigner.LastName;
                            notification.Module = "HELPDESK";
                            notification.FormNumber = ticket.TicketNumber;
                            notification.Message = "New request ticket " + ticket.TicketNumber + " has been assigned to you by " + notification.Name;
                            notification.Url = baseUrl + "TicketDocuments/Details/" + ticket.TicketID;
                            notification.ReceiveId = data.UserId;
                            notification.CreatedAt = currentDateTime;
                            db.Notifications.Add(notification);
                            db.SaveChanges();
                        }
                    }
                    else if (ticket.AssignDate2 == null)
                    {
                        var user = Request.Form.GetValues("UserList");
                        ticket.AssignDate2 = DateTime.Now;
                        ticket.AssignTo2 = user[0] != "" ? Convert.ToInt32(user[0]) : 0;
                        ticket.Status = 2;
                        ticket.UpdatedAt = currentDateTime;
                        ticket.UpdatedBy = user[0] != "" ? Convert.ToInt32(user[0]) : 0;
                        ticket.Duration = Convert.ToDecimal(duration);

                        // preparing email body
                        int userId = Convert.ToInt32(Session["UserId"]);
                        User assigner = db.Users.Find(userId);
                        User data = db.Users.Find(ticket.AssignTo2);
                        string baseUrl = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                        string url = "Account/Login?returnUrl=%2FTicketDocuments%2FDetails%2F" + ticket.TicketID;
                        string body = ViewRenderer.RenderRazorViewToString(this, "~/Views/Mail/Assigned_Ticket.cshtml", new EmailTicket()
                        {
                            TicketNumber = ticket.TicketNumber,
                            AssignBy = assigner.FirstName + " " + assigner.LastName,
                            BaseUrl = baseUrl + url,
                            Requester = "",
                            SolvedBy = ""
                        });
                        bool emailStatus = EmailHelper.SendEmail(data.Email, "Assign Ticket", body, "");

                        if (emailStatus)
                        {
                            Notification notification = new Notification();
                            notification.UserId = assigner.UserId;
                            notification.Name = assigner.FirstName + " " + assigner.LastName;
                            notification.Module = "HELPDESK";
                            notification.FormNumber = ticket.TicketNumber;
                            notification.Message = "New request ticket " + ticket.TicketNumber + " has been assigned to you by " + notification.Name;
                            notification.Url = baseUrl + "TicketDocuments/Details/" + ticket.TicketID;
                            notification.ReceiveId = data.UserId;
                            notification.CreatedAt = currentDateTime;
                            db.Notifications.Add(notification);
                            db.SaveChanges();
                        }
                    }
                    db.SaveChanges();

                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return RedirectToAction("Index");
            }
        }

        [HttpGet]
        public ActionResult DownloadAttachment(string file)
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    string path = "/Uploads/Ticket/" + file;
                    string content = MimeMapping.GetMimeMapping(file);
                    return File(path, content, file);
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return RedirectToAction("Index");
            }
        }

        public ActionResult History()
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    List<Module> modules = db.Modules.Where(x => x.ModuleName == "Helpdesk" || x.ModuleName == "Information").ToList();
                    ViewBag.ModuleInternalLeft = modules;

                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return RedirectToAction("OpenHistory");
            }
        }

        public ActionResult Download(int[] ids, string status)
        {
            try
            {
                using (kmsi_portalEntities db = new kmsi_portalEntities())
                {
                    DateTime dateTime = DateTime.Now;
                    string strCSVFileName = string.Format(status + "TicketHistory_" + dateTime.ToString("yyyyMMdd_HHmm") + ".csv");
                    string handle = Guid.NewGuid().ToString();

                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        StreamWriter streamWriter = new StreamWriter(memoryStream);
                        streamWriter.WriteLine("\"Ticket Number\",\"Problem Category\",\"Problem Type\",\"Description\",\"Notes\",\"Status\",\"Submit Date\",\"Submit By\",\"Duration\",\"Feedback\"");

                        var listTickets = db.TicketDocuments.Where(t => ids.Contains(t.TicketID)).OrderBy(t => t.TicketNumber);

                        var Status = string.Empty;
                        foreach (var item in listTickets)
                        {
                            if (item.Status == 1)
                            {
                                Status = "Submitted";
                            } else if (item.Status == 2)
                            {
                                Status = "Solving";
                            } else if (item.Status == 3)
                            {
                                Status = "Solved";
                            } else if (item.Status == 4)
                            {
                                Status = "Closed";
                            }

                            User user = db.Users.Find(item.SubmitBy);
                            string Name = user.FirstName + " " + user.LastName;

                            string Duration = string.Empty;
                            if (item.Duration != null)
                            {
                                TimeSpan time = TimeSpan.FromSeconds(Convert.ToDouble(item.Duration));

                                if (time.Days > 0)
                                {
                                    Duration = time.ToString(@"d\d\,\ hh\:mm\:ss");
                                } else
                                {
                                    Duration = time.ToString(@"hh\:mm\:ss");
                                }
                            }
                            streamWriter.WriteLine(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\"",
                                item.TicketNumber, item.ProblemCategory, item.ProblemType, item.Description, item.Notes, Status, item.SubmitDate.Value.ToString("yyyy-MM-dd"), Name, Duration, item.Feedback));
                        }

                        streamWriter.Flush();
                        memoryStream.Seek(0, SeekOrigin.Begin);
                        memoryStream.Position = 0;
                        TempData[handle] = memoryStream.ToArray();
                    }

                    return Json(new { success = true, FileGuid = handle, FileName = strCSVFileName }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return HttpNotFound();
            }
        }

        [HttpGet]
        public virtual ActionResult DownloadCsv(string fileGuid, string fileName)
        {
            try
            {
                if (TempData[fileGuid] != null)
                {
                    byte[] data = TempData[fileGuid] as byte[];
                    return File(data, "text/plain", fileName);
                }
                else
                {
                    return new EmptyResult();
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return HttpNotFound();
            }
        }

        public ActionResult GenerateXls()
        {
            try
            {
                DateTime dateTime = DateTime.Now;
                string FileName = string.Format("Helpdesk_" + dateTime.ToString("yyyyMMdd_HHmm") + ".xlsx");
                string handle = Guid.NewGuid().ToString();

                using (MemoryStream stream = new MemoryStream())
                {
                    using (ExcelPackage excel = new ExcelPackage())
                    {
                        var ticketData = from t in db.TicketDocuments
                                          from u in db.Users
                                             .Where(x => x.UserId == t.SubmitBy).DefaultIfEmpty()

                                          from u2 in db.Users
                                             .Where(x => x.UserId == t.PickupBy).DefaultIfEmpty()

                                          from u3 in db.Users
                                             .Where(x => x.UserId == t.AssignTo1).DefaultIfEmpty()

                                          from u4 in db.Users
                                             .Where(x => x.UserId == t.AssignTo2).DefaultIfEmpty()
                                          select new
                                          {
                                              t.TicketID,
                                              t.TicketNumber,
                                              t.ProblemCategory,
                                              t.ProblemType,
                                              t.Description,
                                              t.Notes,
                                              t.Status,
                                              t.SubmitDate,
                                              t.SubmitBy,
                                              t.Duration,
                                              t.Feedback,
                                              SubmittedBy = u.FirstName + " " + u.LastName,
                                              PickupName = t.PickupBy.HasValue ? u2.FirstName + " " + u2.LastName : "",
                                              AssignTo = t.AssignTo2.HasValue ? u4.FirstName + " " + u4.LastName : t.AssignTo1.HasValue ? u3.FirstName + " " + u3.LastName : "",
                                              t.PickupBy,
                                              t.AssignTo1,
                                              t.AssignTo2
                                          };

                        var RoleId = Convert.ToInt32(Request.Form.GetValues("RoleId").FirstOrDefault());
                        var UserId = Convert.ToInt32(Session["UserId"]);
                        if (RoleId == 3)
                            ticketData = ticketData.Where(t => t.SubmitBy == UserId);

                        string sheetName = String.Empty;
                        var status = Request.Form.GetValues("Status").FirstOrDefault();
                        if (status != "")
                        {
                            if (Convert.ToInt32(status) == 1)
                            {
                                int[] stat = { 1, 2, 3 };
                                ticketData = ticketData.Where(x => stat.Contains(x.Status));
                                sheetName = "Outstanding_Ticket";
                            }
                            else if (Convert.ToInt32(status) == 0)
                            {
                                ticketData = ticketData.Where(x => x.Status == 4);
                                sheetName = "Closed_Ticket";
                            }
                        }
                        else
                        {
                            int[] stat = { 1, 2, 3 };
                            ticketData = ticketData.Where(x => stat.Contains(x.Status));
                            sheetName = "Outstanding_Ticket";
                        }

                        excel.Workbook.Worksheets.Add(sheetName);

                        var headerRow = new List<string[]>()
                        {
                            new string[] { "Ticket Number", "Created Date", "Problem Category", "Problem Type", "Description", "Notes", "Status", "Duration" }
                        };

                        string headerRange = "A3:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "3";

                        var worksheet = excel.Workbook.Worksheets[sheetName];
                        worksheet.Cells[1, 1, 1, headerRow[0].Length].Merge = true;
                        worksheet.Cells["A1"].Value = "TICKET LIST";
                        worksheet.Cells["A1"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        worksheet.Cells["A1"].Style.Font.Bold = true;
                        worksheet.Cells["A1"].Style.Font.Size = 14;

                        worksheet.Cells[headerRange].LoadFromArrays(headerRow);
                        worksheet.Cells[headerRange].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        worksheet.Cells[headerRange].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                        worksheet.Cells[headerRange].Style.Font.Bold = true;
                        worksheet.Cells[headerRange].Style.Font.Size = 10;
                        worksheet.Cells[headerRange].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                        int rowNumber = 4;
                        foreach (var data in ticketData)
                        {
                            worksheet.Cells[rowNumber, 1].Value = data.TicketNumber;
                            worksheet.Cells[rowNumber, 2].Value = data.SubmitDate.HasValue ? data.SubmitDate.Value.ToString("yyyy-MM-dd") : String.Empty;
                            worksheet.Cells[rowNumber, 3].Value = data.ProblemCategory;
                            worksheet.Cells[rowNumber, 4].Value = data.ProblemType;
                            worksheet.Cells[rowNumber, 5].Value = data.Description;
                            worksheet.Cells[rowNumber, 6].Value = data.Notes;

                            string ticketStatus = String.Empty;
                            if (data.Status == 1)
                            {
                                ticketStatus = "Submitted";
                            }
                            else if (data.Status == 2)
                            {
                                ticketStatus = "Solving";
                            }
                            else if (data.Status == 3)
                            {
                                ticketStatus = "Solved";
                            }
                            else if (data.Status == 4)
                            {
                                ticketStatus = "Closed";
                            }
                            worksheet.Cells[rowNumber, 7].Value = ticketStatus;

                            string Duration = string.Empty;
                            if (data.Duration != null)
                            {
                                TimeSpan time = TimeSpan.FromSeconds(Convert.ToDouble(data.Duration));

                                if (time.Days > 0)
                                {
                                    Duration = time.ToString(@"d\d\,\ hh\:mm\:ss");
                                }
                                else
                                {
                                    Duration = time.ToString(@"hh\:mm\:ss");
                                }
                            }
                            worksheet.Cells[rowNumber, 8].Value = Duration;

                            worksheet.Cells[rowNumber, 1, rowNumber, 8].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                            worksheet.Cells[rowNumber, 1, rowNumber, 8].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                            worksheet.Cells[rowNumber, 1, rowNumber, 8].Style.Font.Size = 8;
                            worksheet.Cells[rowNumber, 1, rowNumber, 8].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                            rowNumber++;
                        }

                        worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();

                        excel.SaveAs(stream);
                    }
                    stream.Position = 0;
                    TempData[handle] = stream.ToArray();
                }

                return new JsonResult()
                {
                    Data = new { FileGuid = handle, FileName = FileName }
                };
            }
            catch (DbEntityValidationException ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);

                foreach (var eve in ex.EntityValidationErrors)
                {
                    sw.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sw.WriteLine("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                }
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult GeneratePdf()
        {
            try
            {
                DateTime dateTime = DateTime.Now;
                string FileName = string.Format("Helpdesk_" + dateTime.ToString("yyyyMMdd_HHmm") + ".pdf");

                using (MemoryStream stream = new MemoryStream())
                {
                    var ticketData = from t in db.TicketDocuments
                                     from u in db.Users
                                        .Where(x => x.UserId == t.SubmitBy).DefaultIfEmpty()

                                     from u2 in db.Users
                                        .Where(x => x.UserId == t.PickupBy).DefaultIfEmpty()

                                     from u3 in db.Users
                                        .Where(x => x.UserId == t.AssignTo1).DefaultIfEmpty()

                                     from u4 in db.Users
                                        .Where(x => x.UserId == t.AssignTo2).DefaultIfEmpty()
                                     select new
                                     {
                                         t.TicketID,
                                         t.TicketNumber,
                                         t.ProblemCategory,
                                         t.ProblemType,
                                         t.Description,
                                         t.Notes,
                                         t.Status,
                                         t.SubmitDate,
                                         t.SubmitBy,
                                         t.Duration,
                                         t.Feedback,
                                         SubmittedBy = u.FirstName + " " + u.LastName,
                                         PickupName = t.PickupBy.HasValue ? u2.FirstName + " " + u2.LastName : "",
                                         AssignTo = t.AssignTo2.HasValue ? u4.FirstName + " " + u4.LastName : t.AssignTo1.HasValue ? u3.FirstName + " " + u3.LastName : "",
                                         t.PickupBy,
                                         t.AssignTo1,
                                         t.AssignTo2
                                     };

                    var RoleId = Convert.ToInt32(Request.Form.GetValues("RoleId").FirstOrDefault());
                    var UserId = Convert.ToInt32(Session["UserId"]);
                    if (RoleId == 3)
                        ticketData = ticketData.Where(t => t.SubmitBy == UserId);

                    var status = Request.Form.GetValues("Status").FirstOrDefault();
                    if (status != "")
                    {
                        if (Convert.ToInt32(status) == 1)
                        {
                            int[] stat = { 1, 2, 3 };
                            ticketData = ticketData.Where(x => stat.Contains(x.Status));
                        }
                        else if (Convert.ToInt32(status) == 0)
                        {
                            ticketData = ticketData.Where(x => x.Status == 4);
                        }
                    }
                    else
                    {
                        int[] stat = { 1, 2, 3 };
                        ticketData = ticketData.Where(x => stat.Contains(x.Status));
                    }

                    Document pdfDoc = new Document(PageSize.A4.Rotate());
                    PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);
                    writer.CloseStream = false;
                    pdfDoc.SetMargins(20f, 20f, 20f, 20f);

                    pdfDoc.Open();
                    Phrase phrase = new Phrase();

                    Font fontCell = new Font(FontFactory.GetFont("Trebuchet MS", 8, Font.NORMAL));
                    Font fontHeader = new Font(FontFactory.GetFont("Trebuchet MS", 10, Font.BOLD));

                    PdfPTable table = new PdfPTable(1);
                    table.WidthPercentage = 100;
                    table.SpacingAfter = 10f;

                    Image jpg = Image.GetInstance(Server.MapPath("~/Content/img/logo-komatsu.png"));
                    jpg.ScalePercent(50);
                    PdfPCell imageCell = new PdfPCell(jpg);
                    imageCell.Colspan = 1;
                    imageCell.Border = 0;
                    imageCell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(imageCell);

                    table.AddCell(new PdfPCell(new Phrase("TICKET LIST", FontFactory.GetFont("Trebuchet MS", 20, Font.BOLD, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });
                    pdfDoc.Add(table);

                    string[] headerRow = new string[] { "Ticket Number", "Created Date", "Problem Category", "Problem Type", "Description", "Notes", "Status", "Duration" };
                    int headerCount = headerRow.Count();

                    table = new PdfPTable(headerCount);
                    table.WidthPercentage = 100;
                    table.SpacingBefore = 5f;
                    table.HeaderRows = 1;
                    for (int i = 0; i < headerRow.Count(); i++)
                    {
                        table.AddCell(new PdfPCell(new Phrase(headerRow[i], FontFactory.GetFont("Trebuchet MS", 10)))
                        {
                            PaddingTop = 3,
                            PaddingBottom = 8,
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                    }

                    foreach (var data in ticketData)
                    {
                        addCell(table, data.TicketNumber);
                        addCell(table, data.SubmitDate.HasValue ? data.SubmitDate.Value.ToString("yyyy-MM-dd") : String.Empty);
                        addCell(table, data.ProblemCategory);
                        addCell(table, data.ProblemType);
                        addCell(table, data.Description);
                        addCell(table, data.Notes);

                        string ticketStatus = String.Empty;
                        if (data.Status == 1)
                        {
                            ticketStatus = "Submitted";
                        }
                        else if (data.Status == 2)
                        {
                            ticketStatus = "Solving";
                        }
                        else if (data.Status == 3)
                        {
                            ticketStatus = "Solved";
                        }
                        else if (data.Status == 4)
                        {
                            ticketStatus = "Closed";
                        }
                        addCell(table, ticketStatus);

                        string Duration = string.Empty;
                        if (data.Duration != null)
                        {
                            TimeSpan time = TimeSpan.FromSeconds(Convert.ToDouble(data.Duration));

                            if (time.Days > 0)
                            {
                                Duration = time.ToString(@"d\d\,\ hh\:mm\:ss");
                            }
                            else
                            {
                                Duration = time.ToString(@"hh\:mm\:ss");
                            }
                        }
                        addCell(table, Duration);
                    }

                    pdfDoc.Add(table);

                    pdfDoc.Close();

                    var bytes = stream.ToArray();
                    Session[FileName] = bytes;
                }

                return Json(new { success = true, FileName }, JsonRequestBehavior.AllowGet);
            }
            catch (DbEntityValidationException ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);

                foreach (var eve in ex.EntityValidationErrors)
                {
                    sw.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sw.WriteLine("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                }
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [HttpGet]
        public virtual ActionResult DownloadXls(string fileGuid, string fileName)
        {
            if (TempData[fileGuid] != null)
            {
                byte[] data = TempData[fileGuid] as byte[];
                return File(data, "application/vnd.ms-excel", fileName);
            }
            else
            {
                return new EmptyResult();
            }
        }

        [HttpGet]
        public virtual ActionResult DownloadPdf(string fileName)
        {
            try
            {
                var ms = Session[fileName] as byte[];
                if (ms == null)
                    return new EmptyResult();
                Session[fileName] = null;
                return File(ms, "application/pdf", fileName);
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return HttpNotFound();
            }
        }

        private static void addCell(PdfPTable table, string text)
        {
            Font fontCell = new Font(FontFactory.GetFont("Trebuchet MS", 8, Font.NORMAL));
            PdfPCell cell = new PdfPCell(new Phrase(text, fontCell));

            cell.PaddingTop = 3;
            cell.PaddingBottom = 8;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;

            table.AddCell(cell);
        }
    }
}
