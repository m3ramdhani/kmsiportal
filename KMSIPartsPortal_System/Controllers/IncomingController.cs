﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Web;
using System.Web.Mvc;
using iTextSharp.text;
using iTextSharp.text.pdf;
using KMSIPartsPortal_System.Helpers;
using KMSIPartsPortal_System.Models;
using OfficeOpenXml;

namespace KMSIPartsPortal_System.Controllers
{
    public class IncomingController : Controller
    {
        private kmsi_portalEntities db = new kmsi_portalEntities();

        /* start of Forecast */
        public ActionResult Forecast()
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    List<MenuModels> MenuMaster = (List<MenuModels>)Session["MenuMaster"];
                    Int32 RoleID = Convert.ToInt32(Session["RoleId"]);
                    
                    if (RoleID == 0)
                    {
                        ViewBag.IsInput = 1;
                        ViewBag.IsUpdate = 1;
                        ViewBag.IsDelete = 1;
                        ViewBag.IsUpload = 1;
                        ViewBag.IsDownload = 1;
                    }
                    else
                    {
                        var access = MenuMaster.Where(x => x.ControllerName == "Incoming" && x.ActionName == "Forecast" && x.RoleId == RoleID).FirstOrDefault();

                        if (access.IsView == 1)
                        {
                            ViewBag.IsInput = access.IsInput;
                            ViewBag.IsUpdate = access.IsUpdate;
                            ViewBag.IsDelete = access.IsDelete;
                            ViewBag.IsUpload = access.IsUpload;
                            ViewBag.IsDownload = access.IsDownload;
                        }
                        else
                        {
                            ViewBag.Message = "You don't have access to this page! Please contact administrator.";
                        }
                    }
                    
                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult ForecastList()
        {
            try
            {
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                var status = Request.Form.GetValues("status").FirstOrDefault();

                // Paging Size
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                Int32 IsValid = Convert.ToInt32(status);

                // Getting all Purchase data
                var forecastData = db.ForecastLists.AsEnumerable()
                                .Join(db.Users, f => f.CreatedBy, u => u.UserId, (f, u) => new
                                {
                                    ID = f.ID,
                                    ForecastNumber = f.ForecastNumber,
                                    ForecastDate = f.ForecastDate.HasValue ? f.ForecastDate.Value.ToString("yyyy-MM-dd") : "",
                                    Requester = u.FirstName + " " + u.LastName,
                                    Remarks = f.Remarks,
                                    IsValid = f.IsValid
                                })
                                .GroupBy(x => x.ForecastNumber)
                                .Select(group => group.First());

                if (IsValid == 1)
                {
                    forecastData = forecastData.Where(x => x.IsValid == IsValid);
                }
                else
                {
                    forecastData = forecastData.Where(x => x.IsValid != 1);
                }

                // Sorting
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    forecastData = forecastData.OrderBy(sortColumn + " " + sortColumnDir);
                }
                // Search
                if (!string.IsNullOrEmpty(searchValue))
                {
                    forecastData = forecastData.Where(p => p.ForecastNumber.ToLower().Contains(searchValue.ToLower())
                        || p.ForecastDate.ToLower().Contains(searchValue.ToLower())
                        || p.Requester.ToLower().Contains(searchValue.ToLower())
                        || p.Remarks.ToString().ToLower().Contains(searchValue.ToLower()));
                }

                // total number of rows count
                recordsTotal = forecastData.Count();
                // Paging
                var data = forecastData.Skip(skip).Take(pageSize).ToList();
                // Returning Json Data
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [HandleError]
        public ActionResult UploadForecast(HttpPostedFileBase file)
        {
            if (Session["UserId"] != null)
            {
                TempData["ActionMessage"] = "Upload Failed";
                try
                {
                    if (file != null)
                    {
                        if (file.ContentType == "application/vnd.ms-excel" || file.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                        {
                            var fileName = file.FileName;
                            var targetPath = Server.MapPath("~/Uploads/");
                            file.SaveAs(targetPath + fileName);
                            var pathToExcelFile = targetPath + fileName;
                            string FileName = Path.GetFileName(file.FileName);
                            string Extension = Path.GetExtension(file.FileName);
                            DataTable dataTable = FileImport.ImportToGrid(pathToExcelFile, Extension, "Yes");
                            foreach (DataRow row in dataTable.Rows)
                            {
                                try
                                {
                                    string PartNumber = row["PART NO"].ToString().Trim();
                                    var PartExist = db.MasterParts.FirstOrDefault(x => x.PartNumber == PartNumber);
                                    if (PartExist != null)
                                    {
                                        string forecastNumber = row["FORECAST NO"].ToString().Trim();
                                        string forecastDateFormat = row["FORECAST DATE (YYYYMMDD)"].ToString().Trim();
                                        DateTime? ForecastDate = null;
                                        if (forecastDateFormat != "")
                                        {
                                            string day = forecastDateFormat.Substring(6, 2);
                                            string month = forecastDateFormat.Substring(4, 2);
                                            string year = forecastDateFormat.Substring(0, 4);
                                            string forecastDate = year + '-' + month + '-' + day;
                                            ForecastDate = Convert.ToDateTime(forecastDate);
                                        }
                                        int quantity = Convert.ToInt32(row["QUANTITY"]);
                                        var ForecastExist = db.ForecastLists.FirstOrDefault(x => x.ForecastNumber == forecastNumber && x.ForecastDate == ForecastDate && x.PartNumber == PartNumber && x.Quantity == quantity);
                                        if (ForecastExist != null)
                                        {
                                            ForecastExist.Quantity += quantity;
                                            db.SaveChanges();

                                            TempData["ActionMessage"] = "Upload Success";
                                        }
                                        else
                                        {
                                            ForecastList forecast = new ForecastList();
                                            forecast.ForecastNumber = forecastNumber;
                                            forecast.PartNumber = PartNumber;
                                            forecast.Quantity = Convert.ToInt32(row["QUANTITY"]);
                                            forecast.ForecastDate = ForecastDate;
                                            forecast.Remarks = row["REMARKS"].ToString();
                                            forecast.CreatedBy = Convert.ToInt32(Session["UserId"]);
                                            forecast.CreatedAt = DateTime.Now;
                                            db.ForecastLists.Add(forecast);
                                            db.SaveChanges();

                                            TempData["ActionMessage"] = "Upload Success";
                                        }
                                    }

                                    if (System.IO.File.Exists(pathToExcelFile))
                                    {
                                        System.IO.File.Delete(pathToExcelFile);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                                    var sw = new System.IO.StreamWriter(filename, true);
                                    sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                                    sw.Close();
                                }
                            }
                        }
                    }
                    else
                    {
                        var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                        var sw = new System.IO.StreamWriter(filename, true);
                        sw.WriteLine(DateTime.Now.ToString() + " " + "File Not Selected!");
                        sw.Close();
                    }
                }
                catch (Exception ex)
                {
                    var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                    var sw = new System.IO.StreamWriter(filename, true);
                    sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                    sw.Close();
                }

                return RedirectToAction("Forecast");
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        public ActionResult GenerateXlsForecast(int[] ids)
        {
            DateTime dateTime = DateTime.Now;
            string FileName = string.Format("ForecastPO_" + dateTime.ToString("yyyyMMdd_HHmm") + ".xlsx");

            string handle = Guid.NewGuid().ToString();

            using (MemoryStream memoryStream = new MemoryStream())
            {
                using (ExcelPackage excel = new ExcelPackage())
                {
                    excel.Workbook.Worksheets.Add("Forecast");

                    var headerRow = new List<string[]>()
                    {
                        new string[] { "Supplier Code", "Type PO", "Part Number", "PO Quantity", "Stock Point", "Remark" }
                    };

                    string headerRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";

                    var worksheet = excel.Workbook.Worksheets["Forecast"];

                    worksheet.Cells[headerRange].LoadFromArrays(headerRow);
                    worksheet.Cells[headerRange].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[headerRange].Style.Font.Bold = true;
                    worksheet.Cells[headerRange].Style.Font.Size = 12;
                    worksheet.Cells[headerRange].AutoFitColumns();

                    var forecastData = from f in db.IncomingForecasts
                                       where ids.Contains(f.ID)
                                       select new
                                       {
                                           f.SupplierCode,
                                           f.TypePO,
                                           f.PartNumber,
                                           f.ForecastQty,
                                           f.StockPoint,
                                           f.Remark
                                       };

                    int rowNumber = 2;
                    foreach (var data in forecastData)
                    {
                        worksheet.Cells[rowNumber, 1].Value = data.SupplierCode;
                        worksheet.Cells[rowNumber, 2].Value = data.TypePO;
                        worksheet.Cells[rowNumber, 3].Value = data.PartNumber;
                        worksheet.Cells[rowNumber, 4].Value = data.ForecastQty;
                        worksheet.Cells[rowNumber, 5].Value = data.StockPoint;
                        worksheet.Cells[rowNumber, 6].Value = data.Remark;

                        rowNumber++;
                    }

                    excel.SaveAs(memoryStream);
                }
                memoryStream.Position = 0;
                TempData[handle] = memoryStream.ToArray();
            }

            // Note we are returning a filename as well as the handle
            return new JsonResult()
            {
                Data = new { FileGuid = handle, FileName = FileName }
            };
        }

        public ActionResult GeneratePdfForecast(int[] ids)
        {
            try
            {
                DateTime dateTime = DateTime.Now;
                string strPDFFileName = string.Format("ForecastPO_" + dateTime.ToString("yyyyMMdd_HHmm") + ".pdf");
                string handle = Guid.NewGuid().ToString();

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    Document pdfDoc = new Document(PageSize.A4.Rotate());
                    PdfWriter.GetInstance(pdfDoc, memoryStream).CloseStream = false;

                    pdfDoc.SetMargins(28f, 28f, 28f, 28f);
                    //Create PDF Table

                    //file will created in this path
                    string strAttachment = Server.MapPath("~/PDFs/" + strPDFFileName);

                    pdfDoc.Open();

                    Phrase phrase = new Phrase();

                    //Table
                    PdfPTable table = new PdfPTable(1);
                    table.WidthPercentage = 100;
                    table.SpacingAfter = 10f;


                    //add a LEFT LABEL 
                    table.AddCell(new PdfPCell(new Phrase("K-PINTAR PARTS PORTAL SYSTEM", FontFactory.GetFont("Calibri", 7, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });

                    // invoke LEFT image
                    iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Content/img/logo_kmsi.png"));
                    jpg.ScaleAbsolute(120f, 33f);
                    PdfPCell imageCell = new PdfPCell(jpg);
                    imageCell.Colspan = 2; // either 1 if you need to insert one cell
                    imageCell.Border = 0;
                    imageCell.HorizontalAlignment = Element.ALIGN_LEFT;

                    //add a RIGHT LABEL 
                    table.AddCell(new PdfPCell(new Phrase("VENDOR STOCK SYSTEM", FontFactory.GetFont("Calibri", 7, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_RIGHT,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });

                    // invoke RIGHT image
                    iTextSharp.text.Image rightJpg = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Content/img/logo-komatsu.png"));
                    rightJpg.ScaleAbsolute(120f, 40f);
                    PdfPCell rightImageCell = new PdfPCell(rightJpg);
                    rightImageCell.Colspan = 2; // either 1 if you need to insert one cell
                    rightImageCell.Border = 0;
                    rightImageCell.HorizontalAlignment = Element.ALIGN_RIGHT;

                    // add a LEFT image to PdfPTables
                    table.AddCell(imageCell);
                    // add a RIGHT image to PdfPTables
                    //table.AddCell(rightImageCell);



                    table.AddCell(new PdfPCell(new Phrase("FORECAST", FontFactory.GetFont("Calibri", 20, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });
                    pdfDoc.Add(table);

                    var forecastData = from f in db.IncomingForecasts
                                       where ids.Contains(f.ID)
                                       select new
                                       {
                                           f.SupplierCode,
                                           f.TypePO,
                                           f.PartNumber,
                                           f.ForecastQty,
                                           f.StockPoint,
                                           f.Remark
                                       };

                    //Table
                    float[] widths = new float[] { 70f, 70f, 70f, 70f, 70f, 280f };
                    table = new PdfPTable(6);
                    table.TotalWidth = 630f;
                    table.LockedWidth = true;
                    table.SetWidths(widths);
                    table.SpacingBefore = 5f;
                    table.HeaderRows = 1;

                    //Cell content
                    //header
                    string[] headers = new string[] { "Supplier Code", "Type PO", "Part Number", "PO Quantity", "Stock Point", "Remark" };
                    for (int i = 0; i < headers.Count(); i++)
                    {
                        table.AddCell(new PdfPCell(new Phrase(headers[i], FontFactory.GetFont("Calibri", 10)))
                        {
                            PaddingTop = 3,
                            PaddingBottom = 8,
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                    }

                    foreach (var item in forecastData)
                    {
                        table.AddCell(new PdfPCell(new Phrase(item.SupplierCode, FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.TypePO != null) ? item.TypePO : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.PartNumber != null) ? item.PartNumber : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.ForecastQty != null) ? item.ForecastQty.ToString() : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.StockPoint != null) ? item.StockPoint : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.Remark != null) ? item.Remark : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                    }

                    pdfDoc.Add(table);

                    //Closing the document
                    pdfDoc.Close();

                    var bytes = memoryStream.ToArray();
                    Session[strPDFFileName] = bytes;
                }

                return Json(new { success = true, strPDFFileName }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }
        /* end of Forecast */

        /* start of Incoming Purchase */
        public ActionResult Purchase()
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    List<MenuModels> MenuMaster = (List<MenuModels>)Session["MenuMaster"];
                    Int32 RoleID = Convert.ToInt32(Session["RoleId"]);

                    if (RoleID == 0)
                    {
                        ViewBag.IsInput = 1;
                        ViewBag.IsUpdate = 1;
                        ViewBag.IsDelete = 1;
                        ViewBag.IsUpload = 1;
                        ViewBag.IsDownload = 1;
                    }
                    else
                    {
                        var access = MenuMaster.Where(x => x.ControllerName == "Incoming" && x.ActionName == "Purchase" && x.RoleId == RoleID).FirstOrDefault();

                        if (access.IsView == 1)
                        {
                            ViewBag.IsInput = access.IsInput;
                            ViewBag.IsUpdate = access.IsUpdate;
                            ViewBag.IsDelete = access.IsDelete;
                            ViewBag.IsUpload = access.IsUpload;
                            ViewBag.IsDownload = access.IsDownload;
                        }
                        else
                        {
                            ViewBag.Message = "You don't have access to this page! Please contact administrator.";
                        }
                    }

                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }


        public ActionResult PurchaseListReport()
        {
            try
            {
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                var status = Request.Form.GetValues("status").FirstOrDefault();

                // Paging Size
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                Int32 IsCompleted = Convert.ToInt32(status);

                // Getting all Purchase data
                var purchaseData = (from po in db.PurchaseLists
                                    join u in db.Users on new { By = po.CreatedBy } equals new { By = (System.Int32?)u.UserId }
                                    orderby po.PurchaseDate descending
                                    group po by new { po.PurchaseNumber, po.PurchaseDate, po.ItemCode, po.ForecastNumber, po.ItemName, u.FirstName, u.LastName } into poGroup
                                    select new
                                    {
                                        PurchaseNumber = poGroup.Key.PurchaseNumber,
                                        ForecastNumber = poGroup.Key.ForecastNumber,
                                        PurchaseDate = poGroup.Key.PurchaseDate,
                                        Item = poGroup.Key.ItemCode,
                                        ItemName = poGroup.Key.ItemName,
                                        OrderQty = (System.Int32?)
                                           (from id in db.PurchaseLists
                                            where id.PurchaseNumber == poGroup.Key.PurchaseNumber && id.ItemCode == poGroup.Key.ItemCode
                                            select new
                                            {
                                                OrderQty = id.OrderQuantity
                                            }).FirstOrDefault().OrderQty,
                                        PackingQty = (System.Int32?)
                                           (from id in db.IncomingLists
                                            where id.PurchaseNumber == poGroup.Key.PurchaseNumber
                                            group id by new
                                            {
                                                id.PurchaseNumber

                                            } into g
                                            select new
                                            {
                                                PackingQty = g.Sum(i => i.Quantity)
                                            }).FirstOrDefault().PackingQty ?? 0,
                                        FirstName = poGroup.Key.FirstName,
                                        LastName = poGroup.Key.LastName
                                    }).ToList().Select(x => new
                                    {
                                        PurchaseNumber = x.PurchaseNumber,
                                        ForecastNumber = x.ForecastNumber,
                                        PurchaseDate = (string)x.PurchaseDate.Value.ToString("yyyy-MM-dd"),
                                        Item = x.Item,
                                        ItemName = x.ItemName,
                                        OrderQty = x.OrderQty,
                                        PackingQty = x.PackingQty,
                                        Requester = x.FirstName + " " + x.LastName
                                    });

                if (IsCompleted == 1)
                {
                    purchaseData = purchaseData.Where(x => x.PackingQty >= x.OrderQty);
                }
                else if (IsCompleted == 0)
                {
                    purchaseData = purchaseData.Where(x => x.PackingQty < x.OrderQty);
                }

                // Sorting
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    purchaseData = purchaseData.OrderBy(sortColumn + " " + sortColumnDir);
                }
                // Search
                if (!string.IsNullOrEmpty(searchValue))
                {
                    purchaseData = purchaseData.Where(p => p.PurchaseNumber.ToLower().Contains(searchValue.ToLower())
                        || p.PurchaseDate.ToLower().Contains(searchValue.ToLower())
                        //|| p.Item.ToLower().Contains(searchValue.ToLower())
                        //|| p.ItemName.ToLower().Contains(searchValue.ToLower())
                        || p.OrderQty.ToString().ToLower().Contains(searchValue.ToLower())
                        || p.ForecastNumber.ToString().ToLower().Contains(searchValue.ToLower())
                        || p.Requester.ToLower().Contains(searchValue.ToLower()));
                }

                // total number of rows count
                recordsTotal = purchaseData.Count();
                // Paging
                var data = purchaseData.Skip(skip).Take(pageSize).ToList();
                // Returning Json Data
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }


        public ActionResult PurchaseList()
        {
            try
            {
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                var status = Request.Form.GetValues("status").FirstOrDefault();

                // Paging Size
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                Int32 IsCompleted = Convert.ToInt32(status);

                // Getting all Purchase data
                var purchaseData = (from po in db.PurchaseLists
                                    join u in db.Users on new { By = po.CreatedBy } equals new { By = (System.Int32?)u.UserId }
                                    orderby po.PurchaseDate descending
                                    group po by new { po.ItemCode, po.PurchaseNumber, po.PurchaseDate, po.ForecastNumber, u.FirstName, u.LastName } into poGroup
                                    //group po by new { po.PurchaseNumber, po.PurchaseDate, po.ItemCode, po.ForecastNumber, po.ItemName, u.FirstName, u.LastName } into poGroup
                                    select new
                                    {
                                        PurchaseNumber = poGroup.Key.PurchaseNumber,
                                        PartNumber = poGroup.Key.ItemCode,
                                        ForecastNumber = poGroup.Key.ForecastNumber,
                                        PurchaseDate = poGroup.Key.PurchaseDate,
                                        //Item = poGroup.Key.ItemCode,
                                        //ItemName = poGroup.Key.ItemName,
                                        OrderQty = (System.Int32?)
                                           (from id in db.PurchaseLists
                                            where id.PurchaseNumber == poGroup.Key.PurchaseNumber && id.ItemCode == poGroup.Key.ItemCode
                                            //where id.PurchaseNumber == poGroup.Key.PurchaseNumber && id.ItemCode == poGroup.Key.ItemCode
                                            select new
                                            {
                                                OrderQty = id.OrderQuantity
                                            }).FirstOrDefault().OrderQty,
                                        PackingQty = (System.Int32?)
                                           (from id in db.IncomingLists
                                            where id.PurchaseNumber == poGroup.Key.PurchaseNumber 
                                            group id by new
                                            {
                                                id.PurchaseNumber
                                            
                                            } into g
                                            select new
                                            {
                                                PackingQty = g.Sum(i => i.Quantity)
                                            }).FirstOrDefault().PackingQty ?? 0,
                                        FirstName = poGroup.Key.FirstName,
                                        LastName = poGroup.Key.LastName
                                    }).ToList().Select(x => new
                                    {
                                        PurchaseNumber = x.PurchaseNumber,
                                        ForecastNumber = x.ForecastNumber,
                                        PartNumber = x.PartNumber,
                                        PurchaseDate = (string) x.PurchaseDate.Value.ToString("yyyy-MM-dd"),
                                        //Item = x.Item,
                                        //ItemName = x.ItemName,
                                        OrderQty = x.OrderQty,
                                        PackingQty = x.PackingQty,
                                        Requester = x.FirstName + " " + x.LastName
                                    });

                if (IsCompleted == 1)
                {
                    purchaseData = purchaseData.Where(x => x.PackingQty >= x.OrderQty);
                }
                else if (IsCompleted == 0)
                {
                    purchaseData = purchaseData.Where(x => x.PackingQty < x.OrderQty);
                }

                // Sorting
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    purchaseData = purchaseData.OrderBy(sortColumn + " " + sortColumnDir);
                }
                // Search
                if (!string.IsNullOrEmpty(searchValue))
                {
                    purchaseData = purchaseData.Where(p => p.PurchaseNumber.ToLower().Contains(searchValue.ToLower())
                        || p.PurchaseDate.ToLower().Contains(searchValue.ToLower())
                        || p.PartNumber.ToLower().Contains(searchValue.ToLower())
                        //|| p.ItemName.ToLower().Contains(searchValue.ToLower())
                        || p.OrderQty.ToString().ToLower().Contains(searchValue.ToLower())
                        || p.ForecastNumber.ToString().ToLower().Contains(searchValue.ToLower())
                        || p.Requester.ToLower().Contains(searchValue.ToLower()));
                }

                // total number of rows count
                recordsTotal = purchaseData.Count();
                // Paging
                var data = purchaseData.Skip(skip).Take(pageSize).ToList();
                // Returning Json Data
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [HandleError]
        public ActionResult UploadPurchase(HttpPostedFileBase[] file)
        {
            if (Session["UserId"] != null)
            {
                TempData["ActionMessage"] = "Upload Failed";
                try
                {
                    foreach (HttpPostedFileBase f in file)
                    {
                        if (f != null)
                        {
                            if (f.ContentType == "application/vnd.ms-excel" || f.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                            {
                                var fileName = f.FileName;
                                var targetPath = Server.MapPath("~/Uploads/");
                                f.SaveAs(targetPath + fileName);
                                var pathToExcelFile = targetPath + fileName;
                                string FileName = Path.GetFileName(f.FileName);
                                string Extension = Path.GetExtension(f.FileName);
                                DataTable dataTable = FileImport.ImportToGrid(pathToExcelFile, Extension, "Yes");
                                foreach (DataRow row in dataTable.Rows)
                                {
                                    try
                                    {
                                        string forecastNumber = row["FORECAST NO"].ToString().Trim();
                                        string PurchaseNumber = row["SALES NO"].ToString().Trim();
                                        string poDateFormat = row["SALES DATE (YYYYMMDD)"].ToString().Trim();
                                        DateTime? PurchaseDate = null;
                                        if (poDateFormat != "")
                                        {
                                            string day = poDateFormat.Substring(6, 2);
                                            string month = poDateFormat.Substring(4, 2);
                                            string year = poDateFormat.Substring(0, 4);
                                            string purchaseDate = year + '-' + month + '-' + day;
                                            PurchaseDate = Convert.ToDateTime(purchaseDate);
                                        }
                                        string PartNumber = row["PART NO"].ToString().Trim();
                                        string PartName = row["PART NAME"].ToString().Trim();
                                        int Qty = Convert.ToInt32(row["QUANTITY"]);
                                        string Unit = row["UNIT"].ToString().Trim();

                                        var PartExist = db.MasterParts.Where(x => x.PartNumber.Equals(PartNumber));
                                        if (PartExist != null)
                                        {
                                            if (Unit.ToUpper().Equals("CM") || Unit.ToUpper().Equals("PCS"))
                                            {
                                                PurchaseList PurchaseExist = db.PurchaseLists
                                                    .FirstOrDefault(x => x.ForecastNumber == forecastNumber
                                                        && x.PurchaseNumber == PurchaseNumber
                                                        && x.PurchaseDate == PurchaseDate
                                                        && x.ItemCode == PartNumber
                                                        && x.ItemName == PartName
                                                        && x.OrderQuantity == Qty
                                                        && x.Unit == Unit);
                                                if (PurchaseExist != null)
                                                {
                                                    PurchaseExist.OrderQuantity += Qty;
                                                    db.SaveChanges();

                                                    TempData["ActionMessage"] = "Upload Success";
                                                }
                                                else
                                                {
                                                    PurchaseList purchase = new PurchaseList();
                                                    purchase.PurchaseNumber = PurchaseNumber;
                                                    purchase.ForecastNumber = forecastNumber;
                                                    purchase.ItemCode = PartNumber;
                                                    purchase.OrderQuantity = Qty;
                                                    purchase.PurchaseDate = PurchaseDate;
                                                    purchase.ItemName = PartName;
                                                    purchase.Unit = Unit;
                                                    purchase.CreatedBy = Convert.ToInt32(Session["UserId"]);
                                                    purchase.CreatedAt = DateTime.Now;
                                                    db.PurchaseLists.Add(purchase);
                                                    db.SaveChanges();

                                                    TempData["ActionMessage"] = "Upload Success";
                                                }
                                            }
                                        }

                                        if (System.IO.File.Exists(pathToExcelFile))
                                        {
                                            System.IO.File.Delete(pathToExcelFile);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                                        var sw = new System.IO.StreamWriter(filename, true);
                                        sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                                        sw.Close();
                                    }
                                }
                            }
                        }
                        else
                        {
                            var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                            var sw = new System.IO.StreamWriter(filename, true);
                            sw.WriteLine(DateTime.Now.ToString() + " " + "File Not Selected!");
                            sw.Close();
                        }
                    }
                }
                catch (Exception ex)
                {
                    var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                    var sw = new System.IO.StreamWriter(filename, true);
                    sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                    sw.Close();
                }

                return RedirectToAction("Purchase");
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        public ActionResult GenerateXlsPurchase(string[] ids, string type="")
        {
            DateTime dateTime = DateTime.Now;
            string FileName = string.Format(type + " SALES ORDER_" + dateTime.ToString("yyyyMMdd_HHmm") + ".xlsx");
            if (type == "REPORT")
            {
                FileName = string.Format("SUPPLIER RAW MATERIAL REPORT_" + dateTime.ToString("yyyyMMdd_HHmm") + ".xlsx");
            }

            string handle = Guid.NewGuid().ToString();

            using (MemoryStream memoryStream = new MemoryStream())
            {
                using (ExcelPackage excel = new ExcelPackage())
                {
                    excel.Workbook.Worksheets.Add(type + " Sales Order");

                    var headerRow = new List<string[]>()
                    {
                        new string[] { "Sales No.", "Sales Date", "Part Number", "Part Name", "Qty", "Unit", "Forecast Number" }
                    };

                    string headerRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";

                    var worksheet = excel.Workbook.Worksheets[type + " Sales Order"];

                    worksheet.Cells[headerRange].LoadFromArrays(headerRow);
                    worksheet.Cells[headerRange].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[headerRange].Style.Font.Bold = true;
                    worksheet.Cells[headerRange].Style.Font.Size = 12;
                    worksheet.Cells[headerRange].AutoFitColumns();

                    var purchaseData = (from po in db.PurchaseLists
                                        where ids.Contains(po.PurchaseNumber)
                                        join u in db.Users on new { By = po.CreatedBy } equals new { By = (System.Int32?)u.UserId }
                                        orderby po.PurchaseDate descending
                                        group po by new { po.PurchaseNumber, po.PurchaseDate, po.ForecastNumber, po.ItemCode, po.ItemName, po.Unit, u.FirstName, u.LastName } into poGroup
                                        select new
                                        {
                                            PurchaseNumber = poGroup.Key.PurchaseNumber,
                                            ForecastNumber = poGroup.Key.ForecastNumber,
                                            PurchaseDate = poGroup.Key.PurchaseDate,
                                            Item = poGroup.Key.ItemCode,
                                            ItemName = poGroup.Key.ItemName,
                                            Unit = poGroup.Key.Unit,
                                            OrderQty = (System.Int32?)
                                               (from id in db.PurchaseLists
                                                where id.PurchaseNumber == poGroup.Key.PurchaseNumber && id.ItemCode == poGroup.Key.ItemCode
                                                select new
                                                {
                                                    OrderQty = id.OrderQuantity
                                                }).FirstOrDefault().OrderQty
                                        }).ToList().Select(x => new
                                        {
                                            PurchaseNumber = x.PurchaseNumber != null ? x.PurchaseNumber : "",
                                            ForecastNumber = x.ForecastNumber != null ? x.ForecastNumber : "",
                                            PurchaseDate = x.PurchaseDate.HasValue ? x.PurchaseDate.Value.ToString("yyyy-MM-dd") : "",
                                            Item = x.Item != null ? x.Item : "",
                                            ItemName = x.ItemName != null ? x.ItemName : "",
                                            OrderQty = x.OrderQty != null ? x.OrderQty.ToString() : "",
                                            Unit = x.Unit != null ? x.Unit.ToString() : ""

                                        });

                    int rowNumber = 2;
                    foreach (var data in purchaseData)
                    {
                        worksheet.Cells[rowNumber, 1].Value = data.PurchaseNumber;
                        worksheet.Cells[rowNumber, 2].Style.Numberformat.Format = "yyyy-mm-dd";
                        worksheet.Cells[rowNumber, 2].Value = data.PurchaseDate;
                        worksheet.Cells[rowNumber, 3].Value = data.Item;
                        worksheet.Cells[rowNumber, 4].Value = data.ItemName;
                        worksheet.Cells[rowNumber, 5].Value = data.OrderQty;
                        worksheet.Cells[rowNumber, 6].Value = data.Unit;
                        worksheet.Cells[rowNumber, 7].Value = data.ForecastNumber;

                        rowNumber++;
                    }

                    excel.SaveAs(memoryStream);
                }
                memoryStream.Position = 0;
                TempData[handle] = memoryStream.ToArray();
            }

            // Note we are returning a filename as well as the handle
            return new JsonResult()
            {
                Data = new { FileGuid = handle, FileName = FileName }
            };
        }

        public ActionResult GeneratePdfPurchase(string[] ids, string type="")
        {
            try
            {
                DateTime dateTime = DateTime.Now;
                string strPDFFileName = string.Format(type + " SALES ORDER_" + dateTime.ToString("yyyyMMdd_HHmm") + ".pdf");
                string titlePdf = type + " SALES ORDER";
                if (type == "REPORT")
                {
                    strPDFFileName = string.Format("SUPPLIER RAW MATERIAL INFORMATION_" + dateTime.ToString("yyyyMMdd_HHmm") + ".pdf");
                    titlePdf = "SUPPLIER RAW MATERIAL INFORMATION";
                }
                string handle = Guid.NewGuid().ToString();

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    Document pdfDoc = new Document(PageSize.A4.Rotate());
                    PdfWriter.GetInstance(pdfDoc, memoryStream).CloseStream = false;

                    pdfDoc.SetMargins(28f, 28f, 28f, 28f);
                    //Create PDF Table

                    //file will created in this path
                    string strAttachment = Server.MapPath("~/PDFs/" + strPDFFileName);

                    pdfDoc.Open();

                    Phrase phrase = new Phrase();

                    //Table
                    PdfPTable table = new PdfPTable(1);
                    table.WidthPercentage = 100;
                    table.SpacingAfter = 10f;


                    //add a LEFT LABEL 
                    table.AddCell(new PdfPCell(new Phrase("K-PINTAR PARTS PORTAL SYSTEM", FontFactory.GetFont("Calibri", 7, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });

                    // invoke LEFT image
                    iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Content/img/logo_kmsi.png"));
                    jpg.ScaleAbsolute(120f, 33f);
                    PdfPCell imageCell = new PdfPCell(jpg);
                    imageCell.Colspan = 2; // either 1 if you need to insert one cell
                    imageCell.Border = 0;
                    imageCell.HorizontalAlignment = Element.ALIGN_LEFT;

                    //add a RIGHT LABEL 
                    table.AddCell(new PdfPCell(new Phrase("VENDOR STOCK SYSTEM", FontFactory.GetFont("Calibri", 7, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_RIGHT,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });

                    // invoke RIGHT image
                    iTextSharp.text.Image rightJpg = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Content/img/logo-komatsu.png"));
                    rightJpg.ScaleAbsolute(120f, 40f);
                    PdfPCell rightImageCell = new PdfPCell(rightJpg);
                    rightImageCell.Colspan = 2; // either 1 if you need to insert one cell
                    rightImageCell.Border = 0;
                    rightImageCell.HorizontalAlignment = Element.ALIGN_RIGHT;

                    // add a LEFT image to PdfPTables
                    table.AddCell(imageCell);
                    // add a RIGHT image to PdfPTables
                    //table.AddCell(rightImageCell);


                    table.AddCell(new PdfPCell(new Phrase(titlePdf, FontFactory.GetFont("Calibri", 20, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });
                    pdfDoc.Add(table);

                    var purchaseData = (from po in db.PurchaseLists
                                        where ids.Contains(po.PurchaseNumber)
                                        join u in db.Users on new { By = po.CreatedBy } equals new { By = (System.Int32?)u.UserId }
                                        orderby po.PurchaseDate descending
                                        group po by new { po.PurchaseNumber, po.PurchaseDate, po.ForecastNumber, po.ItemCode, po.ItemName, po.Unit, u.FirstName, u.LastName } into poGroup
                                        select new
                                        {
                                            PurchaseNumber = poGroup.Key.PurchaseNumber,
                                            ForecastNumber = poGroup.Key.ForecastNumber,
                                            PurchaseDate = poGroup.Key.PurchaseDate,
                                            Item = poGroup.Key.ItemCode,
                                            ItemName = poGroup.Key.ItemName,
                                            Unit = poGroup.Key.Unit,
                                            OrderQty = (System.Int32?)
                                               (from id in db.PurchaseLists
                                                where id.PurchaseNumber == poGroup.Key.PurchaseNumber && id.ItemCode == poGroup.Key.ItemCode
                                                select new
                                                {
                                                    OrderQty = id.OrderQuantity
                                                }).FirstOrDefault().OrderQty
                                        }).ToList().Select(x => new
                                        {
                                            PurchaseNumber = x.PurchaseNumber != null ? x.PurchaseNumber : "",
                                            ForecastNumber = x.ForecastNumber != null ? x.ForecastNumber : "",
                                            PurchaseDate = x.PurchaseDate.HasValue ? x.PurchaseDate.Value.ToString("yyyy-MM-dd") : "",
                                            Item = x.Item != null ? x.Item : "",
                                            ItemName = x.ItemName != null ? x.ItemName : "",
                                            OrderQty = x.OrderQty != null ? x.OrderQty.Value.ToString("N0") : "",
                                            Unit = x.Unit!= null ? x.Unit.ToString() : ""

                                        });

                    //Table
                    //float[] widths = new float[] { 70f, 50f, 70f, 100f, 35f };
                    table = new PdfPTable(7);
                    ////table.TotalWidth = 325f;
                    //table.LockedWidth = true;
                    //table.SetWidths(widths);
                    table.WidthPercentage = 100;
                    table.SpacingBefore = 5f;
                    table.HeaderRows = 1;

                    //Cell content
                    //header
                    string[] headers = new string[] { "Sales No.", "Sales Date", "Part Number", "Part Name", "Qty", "Unit", "Forecast Number" };
                    for (int i = 0; i < headers.Count(); i++)
                    {
                        table.AddCell(new PdfPCell(new Phrase(headers[i], FontFactory.GetFont("Calibri", 10)))
                        {
                            PaddingTop = 3,
                            PaddingBottom = 8,
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                    }

                    foreach (var item in purchaseData)
                    {
                        table.AddCell(new PdfPCell(new Phrase(item.PurchaseNumber, FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.PurchaseDate, FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.Item, FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.ItemName, FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.OrderQty, FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.Unit, FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.ForecastNumber, FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                    }

                    pdfDoc.Add(table);

                    //Closing the document
                    pdfDoc.Close();

                    var bytes = memoryStream.ToArray();
                    Session[strPDFFileName] = bytes;
                }

                return Json(new { success = true, strPDFFileName }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }
        /* end of Incoming Purchase */

        /* start of Incoming Packing */
        public ActionResult Packing()
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    List<MenuModels> MenuMaster = (List<MenuModels>)Session["MenuMaster"];
                    Int32 RoleID = Convert.ToInt32(Session["RoleId"]);

                    if (RoleID == 0)
                    {
                        ViewBag.IsView = 1;
                        ViewBag.IsInput = 1;
                        ViewBag.IsUpdate = 1;
                        ViewBag.IsDelete = 1;
                        ViewBag.IsUpload = 1;
                        ViewBag.IsDownload = 1;
                        ViewBag.RoleType = "";
                    }
                    else
                    {
                        var access = MenuMaster.Where(x => x.ControllerName == "Incoming" && x.ActionName == "Packing" && x.RoleId == RoleID).FirstOrDefault();
                        string RoleType = Session["RoleType"].ToString();

                        ViewBag.IsView = access.IsView;
                        ViewBag.IsInput = access.IsInput;
                        ViewBag.IsUpdate = access.IsUpdate;
                        ViewBag.IsDelete = access.IsDelete;
                        ViewBag.IsUpload = access.IsUpload;
                        ViewBag.IsDownload = access.IsDownload;
                        ViewBag.RoleType = RoleType;
                    }

                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult PackingList()
        {
            try
            {
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                var status = Request.Form.GetValues("status").FirstOrDefault();

                // Paging Size
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                // Getting all Purchase data
                var packingData = db.IncomingLists.AsEnumerable()
                                .Join(db.Users, p => p.CreatedBy, u => u.UserId, (p, u) => new
                                {
                                    ID = p.ID,
                                    DONO = p.DONO,
                                    DeliveryDate = p.IncomingDate.HasValue ? p.IncomingDate.Value.ToString("yyyy-MM-dd") : ""
                                })
                                .GroupBy(x => x.DONO)
                                .Select(g => g.First());

                // Sorting
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    packingData = packingData.OrderBy(sortColumn + " " + sortColumnDir);
                }
                // Search
                if (!string.IsNullOrEmpty(searchValue))
                {
                    packingData = packingData.Where(p => p.DONO.ToLower().Contains(searchValue.ToLower())
                        || p.DeliveryDate.ToLower().Contains(searchValue.ToLower()));
                }

                // total number of rows count
                recordsTotal = packingData.Count();
                // Paging
                var data = packingData.Skip(skip).Take(pageSize).ToList();
                // Returning Json Data
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [HandleError]
        public ActionResult UploadPacking(HttpPostedFileBase file)
        {
            if (Session["UserId"] != null)
            {
                TempData["ActionMessage"] = "Upload Failed";
                try
                {
                    if (file != null)
                    {
                        if (file.ContentType == "application/vnd.ms-excel" || file.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                        {
                            var fileName = file.FileName;
                            var targetPath = Server.MapPath("~/Uploads/");
                            file.SaveAs(targetPath + fileName);
                            var pathToExcelFile = targetPath + fileName;
                            string FileName = Path.GetFileName(file.FileName);
                            string Extension = Path.GetExtension(file.FileName);
                            DataTable dataTable = FileImport.ImportToGrid(pathToExcelFile, Extension, "Yes");
                            List<DeliveryOrder> orders = new List<DeliveryOrder>();
                            DateTime today = DateTime.Now;
                            foreach (DataRow row in dataTable.Rows)
                            {
                                try
                                {
                                    string DONO = row["INCOMING LIST NO"].ToString().Trim();
                                    string doDateFormat = row["DELIVERY DATE (YYYYMMDD)"].ToString().Trim();
                                    DateTime? DeliveryDate = null;
                                    if (doDateFormat != "")
                                    {
                                        string day = doDateFormat.Substring(6, 2);
                                        string month = doDateFormat.Substring(4, 2);
                                        string year = doDateFormat.Substring(0, 4);
                                        string deliveryDate = year + '-' + month + '-' + day;
                                        DeliveryDate = Convert.ToDateTime(deliveryDate);
                                    }
                                    string CNO = row["C/NO"].ToString().Trim();
                                    string PurchaseNumber = row["SALES NO"].ToString().Trim();
                                    string PartNumber = row["PART NO"].ToString().Trim();
                                    string PartName = row["PART NAME"].ToString().Trim();
                                    int Qty = Convert.ToInt32(row["QTY"]);
                                    string Unit = row["UNIT"].ToString().Trim();

                                    var PartExist = db.MasterParts.Where(x => x.PartNumber.Equals(PartNumber));
                                    if (PartExist != null)
                                    {
                                        PurchaseList PurchaseExist = db.PurchaseLists.FirstOrDefault(f => f.PurchaseNumber == PurchaseNumber && f.ItemCode == PartNumber);
                                        if (PurchaseExist != null)
                                        {
                                            var incomingExist = db.IncomingLists
                                                .Where(x => x.DONO == DONO
                                                    && x.IncomingDate == DeliveryDate
                                                    && x.CNO == CNO
                                                    && x.PurchaseNumber == PurchaseNumber
                                                    && x.PartNumber == PartNumber
                                                    && x.Description == PartName
                                                    && x.Quantity == Qty
                                                    && x.Unit == Unit
                                                    && x.StatusId == 1).FirstOrDefault();
                                            IncomingList incoming = null;
                                            if (incomingExist == null)
                                            {
                                                incoming = new IncomingList();
                                                incoming.DONO = DONO;
                                                incoming.PurchaseNumber = PurchaseNumber;
                                                incoming.PartNumber = PartNumber;
                                                incoming.Quantity = Qty;
                                                incoming.IncomingDate = DeliveryDate;
                                                incoming.CNO = CNO;
                                                incoming.Description = PartName;
                                                incoming.Unit = Unit;
                                                incoming.StatusId = db.IncomingStatuses.SingleOrDefault(x => x.Value == "HUB3W").ID;
                                                incoming.CreatedBy = Convert.ToInt32(Session["UserId"]);
                                                incoming.CreatedAt = today;
                                                db.IncomingLists.Add(incoming);
                                                db.SaveChanges();
                                            }
                                            else
                                            {
                                                incomingExist.Quantity += Qty;
                                                incomingExist.UpdatedBy = Convert.ToInt32(Session["UserId"]);
                                                incomingExist.UpdatedAt = today;
                                                db.SaveChanges();
                                            }

                                            DeliveryOrder order = new DeliveryOrder();
                                            order.DONO = DONO;
                                            order.CNO = CNO;
                                            order.PurchaseNumber = PurchaseNumber;
                                            order.PartNumber = PartNumber;
                                            order.Description = PartName;
                                            order.Quantity = Qty;
                                            order.Unit = Unit;
                                            orders.Add(order);

                                            TempData["ActionMessage"] = "Upload Success";
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                                    var sw = new System.IO.StreamWriter(filename, true);
                                    sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                                    sw.Close();
                                }
                            }

                            if (System.IO.File.Exists(pathToExcelFile))
                            {
                                System.IO.File.Delete(pathToExcelFile);
                            }

                            if (orders.Count() > 0)
                            {
                                List<List<DeliveryOrder>> deliveries = orders.GroupBy(item => item.DONO)
                                                                            .Select(group => group.ToList())
                                                                            .ToList();

                                // preparing email body
                                int userId = Convert.ToInt32(Session["UserId"]);
                                User user = db.Users.Find(userId);
                                var userMailLists = new[] { "KMSI_Warehouse" };
                                List<User> userLists = db.Users.Where(x => userMailLists.Contains(x.Role.RoleName)).ToList();

                                string baseUrl = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                                foreach (var data in userLists)
                                {
                                    string body = ViewRenderer.RenderRazorViewToString(this, "~/Views/Mail/ValidationPOPL.cshtml", new EmailIncomingList()
                                    {
                                        UploadDate = today.ToString("dd/MM/yyyy"),
                                        Documents = deliveries,
                                        BaseUrl = baseUrl,
                                        DownloadUrl = "#"
                                    });

                                    bool emailStatus = EmailHelper.SendEmail(data.Email, "INCOMING LIST INFORMATION", body, "");

                                    if (emailStatus)
                                    {
                                        Notification notification = new Notification();
                                        notification.UserId = user.UserId;
                                        notification.Name = user.FirstName + " " + user.LastName;
                                        notification.Module = "VSS";
                                        notification.Message = "New Incoming List has been uploaded by " + notification.Name;
                                        notification.Url = baseUrl;
                                        notification.ReceiveId = data.UserId;
                                        notification.CreatedAt = DateTime.Now;
                                        db.Notifications.Add(notification);
                                        db.SaveChanges();
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                        var sw = new System.IO.StreamWriter(filename, true);
                        sw.WriteLine(DateTime.Now.ToString() + " " + "File Not Selected!");
                        sw.Close();
                    }
                }
                catch (Exception ex)
                {
                    var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                    var sw = new System.IO.StreamWriter(filename, true);
                    sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                    sw.Close();
                }

                return RedirectToAction("Purchase");
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        public ActionResult GenerateXlsPacking(int[] ids)
        {
            DateTime dateTime = DateTime.Now;
            string FileName = string.Format("PackingList_" + dateTime.ToString("yyyyMMdd_HHmm") + ".xlsx");

            string handle = Guid.NewGuid().ToString();

            using (MemoryStream memoryStream = new MemoryStream())
            {
                using (ExcelPackage excel = new ExcelPackage())
                {
                    excel.Workbook.Worksheets.Add("Delivery Order");

                    var headerRow = new List<string[]>()
                    {
                        new string[] { "DO Number", "PO Number", "DO Date", "Part Number", "Description", "Quantity", "Remarks", "Stock Point" }
                    };

                    string headerRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";

                    var worksheet = excel.Workbook.Worksheets["Delivery Order"];

                    worksheet.Cells[headerRange].LoadFromArrays(headerRow);
                    worksheet.Cells[headerRange].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[headerRange].Style.Font.Bold = true;
                    worksheet.Cells[headerRange].Style.Font.Size = 12;
                    worksheet.Cells[headerRange].AutoFitColumns();

                    var packingData = from p in db.IncomingGoods
                                      where ids.Contains(p.ID)
                                      orderby p.ID descending
                                      select new
                                      {
                                          p.ID,
                                          p.DONumber,
                                          p.PONumber,
                                          p.DODate,
                                          p.PartNumber,
                                          p.Description,
                                          p.PackingQty,
                                          p.Remark,
                                          p.StockPoint
                                      };

                    int rowNumber = 2;
                    foreach (var data in packingData)
                    {
                        worksheet.Cells[rowNumber, 1].Value = data.DONumber;
                        worksheet.Cells[rowNumber, 2].Value = data.PONumber;
                        worksheet.Cells[rowNumber, 3].Style.Numberformat.Format = "yyyy-mm-dd";
                        worksheet.Cells[rowNumber, 3].Value = data.DODate;
                        worksheet.Cells[rowNumber, 3].Value = data.PartNumber;
                        worksheet.Cells[rowNumber, 3].Value = data.Description;
                        worksheet.Cells[rowNumber, 3].Value = data.PackingQty;
                        worksheet.Cells[rowNumber, 3].Value = data.Remark;
                        worksheet.Cells[rowNumber, 3].Value = data.StockPoint;

                        rowNumber++;
                    }

                    excel.SaveAs(memoryStream);
                }
                memoryStream.Position = 0;
                TempData[handle] = memoryStream.ToArray();
            }

            // Note we are returning a filename as well as the handle
            return new JsonResult()
            {
                Data = new { FileGuid = handle, FileName = FileName }
            };
        }

        public ActionResult GeneratePdfPacking(int[] ids)
        {
            try
            {
                DateTime dateTime = DateTime.Now;
                string strPDFFileName = string.Format("PackingList_" + dateTime.ToString("yyyyMMdd_HHmm") + ".pdf");
                string handle = Guid.NewGuid().ToString();

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    Document pdfDoc = new Document(PageSize.A4.Rotate());
                    PdfWriter.GetInstance(pdfDoc, memoryStream).CloseStream = false;

                    pdfDoc.SetMargins(28f, 28f, 28f, 28f);
                    //Create PDF Table

                    //file will created in this path
                    string strAttachment = Server.MapPath("~/PDFs/" + strPDFFileName);

                    pdfDoc.Open();

                    Phrase phrase = new Phrase();

                    //Table
                    PdfPTable table = new PdfPTable(1);
                    table.WidthPercentage = 100;
                    table.SpacingAfter = 10f;

                    table.AddCell(new PdfPCell(new Phrase("PACKING LIST", FontFactory.GetFont("Calibri", 20, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });
                    pdfDoc.Add(table);

                    var packingData = from p in db.IncomingGoods
                                      where  ids.Contains(p.ID)
                                      orderby p.ID descending
                                      select new
                                      {
                                          p.ID,
                                          p.DONumber,
                                          p.PONumber,
                                          p.DODate,
                                          p.PartNumber,
                                          p.Description,
                                          p.PackingQty,
                                          p.Remark,
                                          p.StockPoint
                                      };

                    //Table
                    float[] widths = new float[] { 70f, 70f, 50f, 70f, 100f, 35f, 70f, 70f };
                    table = new PdfPTable(8);
                    table.TotalWidth = 535f;
                    table.LockedWidth = true;
                    table.SetWidths(widths);
                    table.SpacingBefore = 5f;
                    table.HeaderRows = 1;

                    //Cell content
                    //header
                    string[] headers = new string[] { "DO Number", "PO Number", "DO Date", "Part Number", "Description", "Quantity", "Remarks", "Stock Point" };
                    for (int i = 0; i < headers.Count(); i++)
                    {
                        table.AddCell(new PdfPCell(new Phrase(headers[i], FontFactory.GetFont("Calibri", 10)))
                        {
                            PaddingTop = 3,
                            PaddingBottom = 8,
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                    }

                    foreach (var item in packingData)
                    {
                        table.AddCell(new PdfPCell(new Phrase((item.DONumber != null) ? item.DONumber : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.PONumber != null) ? item.PONumber : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.DODate != null) ? item.DODate.Value.ToString("yyyy-MM-dd") : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.PartNumber != null) ? item.PartNumber : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.Description != null) ? item.Description : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.PackingQty != null) ? item.PackingQty.ToString() : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.Remark != null) ? item.Remark : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.StockPoint != null) ? item.StockPoint : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                    }

                    pdfDoc.Add(table);

                    //Closing the document
                    pdfDoc.Close();

                    var bytes = memoryStream.ToArray();
                    Session[strPDFFileName] = bytes;
                }

                return Json(new { success = true, strPDFFileName }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }
        /* end of Incoming Packing */

        /* start of Incoming Receive */
        public ActionResult Receive()
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    List<MenuModels> MenuMaster = (List<MenuModels>)Session["MenuMaster"];
                    Int32 RoleID = Convert.ToInt32(Session["RoleId"]);

                    if (RoleID == 0)
                    {
                        ViewBag.IsView = 1;
                        ViewBag.IsInput = 1;
                        ViewBag.IsUpdate = 1;
                        ViewBag.IsDelete = 1;
                        ViewBag.IsUpload = 1;
                        ViewBag.IsDownload = 1;
                        ViewBag.RoleType = "";
                    }
                    else
                    {
                        var access = MenuMaster.Where(x => x.ControllerName == "Incoming" && x.ActionName == "Receive" && x.RoleId == RoleID).FirstOrDefault();
                        string RoleType = Session["RoleType"].ToString();

                        ViewBag.IsView = access.IsView;
                        ViewBag.IsInput = access.IsInput;
                        ViewBag.IsUpdate = access.IsUpdate;
                        ViewBag.IsDelete = access.IsDelete;
                        ViewBag.IsUpload = access.IsUpload;
                        ViewBag.IsDownload = access.IsDownload;
                        ViewBag.RoleType = RoleType;
                    }

                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult ReceiveList()
        {
            try
            {
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();

                // Paging Size
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                // Getting all Purchase data
                var receiveData = db.IncomingLists.AsEnumerable()
                                .Select(p => new
                                {
                                    ID = p.ID,
                                    DONO = p.DONO,
                                    DeliveryDate = p.IncomingDate.HasValue ? p.IncomingDate.Value.ToString("yyyy-MM-dd") : "",
                                    Status = p.IncomingStatus != null ? p.IncomingStatus.Name : "",
                                    StatusName = p.IncomingStatus != null ? p.IncomingStatus.Value : "",
                                    IsValid = p.IsValid,
                                    IsShipment = p.IsShipment
                                })
                                .GroupBy(x => x.DONO)
                                .Select(g => g.First());

                // Sorting
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    receiveData = receiveData.OrderBy(sortColumn + " " + sortColumnDir);
                }
                // Search
                if (!string.IsNullOrEmpty(searchValue))
                {
                    receiveData = receiveData.Where(p => p.DONO.ToLower().Contains(searchValue.ToLower())
                        || p.DeliveryDate.ToLower().Contains(searchValue.ToLower()));
                }

                // total number of rows count
                recordsTotal = receiveData.Count();
                // Paging
                var data = receiveData.Skip(skip).Take(pageSize).ToList();
                // Returning Json Data
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [HandleError]
        public ActionResult UploadReceive(HttpPostedFileBase file)
        {
            try
            {
                if (file != null)
                {
                    if (file.ContentType == "application/vnd.ms-excel" || file.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                    {
                        var fileName = file.FileName;
                        var targetPath = Server.MapPath("~/Uploads/");
                        file.SaveAs(targetPath + fileName);
                        var pathToExcelFile = targetPath + fileName;
                        string FileName = Path.GetFileName(file.FileName);
                        string Extension = Path.GetExtension(file.FileName);
                        DataTable dataTable = FileImport.ImportToGrid(pathToExcelFile, Extension, "Yes");
                        foreach (DataRow row in dataTable.Rows)
                        {
                            try
                            {
                                var DONO = row["DO NUMBER"].ToString();
                                var PartNumber = row["PART NUMBER"].ToString();
                                var PartExist = db.MasterParts.Where(x => x.PartNumber.Equals(PartNumber));
                                if (PartExist != null)
                                {
                                    var PackingExist = db.IncomingGoods.FirstOrDefault(f => f.DONumber == DONO && f.PartNumber == PartNumber);
                                    if (PackingExist != null)
                                    {
                                        IncomingGood po = new IncomingGood();
                                        po.ReceiveQty += Convert.ToInt32(row["QUANTITY"]);
                                        po.UpdatedAt = DateTime.Now;
                                        po.UpdatedBy = Convert.ToInt32(Session["UserId"]);
                                        db.SaveChanges();

                                        if (po.ID != 0)
                                        {
                                            if (System.IO.File.Exists(pathToExcelFile))
                                            {
                                                System.IO.File.Delete(pathToExcelFile);
                                            }
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                                var sw = new System.IO.StreamWriter(filename, true);
                                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                                sw.Close();
                            }
                        }
                    }
                }
                else
                {
                    var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                    var sw = new System.IO.StreamWriter(filename, true);
                    sw.WriteLine(DateTime.Now.ToString() + " " + "File Not Selected!");
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();
            }

            return RedirectToAction("Packing");
        }

        public ActionResult GenerateXlsReceive(int[] ids)
        {
            DateTime dateTime = DateTime.Now;
            string FileName = string.Format("PackingList_" + dateTime.ToString("yyyyMMdd_HHmm") + ".xlsx");

            string handle = Guid.NewGuid().ToString();

            using (MemoryStream memoryStream = new MemoryStream())
            {
                using (ExcelPackage excel = new ExcelPackage())
                {
                    excel.Workbook.Worksheets.Add("Delivery Order");

                    var headerRow = new List<string[]>()
                    {
                        new string[] { "DO Number", "PO Number", "DO Date", "Part Number", "Description", "Quantity", "Remarks", "Stock Point" }
                    };

                    string headerRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";

                    var worksheet = excel.Workbook.Worksheets["Delivery Order"];

                    worksheet.Cells[headerRange].LoadFromArrays(headerRow);
                    worksheet.Cells[headerRange].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[headerRange].Style.Font.Bold = true;
                    worksheet.Cells[headerRange].Style.Font.Size = 12;
                    worksheet.Cells[headerRange].AutoFitColumns();

                    var receiveData = from p in db.IncomingGoods
                                      where ids.Contains(p.ID) && p.ReceiveQty != null
                                      orderby p.ID descending
                                      select new
                                      {
                                          p.ID,
                                          p.DONumber,
                                          p.PONumber,
                                          p.DODate,
                                          p.PartNumber,
                                          p.Description,
                                          p.ReceiveQty,
                                          p.Remark,
                                          p.StockPoint
                                      };

                    int rowNumber = 2;
                    foreach (var data in receiveData)
                    {
                        worksheet.Cells[rowNumber, 1].Value = data.DONumber;
                        worksheet.Cells[rowNumber, 2].Value = data.PONumber;
                        worksheet.Cells[rowNumber, 3].Style.Numberformat.Format = "yyyy-mm-dd";
                        worksheet.Cells[rowNumber, 3].Value = data.DODate;
                        worksheet.Cells[rowNumber, 3].Value = data.PartNumber;
                        worksheet.Cells[rowNumber, 3].Value = data.Description;
                        worksheet.Cells[rowNumber, 3].Value = data.ReceiveQty;
                        worksheet.Cells[rowNumber, 3].Value = data.Remark;
                        worksheet.Cells[rowNumber, 3].Value = data.StockPoint;

                        rowNumber++;
                    }

                    excel.SaveAs(memoryStream);
                }
                memoryStream.Position = 0;
                TempData[handle] = memoryStream.ToArray();
            }

            // Note we are returning a filename as well as the handle
            return new JsonResult()
            {
                Data = new { FileGuid = handle, FileName = FileName }
            };
        }

        public ActionResult GeneratePdfReceive(int[] ids)
        {
            try
            {
                DateTime dateTime = DateTime.Now;
                string strPDFFileName = string.Format("PackingList_" + dateTime.ToString("yyyyMMdd_HHmm") + ".pdf");
                string handle = Guid.NewGuid().ToString();

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    Document pdfDoc = new Document(PageSize.A4.Rotate());
                    PdfWriter.GetInstance(pdfDoc, memoryStream).CloseStream = false;

                    pdfDoc.SetMargins(28f, 28f, 28f, 28f);
                    //Create PDF Table

                    //file will created in this path
                    string strAttachment = Server.MapPath("~/PDFs/" + strPDFFileName);

                    pdfDoc.Open();

                    Phrase phrase = new Phrase();

                    //Table
                    PdfPTable table = new PdfPTable(1);
                    table.WidthPercentage = 100;
                    table.SpacingAfter = 10f;

                    table.AddCell(new PdfPCell(new Phrase("PACKING LIST", FontFactory.GetFont("Calibri", 20, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });
                    pdfDoc.Add(table);

                    var receiveData = from p in db.IncomingGoods
                                      where ids.Contains(p.ID) && p.ReceiveQty != null
                                      orderby p.ID descending
                                      select new
                                      {
                                          p.ID,
                                          p.DONumber,
                                          p.PONumber,
                                          p.DODate,
                                          p.PartNumber,
                                          p.Description,
                                          p.ReceiveQty,
                                          p.Remark,
                                          p.StockPoint
                                      };

                    //Table
                    float[] widths = new float[] { 70f, 70f, 50f, 70f, 100f, 35f, 70f, 70f };
                    table = new PdfPTable(8);
                    table.TotalWidth = 535f;
                    table.LockedWidth = true;
                    table.SetWidths(widths);
                    table.SpacingBefore = 5f;
                    table.HeaderRows = 1;

                    //Cell content
                    //header
                    string[] headers = new string[] { "DO Number", "PO Number", "DO Date", "Part Number", "Description", "Quantity", "Remarks", "Stock Point" };
                    for (int i = 0; i < headers.Count(); i++)
                    {
                        table.AddCell(new PdfPCell(new Phrase(headers[i], FontFactory.GetFont("Calibri", 10)))
                        {
                            PaddingTop = 3,
                            PaddingBottom = 8,
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                    }

                    foreach (var item in receiveData)
                    {
                        table.AddCell(new PdfPCell(new Phrase((item.DONumber != null) ? item.DONumber : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.PONumber != null) ? item.PONumber : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.DODate != null) ? item.DODate.Value.ToString("yyyy-MM-dd") : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.PartNumber != null) ? item.PartNumber : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.Description != null) ? item.Description : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.ReceiveQty != null) ? item.ReceiveQty.ToString() : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.Remark != null) ? item.Remark : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.StockPoint != null) ? item.StockPoint : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                    }

                    pdfDoc.Add(table);

                    //Closing the document
                    pdfDoc.Close();

                    var bytes = memoryStream.ToArray();
                    Session[strPDFFileName] = bytes;
                }

                return Json(new { success = true, strPDFFileName }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }
        /* end of Incoming Receive */

        public FileResult DownloadExcel(string type)
        {
            string filename = "";
            switch (type)
            {
                case "forecast":
                    filename = "Forecast";
                    break;
                case "purchase":
                    filename = "Outstanding_Sales_Order";
                    break;
                case "packing":
                    filename = "Incoming_Document";
                    break;
                case "receive":
                    filename = "Goods_Receipt_Format";
                    break;
            }

            string path = "/Docs/" + filename + ".xlsx";
            return File(path, "application/vnd.ms-excel", filename + ".xlsx");
        }

        [HttpGet]
        public virtual ActionResult DownloadXls(string fileGuid, string fileName)
        {
            if (TempData[fileGuid] != null)
            {
                byte[] data = TempData[fileGuid] as byte[];
                return File(data, "application/vnd.ms-excel", fileName);
            }
            else
            {
                // Problem - Log the error, generate a blank file,
                //           redirect to another controller action - whatever fits with your application
                return new EmptyResult();
            }
        }

        [HttpGet]
        public virtual ActionResult DownloadPdf(string fileName)
        {
            try
            {
                var ms = Session[fileName] as byte[];
                if (ms == null)
                    return new EmptyResult();
                Session[fileName] = null;
                return File(ms, "application/octet-stream", fileName);
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return HttpNotFound();
            }
        }
    }
}
