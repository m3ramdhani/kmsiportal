﻿using ActionMailer.Net;
using ActionMailer.Net.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KMSIPartsPortal_System.Models;
using KMSIPartsPortal_System.Helpers;

namespace KMSIPartsPortal_System.Controllers
{
    public class MailController : MailerBase
    {
        // GET: Mail
        public ActionResult Index()
        {
            string result = ViewRenderer.RenderRazorViewToString(this, "~/Views/Mail/Email.cshtml", new Email()
            {
                To = "m3ramdhani@gmail.com",
                Date = DateTime.Now
            });

            return View();
        }

        public EmailResult Email(Email email)
        {
            To.Add(email.To);
            
            return Email("Email", email);
        }
    }
}