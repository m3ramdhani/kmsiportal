﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Web;
using System.Web.Mvc;
using KMSIPartsPortal_System.Models;

namespace KMSIPartsPortal_System.Controllers
{
    public class PackingController : Controller
    {
        private kmsi_portalEntities db = new kmsi_portalEntities();

        public ActionResult Details(string poNumber, string paramType)
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    ViewBag.poNumber = poNumber;
                    ViewBag.paramType = paramType;
                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult DetailList()
        {
            try
            {
                // Creating instance of DatabaseContext class
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                var number = Request.Form.GetValues("param").FirstOrDefault();
                var paramType = Request.Form.GetValues("paramType").FirstOrDefault();

                // Paging Size
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                // Getting all data
                var packingData = db.IncomingLists.AsEnumerable()
                .Where(x => x.PurchaseNumber == number)
                .Join(db.Users, p => p.CreatedBy, u => u.UserId, (p, u) => new
                {
                    ID = p.ID,
                    DONO = p.DONO,
                    DeliveryDate = p.IncomingDate.HasValue ? p.IncomingDate.Value.ToString("yyyy-MM-dd") : "",
                    CNO = p.CNO,
                    PurchaseNumber = p.PurchaseNumber,
                    PartNumber = p.PartNumber,
                    Description = p.Description,
                    Quantity = p.Quantity,
                    Unit = p.Unit,
                    IssuedBy = u.FirstName + " " + u.LastName
                });

                if (paramType == "DONO") {
                    packingData = db.IncomingLists.AsEnumerable()
                    .Where(x => x.DONO == number)
                    .Join(db.Users, p => p.CreatedBy, u => u.UserId, (p, u) => new
                    {
                        ID = p.ID,
                        DONO = p.DONO,
                        DeliveryDate = p.IncomingDate.HasValue ? p.IncomingDate.Value.ToString("yyyy-MM-dd") : "",
                        CNO = p.CNO,
                        PurchaseNumber = p.PurchaseNumber,
                        PartNumber = p.PartNumber,
                        Description = p.Description,
                        Quantity = p.Quantity,
                        Unit = p.Unit,
                        IssuedBy = u.FirstName + " " + u.LastName
                    });
                }


                // Sorting
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    packingData = packingData.OrderBy(sortColumn + " " + sortColumnDir);
                }
                // Search
                if (!string.IsNullOrEmpty(searchValue))
                {
                    packingData = packingData.Where(p => p.DONO.ToLower().Contains(searchValue.ToLower())
                        || p.DeliveryDate.ToLower().Contains(searchValue.ToLower())
                        || p.CNO.ToLower().Contains(searchValue.ToLower())
                        || p.PurchaseNumber.ToLower().Contains(searchValue.ToLower())
                        || p.PartNumber.ToLower().Contains(searchValue.ToLower())
                        || p.Description.ToLower().Contains(searchValue.ToLower())
                        || p.Quantity.ToString().ToLower().Contains(searchValue.ToLower())
                        || p.Unit.ToLower().Contains(searchValue.ToLower())
                        || p.IssuedBy.ToString().ToLower().Contains(searchValue.ToLower()));
                }

                // total number of rows count
                recordsTotal = packingData.Count();
                // Paging
                var data = packingData.Skip(skip).Take(pageSize).ToList();

                // Returning Json Data
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }
    }
}