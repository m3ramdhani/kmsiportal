﻿using KMSIPartsPortal_System.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace KMSIPartsPortal_System.Controllers
{
    public class DashboardController : Controller
    {
        kmsi_portalEntities db = new kmsi_portalEntities();

        public ActionResult ToDashboard(int id) {
            List<MenuModels> _menus = new List<MenuModels>();

            if (Session["RoleId"] == null)
            {
                _menus = db.SubMenus.Where(x => x.MainMenu.ModuleID == id).Select(x => new MenuModels
                {
                    ModuleId = x.MainMenu.ModuleID,
                    ModuleName = x.MainMenu.Module.ModuleName,
                    MainMenuId = x.MainMenuID,
                    MainMenuName = x.MainMenu.MenuName,
                    SubMenuId = x.ID,
                    SubMenuName = x.SubMenuName,
                    ControllerName = x.Controller,
                    ActionName = x.Link
                }).ToList();
            }
            else
            {
                int RoleID = Convert.ToInt32(Session["RoleId"]);
                
                if (id == 3 || id == 11)
                {
                    _menus = db.SidebarMenus.Where(x => x.RoleID == RoleID).Select(x => new MenuModels
                    {
                        ModuleId = x.SubMenu.MainMenu.ModuleID,
                        ModuleName = x.SubMenu.MainMenu.Module.ModuleName,
                        MainMenuId = x.SubMenu.MainMenuID,
                        MainMenuName = x.SubMenu.MainMenu.MenuName.ToUpper(),
                        SubMenuId = x.MenuID,
                        SubMenuName = x.SubMenu.SubMenuName,
                        ControllerName = x.SubMenu.Controller,
                        ActionName = x.SubMenu.Link,
                        RoleId = x.RoleID,
                        RoleName = x.Role.RoleName,
                        IsInput = x.IsInput,
                        IsUpdate = x.IsUpdate,
                        IsDelete = x.IsDelete,
                        IsView = x.IsView,
                        IsUpload = x.IsUpload,
                        IsDownload = x.IsDownload
                    }).ToList();
                }
                else
                {
                    _menus = db.SidebarMenus.Where(x => x.RoleID == RoleID && x.SubMenu.MainMenu.ModuleID == id).Select(x => new MenuModels
                    {
                        ModuleId = x.SubMenu.MainMenu.ModuleID,
                        ModuleName = x.SubMenu.MainMenu.Module.ModuleName,
                        MainMenuId = x.SubMenu.MainMenuID,
                        MainMenuName = x.SubMenu.MainMenu.MenuName,
                        SubMenuId = x.MenuID,
                        SubMenuName = x.SubMenu.SubMenuName,
                        ControllerName = x.SubMenu.Controller,
                        ActionName = x.SubMenu.Link,
                        RoleId = x.RoleID,
                        RoleName = x.Role.RoleName,
                        IsInput = x.IsInput,
                        IsUpdate = x.IsUpdate,
                        IsDelete = x.IsDelete,
                        IsView = x.IsView,
                        IsUpload = x.IsUpload,
                        IsDownload = x.IsDownload
                    }).ToList();
                }
            }

            Session["MenuMaster"] = _menus;
            Session["ModuleId"] = id;

            if (_menus.Count() == 0) {
                TempData["ActionMessage"] = "You dont have permission.";
                return RedirectToAction("Internal", "Dashboard");
            }

            if (Session["RoleId"] == null) {
                if (id == 1)
                {
                    return RedirectToAction("Fmc", "Dashboard");
                }
                else if (id == 2)
                {
                    return RedirectToAction("Purchasing", "Dashboard");
                }
                else if (id == 3)
                {
                    return RedirectToAction("KmsiHelpdesk", "Dashboard");
                }
                else if (id == 4)
                {
                    return RedirectToAction("KmsiGstock", "Dashboard");
                }
                else
                {
                    TempData["ActionMessage"] = "You dont have permission.";
                    return RedirectToAction("Internal", "Dashboard");
                }
            }
            else
            {
                if (id == 1)
                {

                    if (Session["RoleId"].ToString() == "4")
                    {
                        return RedirectToAction("Fmc", "Dashboard");
                    }
                    else
                    {
                        return RedirectToAction("UtFmc", "Dashboard");
                    }
                }
                else if (id == 2)
                {
                    if (Session["RoleId"].ToString() == "1")
                    {
                        return RedirectToAction("Purchasing", "Dashboard");
                    }
                    else
                    {
                        return RedirectToAction("Supplier", "Dashboard");
                    }
                }
                else if (id == 3)
                {
                    return RedirectToAction("Index", "TicketDocuments");
                    //if (Session["RoleId"].ToString() == "5")
                    //{
                    //    return RedirectToAction("Helpdesk", "Dashboard");
                    //}
                    //else
                    //{
                    //    return RedirectToAction("Helpdesk", "Dashboard");
                    //}
                }
                else if (id == 4)
                {
                    if (Session["RoleId"].ToString() == "7")
                    {
                        return RedirectToAction("Ggi", "Dashboard");
                    }
                    else
                    {
                        return RedirectToAction("KmsiGstock", "Dashboard");
                    }
                }
                else if (id == 11)
                {
                    return RedirectToAction("Index", "InquiryStock");
                }
                else
                {
                    TempData["ActionMessage"] = "You dont have permission.";
                    return RedirectToAction("Internal", "Dashboard");
                }
            }


        }

        public ActionResult ProSupplier()
        {
            var id = Convert.ToInt32(Session["SupplierId"]);
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Supplier supplier = db.Suppliers.Find(id);

            if (supplier == null)
            {
                return HttpNotFound();
            }
            return View(supplier);
        }

        public ActionResult ProKmsi()
        {
            return View();
        }

        public ActionResult GStockKmsi()
        {
            return View();
        }

        public ActionResult GStockGgi()
        {
            ViewBag.TotalStocks = db.Stocks.Where(x => x.Position == "GGI").Sum(x => x.Quantity);
            return View();
        }

        public ActionResult Internal()
        {
            List<Module> modules = db.Modules.ToList();
            ViewBag.Title = "Internal User";
            Int32 limitLeft = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(modules.Count) / 2));
            Int32 limitRight = modules.Count - limitLeft;
            ViewBag.ModuleInternalLeft = modules.Skip(0).Take(limitLeft);
            ViewBag.ModuleInternalRight = modules.Skip(limitLeft).Take(limitRight);
            return View(modules);
        }

        public ActionResult Supplier()
        {
            var supplierCode = (string)Session["SupplierCode"];

            var purchaseData = db.vSummaryOspoes.AsEnumerable()
                        .Where(p => p.ShipQty < p.ReqQty || p.ShipQty == 0)
                        .Select(x => new
                        {
                            x.SupplierCode,
                            x.SupplierName,
                            PurchaseDate = x.PurchaseDate.Value.ToString("yyyy-MM-dd"),
                            x.PurchaseNumber,
                            x.Amount,
                            x.Currency,
                            x.Aging,
                            x.Remarks,
                            x.ShipQty,
                            x.ReqQty
                        });

            if (purchaseData == null)
            {
                ViewBag.Ospo = 0;
            }
            else
            {
                ViewBag.Ospo = purchaseData.Select(g => g.PurchaseNumber).Count();
            }

            var purchaseDataInv = (from i in db.PurchaseInvoices
                                   join d in db.PurchaseInvoiceDetails on i.InvoiceNumber equals d.InvoiceNumber
                                   orderby i.InvoiceID ascending
                                   group d by new
                                   {
                                       i.InvoiceID,
                                       i.SupplierID,
                                       i.SupplierCode,
                                       i.SupplierName,
                                       i.InvoiceDate,
                                       i.InvoiceNumber,
                                       i.InvoiceDueDate,
                                       i.Description,
                                       i.Currency,
                                       i.Request,
                                       i.ReqComment,
                                       i.fileINV,
                                       i.fileFP,
                                       i.FixedStatus
                                   } into invoiceGroup
                                   select new
                                   {
                                       InvoiceID = invoiceGroup.Key.InvoiceID,
                                       SupplierID = invoiceGroup.Key.SupplierID,
                                       SupplierCode = invoiceGroup.Key.SupplierCode,
                                       SupplierName = invoiceGroup.Key.SupplierName,
                                       InvoiceDate = invoiceGroup.Key.InvoiceDate,
                                       InvoiceNumber = invoiceGroup.Key.InvoiceNumber,
                                       InvoiceDueDate = invoiceGroup.Key.InvoiceDueDate,
                                       Description = invoiceGroup.Key.Description,
                                       Currency = invoiceGroup.Key.Currency,
                                       PrincipalAmount = invoiceGroup.Sum(a => a.UnitPrice * a.ShipQty),
                                       PrincipalTax = invoiceGroup.Sum(a => a.UnitPrice * a.ShipQty) * (decimal)0.1,
                                       TotalAmount = invoiceGroup.Sum(a => a.UnitPrice * a.ShipQty) + invoiceGroup.Sum(a => a.UnitPrice * a.ShipQty) * (decimal)0.1,
                                       Request = invoiceGroup.Key.Request,
                                       ReqComment = invoiceGroup.Key.ReqComment,
                                       fileINV = invoiceGroup.Key.fileINV,
                                       fileFP = invoiceGroup.Key.fileFP,
                                       FixedStatus = invoiceGroup.Key.FixedStatus
                                   })
                                .Where(x => x.fileINV == null || x.fileFP == null);

            if (!string.IsNullOrEmpty(supplierCode))
            {
                purchaseDataInv = purchaseDataInv.Where(p => p.SupplierCode.Equals(supplierCode));
            }
            else
            {
                purchaseDataInv = purchaseDataInv.Where(p => p.FixedStatus != 1);
            }

            if (purchaseDataInv == null)
            {
                ViewBag.OsInv = 0;
            }
            else
            {
                ViewBag.OsInv = purchaseDataInv.Count();
            }

            ViewBag.Title = "Supplier User";
            return View();
        }

        public ActionResult Fmc()
        {
            ViewBag.Title = "UT FMC";
            return View();
        }

        public ActionResult Ggi()
        {
            var pNumber = db.MasterParts.Select(x => x.PartNumber).ToList();

            var HOSE = db.MasterParts.Where(x => x.PartName == "HOSE").Select(x => x.PartNumber).ToList();
            var FITTING = db.MasterParts.Where(x => x.PartName == "FITTING").Select(x => x.PartNumber).ToList();
            var SPRING = db.MasterParts.Where(x => x.PartName == "SPRING").Select(x => x.PartNumber).ToList();
            var CONDUIT = db.MasterParts.Where(x => x.PartName == "CONDUIT").Select(x => x.PartNumber).ToList();
            var NYLON = db.MasterParts.Where(x => x.PartName == "SPIRAL TUBE (NYLON)").Select(x => x.PartNumber).ToList();
            var SPIRAL = db.MasterParts.Where(x => x.PartName == "SPIRAL TUBE").Select(x => x.PartNumber).ToList();

            ViewBag.HoseGgiStock = db.Stocks.Where(x => x.Position == "GGI" && HOSE.Contains(x.PartNumber)).Sum(x => x.Quantity) ?? 0;
            ViewBag.FittingGgiStock = db.Stocks.Where(x => x.Position == "GGI" && FITTING.Contains(x.PartNumber)).Sum(x => x.Quantity) ?? 0;
            ViewBag.SpringGgiStock = db.Stocks.Where(x => x.Position == "GGI" && SPRING.Contains(x.PartNumber)).Sum(x => x.Quantity) ?? 0;
            ViewBag.ConduitGgiStock = db.Stocks.Where(x => x.Position == "GGI" && CONDUIT.Contains(x.PartNumber)).Sum(x => x.Quantity) ?? 0;
            ViewBag.NylonGgiStock = db.Stocks.Where(x => x.Position == "GGI" && NYLON.Contains(x.PartNumber)).Sum(x => x.Quantity) ?? 0;
            ViewBag.SpiralGgiStock = db.Stocks.Where(x => x.Position == "GGI" && SPIRAL.Contains(x.PartNumber)).Sum(x => x.Quantity) ?? 0;

            ViewBag.HoseKmsiStock = db.Stocks.Where(x => x.Position == "KMSI" && HOSE.Contains(x.PartNumber)).Sum(x => x.Quantity) ?? 0;
            ViewBag.FittingKmsiStock = db.Stocks.Where(x => x.Position == "KMSI" && FITTING.Contains(x.PartNumber)).Sum(x => x.Quantity) ?? 0;
            ViewBag.SpringKmsiStock = db.Stocks.Where(x => x.Position == "KMSI" && SPRING.Contains(x.PartNumber)).Sum(x => x.Quantity) ?? 0;
            ViewBag.ConduitKmsiStock = db.Stocks.Where(x => x.Position == "KMSI" && CONDUIT.Contains(x.PartNumber)).Sum(x => x.Quantity) ?? 0;
            ViewBag.NylonKmsiStock = db.Stocks.Where(x => x.Position == "KMSI" && NYLON.Contains(x.PartNumber)).Sum(x => x.Quantity) ?? 0;
            ViewBag.SpiralKmsiStock = db.Stocks.Where(x => x.Position == "KMSI" && SPIRAL.Contains(x.PartNumber)).Sum(x => x.Quantity) ?? 0;

            ViewBag.TotalStocks = db.Stocks.Where(x => x.Position == "GGI" && pNumber.Contains(x.PartNumber)).Sum(x => x.Quantity) ?? 0;
            ViewBag.Title = "GGI";
            return View();
        }

        public ActionResult Distributor()
        {
            List<Module> modules = db.Modules.Where(x => x.ModuleName == "Helpdesk" || x.ModuleName == "Information").ToList();
            ViewBag.ModuleInternalLeft = modules;
            ViewBag.Title = "Distributor User";

            List<MenuModels> _menus = new List<MenuModels>();

            if (Session["RoleId"] == null)
            {
                _menus = db.SubMenus.Where(x => x.MainMenu.ModuleID == 11).Select(x => new MenuModels
                {
                    ModuleId = x.MainMenu.ModuleID,
                    ModuleName = x.MainMenu.Module.ModuleName,
                    MainMenuId = x.MainMenuID,
                    MainMenuName = x.MainMenu.MenuName,
                    SubMenuId = x.ID,
                    SubMenuName = x.SubMenuName,
                    ControllerName = x.Controller,
                    ActionName = x.Link
                }).ToList();
            }
            else
            {
                int RoleID = Convert.ToInt32(Session["RoleId"]);
                _menus = db.SidebarMenus.Where(x => x.RoleID == RoleID).Select(x => new MenuModels
                {
                    ModuleId = x.SubMenu.MainMenu.ModuleID,
                    ModuleName = x.SubMenu.MainMenu.Module.ModuleName,
                    MainMenuId = x.SubMenu.MainMenuID,
                    MainMenuName = x.SubMenu.MainMenu.MenuName,
                    SubMenuId = x.MenuID,
                    SubMenuName = x.SubMenu.SubMenuName,
                    ControllerName = x.SubMenu.Controller,
                    ActionName = x.SubMenu.Link,
                    RoleId = x.RoleID,
                    RoleName = x.Role.RoleName,
                    IsInput = x.IsInput,
                    IsUpdate = x.IsUpdate,
                    IsDelete = x.IsDelete,
                    IsView = x.IsView,
                    IsUpload = x.IsUpload,
                    IsDownload = x.IsDownload
                }).ToList();
            }

            Session["MenuMaster"] = _menus;

            return View();
        }

        public ActionResult Purchasing()
        {
            var purchaseData = db.vSummaryOspoes.AsEnumerable()
                        .Where(p => p.ShipQty < p.ReqQty || p.ShipQty == 0)
                        .Select(x => new
                        {
                            x.SupplierCode,
                            x.SupplierName,
                            PurchaseDate = x.PurchaseDate.Value.ToString("yyyy-MM-dd"),
                            x.PurchaseNumber,
                            x.Amount,
                            x.Currency,
                            x.Aging,
                            x.Remarks,
                            x.ShipQty,
                            x.ReqQty
                        });

            if (purchaseData == null)
            {
                ViewBag.Ospo = 0;
            }
            else
            {
                ViewBag.Ospo = purchaseData.Select(g => g.PurchaseNumber).Count();
            }

            var purchaseDataInv = (from i in db.PurchaseInvoices
                                   join d in db.PurchaseInvoiceDetails on i.InvoiceNumber equals d.InvoiceNumber
                                   orderby i.InvoiceID ascending
                                   group d by new
                                   {
                                       i.InvoiceID,
                                       i.SupplierID,
                                       i.SupplierCode,
                                       i.SupplierName,
                                       i.InvoiceDate,
                                       i.InvoiceNumber,
                                       i.InvoiceDueDate,
                                       i.Description,
                                       i.Currency,
                                       i.Request,
                                       i.ReqComment,
                                       i.fileINV,
                                       i.fileFP,
                                       i.FixedStatus
                                   } into invoiceGroup
                                   select new
                                   {
                                       InvoiceID = invoiceGroup.Key.InvoiceID,
                                       SupplierID = invoiceGroup.Key.SupplierID,
                                       SupplierCode = invoiceGroup.Key.SupplierCode,
                                       SupplierName = invoiceGroup.Key.SupplierName,
                                       InvoiceDate = invoiceGroup.Key.InvoiceDate,
                                       InvoiceNumber = invoiceGroup.Key.InvoiceNumber,
                                       InvoiceDueDate = invoiceGroup.Key.InvoiceDueDate,
                                       Description = invoiceGroup.Key.Description,
                                       Currency = invoiceGroup.Key.Currency,
                                       PrincipalAmount = invoiceGroup.Sum(a => a.UnitPrice * a.ShipQty),
                                       PrincipalTax = invoiceGroup.Sum(a => a.UnitPrice * a.ShipQty) * (decimal)0.1,
                                       TotalAmount = invoiceGroup.Sum(a => a.UnitPrice * a.ShipQty) + invoiceGroup.Sum(a => a.UnitPrice * a.ShipQty) * (decimal)0.1,
                                       Request = invoiceGroup.Key.Request,
                                       ReqComment = invoiceGroup.Key.ReqComment,
                                       fileINV = invoiceGroup.Key.fileINV,
                                       fileFP = invoiceGroup.Key.fileFP,
                                       FixedStatus = invoiceGroup.Key.FixedStatus
                                   })
                                .Where(x => x.fileINV == null || x.fileFP == null);

            var supplierCode = (string)Session["SupplierCode"];
            if (!string.IsNullOrEmpty(supplierCode))
            {
                purchaseDataInv = purchaseDataInv.Where(p => p.SupplierCode.Equals(supplierCode));
            }
            else
            {
                purchaseDataInv = purchaseDataInv.Where(p => p.FixedStatus != 1);
            }

            if (purchaseDataInv == null) {
                ViewBag.OsInv = 0;
            }
            else
            {
                ViewBag.OsInv = purchaseDataInv.Count();
            }

            ViewBag.Title = "Dashboard";
            return View();
        }

        public ActionResult Helpdesk()
        {
            List<Module> modules = db.Modules.Where(x => x.ModuleName == "Helpdesk" || x.ModuleName == "Information").ToList();
            ViewBag.ModuleInternalLeft = modules;

            ViewBag.Title = "Dashboard";
            return View();
        }

        public ActionResult KmsiGstock()
        {
            var HOSE = db.MasterParts.Where(x => x.PartName == "HOSE").Select(x => x.PartNumber).ToList();
            var FITTING = db.MasterParts.Where(x => x.PartName == "FITTING").Select(x => x.PartNumber).ToList();
            var SPRING = db.MasterParts.Where(x => x.PartName == "SPRING").Select(x => x.PartNumber).ToList();
            var CONDUIT = db.MasterParts.Where(x => x.PartName == "CONDUIT").Select(x => x.PartNumber).ToList();
            var NYLON = db.MasterParts.Where(x => x.PartName == "SPIRAL TUBE (NYLON)").Select(x => x.PartNumber).ToList();
            var SPIRAL = db.MasterParts.Where(x => x.PartName == "SPIRAL TUBE").Select(x => x.PartNumber).ToList();

            ViewBag.HoseGgiStock = db.Stocks.Where(x => x.Position == "GGI" && HOSE.Contains(x.PartNumber)).Sum(x => x.Quantity) ?? 0;
            ViewBag.FittingGgiStock = db.Stocks.Where(x => x.Position == "GGI" && FITTING.Contains(x.PartNumber)).Sum(x => x.Quantity) ?? 0;
            ViewBag.SpringGgiStock = db.Stocks.Where(x => x.Position == "GGI" && SPRING.Contains(x.PartNumber)).Sum(x => x.Quantity) ?? 0;
            ViewBag.ConduitGgiStock = db.Stocks.Where(x => x.Position == "GGI" && CONDUIT.Contains(x.PartNumber)).Sum(x => x.Quantity) ?? 0;
            ViewBag.NylonGgiStock = db.Stocks.Where(x => x.Position == "GGI" && NYLON.Contains(x.PartNumber)).Sum(x => x.Quantity) ?? 0;
            ViewBag.SpiralGgiStock = db.Stocks.Where(x => x.Position == "GGI" && SPIRAL.Contains(x.PartNumber)).Sum(x => x.Quantity) ?? 0;

            ViewBag.HoseKmsiStock = db.Stocks.Where(x => x.Position == "KMSI" && HOSE.Contains(x.PartNumber)).Sum(x => x.Quantity) ?? 0;
            ViewBag.FittingKmsiStock = db.Stocks.Where(x => x.Position == "KMSI" && FITTING.Contains(x.PartNumber)).Sum(x => x.Quantity) ?? 0;
            ViewBag.SpringKmsiStock = db.Stocks.Where(x => x.Position == "KMSI" && SPRING.Contains(x.PartNumber)).Sum(x => x.Quantity) ?? 0;
            ViewBag.ConduitKmsiStock = db.Stocks.Where(x => x.Position == "KMSI" && CONDUIT.Contains(x.PartNumber)).Sum(x => x.Quantity) ?? 0;
            ViewBag.NylonKmsiStock = db.Stocks.Where(x => x.Position == "KMSI" && NYLON.Contains(x.PartNumber)).Sum(x => x.Quantity) ?? 0;
            ViewBag.SpiralKmsiStock = db.Stocks.Where(x => x.Position == "KMSI" && SPIRAL.Contains(x.PartNumber)).Sum(x => x.Quantity) ?? 0;

            ViewBag.Title = "Dashboard";
            return View();
        }

        public ActionResult UtFmc()
        {
            ViewBag.Title = "Dashboard";
            return View();
        }
    }
}