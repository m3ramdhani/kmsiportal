﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Web;
using System.Web.Mvc;
using KMSIPartsPortal_System.Models;

namespace KMSIPartsPortal_System.Controllers
{
    public class MenuListsController : Controller
    {
        private kmsi_portalEntities db = new kmsi_portalEntities();

        // GET: MenuLists
        public ActionResult Index()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return HttpNotFound();
            }
        }

        public ActionResult List()
        {
            try
            {
                // Creating instance of DatabaseContext class
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();

                // Paging Size
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                // Getting all data
                var menuLists = from l in db.MenuLists
                                join m in db.MainMenus on new { ID = l.MainMenuID } equals new { ID = m.MenuID }
                                select new
                                {
                                    l.ID,
                                    l.Menu_Name,
                                    l.Controller,
                                    l.Action,
                                    MainMenu = m.MainMenu1
                                };

                // Sorting
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    menuLists = menuLists.OrderBy(sortColumn + " " + sortColumnDir);
                }
                // Search
                if (!string.IsNullOrEmpty(searchValue))
                {
                    menuLists = menuLists.Where(m => m.Menu_Name.ToLower().Contains(searchValue.ToLower())
                        || m.Controller.ToLower().Contains(searchValue.ToLower())
                        || m.Action.ToLower().Contains(searchValue.ToLower())
                        || m.MainMenu.ToLower().Contains(searchValue.ToLower())
                    );
                }

                // total number of rows count
                recordsTotal = menuLists.Count();
                // Paging
                var data = menuLists.Skip(skip).Take(pageSize).ToList();

                // Returning Json Data
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return HttpNotFound();
            }
        }

        // GET: MenuLists/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MenuList menuList = db.MenuLists.Find(id);
            if (menuList == null)
            {
                return HttpNotFound();
            }
            return View(menuList);
        }

        // GET: MenuLists/Create
        public ActionResult Create()
        {
            ViewBag.feature = db.MainMenus.ToList();

            return View();
        }

        // POST: MenuLists/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Menu_Name,Controller,Action,MainMenuID")] MenuList menuList)
        {
            if (ModelState.IsValid)
            {
                db.MenuLists.Add(menuList);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(menuList);
        }

        // GET: MenuLists/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MenuList menuList = db.MenuLists.Find(id);
            ViewBag.feature = db.MainMenus.ToList();
            if (menuList == null)
            {
                return HttpNotFound();
            }
            return View(menuList);
        }

        // POST: MenuLists/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Menu_Name,Controller,Action,MainMenuID")] MenuList menuList)
        {
            if (ModelState.IsValid)
            {
                db.Entry(menuList).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(menuList);
        }

        // GET: MenuLists/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MenuList menuList = db.MenuLists.Find(id);
            if (menuList == null)
            {
                return HttpNotFound();
            }
            return View(menuList);
        }

        // POST: MenuLists/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            MenuList menuList = db.MenuLists.Find(id);
            db.MenuLists.Remove(menuList);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
