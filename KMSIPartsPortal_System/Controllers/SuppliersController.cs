﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Web;
using System.Web.Mvc;
using KMSIPartsPortal_System.Models;
using OfficeOpenXml;
using KMSIPartsPortal_System.Helpers;

namespace KMSIPartsPortal_System.Controllers
{
    public class SuppliersController : Controller
    {
        private kmsi_portalEntities db = new kmsi_portalEntities();

        // GET: Suppliers
        public ActionResult Index()
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult List()
        {
            try
            {
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();

                // Paging Size
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                // Getting all Work Order data
                var supplierData = from s in db.Suppliers
                                   orderby s.SupplierName ascending
                                   select new
                                   {
                                       s.SupplierId,
                                       s.SupplierCode,
                                       s.SupplierName
                                   };

                // Sorting
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    supplierData = supplierData.OrderBy(sortColumn + " " + sortColumnDir);
                }
                // Search
                if (!string.IsNullOrEmpty(searchValue))
                {
                    supplierData = supplierData.Where(w => w.SupplierCode.ToLower().Contains(searchValue.ToLower())
                        || w.SupplierName.ToLower().Contains(searchValue.ToLower())
                    );
                }

                // total number of rows count
                recordsTotal = supplierData.Count();

                // Paging
                var data = supplierData.Skip(skip).Take(pageSize).ToList();
                // Returning Json Data
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        // GET: Suppliers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Supplier supplier = db.Suppliers.Find(id);
            if (supplier == null)
            {
                return HttpNotFound();
            }
            return View(supplier);
        }

        // GET: Suppliers/Create
        public ActionResult Create()
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        // POST: Suppliers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Supplier supplier)
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    if (ModelState.IsValid)
                    {
                        db.Suppliers.Add(supplier);
                        db.SaveChanges();

                        int SupplierID = supplier.SupplierId;

                        if (SupplierID != 0 && supplier.fileUpload != null)
                        {
                            string path = Server.MapPath("~/Uploads/Supplier/");
                            if (!Directory.Exists(path))
                                Directory.CreateDirectory(path);

                            string FileName = Path.GetFileName(supplier.fileUpload.FileName);

                            Supplier sup = db.Suppliers.Find(SupplierID);

                            if (sup != null)
                            {
                                sup.AgreementFile = FileName;
                                var SavePathAttachment = Path.Combine(path + FileName);
                                supplier.fileUpload.SaveAs(SavePathAttachment);
                                db.SaveChanges();
                            }
                        }

                        return RedirectToAction("Index");
                    }

                    return View(supplier);
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        // GET: Suppliers/Edit/5
        public ActionResult Edit(int? id)
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    if (id == null)
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }
                    Supplier supplier = db.Suppliers.Find(id);
                    if (supplier == null)
                    {
                        return HttpNotFound();
                    }
                    return View(supplier);
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        // POST: Suppliers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Supplier supplier)
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    if (ModelState.IsValid)
                    {
                        var currentSupplier = db.Suppliers.FirstOrDefault(s => s.SupplierId == supplier.SupplierId);
                        currentSupplier.SupplierCode = supplier.SupplierCode;
                        currentSupplier.SupplierName = supplier.SupplierName;
                        currentSupplier.Locat = supplier.Locat;
                        currentSupplier.Addr = supplier.Addr;
                        currentSupplier.Contact = supplier.Contact;
                        currentSupplier.Phone = supplier.Phone;
                        currentSupplier.eMail = supplier.eMail;
                        currentSupplier.RelationToKMSI = supplier.RelationToKMSI;
                        currentSupplier.SupplierType = supplier.SupplierType;
                        currentSupplier.MinimumOrder = supplier.MinimumOrder;
                        currentSupplier.Currency = supplier.Currency;
                        currentSupplier.TransactionType = supplier.TransactionType;
                        currentSupplier.AgreementNo = supplier.AgreementNo;
                        currentSupplier.TaxID = supplier.TaxID;
                        currentSupplier.OrderMethods = supplier.OrderMethods;
                        currentSupplier.BankName = supplier.BankName;
                        currentSupplier.BankCity = supplier.BankCity;
                        currentSupplier.AccountNumber = supplier.AccountNumber;
                        db.SaveChanges();

                        int SupplierID = supplier.SupplierId;

                        if (SupplierID != 0 && supplier.fileUpload != null)
                        {
                            string path = Server.MapPath("~/Uploads/Supplier/");
                            if (!Directory.Exists(path))
                                Directory.CreateDirectory(path);

                            string FileName = Path.GetFileName(supplier.fileUpload.FileName);

                            Supplier sup = db.Suppliers.Find(SupplierID);

                            if (sup != null)
                            {
                                sup.AgreementFile = FileName;
                                var SavePathAttachment = Path.Combine(path + FileName);
                                supplier.fileUpload.SaveAs(SavePathAttachment);
                                db.SaveChanges();
                            }
                        }
                        return RedirectToAction("Details", new
                        {
                            id = SupplierID
                        });
                    }

                    return View(supplier);
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        // GET: Suppliers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Supplier supplier = db.Suppliers.Find(id);
            if (supplier == null)
            {
                return HttpNotFound();
            }
            return View(supplier);
        }

        // POST: Suppliers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Supplier supplier = db.Suppliers.Find(id);
            db.Suppliers.Remove(supplier);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [HandleError]
        public ActionResult UploadExcel(HttpPostedFileBase fileUpload, Supplier supplier)
        {
            try
            {
                if (fileUpload != null)
                {
                    if (fileUpload.ContentType == "application/vnd.ms-excel" || fileUpload.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                    {
                        var fileName = fileUpload.FileName;
                        var targetPath = Server.MapPath("~/Uploads/");
                        fileUpload.SaveAs(targetPath + fileName);
                        var pathToExcelFile = targetPath + fileName;
                        string FileName = Path.GetFileName(fileUpload.FileName);
                        string Extension = Path.GetExtension(fileUpload.FileName);
                        var file = new System.IO.FileInfo(pathToExcelFile);

                        using (var excelPackage = new ExcelPackage(file))
                        {
                            ExcelWorkbook excelWorkbook = excelPackage.Workbook;
                            ExcelWorksheet worksheets = excelWorkbook.Worksheets[1];

                            var currentSupplier = db.Suppliers.FirstOrDefault(s => s.SupplierId == supplier.SupplierId);
                            currentSupplier.SupplierName = worksheets.Cells["B1"].Value.ToString();
                            currentSupplier.Addr = worksheets.Cells["B2"].Value.ToString();
                            currentSupplier.MailAddr = worksheets.Cells["B3"].Value.ToString();
                            currentSupplier.Phone = worksheets.Cells["B4"].Value.ToString();
                            currentSupplier.Fax = worksheets.Cells["B5"].Value.ToString();
                            currentSupplier.eMail = worksheets.Cells["B6"].Value.ToString();
                            currentSupplier.Established = worksheets.Cells["B7"].Value.ToString();
                            currentSupplier.BankName = worksheets.Cells["B8"].Value.ToString();
                            currentSupplier.Currency = worksheets.Cells["B9"].Value.ToString();
                            currentSupplier.BankAddr = worksheets.Cells["B10"].Value.ToString();
                            currentSupplier.AccountNumber = worksheets.Cells["B11"].Value.ToString();
                            currentSupplier.TransactionType = worksheets.Cells["B12"].Value.ToString();
                            currentSupplier.PayTerm = worksheets.Cells["B13"].Value.ToString();
                            currentSupplier.MemoOfAssoc = worksheets.Cells["B14"].Value.ToString();
                            currentSupplier.MemoOfAssocLatest = worksheets.Cells["B15"].Value.ToString();
                            currentSupplier.BusinessPermit = worksheets.Cells["B16"].Value.ToString();
                            currentSupplier.LetterOfDomicile = worksheets.Cells["B17"].Value.ToString();
                            currentSupplier.CompRegCert = worksheets.Cells["B18"].Value.ToString();
                            currentSupplier.AgreeWithKmsi = worksheets.Cells["B19"].Value.ToString();
                            currentSupplier.TaxID = worksheets.Cells["B20"].Value.ToString();
                            currentSupplier.TaxObjNumber = worksheets.Cells["B21"].Value.ToString();
                            currentSupplier.SertKlasifikasiUsaha = worksheets.Cells["B22"].Value.ToString();
                            currentSupplier.SKBebasPotongan = worksheets.Cells["B23"].Value.ToString();
                            currentSupplier.Contact = worksheets.Cells["B24"].Value.ToString();
                            currentSupplier.ContactPhone = worksheets.Cells["B25"].Value.ToString();
                            currentSupplier.ContactEmail = worksheets.Cells["B26"].Value.ToString();
                            currentSupplier.LastUpdTime = DateTime.Now;
                            var userID = Convert.ToInt32(Session["UserId"]);
                            var user = db.Users.Find(userID);
                            currentSupplier.LastUser = user.FirstName + " " + user.LastName;
                            db.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();
            }

            return RedirectToAction("Details", new
            {
                id = supplier.SupplierId
            });
        }

        public FileResult DownloadExcel()
        {
            string path = "/Docs/SupplierDataTemplate.xlsx";
            return File(path, "application/vnd.ms-excel", "SupplierDataTemplate.xlsx");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UploadDocs(Supplier supplier)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int SupplierID = supplier.SupplierId;

                    if (supplier.fileUpload1 != null)
                    {
                        string path = Server.MapPath("~/Uploads/Supplier/");
                        if (!Directory.Exists(path))
                            Directory.CreateDirectory(path);

                        string FileName = Path.GetFileName(supplier.fileUpload1.FileName);

                        Supplier sup = db.Suppliers.Find(supplier.SupplierId);

                        if (sup != null)
                        {
                            string saveName = Encryptor.ComputeHash(FileName, "MD5", null);
                            sup.fileOfCompanyProfile = saveName + ".pdf";
                            var SavePathAttachment = Path.Combine(path + saveName + ".pdf");
                            supplier.fileUpload1.SaveAs(SavePathAttachment);
                            db.SaveChanges();
                        }
                    }

                    if (supplier.fileUpload2 != null)
                    {
                        string path = Server.MapPath("~/Uploads/Supplier/");
                        if (!Directory.Exists(path))
                            Directory.CreateDirectory(path);

                        string FileName = Path.GetFileName(supplier.fileUpload2.FileName);

                        Supplier sup = db.Suppliers.Find(supplier.SupplierId);

                        if (sup != null)
                        {
                            string saveName = Encryptor.ComputeHash(FileName, "MD5", null);
                            sup.fileOfCompanyOrganization = saveName + ".pdf";
                            var SavePathAttachment = Path.Combine(path + saveName + ".pdf");
                            supplier.fileUpload2.SaveAs(SavePathAttachment);
                            db.SaveChanges();
                        }
                    }

                    if (supplier.fileUpload3 != null)
                    {
                        string path = Server.MapPath("~/Uploads/Supplier/");
                        if (!Directory.Exists(path))
                            Directory.CreateDirectory(path);

                        string FileName = Path.GetFileName(supplier.fileUpload3.FileName);

                        Supplier sup = db.Suppliers.Find(supplier.SupplierId);

                        if (sup != null)
                        {
                            string saveName = Encryptor.ComputeHash(FileName, "MD5", null);
                            sup.fileOfAktaPendirian = saveName + ".pdf";
                            var SavePathAttachment = Path.Combine(path + saveName + ".pdf");
                            supplier.fileUpload3.SaveAs(SavePathAttachment);
                            db.SaveChanges();
                        }
                    }

                    if (supplier.fileUpload4 != null)
                    {
                        string path = Server.MapPath("~/Uploads/Supplier/");
                        if (!Directory.Exists(path))
                            Directory.CreateDirectory(path);

                        string FileName = Path.GetFileName(supplier.fileUpload4.FileName);

                        Supplier sup = db.Suppliers.Find(supplier.SupplierId);

                        if (sup != null)
                        {
                            string saveName = Encryptor.ComputeHash(FileName, "MD5", null);
                            sup.fileOfBankConfirmation = saveName + ".pdf";
                            var SavePathAttachment = Path.Combine(path + saveName + ".pdf");
                            supplier.fileUpload4.SaveAs(SavePathAttachment);
                            db.SaveChanges();
                        }
                    }

                    if (supplier.fileUpload5 != null)
                    {
                        string path = Server.MapPath("~/Uploads/Supplier/");
                        if (!Directory.Exists(path))
                            Directory.CreateDirectory(path);

                        string FileName = Path.GetFileName(supplier.fileUpload5.FileName);

                        Supplier sup = db.Suppliers.Find(supplier.SupplierId);

                        if (sup != null)
                        {
                            string saveName = Encryptor.ComputeHash(FileName, "MD5", null);
                            sup.fileOfBusinessPermit = saveName + ".pdf";
                            var SavePathAttachment = Path.Combine(path + saveName + ".pdf");
                            supplier.fileUpload5.SaveAs(SavePathAttachment);
                            db.SaveChanges();
                        }
                    }

                    if (supplier.fileUpload6 != null)
                    {
                        string path = Server.MapPath("~/Uploads/Supplier/");
                        if (!Directory.Exists(path))
                            Directory.CreateDirectory(path);

                        string FileName = Path.GetFileName(supplier.fileUpload6.FileName);

                        Supplier sup = db.Suppliers.Find(supplier.SupplierId);

                        if (sup != null)
                        {
                            string saveName = Encryptor.ComputeHash(FileName, "MD5", null);
                            sup.fileOfDomicile = saveName + ".pdf";
                            var SavePathAttachment = Path.Combine(path + saveName + ".pdf");
                            supplier.fileUpload6.SaveAs(SavePathAttachment);
                            db.SaveChanges();
                        }
                    }

                    if (supplier.fileUpload7 != null)
                    {
                        string path = Server.MapPath("~/Uploads/Supplier/");
                        if (!Directory.Exists(path))
                            Directory.CreateDirectory(path);

                        string FileName = Path.GetFileName(supplier.fileUpload7.FileName);

                        Supplier sup = db.Suppliers.Find(supplier.SupplierId);

                        if (sup != null)
                        {
                            string saveName = Encryptor.ComputeHash(FileName, "MD5", null);
                            sup.fileOfTaxID = saveName + ".pdf";
                            var SavePathAttachment = Path.Combine(path + saveName + ".pdf");
                            supplier.fileUpload7.SaveAs(SavePathAttachment);
                            db.SaveChanges();
                        }
                    }

                    if (supplier.fileUpload8 != null)
                    {
                        string path = Server.MapPath("~/Uploads/Supplier/");
                        if (!Directory.Exists(path))
                            Directory.CreateDirectory(path);

                        string FileName = Path.GetFileName(supplier.fileUpload8.FileName);

                        Supplier sup = db.Suppliers.Find(supplier.SupplierId);

                        if (sup != null)
                        {
                            string saveName = Encryptor.ComputeHash(FileName, "MD5", null);
                            sup.fileOfTaxableConfirmation = saveName + ".pdf";
                            var SavePathAttachment = Path.Combine(path + saveName + ".pdf");
                            supplier.fileUpload8.SaveAs(SavePathAttachment);
                            db.SaveChanges();
                        }
                    }

                    if (supplier.fileUpload9 != null)
                    {
                        string path = Server.MapPath("~/Uploads/Supplier/");
                        if (!Directory.Exists(path))
                            Directory.CreateDirectory(path);

                        string FileName = Path.GetFileName(supplier.fileUpload9.FileName);

                        Supplier sup = db.Suppliers.Find(supplier.SupplierId);

                        if (sup != null)
                        {
                            string saveName = Encryptor.ComputeHash(FileName, "MD5", null);
                            sup.fileOfSKTerdaftar = saveName + ".pdf";
                            var SavePathAttachment = Path.Combine(path + saveName + ".pdf");
                            supplier.fileUpload9.SaveAs(SavePathAttachment);
                            db.SaveChanges();
                        }
                    }

                    if (supplier.fileUpload10 != null)
                    {
                        string path = Server.MapPath("~/Uploads/Supplier/");
                        if (!Directory.Exists(path))
                            Directory.CreateDirectory(path);

                        string FileName = Path.GetFileName(supplier.fileUpload10.FileName);

                        Supplier sup = db.Suppliers.Find(supplier.SupplierId);

                        if (sup != null)
                        {
                            string saveName = Encryptor.ComputeHash(FileName, "MD5", null);
                            sup.fileOfStatTaxableEnterprise = saveName + ".pdf";
                            var SavePathAttachment = Path.Combine(path + saveName + ".pdf");
                            supplier.fileUpload10.SaveAs(SavePathAttachment);
                            db.SaveChanges();
                        }
                    }

                    if (supplier.fileUpload11 != null)
                    {
                        string path = Server.MapPath("~/Uploads/Supplier/");
                        if (!Directory.Exists(path))
                            Directory.CreateDirectory(path);

                        string FileName = Path.GetFileName(supplier.fileUpload11.FileName);

                        Supplier sup = db.Suppliers.Find(supplier.SupplierId);

                        if (sup != null)
                        {
                            string saveName = Encryptor.ComputeHash(FileName, "MD5", null);
                            sup.fileOfNonNPWP = saveName + ".pdf";
                            var SavePathAttachment = Path.Combine(path + saveName + ".pdf");
                            supplier.fileUpload11.SaveAs(SavePathAttachment);
                            db.SaveChanges();
                        }
                    }

                    if (supplier.fileUpload12 != null)
                    {
                        string path = Server.MapPath("~/Uploads/Supplier/");
                        if (!Directory.Exists(path))
                            Directory.CreateDirectory(path);

                        string FileName = Path.GetFileName(supplier.fileUpload12.FileName);

                        Supplier sup = db.Suppliers.Find(supplier.SupplierId);

                        if (sup != null)
                        {
                            string saveName = Encryptor.ComputeHash(FileName, "MD5", null);
                            sup.fileOfNonTaxable = saveName + ".pdf";
                            var SavePathAttachment = Path.Combine(path + saveName + ".pdf");
                            supplier.fileUpload12.SaveAs(SavePathAttachment);
                            db.SaveChanges();
                        }
                    }

                    if (supplier.fileUpload13 != null)
                    {
                        string path = Server.MapPath("~/Uploads/Supplier/");
                        if (!Directory.Exists(path))
                            Directory.CreateDirectory(path);

                        string FileName = Path.GetFileName(supplier.fileUpload13.FileName);

                        Supplier sup = db.Suppliers.Find(supplier.SupplierId);

                        if (sup != null)
                        {
                            string saveName = Encryptor.ComputeHash(FileName, "MD5", null);
                            sup.fileOfSKUKontsruksi = saveName + ".pdf";
                            var SavePathAttachment = Path.Combine(path + saveName + ".pdf");
                            supplier.fileUpload13.SaveAs(SavePathAttachment);
                            db.SaveChanges();
                        }
                    }

                    if (supplier.fileUpload14 != null)
                    {
                        string path = Server.MapPath("~/Uploads/Supplier/");
                        if (!Directory.Exists(path))
                            Directory.CreateDirectory(path);

                        string FileName = Path.GetFileName(supplier.fileUpload14.FileName);

                        Supplier sup = db.Suppliers.Find(supplier.SupplierId);

                        if (sup != null)
                        {
                            string saveName = Encryptor.ComputeHash(FileName, "MD5", null);
                            sup.fileOfBebasPotonganPajak = saveName + ".pdf";
                            var SavePathAttachment = Path.Combine(path + saveName + ".pdf");
                            supplier.fileUpload14.SaveAs(SavePathAttachment);
                            db.SaveChanges();
                        }
                    }

                    if (supplier.fileUpload15 != null)
                    {
                        string path = Server.MapPath("~/Uploads/Supplier/");
                        if (!Directory.Exists(path))
                            Directory.CreateDirectory(path);

                        string FileName = Path.GetFileName(supplier.fileUpload15.FileName);

                        Supplier sup = db.Suppliers.Find(supplier.SupplierId);

                        if (sup != null)
                        {
                            string saveName = Encryptor.ComputeHash(FileName, "MD5", null);
                            sup.scanOfDataForm = saveName + ".pdf";
                            var SavePathAttachment = Path.Combine(path + saveName + ".pdf");
                            supplier.fileUpload15.SaveAs(SavePathAttachment);
                            db.SaveChanges();
                        }
                    }

                    return RedirectToAction("Details", new
                    {
                        id = SupplierID
                    });
                }
                return View(supplier);
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UploadImg(Supplier supplier)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int SupplierID = supplier.SupplierId;

                    if (supplier.logoUpload != null)
                    {
                        string path = Server.MapPath("~/Content/img/supplier/");
                        if (!Directory.Exists(path))
                            Directory.CreateDirectory(path);

                        string FileName = Path.GetFileName(supplier.logoUpload.FileName);
                        string FileExt = Path.GetExtension(supplier.logoUpload.FileName);

                        Supplier sup = db.Suppliers.Find(supplier.SupplierId);

                        if (sup != null)
                        {
                            string saveName = "sp-logo-" + SupplierID + FileExt;
                            sup.SupplierLogo = saveName;
                            var SavePathAttachment = Path.Combine(path + saveName);
                            supplier.logoUpload.SaveAs(SavePathAttachment);
                            db.SaveChanges();
                        }
                    }

                    return RedirectToAction("Details", new
                    {
                        id = SupplierID
                    });
                }
                return View(supplier);
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }
    }
}
