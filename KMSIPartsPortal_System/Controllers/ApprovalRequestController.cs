﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using KMSIPartsPortal_System.Helpers;
using KMSIPartsPortal_System.Models;

namespace KMSIPartsPortal_System.Controllers
{
    public class ApprovalRequestController : Controller
    {
        private kmsi_portalEntities db = new kmsi_portalEntities();

        // GET: ApprovalRequest
        public ActionResult Index()
        {
            ViewBag.role = db.Roles.ToList();
            ViewBag.supplier = db.Suppliers.ToList();
            ViewBag.distributor = db.DistributorTypes.ToList();
            return View();
        }

        // GET: ApprovalRequest/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: ApprovalRequest/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ApprovalRequest/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: ApprovalRequest/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: ApprovalRequest/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: ApprovalRequest/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: ApprovalRequest/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult Approve(int id, User user)
        {
            try
            {
                // TODO: Add delete logic here
                User User = db.Users.Find(id);
                String Password = StringGenerator.RandomStringAlphanumeric(8);
                User.Password = Encryptor.ComputeHash(Password, "SHA512", null);
                User.UserRole = user.UserRole;
                User.SupplierId = user.SupplierId;
                User.DistributorType = user.DistributorType;
                User.IsActive = true;
                User.ActivatedAt = DateTime.Now;
                db.SaveChanges();

                // preparing email body
                int userId = Convert.ToInt32(Session["UserId"]);
                User approver = db.Users.Find(userId);
                string baseUrl = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~")) + "Account/Login";
                string body = ViewRenderer.RenderRazorViewToString(this, "~/Views/Mail/ApproveRequest.cshtml", new EmailAccount()
                {
                    Name = User.FirstName + " " + User.LastName,
                    Company = User.CompanyName,
                    Position = User.Position,
                    RequestDt = User.CreatedAt.HasValue ? User.CreatedAt.Value.ToString("yyyy-MM-dd") : "",
                    BaseUrl = baseUrl,
                    ApprovedDt = User.ActivatedAt.HasValue ? User.ActivatedAt.Value.ToString("yyyy-MM-dd") : "",
                    ApproverName = approver.FirstName + " " + approver.LastName,
                    Username = User.Username,
                    Password = Password
                });
                bool emailStatus = EmailHelper.SendEmail(User.Email, "Request Access Approved", body, "");

                if (emailStatus)
                {
                    Notification notification = new Notification();
                    notification.UserId = approver.UserId;
                    notification.Name = approver.FirstName + " " + approver.LastName;
                    notification.Module = "REQUESTACCESS";
                    notification.Message = "Your request access has been approved by " + notification.Name;
                    notification.Url = baseUrl;
                    notification.ReceiveId = User.UserId;
                    notification.CreatedAt = DateTime.Now;
                    db.Notifications.Add(notification);
                    db.SaveChanges();
                }

                TempData["message"] = "Success give access";
                return RedirectToAction("Index");
            }
            catch
            {

                TempData["message"] = "Failed give access";
                return RedirectToAction("Index");
            }
        }

        public ActionResult List()
        {
            try
            {
                // Creating instance of DatabaseContext class
                using (kmsi_portalEntities db = new kmsi_portalEntities())
                {
                    var draw = Request.Form.GetValues("draw").FirstOrDefault();
                    var start = Request.Form.GetValues("start").FirstOrDefault();
                    var length = Request.Form.GetValues("length").FirstOrDefault();
                    var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                    var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                    var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();

                    // Paging Size
                    int pageSize = length != null ? Convert.ToInt32(length) : 0;
                    int skip = start != null ? Convert.ToInt32(start) : 0;
                    int recordsTotal = 0;

                    // Getting all Purchase data
                    var userData = (from pd in db.Users
                                    where pd.IsActive == false
                                    select new
                                    {
                                        pd.UserId,
                                        pd.IsActive,
                                        pd.UserRole,
                                        pd.Username,
                                        pd.FirstName,
                                        pd.LastName,
                                        pd.Email
                                    });
                    // Sorting
                    if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                    {
                        userData = userData.OrderBy(sortColumn + " " + sortColumnDir);
                    }

                    // Search
                    if (!string.IsNullOrEmpty(searchValue))
                    {
                        userData = userData.Where(p => p.Username.ToLower().Contains(searchValue.ToLower())
                            || p.FirstName.ToLower().Contains(searchValue.ToLower())
                            || p.LastName.ToLower().Contains(searchValue.ToLower())
                            || p.Email.ToString().ToLower().Contains(searchValue.ToLower()));
                    }

                    // total number of rows count
                    recordsTotal = userData.Count();
                    // Paging
                    var data = userData.Skip(skip).Take(pageSize).ToList();
                    // Returning Json Data
                    return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }
    }
}
