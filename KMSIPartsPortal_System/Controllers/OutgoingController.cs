﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Web;
using System.Web.Mvc;
using iTextSharp.text;
using iTextSharp.text.pdf;
using KMSIPartsPortal_System.Helpers;
using KMSIPartsPortal_System.Models;
using OfficeOpenXml;

namespace KMSIPartsPortal_System.Controllers
{
    public class OutgoingController : Controller
    {
        private kmsi_portalEntities db = new kmsi_portalEntities();

        public ActionResult Purchase()
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    List<MenuModels> MenuMaster = (List<MenuModels>)Session["MenuMaster"];
                    Int32 RoleID = Convert.ToInt32(Session["RoleId"]);

                    if (RoleID == 0)
                    {
                        ViewBag.IsView = 1;
                        ViewBag.IsInput = 1;
                        ViewBag.IsUpdate = 1;
                        ViewBag.IsDelete = 1;
                        ViewBag.IsUpload = 1;
                        ViewBag.IsDownload = 1;
                    }
                    else
                    {
                        var access = MenuMaster.Where(x => x.ControllerName == "Outgoing" && x.ActionName == "Purchase" && x.RoleId == RoleID).FirstOrDefault();

                        ViewBag.IsView = access.IsView;
                        ViewBag.IsInput = access.IsInput;
                        ViewBag.IsUpdate = access.IsUpdate;
                        ViewBag.IsDelete = access.IsDelete;
                        ViewBag.IsUpload = access.IsUpload;
                        ViewBag.IsDownload = access.IsDownload;
                    }

                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult PurchaseList()
        {
            try
            {
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                var status = Request.Form.GetValues("status").FirstOrDefault();

                // Paging Size
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                Int32 IsComplete = Convert.ToInt32(status);

                // Getting all Purchase data
                var purchaseData = (from po in db.OutgoingPurchases
                             orderby po.PurchaseDate descending
                             group po by new { po.PurchaseNumber, po.PurchaseDate, po.PartNumber, po.PartName, po.SupplierNumber, po.SupplierName, po.AmountIDR } into poGroup
                             select new
                             {
                                 PurchaseNumber = poGroup.Key.PurchaseNumber,
                                 PurchaseDate = poGroup.Key.PurchaseDate,
                                 PartNumber = poGroup.Key.PartNumber,
                                 PartName = poGroup.Key.PartName,
                                 SupplierCode = poGroup.Key.SupplierNumber,
                                 SupplierName = poGroup.Key.SupplierName,
                                 AmountIDR = poGroup.Key.AmountIDR,
                                 OrderQty = (System.Int32?)
                                    (from x in db.OutgoingPurchases
                                     where x.PurchaseNumber == poGroup.Key.PurchaseNumber && x.PartNumber == poGroup.Key.PartNumber
                                     select new
                                     {
                                         OrderQty = x.Quantity
                                     }).FirstOrDefault().OrderQty ?? 0,
                                 DeliveryQty = (System.Int32?)
                                    (from x in db.DeliveryOrders
                                     where x.PurchaseNumber == poGroup.Key.PurchaseNumber && x.PartNumber == poGroup.Key.PartNumber
                                     group x by new
                                     {
                                         x.PurchaseNumber,
                                         x.PartNumber
                                     } into g
                                     select new
                                     {
                                         DeliveryQty = g.Sum(i => i.Quantity)
                                     }).FirstOrDefault().DeliveryQty ?? 0,
                                 InvoiceQty = (System.Int32?)
                                    (from x in db.OutgoingInvoices
                                     where x.PurchaseNumber == poGroup.Key.PurchaseNumber && x.PartNumber == poGroup.Key.PartNumber
                                     group x by new
                                     {
                                         x.PurchaseNumber,
                                         x.PartNumber
                                     } into g
                                     select new
                                     {
                                         InvoiceQty = g.Sum(i => i.Quantity)
                                     }).FirstOrDefault().InvoiceQty ?? 0
                             }).ToList().Select(x => new
                             {
                                 PurchaseNumber = x.PurchaseNumber,
                                 PurchaseDate = (string) x.PurchaseDate.Value.ToString("yyyy-MM-dd"),
                                 PartNumber = x.PartNumber,
                                 PartName = x.PartName,
                                 Amount = x.AmountIDR.HasValue ? x.AmountIDR.Value.ToString("C", CultureInfo.CreateSpecificCulture("id-ID")) : "",
                                 OrderQty = x.OrderQty,
                                 DeliveryQty = x.DeliveryQty,
                                 InvoiceQty = x.InvoiceQty,
                                 SupplierCode = x.SupplierCode,
                                 SupplierName = x.SupplierName
                             });

                //var purchaseData = db.OutgoingPurchases.AsEnumerable()
                //                .Join(db.Users, p => p.AcknowledgeBy, u => u.UserId, (p, u) => new
                //                {
                //                    ID = p.ID,
                //                    PurchaseNumber = p.PurchaseNumber,
                //                    PurchaseDate = p.PurchaseDate.HasValue ? p.PurchaseDate.Value.ToString("yyyy-MM-dd") : "",
                //                    SupplierCode = p.SupplierNumber,
                //                    SupplierName = p.SupplierName,
                //                    User = u.FirstName + " " + u.LastName
                //                })
                //                .GroupBy(x => x.PurchaseNumber)
                //                .Select(g => g.First());

                if (IsComplete == 1)
                {
                    purchaseData = purchaseData.Where(x => x.InvoiceQty == x.OrderQty);
                }
                else if (IsComplete == 0)
                {
                    purchaseData = purchaseData.Where(x => x.InvoiceQty < x.OrderQty);
                }

                // Sorting
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    purchaseData = purchaseData.OrderBy(sortColumn + " " + sortColumnDir);
                }
                // Search
                if (!string.IsNullOrEmpty(searchValue))
                {
                    purchaseData = purchaseData.Where(p => p.PurchaseNumber.ToLower().Contains(searchValue.ToLower())
                        || p.PurchaseDate.ToLower().Contains(searchValue.ToLower())
                        || p.PartNumber.ToLower().Contains(searchValue.ToLower())
                        || p.PartName.ToLower().Contains(searchValue.ToLower()));
                }

                // total number of rows count
                recordsTotal = purchaseData.Count();
                // Paging
                var data = purchaseData.Skip(skip).Take(pageSize).ToList();
                // Returning Json Data
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult Delivery()
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    List<MenuModels> MenuMaster = (List<MenuModels>)Session["MenuMaster"];
                    Int32 RoleID = Convert.ToInt32(Session["RoleId"]);

                    if (RoleID == 0)
                    {
                        ViewBag.IsView = 1;
                        ViewBag.IsInput = 1;
                        ViewBag.IsUpdate = 1;
                        ViewBag.IsDelete = 1;
                        ViewBag.IsUpload = 1;
                        ViewBag.IsDownload = 1;
                    }
                    else
                    {
                        var access = MenuMaster.Where(x => x.ControllerName == "Outgoing" && x.ActionName == "Delivery" && x.RoleId == RoleID).FirstOrDefault();

                        ViewBag.IsView = access.IsView;
                        ViewBag.IsInput = access.IsInput;
                        ViewBag.IsUpdate = access.IsUpdate;
                        ViewBag.IsDelete = access.IsDelete;
                        ViewBag.IsUpload = access.IsUpload;
                        ViewBag.IsDownload = access.IsDownload;
                    }

                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }
    }
}