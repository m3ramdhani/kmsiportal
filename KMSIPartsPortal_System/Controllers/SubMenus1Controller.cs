﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Web;
using System.Web.Mvc;
using KMSIPartsPortal_System.Models;

namespace KMSIPartsPortal_System.Controllers
{
    public class SubMenus1Controller : Controller
    {
        private kmsi_portalEntities db = new kmsi_portalEntities();

        // GET: SubMenus
        public ActionResult Index(int id)
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    ViewBag.RoleId = id;
                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return HttpNotFound();
            }
        }

        public ActionResult List()
        {
            try
            {
                // Creating instance of DatabaseContext class
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();

                // Paging Size
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                // Getting all data
                var menuLists = from s in db.SubMenus
                                select new
                                {
                                    s.SubMenuID,
                                    s.SubMenu1,
                                    s.Controller,
                                    s.Action,
                                    s.MainMenu.MainMenu1,
                                    s.RoleID,
                                    s.Role.RoleName
                                };

                var id = Request.Form.GetValues("id").FirstOrDefault();
                if (id != "")
                {
                    int RoleID = Convert.ToInt32(id);
                    menuLists = menuLists.Where(s => s.RoleID == RoleID);
                }

                // Sorting
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    menuLists = menuLists.OrderBy(sortColumn + " " + sortColumnDir);
                }
                // Search
                if (!string.IsNullOrEmpty(searchValue))
                {
                    menuLists = menuLists.Where(m => m.SubMenu1.ToLower().Contains(searchValue.ToLower())
                        || m.Controller.ToLower().Contains(searchValue.ToLower())
                        || m.Action.ToLower().Contains(searchValue.ToLower())
                        || m.MainMenu1.ToLower().Contains(searchValue.ToLower())
                    );
                }

                // total number of rows count
                recordsTotal = menuLists.Count();
                // Paging
                var data = menuLists.Skip(skip).Take(pageSize).ToList();

                // Returning Json Data
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return HttpNotFound();
            }
        }

        // GET: SubMenus/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SubMenu subMenu = db.SubMenus.Find(id);
            if (subMenu == null)
            {
                return HttpNotFound();
            }
            return View(subMenu);
        }

        // GET: SubMenus/Create
        public ActionResult Create(int id)
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    var menus = db.MenuLists
                        .Select(m => new
                        {
                            Value = m.ID,
                            Text = m.MainMenu.MainMenu1 + " - " + m.Menu_Name
                        })
                        .ToList();
                    ViewBag.id = id;
                    ViewBag.MenuID = new SelectList(menus, "Value", "Text");
                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return HttpNotFound();
            }
        }

        // POST: SubMenus/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(SubMenu subMenu)
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    Int32 RoleID = Convert.ToInt32(Request.Form.GetValues("RoleId").FirstOrDefault());

                    if (ModelState.IsValid)
                    {
                        Int32 MenuID = Convert.ToInt32(Request.Form.GetValues("MenuID").FirstOrDefault());
                        MenuList menu = new MenuList();
                        menu = db.MenuLists.Find(MenuID);

                        var subs = db.SubMenus.Where(s => s.SubMenu1 == menu.Menu_Name
                        && s.Controller == menu.Controller
                        && s.Action == menu.Action
                        && s.MainMenuID == menu.MainMenuID
                        && s.RoleID == RoleID).ToList();

                        if (subs.Count == 0)
                        {
                            subMenu.SubMenu1 = menu.Menu_Name;
                            subMenu.Controller = menu.Controller;
                            subMenu.Action = menu.Action;
                            subMenu.MainMenuID = menu.MainMenuID;
                            subMenu.RoleID = RoleID;

                            db.SubMenus.Add(subMenu);
                            db.SaveChanges();

                            return RedirectToAction("Index", new
                            {
                                id = RoleID
                            });
                        }                   
                    }

                    return RedirectToAction("Create", new
                    {
                        id = RoleID
                    });
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return HttpNotFound();
            }
        }

        // GET: SubMenus/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SubMenu subMenu = db.SubMenus.Find(id);
            if (subMenu == null)
            {
                return HttpNotFound();
            }
            ViewBag.MainMenuID = new SelectList(db.MainMenus, "MenuID", "MainMenu1", subMenu.MainMenuID);
            ViewBag.RoleID = new SelectList(db.Roles, "RoleId", "RoleName", subMenu.RoleID);
            return View(subMenu);
        }

        // POST: SubMenus/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SubMenuID,SubMenu1,Controller,Action,MainMenuID,RoleID")] SubMenu subMenu)
        {
            if (ModelState.IsValid)
            {
                db.Entry(subMenu).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.MainMenuID = new SelectList(db.MainMenus, "MenuID", "MainMenu1", subMenu.MainMenuID);
            ViewBag.RoleID = new SelectList(db.Roles, "RoleId", "RoleName", subMenu.RoleID);
            return View(subMenu);
        }

        // GET: SubMenus/Delete/5
        public ActionResult Delete(int? id, int? role)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SubMenu subMenu = db.SubMenus.Find(id);
            if (subMenu == null)
            {
                return HttpNotFound();
            }

            db.SubMenus.Remove(subMenu);
            db.SaveChanges();
            return RedirectToAction("Index", new
            {
                id = role
            });
        }

        // POST: SubMenus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SubMenu subMenu = db.SubMenus.Find(id);
            db.SubMenus.Remove(subMenu);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
