﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using KMSIPartsPortal_System.Models;

namespace KMSIPartsPortal_System.Controllers
{
    public class ContractsController : Controller
    {
        private kmsi_portalEntities db = new kmsi_portalEntities();

        // GET: Contracts
        public ActionResult Index()
        {
            if (Session["UserId"] != null)
            {
                return View(db.Contracts.GroupBy(x => x.ContractCode).Select(x => x.FirstOrDefault()));
            }
            else
            {
                return RedirectToAction("Login", "Action");
            }
        }

        // GET: Contracts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contract contract = db.Contracts.Find(id);
            if (contract == null)
            {
                return HttpNotFound();
            }
            return View(contract);
        }

        // GET: Contracts/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Contracts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ContractId,ContractCode,SMRContract,USDperHours,AmountContract")] Contract contract)
        {
            if (ModelState.IsValid)
            {
                db.Contracts.Add(contract);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(contract);
        }

        // GET: Contracts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contract contract = db.Contracts.Find(id);
            if (contract == null)
            {
                return HttpNotFound();
            }
            return View(contract);
        }

        // POST: Contracts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ContractId,ContractCode,SMRContract,USDperHours,AmountContract")] Contract contract)
        {
            if (ModelState.IsValid)
            {
                db.Entry(contract).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(contract);
        }

        // GET: Contracts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contract contract = db.Contracts.Find(id);
            if (contract == null)
            {
                return HttpNotFound();
            }
            return View(contract);
        }

        // POST: Contracts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Contract contract = db.Contracts.Find(id);
            db.Contracts.Remove(contract);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpPost]
        public JsonResult GetSearchUnit(string Prefix)
        {
            kmsi_portalEntities db = new kmsi_portalEntities();
            var contracts = db.Contracts
                    .Join(db.Customers,
                        contract => contract.CustomerId,
                        customer => customer.CustomerCode,
                        (contract, customer) => new { Contract = contract, Customer = customer }).ToList();

            if (string.IsNullOrEmpty(Prefix))
            {
                contracts = db.Contracts
                    .Join(db.Customers,
                        contract => contract.CustomerId,
                        customer => customer.CustomerCode,
                        (contract, customer) => new { Contract = contract, Customer = customer }).ToList();
            }
            else
            {
                //contracts = db.Contracts.Where(x => x.UnitSerialNumber.Contains(Prefix)).ToList();
                contracts = db.Contracts
                    .Join(db.Customers,
                        contract => contract.CustomerId,
                        customer => customer.CustomerCode,
                        (contract, customer) => new { Contract = contract, Customer = customer })
                    .Where(x => x.Contract.UnitSerialNumber.Contains(Prefix)).ToList();
            }

            return new JsonResult { Data = contracts, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public FileResult DownloadFormat()
        {
            string path = "/Docs/Contract_Format.xlsx";
            return File(path, "application/vnd.ms-excel", "Contract_Format.xlsx");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [HandleError]
        public ActionResult Import(HttpPostedFileBase file)
        {
            string filePath = string.Empty;
            if (file != null)
            {
                string path = Server.MapPath("~/Uploads/");
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

                filePath = path + Path.GetFileName(file.FileName);
                string extension = Path.GetExtension(file.FileName).ToLower();
                file.SaveAs(filePath);

                string conString = string.Empty;
                switch (extension)
                {
                    case ".xls": //Excel 97-03.
                        conString = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                        break;

                    case ".xlsx": //Excel 07 and above.
                        conString = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                        break;
                }

                try
                {
                    DataTable dt = new DataTable();
                    conString = string.Format(conString, filePath);

                    using (OleDbConnection connExcel = new OleDbConnection(conString))
                    {
                        using (OleDbCommand cmdExcel = new OleDbCommand())
                        {
                            using (OleDbDataAdapter odaExcel = new OleDbDataAdapter())
                            {
                                cmdExcel.Connection = connExcel;

                                //Get the name of First Sheet.
                                connExcel.Open();
                                DataTable dtExcelSchema;
                                dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                                string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                                connExcel.Close();

                                //Read Data from First Sheet.
                                connExcel.Open();
                                cmdExcel.CommandText = "SELECT * FROM [" + sheetName + "]";
                                odaExcel.SelectCommand = cmdExcel;
                                odaExcel.Fill(dt);
                                connExcel.Close();
                            }
                        }
                    }

                    conString = ConfigurationManager.ConnectionStrings["Constring"].ConnectionString;
                    using (SqlConnection con = new SqlConnection(conString))
                    {
                        using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                        {
                            // Set the database table name.
                            sqlBulkCopy.DestinationTableName = "dbo.Contract";

                            //[OPTIONAL]: Map teh Excel columns with that of the database table
                            sqlBulkCopy.ColumnMappings.Add("Contract Code", "ContractCode");
                            sqlBulkCopy.ColumnMappings.Add("Unit Serial Number", "UnitSerialNumber");
                            sqlBulkCopy.ColumnMappings.Add("Unit Model", "UnitModel");
                            sqlBulkCopy.ColumnMappings.Add("Unit Code", "UnitCode");
                            sqlBulkCopy.ColumnMappings.Add("Start Date", "ContractStartDate");
                            sqlBulkCopy.ColumnMappings.Add("End Date", "ContractEndDate");
                            sqlBulkCopy.ColumnMappings.Add("Total SMR Contract", "SMRContract");
                            sqlBulkCopy.ColumnMappings.Add("USD / Hours", "USDperHours");
                            sqlBulkCopy.ColumnMappings.Add("Customer ID", "CustomerId");

                            con.Open();
                            sqlBulkCopy.WriteToServer(dt);
                            con.Close();
                        }
                    }

                    ViewBag.Message = "Success Import";
                    ModelState.Clear();
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            return View();
        }

        public DataTable ImportToGrid(string FilePath, string Extension, string isHDR)
        {
            DataTable dt = new DataTable();
            try
            {
                string conString = string.Empty;
                switch (Extension)
                {
                    case ".xls": //Excel 97-03.
                        conString = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                        break;

                    case ".xlsx": //Excel 07 and above.
                        conString = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                        break;
                }
                conString = String.Format(conString, FilePath, isHDR);
                OleDbConnection conExcel = new OleDbConnection(conString);
                OleDbCommand cmdExcel = new OleDbCommand();
                OleDbDataAdapter oda = new OleDbDataAdapter();
                cmdExcel.Connection = conExcel;

                //Get the name of First Sheet
                conExcel.Open();
                DataTable dtExcelSchema;
                dtExcelSchema = conExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                string SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                conExcel.Close();

                //Read Data from First Sheet
                conExcel.Open();
                cmdExcel.CommandText = "SELECT * FROM [" + SheetName + "]";
                oda.SelectCommand = cmdExcel;
                oda.Fill(dt);
                conExcel.Close();
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();
            }
            return dt;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [HandleError]
        public ActionResult UploadExcel(Contract contract, HttpPostedFileBase file)
        {
            TempData["ActionMessage"] = "Upload failed";
            try
            {
                var data = new List<string>();
                if (file != null)
                {
                    if (file.ContentType == "application/vnd.ms-excel" || file.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                    {
                        var fileName = file.FileName;
                        var targetPath = Server.MapPath("~/Uploads/");
                        file.SaveAs(targetPath + fileName);
                        var pathToExcelFile = targetPath + fileName;
                        string FileName = Path.GetFileName(file.FileName);
                        string Extension = Path.GetExtension(file.FileName);
                        DataTable dataTable = ImportToGrid(pathToExcelFile, Extension, "Yes");
                        List<Contract> contracts = new List<Contract>();
                        foreach (DataRow item in dataTable.Rows)
                        {
                            try
                            {
                                var ContractCode = item["CONTRACT CODE"].ToString();
                                var UnitModel = item["UNIT MODEL"].ToString();
                                var UnitCode = item["UNIT CODE"].ToString();
                                var UnitSerialNumber = item["UNIT SERIAL NUMBER"].ToString();
                                var StartDate = Convert.ToDateTime(item["START DATE"]);
                                var EndDate = Convert.ToDateTime(item["END DATE"]);
                                var isExist = db.Contracts.Where(x => x.ContractCode == ContractCode
                                                && x.UnitModel == UnitModel
                                                && x.UnitCode == UnitCode
                                                && x.UnitSerialNumber == UnitSerialNumber
                                                && x.ContractStartDate == StartDate
                                                && x.ContractEndDate == EndDate
                                              ).FirstOrDefault();
                                if (isExist == null)
                                {
                                    contract.CustomerId = item["CUSTOMER ID"].ToString();
                                    contract.ContractCode = ContractCode;
                                    contract.UnitSerialNumber = UnitSerialNumber;
                                    contract.UnitModel = UnitModel;
                                    contract.UnitCode = UnitCode;
                                    contract.ContractStartDate = StartDate;
                                    contract.ContractEndDate = EndDate;
                                    contract.SMRContract = Convert.ToDecimal(item["TOTAL SMR CONTRACT"]);
                                    contract.USDperHours = Convert.ToDecimal(item["USD / HOURS"]);
                                    db.Contracts.Add(contract);
                                    db.SaveChanges();

                                    TempData["ActionMessage"] = "Upload Success";
                                }
                            }
                            catch (Exception ex)
                            {
                                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                                var sw = new System.IO.StreamWriter(filename, true);
                                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                                sw.Close();
                            }
                        }

                        if (System.IO.File.Exists(pathToExcelFile))
                        {
                            System.IO.File.Delete(pathToExcelFile);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();
            }

            return RedirectToAction("Index");
        }
    }
}
