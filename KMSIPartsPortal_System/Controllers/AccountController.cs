﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using KMSIPartsPortal_System.Helpers;
using KMSIPartsPortal_System.Models;
using System.Web.Security;
using Newtonsoft.Json;
using System.Net;
using System.Data.Entity.Validation;

namespace KMSIPartsPortal_System.Controllers
{
    public class AccountController : Controller
    {
        private ApplicationUserManager _userManager;
        private kmsi_portalEntities db = new kmsi_portalEntities();

        // GET: Account
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List()
        {
            try
            {
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();

                // Paging Size
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                // Getting all data
                var accountData = db.Users.AsEnumerable()
                    .GroupJoin(db.Roles, u => u.UserRole, r => r.RoleId, (u, r) => new { u, r }).SelectMany(x => x.r.DefaultIfEmpty(), (x, rol) => new { x.u, rol })
                    .GroupJoin(db.Suppliers, u => u.u.SupplierId, s => s.SupplierId, (u, s) => new { u, s }).SelectMany(x => x.s.DefaultIfEmpty(), (x, sup) => new { x.u, sup })
                    .Select(x => new
                    {
                        x.u.u.UserId,
                        x.u.u.Username,
                        Name = x.u.u.FirstName + " " + x.u.u.LastName,
                        x.u.u.Email,
                        SupplierName = (x.sup != null) ? x.sup.SupplierName : "",
                        RoleName = (x.u.rol != null) ? x.u.rol.RoleName : "",
                        x.u.u.DistributorType,
                        Status = x.u.u.IsActive == true ? "Active" : "Inactive"
                    });

                // Sorting
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    accountData = accountData.OrderBy(sortColumn + " " + sortColumnDir);
                }
                // Search
                if (!string.IsNullOrEmpty(searchValue))
                {
                    accountData = accountData.Where(x => x.Username.ToLower().Contains(searchValue.ToLower())
                        || x.Name.ToLower().Contains(searchValue.ToLower())
                        || x.Email.ToLower().Contains(searchValue.ToLower())
                        || x.SupplierName.ToLower().Contains(searchValue.ToLower())
                        || x.RoleName.ToLower().Contains(searchValue.ToLower())
                        || x.DistributorType.ToLower().Contains(searchValue.ToLower())
                        || x.Status.ToLower().Contains(searchValue.ToLower())
                    );
                }

                // total number of rows count
                recordsTotal = accountData.Count();
                // Paging
                var data = accountData.Skip(skip).Take(pageSize).ToList();
                // Returning Json Data
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        // GET: Account
        public ActionResult Edit(int? id)
        {
            var suppliers = db.Suppliers.ToList();
            if (suppliers != null)
            {
                ViewBag.data = suppliers;
            }

            User dataUser = db.Users.Where(x => x.UserId == id).FirstOrDefault();
            
            ViewBag.role = db.Roles.ToList();
            ViewBag.distributor = db.DistributorTypes.ToList();
            ViewBag.User = dataUser;
            return View();
        }

        public ActionResult EditAction(FormCollection collection)
        {
            TempData["ActionMessage"] = "Update failed";
            try
            {
                int id = Convert.ToInt32(Request.Form.Get("Id"));
                int SupplierId = Convert.ToInt32(Request.Form.Get("SupplierId"));
                int UserRole = Convert.ToInt32(Request.Form.Get("UserRole"));
                string DistributorType = Request.Form.Get("DistributorType");
                string FirstName = Request.Form.Get("FirstName");
                string LastName = Request.Form.Get("LastName");
                string Email = Request.Form.Get("Email");
                string Username = Request.Form.Get("Username"); 
                string IsActive = Request.Form.Get("IsActive");
                string ConfirmPassword = Request.Form.Get("ConfirmPassword");
                string Password = Request.Form.Get("Password");

                User user = db.Users.Where(x => x.UserId == id).FirstOrDefault();
                if(Password != "")
                {
                    if (ConfirmPassword != Password)
                    {
                        TempData["ActionMessage"] = "Password tidak sama dengan confirm password";
                        return RedirectToAction("Edit/" + id, "Account");
                    }
                    else
                    {
                        user.Password = Encryptor.ComputeHash(Password, "SHA512", null); ;
                    }
                }

                user.FirstName = FirstName;
                user.LastName = LastName;
                user.SupplierId = SupplierId;
                user.UserRole = UserRole;
                user.Email = Email;
                user.Username = Username;
                user.DistributorType = DistributorType;
                if(IsActive == "ACTIVE")
                {
                    user.IsActive = true;
                }
                else
                {
                    user.IsActive = false;
                }
                db.SaveChanges();

                TempData["ActionMessage"] = "Update Success";
                return RedirectToAction("Index", "Account");
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult Delete(int? id)
        {
            TempData["ActionMessage"] = "Delete failed";
            try { 
                User user = db.Users.Where(x=> x.UserId == id).FirstOrDefault();
                db.Users.Remove(user);
                db.SaveChanges();

                TempData["ActionMessage"] = "Delete Success";
                return RedirectToAction("Index", "Account");
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        // GET: Account
        public ActionResult Detail(int? id)
        {
            var suppliers = db.Suppliers.ToList();
            if (suppliers != null)
            {
                ViewBag.data = suppliers;
            }

            User dataUser = db.Users.Where(x => x.UserId == id).FirstOrDefault();

            ViewBag.role = db.Roles.ToList();
            ViewBag.User = dataUser;
            return View();
        }

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.Slider = db.Sliders.ToList();
            ViewBag.role = db.Roles.ToList();
            ViewBag.returnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public String Login(LoginModels userModel, string returnUrl)
        {
            try
            {
                if (userModel.UserName != null && userModel.Password != null)
                {
                    bool isExist = false;
                    isExist = db.Users.Where(x => x.Username.Trim().ToLower() == userModel.UserName.Trim().ToLower() && x.IsActive).Any();
                    if (isExist)
                    {
                        var userDetails = db.Users.Where(x => x.Username.Equals(userModel.UserName) && x.IsActive).ToArray().SingleOrDefault(x => Encryptor.VerifyHash(userModel.Password, "SHA512", x.Password) == true);

                        if (userDetails == null)
                        {
                            TempData["status"] = false;
                            TempData["message"] = "Wrong username or password.";
                            TempData["url"] = null;
                            return JsonConvert.SerializeObject(TempData);
                        }
                        else
                        {
                            if (userDetails.SupplierId != 0)
                            {
                                Supplier supplier = db.Suppliers.SingleOrDefault(s => s.SupplierId == userDetails.SupplierId);
                                Session["SupplierCode"] = supplier.SupplierCode;
                            }

                            LoginModels _loginCredentials = db.Users.Where(x => x.Username.Trim().ToLower() == userModel.UserName.Trim().ToLower()).Select(x => new LoginModels
                            {
                                UserName = x.Username,
                                RoleName = x.Role.RoleName,
                                UserRoleId = x.UserRole,
                                UserId = x.UserId,
                                RoleType = x.Role.RoleType.TypeName
                            }).FirstOrDefault();

                            List<MenuModels> _menus = new List<MenuModels>();

                            if (userDetails.UserRole == null)
                            {
                                _menus = db.SubMenus.Select(x => new MenuModels
                                {
                                    ModuleId = x.MainMenu.ModuleID,
                                    ModuleName = x.MainMenu.Module.ModuleName.ToUpper(),
                                    MainMenuId = x.MainMenuID,
                                    MainMenuName = x.MainMenu.MenuName.ToUpper(),
                                    SubMenuId = x.ID,
                                    SubMenuName = x.SubMenuName.ToUpper(),
                                    ControllerName = x.Controller,
                                    ActionName = x.Link
                                }).ToList();
                            }
                            else
                            {
                                _menus = db.SidebarMenus.Where(x => x.RoleID == _loginCredentials.UserRoleId).Select(x => new MenuModels
                                {
                                    ModuleId = x.SubMenu.MainMenu.ModuleID,
                                    ModuleName = x.SubMenu.MainMenu.Module.ModuleName,
                                    MainMenuId = x.SubMenu.MainMenuID,
                                    MainMenuName = x.SubMenu.MainMenu.MenuName.ToUpper(),
                                    SubMenuId = x.MenuID,
                                    SubMenuName = x.SubMenu.SubMenuName.ToUpper(),
                                    ControllerName = x.SubMenu.Controller,
                                    ActionName = x.SubMenu.Link,
                                    RoleId = x.RoleID,
                                    RoleName = x.Role.RoleName,
                                    IsInput = x.IsInput,
                                    IsUpdate = x.IsUpdate,
                                    IsDelete = x.IsDelete,
                                    IsView = x.IsView,
                                    IsUpload = x.IsUpload,
                                    IsDownload = x.IsDownload
                                }).ToList();
                            }

                            FormsAuthentication.SetAuthCookie(_loginCredentials.UserName, false);
                            Session["UserId"] = userDetails.UserId;
                            Session["FirstName"] = userDetails.FirstName.ToUpper();
                            Session["RoleId"] = userDetails.UserRole;
                            Session["RoleName"] = _loginCredentials.RoleName;
                            Session["SupplierId"] = userDetails.SupplierId;
                            Session["Email"] = userDetails.Email;
                            Session["LoginCredentials"] = _loginCredentials;
                            Session["MenuMaster"] = _menus;
                            Session["UserName"] = _loginCredentials.UserName.ToUpper();
                            Session["RoleType"] = _loginCredentials.RoleType;

                            if (_loginCredentials.RoleType == "Internal")
                            {
                                TempData["status"] = true;
                                TempData["message"] = "OK";
                                TempData["url"] = (returnUrl != "") ? returnUrl : "/Dashboard/Internal";
                                return JsonConvert.SerializeObject(TempData);
                            }
                            else
                            {
                                if (_loginCredentials.UserRoleId == 2)
                                {
                                    Session["ModuleId"] = 2;

                                    TempData["status"] = true;
                                    TempData["message"] = "OK";
                                    TempData["url"] = (returnUrl != "") ? returnUrl : "/Dashboard/Supplier";
                                    return JsonConvert.SerializeObject(TempData);
                                }
                                else if (_loginCredentials.UserRoleId == 3)
                                {
                                    Session["ModuleId"] = 3;

                                    TempData["status"] = true;
                                    TempData["message"] = "OK";
                                    TempData["url"] = (returnUrl != "") ? returnUrl : "/Dashboard/Distributor";
                                    return JsonConvert.SerializeObject(TempData);
                                }
                                else if (_loginCredentials.UserRoleId == 8)
                                {
                                    Session["ModuleId"] = 1;

                                    TempData["status"] = true;
                                    TempData["message"] = "OK";
                                    TempData["url"] = (returnUrl != "") ? returnUrl : "/Dashboard/UtFmc";
                                    return JsonConvert.SerializeObject(TempData);
                                }
                                else if (_loginCredentials.UserRoleId == 7 || _loginCredentials.UserRoleId == 9 || _loginCredentials.UserRoleId == 10 || _loginCredentials.UserRoleId == 11 || _loginCredentials.UserRoleId == 12)
                                {
                                    Session["ModuleId"] = 4;

                                    TempData["status"] = true;
                                    TempData["message"] = "OK";
                                    TempData["url"] = (returnUrl != "") ? returnUrl : "/Dashboard/Ggi";
                                    return JsonConvert.SerializeObject(TempData);
                                }
                            }

                            if (_loginCredentials.RoleType == null)
                            {
                                TempData["status"] = true;
                                TempData["message"] = "OK";
                                TempData["url"] = (returnUrl != "") ? returnUrl : "/Dashboard/Internal";
                                return JsonConvert.SerializeObject(TempData);
                            }

                            TempData["status"] = true;
                            TempData["message"] = "OK";
                            TempData["url"] = (returnUrl != "") ? returnUrl : "/Dashboard/Internal";
                            return JsonConvert.SerializeObject(TempData);
                        }
                    }
                    else
                    {
                        ViewBag.LoginErrorMessage = "User not found.";

                        TempData["status"] = false;
                        TempData["message"] = "User not found.";
                        TempData["url"] = null;
                        return JsonConvert.SerializeObject(TempData);
                    }
                }
                else
                {
                    TempData["status"] = false;
                    TempData["message"] = "Please input username and password";
                    TempData["url"] = null;
                    return JsonConvert.SerializeObject(TempData);
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                TempData["status"] = false;
                TempData["message"] = "Error. Please check your connection";
                return JsonConvert.SerializeObject(TempData);
            }
        }

        public ActionResult Register()
        {
            using (kmsi_portalEntities db = new kmsi_portalEntities())
            {
                var suppliers = db.Suppliers.ToList();
                if (suppliers != null)
                {
                    ViewBag.data = suppliers;
                }

                ViewBag.role = db.Roles.ToList();
                ViewBag.distributor = db.DistributorTypes.ToList();

                return View();
            }
        }

        [HttpPost]
        public ActionResult Register(User model)
        {
            try
            {
                using (kmsi_portalEntities db = new kmsi_portalEntities())
                {
                    model.Password = Encryptor.ComputeHash(model.Password, "SHA512", null);
                    model.IsActive = true;
                    db.Users.Add(model);
                    db.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            Session.Clear();
            Session.RemoveAll();
            return RedirectToAction("Login", "Account");
        }

        public String SendEmailForgotPassword(FormCollection collection)
        {
            try
            {
                string baseUrl = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                string code = StringGenerator.RandomStringAlphanumeric(30);
                string Url_Forgot = baseUrl + "Account/ForgotPassword?code=" + code;
                var Email = Request.Form.Get("Email");

                User User = db.Users.Where(p => p.Email == Email).FirstOrDefault();
                User.CodeForgotPass = code;
                db.SaveChanges();

                string body = ViewRenderer.RenderRazorViewToString(this, "~/Views/Mail/ForgotPassword.cshtml", new EmailAccount()
                {
                    Name = User.FirstName + " " + User.LastName,
                    Company = User.CompanyName,
                    Position = User.Position,
                    RequestDt = "",
                    BaseUrl = Url_Forgot,
                    ApprovedDt = "",
                    ApproverName = "",
                    Username = "",
                    Password = ""
                });
                bool emailStat = EmailHelper.SendEmail(Email, "User Forgot Password", body, "");

                if (emailStat == true)
                {
                    TempData["status"] = true;
                    TempData["message"] = "Check your email. We send you forgot password link.";
                }
                else
                {
                    TempData["status"] = false;
                    TempData["message"] = "Failed send email. Please check your email";
                }

                return JsonConvert.SerializeObject(TempData);
            }
            catch
            {
                TempData["status"] = false;
                TempData["message"] = "Error. Please check your connection";
                return JsonConvert.SerializeObject(TempData);
            }
        }

        public ActionResult ForgotPassword(String code)
        {
            ViewBag.Slider = db.Sliders.ToList();
            ViewBag.Code = code;

            User User = db.Users.Where(p => p.CodeForgotPass == code).FirstOrDefault();
            if (User == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                return View();
            }
        }


        public String ChangePassword(FormCollection collection)
        {
            try
            {
                var Code = Request.Form.Get("Code");
                var Password = Request.Form.Get("Password");
                User User = db.Users.Where(p => p.CodeForgotPass == Code).FirstOrDefault();
                User.Password = Encryptor.ComputeHash(Password, "SHA512", null);
                User.CodeForgotPass = null;
                db.SaveChanges();

                string baseUrl = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                TempData["status"] = true;
                TempData["message"] = "Success Change Password";
                TempData["url"] = baseUrl + "Account/Login";
                return JsonConvert.SerializeObject(TempData);
            }
            catch
            {
                TempData["status"] = false;
                TempData["message"] = "Failed change password";
                return JsonConvert.SerializeObject(TempData);
            }
        }

        public String RequestAccess(FormCollection collection)
        {
            try
            {
                var Username = Request.Form.Get("Username");
                var Email = Request.Form.Get("Email");
                var checkUserEmail = db.Users.Where(p => p.Email == Email).FirstOrDefault();
                var checkUserUsername = db.Users.Where(p => p.Username == Username).FirstOrDefault();

                if (checkUserEmail != null) {
                    TempData["status"] = true;
                    TempData["message"] = "Email already used by other user";
                    return JsonConvert.SerializeObject(TempData);
                }

                if (checkUserUsername != null)
                {
                    TempData["status"] = true;
                    TempData["message"] = "Username already used by other user";
                    return JsonConvert.SerializeObject(TempData);
                }

                User User = new User();
                User.FirstName = Request.Form.Get("FirstName");
                User.LastName = Request.Form.Get("LastName");
                //String Password = StringGenerator.RandomStringAlphanumeric(8);
                //User.Password = Encryptor.ComputeHash(Password, "SHA512", null);
                //User.Password = Request.Form.Get("Password");
                User.Username = Request.Form.Get("Username");
                User.Email = Request.Form.Get("Email");
                User.CompanyName = Request.Form.Get("CompanyName");
                User.Position = Request.Form.Get("Position");
                User.Phone = Request.Form.Get("Phone");
                User.SupplierId = 0;
                User.IsActive = false;
                User.CreatedAt = DateTime.Now;
                db.Users.Add(User);
                db.SaveChanges();

                // preparing email body
                int userId = Convert.ToInt32(Session["UserId"]);
                User user = db.Users.Find(userId);
                List<int?> roleIds = new List<int?> { 13 };
                List<User> userLists = db.Users.Where(x => roleIds.Contains(x.UserRole)).ToList();

                foreach (var data in userLists)
                {
                    string baseUrl = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~")) + "ApprovalRequest/Index";
                    string body = ViewRenderer.RenderRazorViewToString(this, "~/Views/Mail/RequestAccess.cshtml", new EmailAccount()
                    {
                        Name = User.FirstName + " " + User.LastName,
                        Company = User.CompanyName,
                        Position = User.Position,
                        RequestDt = User.CreatedAt.HasValue ? User.CreatedAt.Value.ToString("yyyy-MM-dd") : "",
                        BaseUrl = baseUrl,
                        ApprovedDt = "",
                        ApproverName = "",
                        Username = "",
                        Password = ""
                    });
                    bool emailStatus = EmailHelper.SendEmail(data.Email, "New Request Access", body, "");

                    if (emailStatus)
                    {
                        Notification notification = new Notification();
                        notification.UserId = user.UserId;
                        notification.Name = user.FirstName + " " + user.LastName;
                        notification.Module = "REQUESTACCESS";
                        notification.Message = "New user request access from " + notification.Name;
                        notification.Url = baseUrl;
                        notification.ReceiveId = data.UserId;
                        notification.CreatedAt = DateTime.Now;
                        db.Notifications.Add(notification);
                        db.SaveChanges();
                    }
                }

                TempData["status"] = true;
                TempData["message"] = "Success Send Request Access";
                return JsonConvert.SerializeObject(TempData);
            }
            catch(DbEntityValidationException ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);

                foreach (var eve in ex.EntityValidationErrors)
                {
                    sw.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sw.WriteLine("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                }
                sw.Close();

                TempData["status"] = false;
                TempData["message"] = "Failed Send Request Access";
                return JsonConvert.SerializeObject(TempData);
            }
        }
    }
}