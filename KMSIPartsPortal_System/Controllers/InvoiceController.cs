﻿using KMSIPartsPortal_System.Models;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace KMSIPartsPortal_System.Controllers
{
    public class InvoiceController : Controller
    {
        private kmsi_portalEntities db = new kmsi_portalEntities();

        // GET: Invoice
        public ActionResult Index()
        {
            if (Session["UserId"] != null)
            {
                var supplierId = Convert.ToInt32(Session["SupplierId"]);
                if (supplierId > 0)
                    ViewData["Supplier"] = db.Suppliers.Where(p => p.SupplierId == supplierId).First();

                return View();
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        public ActionResult List()
        {
            try
            {
                // Creating instance of DatabaseContext class
                using (kmsi_portalEntities db = new kmsi_portalEntities())
                {
                    var draw = Request.Form.GetValues("draw").FirstOrDefault();
                    var start = Request.Form.GetValues("start").FirstOrDefault();
                    var length = Request.Form.GetValues("length").FirstOrDefault();
                    var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                    var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                    var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();

                    // Paging Size
                    int pageSize = length != null ? Convert.ToInt32(length) : 0;
                    int skip = start != null ? Convert.ToInt32(start) : 0;
                    int recordsTotal = 0;

                    // Getting all Purchase data
                    var purchaseData = (from i in db.PurchaseInvoiceDetails
                                        join inv in db.PurchaseInvoices on i.InvoiceNumber equals inv.InvoiceNumber
                                        orderby i.PurchaseInvoiceDetailId descending
                                        select new
                                        {
                                            i.PurchaseInvoiceDetailId,
                                            i.SupplierCode,
                                            i.InvoiceDate,
                                            i.InvoiceNumber,
                                            i.DeliveryOrder,
                                            i.PurchaseNumber,
                                            i.ItemNumber,
                                            i.PartNumberKomatsu,
                                            i.ShipQty,
                                            i.UnitPrice,
                                            i.CountryOfOrigin,
                                            inv.FixedStatus
                                        });

                    var supplierCode = (string)Session["SupplierCode"];
                    if (!string.IsNullOrEmpty(supplierCode))
                        purchaseData = purchaseData.Where(p => p.SupplierCode.Equals(supplierCode));

                    // Sorting
                    if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                    {
                        purchaseData = purchaseData.OrderBy(sortColumn + " " + sortColumnDir);
                    }
                    // Search
                    if (!string.IsNullOrEmpty(searchValue))
                    {
                        purchaseData = purchaseData.Where(p => 
                                            p.SupplierCode.ToLower().Contains(searchValue.ToLower()) || 
                                            p.InvoiceNumber.ToLower().Contains(searchValue.ToLower()) || 
                                            p.PurchaseNumber.ToLower().Contains(searchValue.ToLower()) || 
                                            p.UnitPrice.ToString().ToLower().Contains(searchValue.ToLower()) || 
                                            p.ItemNumber.ToString().ToLower().Contains(searchValue.ToLower()) || 
                                            p.PartNumberKomatsu.ToLower().Contains(searchValue.ToLower()) ||
                                            p.CountryOfOrigin.ToLower().Contains(searchValue.ToLower()));
                    }

                    // total number of rows count
                    recordsTotal = purchaseData.Count();
                    // Paging
                    var data = purchaseData.Skip(skip).Take(pageSize).ToList();
                    // Returning Json Data
                    return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ActionResult Detail()
        {
            if (Session["UserId"] != null)
            {
                var supplierId = Convert.ToInt32(Session["SupplierId"]);
                if (supplierId > 0)
                    ViewData["Supplier"] = db.Suppliers.Where(p => p.SupplierId == supplierId).First();

                return View();
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        public ActionResult DetailList()
        {
            try
            {
                // Creating instance of DatabaseContext class
                using (kmsi_portalEntities db = new kmsi_portalEntities())
                {
                    var draw = Request.Form.GetValues("draw").FirstOrDefault();
                    var start = Request.Form.GetValues("start").FirstOrDefault();
                    var length = Request.Form.GetValues("length").FirstOrDefault();
                    var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                    var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                    var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();

                    // Paging Size
                    int pageSize = length != null ? Convert.ToInt32(length) : 0;
                    int skip = start != null ? Convert.ToInt32(start) : 0;
                    int recordsTotal = 0;

                    // Getting all Purchase data
                    var purchaseData = (from i in db.PurchaseInvoices
                                        join d in db.PurchaseInvoiceDetails on i.InvoiceNumber equals d.InvoiceNumber
                                        orderby i.InvoiceID ascending
                                        group d by new
                                        {
                                            i.InvoiceID,
                                            i.SupplierID,
                                            i.SupplierCode,
                                            i.SupplierName,
                                            i.InvoiceDate,
                                            i.InvoiceNumber,
                                            i.InvoiceDueDate,
                                            i.Description,
                                            i.Currency,
                                            i.Request,
                                            i.ReqComment,
                                            i.fileINV,
                                            i.fileFP,
                                            i.FixedStatus
                                        } into invoiceGroup
                                        select new
                                        {
                                            InvoiceID = invoiceGroup.Key.InvoiceID,
                                            SupplierID = invoiceGroup.Key.SupplierID,
                                            SupplierCode = invoiceGroup.Key.SupplierCode,
                                            SupplierName = invoiceGroup.Key.SupplierName,
                                            InvoiceDate = invoiceGroup.Key.InvoiceDate,
                                            InvoiceNumber = invoiceGroup.Key.InvoiceNumber,
                                            InvoiceDueDate = invoiceGroup.Key.InvoiceDueDate,
                                            Description = invoiceGroup.Key.Description,
                                            Currency = invoiceGroup.Key.Currency,
                                            PrincipalAmount = invoiceGroup.Sum(a => a.UnitPrice * a.ShipQty),
                                            PrincipalTax = invoiceGroup.Sum(a => a.UnitPrice * a.ShipQty) * (decimal)0.1,
                                            TotalAmount = invoiceGroup.Sum(a => a.UnitPrice * a.ShipQty) + invoiceGroup.Sum(a => a.UnitPrice * a.ShipQty) * (decimal)0.1,
                                            Request = invoiceGroup.Key.Request,
                                            ReqComment = invoiceGroup.Key.ReqComment,
                                            fileINV = invoiceGroup.Key.fileINV,
                                            fileFP = invoiceGroup.Key.fileFP,
                                            FixedStatus = invoiceGroup.Key.FixedStatus
                                        });

                    var supplierCode = (string)Session["SupplierCode"];
                    if (!string.IsNullOrEmpty(supplierCode))
                    {
                        purchaseData = purchaseData.Where(p => p.SupplierCode.Equals(supplierCode));
                    }
                    else
                    {
                        purchaseData = purchaseData.Where(p => p.FixedStatus != 1);
                    }
                        

                    // Sorting
                    if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                    {
                        purchaseData = purchaseData.OrderBy(sortColumn + " " + sortColumnDir);
                    }
                    // Search
                    if (!string.IsNullOrEmpty(searchValue))
                    {
                        purchaseData = purchaseData.Where(p =>
                                            p.SupplierCode.ToLower().Contains(searchValue.ToLower()) ||
                                            p.InvoiceNumber.ToLower().Contains(searchValue.ToLower()) ||
                                            p.SupplierName.ToLower().Contains(searchValue.ToLower()) ||
                                            p.Description.ToString().ToLower().Contains(searchValue.ToLower()));
                    }

                    // total number of rows count
                    recordsTotal = purchaseData.Count();
                    // Paging
                    var data = purchaseData.Skip(skip).Take(pageSize).ToList();
                    // Returning Json Data
                    return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        // GET: Invoice/Create/5
        public ActionResult Create(string poNumber)
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    OutstandingPurchase purchaseOrder = db.OutstandingPurchases.FirstOrDefault(x => x.PO_NUMBER.Trim().Equals(poNumber) && !x.STATUS.Equals("CLOSE"));
                    Supplier supplier = db.Suppliers.FirstOrDefault(s => s.SupplierCode == purchaseOrder.SUPPLIER_CODE.Trim());
                    var purchaseDetails = (from pd in db.OutstandingPurchases
                                           where pd.PO_NUMBER.Trim() == poNumber
                                           orderby pd.PO_DATE descending
                                           select new PurchaseInvoiceViewModel
                                           {
                                               PurchaseDetailId = pd.ID,
                                               PartNumber = pd.PART_NUMBER.Trim(),
                                               ItemNumber = pd.PO_ITEM_NUMBER.Trim(),
                                               RequestQuantity = pd.PO_QTY,
                                               ShipQty = db.PurchaseInvoiceDetails.Where(x => x.PurchaseNumber == pd.PO_NUMBER.Trim() && x.PartNumberKomatsu == pd.PART_NUMBER.Trim()).Select(x => x.ShipQty).DefaultIfEmpty(0).Sum(),
                                               Price = pd.PO_UNIT_PRICE,
                                               Description = pd.PART_NAME.Trim()
                                           }).ToList();

                    ViewBag.details = purchaseDetails;
                    ViewBag.purchase = purchaseOrder;
                    ViewBag.supplier = supplier;

                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return RedirectToAction("Login", "Account");
            }
        }
        
        // POST: Invoice/Create/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [HandleError]
        public ActionResult Create(string poNumber, PurchaseInvoiceViewModel viewModel, FormCollection collection)
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    PurchaseInvoice purchaseInvoice = new PurchaseInvoice();
                    purchaseInvoice.PurchaseID = Convert.ToInt32(poNumber);
                    purchaseInvoice.SupplierCode = viewModel.PurchaseInvoice.SupplierCode;
                    purchaseInvoice.SupplierName = viewModel.PurchaseInvoice.SupplierName;
                    purchaseInvoice.InvoiceDate = viewModel.PurchaseInvoice.InvoiceDate;
                    purchaseInvoice.InvoiceNumber = viewModel.PurchaseInvoice.InvoiceNumber;
                    purchaseInvoice.InvoiceDueDate = viewModel.PurchaseInvoice.InvoiceDueDate;
                    purchaseInvoice.Description = viewModel.PurchaseInvoice.Description;
                    purchaseInvoice.Currency = viewModel.PurchaseInvoice.Currency;
                    purchaseInvoice.PrincipalAmount = viewModel.PurchaseInvoice.PrincipalAmount;
                    purchaseInvoice.PrincipalTax = viewModel.PurchaseInvoice.PrincipalTax;
                    purchaseInvoice.TotalAmount = viewModel.PurchaseInvoice.TotalAmount;
                    purchaseInvoice.PurchaseNumber = viewModel.PurchaseInvoice.PurchaseNumber;
                    db.PurchaseInvoices.Add(purchaseInvoice);
                    db.SaveChanges();

                    int InvoiceID = purchaseInvoice.InvoiceID;

                    PurchaseInvoiceDetail invoiceDetail = new PurchaseInvoiceDetail();

                    string ids = collection[1].ToString();
                    string[] ID = ids.Split(',');
                    int size = ID.Count();
                    for (int i = 0; i < size; i++)
                    {
                        int count = Convert.ToInt32(ID[i]);
                        invoiceDetail.SupplierCode = viewModel.PurchaseInvoice.SupplierCode;
                        invoiceDetail.SupplierName = viewModel.PurchaseInvoice.SupplierName;
                        invoiceDetail.PurchaseNumber = viewModel.PurchaseInvoice.PurchaseNumber;
                        invoiceDetail.InvoiceDate = viewModel.PurchaseInvoice.InvoiceDate;
                        invoiceDetail.InvoiceNumber = viewModel.PurchaseInvoice.InvoiceNumber;
                        invoiceDetail.DeliveryOrder = Request.Form.GetValues("PurchaseInvoiceDetail.DeliveryOrders[" + count + "]").FirstOrDefault();
                        invoiceDetail.ItemNumber = Convert.ToInt32(Request.Form.GetValues("PurchaseInvoiceDetail.ItemNumbers[" + count + "]").FirstOrDefault());
                        invoiceDetail.PartNumberKomatsu = Request.Form.GetValues("PurchaseInvoiceDetail.PartNumbersKomatsu[" + count + "]").FirstOrDefault();
                        invoiceDetail.ShipQty = Convert.ToDecimal(Request.Form.GetValues("PurchaseInvoiceDetail.ShipQties[" + count + "]").FirstOrDefault());
                        invoiceDetail.UnitPrice = Convert.ToDecimal(Request.Form.GetValues("PurchaseInvoiceDetail.UnitPrices[" + count + "]").FirstOrDefault());
                        invoiceDetail.CountryOfOrigin = Request.Form.GetValues("PurchaseInvoiceDetail.CountryOfOrigins[" + count + "]").FirstOrDefault();
                        db.PurchaseInvoiceDetails.Add(invoiceDetail);
                        db.SaveChanges();

                        OutstandingPurchase purchase = db.OutstandingPurchases
                            .SingleOrDefault(x =>
                                x.PO_NUMBER.Trim() == invoiceDetail.PurchaseNumber
                                && x.SUPPLIER_CODE.Trim() == invoiceDetail.SupplierCode
                                && x.PART_NUMBER.Trim() == invoiceDetail.PartNumberKomatsu
                            );
                        Decimal? ShipQty = db.PurchaseInvoiceDetails.Where(x => x.PurchaseNumber == invoiceDetail.PurchaseNumber && x.PartNumberKomatsu == invoiceDetail.PartNumberKomatsu).Select(x => x.ShipQty).DefaultIfEmpty(0).Sum();
                        string status = String.Empty;
                        if (ShipQty < purchase.PO_QTY)
                        {
                            status = "PARTIAL";
                        }
                        else if (ShipQty >= purchase.PO_QTY)
                        {
                            status = "CLOSE";
                        }
                        purchase.STATUS = status;
                        db.SaveChanges();
                    }

                    if (InvoiceID != 0)
                    {
                        string path = Server.MapPath("~/Uploads/");
                        if (!Directory.Exists(path))
                            Directory.CreateDirectory(path);

                        string FileNameInvoice = Path.GetFileName(viewModel.PurchaseInvoice.INVFile.FileName);
                        string FileNameTax = Path.GetFileName(viewModel.PurchaseInvoice.FPFile.FileName);

                        Regex regexINV = new Regex(@"INV-\w+");
                        Regex regexFP = new Regex(@"FP-\w+");
                        if (regexINV.IsMatch(FileNameInvoice) || regexFP.IsMatch(FileNameTax))
                        {
                            PurchaseInvoice p = db.PurchaseInvoices.Where(x => x.InvoiceNumber == purchaseInvoice.InvoiceNumber && x.SupplierCode == purchaseInvoice.SupplierCode).FirstOrDefault();

                            if (p != null)
                            {
                                p.fileINV = FileNameInvoice;
                                p.fileFP = FileNameTax;
                                var ServerSavePathINV = Path.Combine(path + FileNameInvoice);
                                var ServerSavePathFP = Path.Combine(path + FileNameTax);
                                //Save file to server folder  
                                viewModel.PurchaseInvoice.INVFile.SaveAs(ServerSavePathINV);
                                viewModel.PurchaseInvoice.FPFile.SaveAs(ServerSavePathFP);
                                db.SaveChanges();
                            }
                        }
                    }

                    var user = db.Users.Where(x => x.UserRole == 1).ToList();
                    foreach (var u in user)
                    {
                        SendEmail(u.Email, "Invoice", "New Invoice Inserted");
                    }
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();
            }

            return RedirectToAction("Index", "Invoice");
        }

        // GET: Invoice/Edit/5
        public ActionResult Edit(int id)
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    kmsi_portalEntities db = new kmsi_portalEntities();
                    var po = db.PurchaseInvoices.Where(x => x.InvoiceID == id).First();

                    ViewData["po"] = po;
                    ViewData["invDate"] = po.InvoiceDate.Value.ToString("yyyy-MM-dd");
                    ViewData["invDueDate"] = po.InvoiceDueDate.Value.ToString("yyyy-MM-dd");
                    ViewData["Suppliers"] = new SelectList(db.Suppliers, "SupplierId", "SupplierCode", po.SupplierID);
                    ViewData["PurchaseOrders"] = new SelectList(db.PurchaseOrders, "PurchaseId", "PurchaseNumber", po.PurchaseID);

                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return RedirectToAction("Login", "Account");
            }
        }

        // GET : Invoice/DetailEdit/5
        public ActionResult DetailEdit(int id)
        {
            var pd = db.PurchaseInvoiceDetails.Where(d => d.PurchaseInvoiceDetailId == id).FirstOrDefault();

            ViewBag.details = pd;
            return View();
        }

        // POST: Invoice/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
                kmsi_portalEntities db = new kmsi_portalEntities();
                var supplier = db.Suppliers.Find(Convert.ToInt32(collection["po.SupplierID"]));
                var p = db.PurchaseInvoices.Find(id);
                p.PurchaseID = Convert.ToInt32(collection["po.PurchaseID"]);

                p.SupplierID = supplier.SupplierId;
                p.SupplierName = supplier.SupplierName;
                p.SupplierCode = supplier.SupplierCode;

                p.InvoiceDate = Convert.ToDateTime(collection["invDate"]);
                p.InvoiceDueDate = Convert.ToDateTime(collection["invDueDate"]);
                p.InvoiceNumber = collection["po.InvoiceNumber"];

                p.Description = collection["po.Description"];
                p.Currency = collection["po.Currency"];
                p.PrincipalAmount = collection["po.PrincipalAmount"];
                p.PrincipalTax = collection["po.PrincipalTax"];
                p.TotalAmount = collection["po.TotalAmount"];

                db.SaveChanges();

                return RedirectToAction("Detail");
            }
            catch
            {
                return RedirectToAction("Detail");
            }
        }

        // POST: Invoice DetailEdit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DetailEdit(int id, FormCollection collection)
        {
            try
            {
                var d = db.PurchaseInvoiceDetails.Find(id);
                d.PartNumberKomatsu = collection["PurchaseInvoiceDetail.PartNumberKomatsu"];
                d.ItemNumber = Convert.ToInt32(collection["PurchaseInvoiceDetail.ItemNumber"]);
                d.ShipQty = Convert.ToInt32(collection["PurchaseInvoiceDetail.ShipQty"]);
                d.UnitPrice = Convert.ToDecimal(collection["PurchaseInvoiceDetail.UnitPrice"]);
                d.DeliveryOrder = collection["PurchaseInvoiceDetail.DeliveryOrder"];
                d.CountryOfOrigin = collection["PurchaseInvoiceDetail.CountryOfOrigin"];
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();
            }

            return RedirectToAction("Index");
        }

        // GET: Invoice/Delete/5
        public ActionResult Delete(int id)
        {
            try
            {
                // TODO: Add delete logic here
                PurchaseInvoiceDetail invoiceDetail = db.PurchaseInvoiceDetails.Find(id);
                db.PurchaseInvoiceDetails.Remove(invoiceDetail);
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        // POST: UploadFiles
        [HttpPost]
        public ActionResult UploadFiles(HttpPostedFileBase[] file, FormCollection collection)
        {
            string supplierCode = null;
            if (collection[1] != null) supplierCode = collection[1];
            //Ensure model state is valid  
            if (ModelState.IsValid)
            {   //iterating through multiple file collection   
                foreach (HttpPostedFileBase f in file)
                {
                    //Checking file is available to save.  
                    if (f != null)
                    {
                        string path = Server.MapPath("~/Uploads/");
                        if (!Directory.Exists(path))
                            Directory.CreateDirectory(path);

                        var InputFileName = Path.GetFileName(f.FileName);
                        Regex regexINV = new Regex(@"INV-\w+");
                        Regex regexFP = new Regex(@"FP-\w+");
                        if (regexINV.IsMatch(InputFileName) || regexFP.IsMatch(InputFileName))
                        {
                            string invNumber = InputFileName.Split('-')[1].Split('.')[0];

                            kmsi_portalEntities db = new kmsi_portalEntities();
                            PurchaseInvoice p = db.PurchaseInvoices.Where(x => x.InvoiceNumber == invNumber && x.FixedStatus != 1 && x.SupplierCode==supplierCode).FirstOrDefault();

                            if(p != null)
                            {
                                if (InputFileName.Contains("INV-"))
                                {
                                    p.fileINV = InputFileName;
                                }
                                else if (InputFileName.Contains("FP-"))
                                {
                                    p.fileFP = InputFileName;
                                }
                                var ServerSavePath = Path.Combine(path + InputFileName);
                                //Save file to server folder  
                                f.SaveAs(ServerSavePath);
                                db.SaveChanges();

                                //assigning file uploaded status to ViewBag for showing message to user.  
                                ViewBag.UploadStatus = file.Count().ToString() + " files uploaded successfully.";
                            }
                        }
                    }

                }
            }
            return RedirectToAction("Detail");
        }

        // POST: Invoice/RequestDelete/5
        [HttpPost]
        public ActionResult RequestDelete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                var comment = collection["comment"];

                kmsi_portalEntities db = new kmsi_portalEntities();
                PurchaseInvoice p = db.PurchaseInvoices.Find(id);
                p.Request = "Request Delete";
                p.ReqComment = comment;

                db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }

        // POST: Invoice/RequestEdit/5
        [HttpPost]
        public ActionResult RequestEdit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                var comment = collection["comment"];

                kmsi_portalEntities db = new kmsi_portalEntities();
                PurchaseInvoice p = db.PurchaseInvoices.Find(id);
                p.Request = "Request Edit";
                p.ReqComment = comment;

                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }

        public ActionResult Fixed(int[] ids)
        {
            var purchaseInvoice = from item in db.PurchaseInvoices
                                  where ids.Contains(item.InvoiceID)
                                  select item;
            purchaseInvoice.ToList()
                .ForEach(x => x.FixedStatus = 1);
            db.SaveChanges();

            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Unfixed(int[] ids)
        {
            var purchaseInvoice = from item in db.PurchaseInvoices
                                  where ids.Contains(item.InvoiceID)
                                  select item;
            purchaseInvoice.ToList()
                .ForEach(x => x.FixedStatus = 0);
            db.SaveChanges();

            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        public FileResult DownloadExcel()
        {
            string path = "/Docs/Invoice_Format.xlsx";
            return File(path, "application/vnd.ms-excel", "Invoice_Format.xlsx");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [HandleError]
        public ActionResult Import(HttpPostedFileBase file)
        {
            try
            {
                string filePath = string.Empty;
                if (file != null)
                {
                    string path = Server.MapPath("~/Uploads/");
                    if (!Directory.Exists(path))
                        Directory.CreateDirectory(path);

                    filePath = path + Path.GetFileName(file.FileName);
                    string extension = Path.GetExtension(file.FileName).ToLower();
                    file.SaveAs(filePath);

                    string conString = string.Empty;
                    switch (extension)
                    {
                        case ".xls": //Excel 97-03.
                            conString = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                            break;

                        case ".xlsx": //Excel 07 and above.
                            conString = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                            break;
                    }

                    DataTable dt = new DataTable();
                    DataTable dtInv = new DataTable();
                    conString = string.Format(conString, filePath);

                    kmsi_portalEntities db = new kmsi_portalEntities();
                    Int32 SupplierID = Convert.ToInt32(Session["SupplierId"]);
                    string SupplierCode = String.Empty;
                    string SupplierName = String.Empty;
                    if (SupplierID != 0)
                    {
                        Supplier supplier = db.Suppliers.SingleOrDefault(s => s.SupplierId == SupplierID);
                        SupplierCode = supplier.SupplierCode;
                        SupplierName = supplier.SupplierName;
                    }

                    using (OleDbConnection connExcel = new OleDbConnection(conString))
                    {
                        using (OleDbCommand cmdExcel = new OleDbCommand())
                        {
                            using (OleDbDataAdapter odaExcel = new OleDbDataAdapter())
                            {
                                cmdExcel.Connection = connExcel;

                                //Get the name of First Sheet.
                                connExcel.Open();
                                DataTable dtExcelSchema;
                                dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                                string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                                connExcel.Close();

                                //Read Data from First Sheet.
                                connExcel.Open();
                                cmdExcel.CommandText = "SELECT * FROM [" + sheetName + "]";

                                DbDataReader reader = cmdExcel.ExecuteReader();
                                dt.Load(reader);
                                dt.Columns.Add("SupplierCode", typeof(string));
                                dt.Columns.Add("SupplierName", typeof(string));

                                foreach (DataRow row in dt.Rows)
                                {
                                    row["SupplierCode"] = SupplierCode;
                                    row["SupplierName"] = SupplierName;
                                }

                                odaExcel.SelectCommand = cmdExcel;
                                //odaExcel.Fill(dt);
                                connExcel.Close();
                            }
                        }

                        using (OleDbCommand cmdExcelInv = new OleDbCommand())
                        {
                            using (OleDbDataAdapter odaExcelInv = new OleDbDataAdapter())
                            {
                                cmdExcelInv.Connection = connExcel;

                                //Get the name of First Sheet.
                                connExcel.Open();
                                DataTable dtExcelSchema;
                                dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                                string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                                connExcel.Close();

                                //Read Data from First Sheet.
                                connExcel.Open();
                                cmdExcelInv.CommandText = "SELECT * FROM [" + sheetName + "]";

                                DbDataReader readerInv = cmdExcelInv.ExecuteReader();
                                dtInv.Load(readerInv);
                                dtInv.Columns.Add("SupplierID", typeof(int));
                                dtInv.Columns.Add("SupplierCode", typeof(string));
                                dtInv.Columns.Add("SupplierName", typeof(string));

                                foreach (DataRow row in dtInv.Rows)
                                {
                                    row["SupplierID"] = SupplierID;
                                    row["SupplierCode"] = SupplierCode;
                                    row["SupplierName"] = SupplierName;
                                }

                                odaExcelInv.SelectCommand = cmdExcelInv;
                                //odaExcelInv.Fill(dtInv);
                                connExcel.Close();
                            }
                        }
                    }

                    conString = ConfigurationManager.ConnectionStrings["Constring"].ConnectionString;
                    using (SqlConnection con = new SqlConnection(conString))
                    {
                        using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                        {
                            // Set the database table name.
                            sqlBulkCopy.DestinationTableName = "dbo.PurchaseInvoiceDetails";

                            //[OPTIONAL]: Map the Excel columns with that of the database table
                            sqlBulkCopy.ColumnMappings.Add("SupplierCode", "SupplierCode");
                            sqlBulkCopy.ColumnMappings.Add("SupplierName", "SupplierName");
                            sqlBulkCopy.ColumnMappings.Add("PO NUMBER", "PurchaseNumber");
                            sqlBulkCopy.ColumnMappings.Add("INVOICE DATE", "InvoiceDate");
                            sqlBulkCopy.ColumnMappings.Add("INVOICE NUMBER", "InvoiceNumber");
                            sqlBulkCopy.ColumnMappings.Add("DELIVERY ORDER/AWB/BL NUMBER", "DeliveryOrder");
                            sqlBulkCopy.ColumnMappings.Add("ITEM NUMBER", "ItemNumber");
                            sqlBulkCopy.ColumnMappings.Add("PART NUMBER KOMATSU", "PartNumberKomatsu");
                            sqlBulkCopy.ColumnMappings.Add("SHIP QTY", "ShipQty");
                            sqlBulkCopy.ColumnMappings.Add("UNIT PRICE", "UnitPrice");
                            sqlBulkCopy.ColumnMappings.Add("COO (COUNTRY OF ORIGIN)", "CountryOfOrigin");

                            con.Open();
                            sqlBulkCopy.WriteToServer(dt);
                            con.Close();
                        }

                        using (SqlBulkCopy sqlBulkCopyInv = new SqlBulkCopy(con))
                        {
                            // Set the database table name.
                            sqlBulkCopyInv.DestinationTableName = "dbo.PurchaseInvoice";

                            //[OPTIONAL]: Map the Excel columns with that of the database table
                            sqlBulkCopyInv.ColumnMappings.Add("SupplierID", "SupplierID");
                            sqlBulkCopyInv.ColumnMappings.Add("SupplierCode", "SupplierCode");
                            sqlBulkCopyInv.ColumnMappings.Add("SupplierName", "SupplierName");
                            sqlBulkCopyInv.ColumnMappings.Add("INVOICE DATE", "InvoiceDate");
                            sqlBulkCopyInv.ColumnMappings.Add("INVOICE NUMBER", "InvoiceNumber");
                            sqlBulkCopyInv.ColumnMappings.Add("DUE DATE", "InvoiceDueDate");
                            sqlBulkCopyInv.ColumnMappings.Add("DESCRIPTION", "Description");
                            sqlBulkCopyInv.ColumnMappings.Add("CURRENCY", "Currency");

                            con.Open();
                            sqlBulkCopyInv.WriteToServer(dtInv);
                            con.Close();
                        }
                    }

                    ViewBag.Message = "Success Import";
                    ModelState.Clear();
                    return RedirectToAction("Index");
                }
                else
                {
                    var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                    var sw = new System.IO.StreamWriter(filename, true);
                    sw.WriteLine(DateTime.Now.ToString() + " " + "File Not Selected!");
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [HandleError]
        public ActionResult UploadExcel(PurchaseInvoice purchaseInvoice, HttpPostedFileBase file)
        {
            TempData["ActionMessage"] = "Upload Failed";
            try
            {
                Int32 SupplierID = Convert.ToInt32(Session["SupplierId"]);
                string SupplierCode = String.Empty;
                string SupplierName = String.Empty;
                if (SupplierID != 0)
                {
                    Supplier supplier = db.Suppliers.SingleOrDefault(s => s.SupplierId == SupplierID);
                    SupplierCode = supplier.SupplierCode;
                    SupplierName = supplier.SupplierName;
                }

                var data = new List<string>();
                if (file != null)
                {
                    if (file.ContentType == "application/vnd.ms-excel" || file.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                    {
                        var fileName = file.FileName;
                        var targetPath = Server.MapPath("~/Uploads/");
                        file.SaveAs(targetPath + fileName);
                        var pathToExcelFile = targetPath + fileName;
                        string FileName = Path.GetFileName(file.FileName);
                        string Extension = Path.GetExtension(file.FileName);
                        DataTable dataTable = ImportToGrid(pathToExcelFile, Extension, "Yes");
                        foreach (DataRow item in dataTable.Rows)
                        {
                            try
                            {
                                var InvoiceNumber = item["INVOICE NUMBER"].ToString();
                                string invDateFormat = item["INVOICE DATE"].ToString();
                                string day = invDateFormat.Substring(6, 2);
                                string month = invDateFormat.Substring(4, 2);
                                string year = invDateFormat.Substring(0, 4);
                                string invDate = year + '-' + month + '-' + day;
                                var InvoiceDate = Convert.ToDateTime(invDate);
                                var isExist = db.PurchaseInvoices.Where(x => x.InvoiceNumber == InvoiceNumber && x.InvoiceDate == InvoiceDate && x.SupplierCode == SupplierCode).FirstOrDefault();
                                var InvoiceID = 0;
                                if (isExist != null)
                                {
                                    InvoiceID = isExist.InvoiceID;
                                    var PurchaseNumber = item["PO NUMBER"].ToString();
                                    var PartNumber = item["PART NUMBER KOMATSU"].ToString();
                                    var detailExist = db.PurchaseInvoiceDetails.Where(x => x.InvoiceNumber == InvoiceNumber && x.InvoiceDate == InvoiceDate && x.PurchaseNumber == PurchaseNumber && x.PartNumberKomatsu == PartNumber && x.SupplierCode == SupplierCode).FirstOrDefault();
                                    var DetailID = 0;
                                    if (detailExist != null)
                                    {
                                        DetailID = detailExist.PurchaseInvoiceDetailId;
                                    }
                                    else
                                    {
                                        PurchaseInvoiceDetail invoiceDetail = new PurchaseInvoiceDetail();
                                        invoiceDetail.SupplierCode = SupplierCode;
                                        invoiceDetail.SupplierName = SupplierName;
                                        invoiceDetail.PurchaseNumber = PurchaseNumber;
                                        invoiceDetail.InvoiceDate = InvoiceDate;
                                        invoiceDetail.InvoiceNumber = InvoiceNumber;
                                        invoiceDetail.DeliveryOrder = item["DELIVERY ORDER/AWB/BL NUMBER"].ToString();
                                        invoiceDetail.ItemNumber = Convert.ToInt32(item["ITEM NUMBER"]);
                                        invoiceDetail.PartNumberKomatsu = item["PART NUMBER KOMATSU"].ToString();
                                        invoiceDetail.ShipQty = Convert.ToDecimal(item["SHIP QTY"]);
                                        invoiceDetail.UnitPrice = Convert.ToDecimal(item["UNIT PRICE"]);
                                        invoiceDetail.CountryOfOrigin = item["COO (COUNTRY OF ORIGIN)"].ToString();
                                        db.PurchaseInvoiceDetails.Add(invoiceDetail);
                                        db.SaveChanges();

                                        TempData["ActionMessage"] = "Upload Success";
                                    }
                                }
                                else
                                {
                                    string invDueDateFormat = item["DUE DATE"].ToString();
                                    string dueDay = invDueDateFormat.Substring(6, 2);
                                    string dueMonth = invDueDateFormat.Substring(4, 2);
                                    string dueYear = invDueDateFormat.Substring(0, 4);
                                    string invDueDate = dueYear + '-' + dueMonth + '-' + dueDay;
                                    var InvoiceDueDate = Convert.ToDateTime(invDueDate);
                                    PurchaseInvoice invoice = new PurchaseInvoice();
                                    invoice.SupplierID = SupplierID;
                                    invoice.SupplierCode = SupplierCode;
                                    invoice.SupplierName = SupplierName;
                                    invoice.InvoiceDate = InvoiceDate;
                                    invoice.InvoiceNumber = InvoiceNumber;
                                    invoice.InvoiceDueDate = InvoiceDueDate;
                                    invoice.Description = item["DESCRIPTION"].ToString();
                                    invoice.Currency = item["CURRENCY"].ToString();
                                    invoice.PurchaseNumber = item["PO NUMBER"].ToString();
                                    invoice.FixedStatus = 0;
                                    db.PurchaseInvoices.Add(invoice);
                                    db.SaveChanges();

                                    PurchaseInvoiceDetail invoiceDetail = new PurchaseInvoiceDetail();
                                    invoiceDetail.SupplierCode = SupplierCode;
                                    invoiceDetail.SupplierName = SupplierName;
                                    invoiceDetail.PurchaseNumber = item["PO NUMBER"].ToString();
                                    invoiceDetail.InvoiceDate = InvoiceDate;
                                    invoiceDetail.InvoiceNumber = InvoiceNumber;
                                    invoiceDetail.DeliveryOrder = item["DELIVERY ORDER/AWB/BL NUMBER"].ToString();
                                    invoiceDetail.ItemNumber = Convert.ToInt32(item["ITEM NUMBER"]);
                                    invoiceDetail.PartNumberKomatsu = item["PART NUMBER KOMATSU"].ToString();
                                    invoiceDetail.ShipQty = Convert.ToDecimal(item["SHIP QTY"]);
                                    invoiceDetail.UnitPrice = Convert.ToDecimal(item["UNIT PRICE"]);
                                    invoiceDetail.CountryOfOrigin = item["COO (COUNTRY OF ORIGIN)"].ToString();
                                    db.PurchaseInvoiceDetails.Add(invoiceDetail);
                                    db.SaveChanges();

                                    TempData["ActionMessage"] = "Upload Success";
                                }

                                var user = db.Users.Where(x => x.UserRole == 1).ToList();
                                foreach (var u in user)
                                {
                                    SendEmail(u.Email, "Invoice", "New Invoice Inserted");
                                }

                            }
                            catch (Exception ex)
                            {
                                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                                var sw = new System.IO.StreamWriter(filename, true);
                                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                                sw.Close();
                            }
                        }
                    }
                }
                else
                {
                    var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                    var sw = new System.IO.StreamWriter(filename, true);
                    sw.WriteLine(DateTime.Now.ToString() + " " + "File Not Selected!");
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();
            }

            return RedirectToAction("Index");
        }

        public DataTable ImportToGrid(string FilePath, string Extension, string isHDR)
        {
            DataTable dt = new DataTable();
            try
            {
                string conString = string.Empty;
                switch (Extension)
                {
                    case ".xls": //Excel 97-03.
                        conString = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                        break;

                    case ".xlsx": //Excel 07 and above.
                        conString = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                        break;
                }
                conString = String.Format(conString, FilePath, isHDR);
                OleDbConnection conExcel = new OleDbConnection(conString);
                OleDbCommand cmdExcel = new OleDbCommand();
                OleDbDataAdapter oda = new OleDbDataAdapter();
                cmdExcel.Connection = conExcel;

                //Get the name of First Sheet
                conExcel.Open();
                DataTable dtExcelSchema;
                dtExcelSchema = conExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                string SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                conExcel.Close();

                //Read Data from First Sheet
                conExcel.Open();
                cmdExcel.CommandText = "SELECT * FROM [" + SheetName + "]";
                oda.SelectCommand = cmdExcel;
                oda.Fill(dt);
                conExcel.Close();
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();
            }
            return dt;
        }

        public ActionResult FixedInvoice()
        {
            if (Session["UserId"] != null)
            {
                var supplierId = Convert.ToInt32(Session["SupplierId"]);
                if (supplierId > 0)
                    ViewData["Supplier"] = db.Suppliers.Where(p => p.SupplierId == supplierId).First();

                return View();
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        public ActionResult DetailFixed()
        {
            try
            {
                // Creating instance of DatabaseContext class
                using (kmsi_portalEntities db = new kmsi_portalEntities())
                {
                    var draw = Request.Form.GetValues("draw").FirstOrDefault();
                    var start = Request.Form.GetValues("start").FirstOrDefault();
                    var length = Request.Form.GetValues("length").FirstOrDefault();
                    var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                    var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                    var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();

                    // Paging Size
                    int pageSize = length != null ? Convert.ToInt32(length) : 0;
                    int skip = start != null ? Convert.ToInt32(start) : 0;
                    int recordsTotal = 0;

                    // Getting all Purchase data
                    var purchaseData = (from i in db.PurchaseInvoices
                                        join d in db.PurchaseInvoiceDetails on i.InvoiceNumber equals d.InvoiceNumber
                                        where i.FixedStatus == 1
                                        orderby i.InvoiceID ascending
                                        group d by new
                                        {
                                            i.InvoiceID,
                                            i.SupplierID,
                                            i.SupplierCode,
                                            i.SupplierName,
                                            i.InvoiceDate,
                                            i.InvoiceNumber,
                                            i.InvoiceDueDate,
                                            i.Description,
                                            i.Currency,
                                            i.Request,
                                            i.ReqComment,
                                            i.fileINV,
                                            i.fileFP
                                        } into invoiceGroup
                                        select new
                                        {
                                            InvoiceID = invoiceGroup.Key.InvoiceID,
                                            SupplierID = invoiceGroup.Key.SupplierID,
                                            SupplierCode = invoiceGroup.Key.SupplierCode,
                                            SupplierName = invoiceGroup.Key.SupplierName,
                                            InvoiceDate = invoiceGroup.Key.InvoiceDate,
                                            InvoiceNumber = invoiceGroup.Key.InvoiceNumber,
                                            InvoiceDueDate = invoiceGroup.Key.InvoiceDueDate,
                                            Description = invoiceGroup.Key.Description,
                                            Currency = invoiceGroup.Key.Currency,
                                            PrincipalAmount = invoiceGroup.Sum(a => a.UnitPrice * a.ShipQty),
                                            PrincipalTax = invoiceGroup.Sum(a => a.UnitPrice * a.ShipQty) * (decimal)0.1,
                                            TotalAmount = invoiceGroup.Sum(a => a.UnitPrice * a.ShipQty) + invoiceGroup.Sum(a => a.UnitPrice * a.ShipQty) * (decimal)0.1,
                                            Request = invoiceGroup.Key.Request,
                                            ReqComment = invoiceGroup.Key.ReqComment,
                                            fileINV = invoiceGroup.Key.fileINV,
                                            fileFP = invoiceGroup.Key.fileFP
                                        });

                    var supplierCode = (string)Session["SupplierCode"];
                    if (!string.IsNullOrEmpty(supplierCode))
                        purchaseData = purchaseData.Where(p => p.SupplierCode.Equals(supplierCode));

                    // Sorting
                    if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                    {
                        purchaseData = purchaseData.OrderBy(sortColumn + " " + sortColumnDir);
                    }
                    // Search
                    if (!string.IsNullOrEmpty(searchValue))
                    {
                        purchaseData = purchaseData.Where(p =>
                                            p.SupplierCode.ToLower().Contains(searchValue.ToLower()) ||
                                            p.InvoiceNumber.ToLower().Contains(searchValue.ToLower()) ||
                                            p.SupplierName.ToLower().Contains(searchValue.ToLower()) ||
                                            p.Description.ToString().ToLower().Contains(searchValue.ToLower()));
                    }

                    // total number of rows count
                    recordsTotal = purchaseData.Count();
                    // Paging
                    var data = purchaseData.Skip(skip).Take(pageSize).ToList();
                    // Returning Json Data
                    return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool SendEmail(string toEmail, string subject, string emailBody)
        {
            try
            {
                string senderEmail = System.Configuration.ConfigurationManager.AppSettings["senderEmail"].ToString();
                string senderPassword = System.Configuration.ConfigurationManager.AppSettings["senderPassword"].ToString();

                SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                client.EnableSsl = true;
                client.Timeout = 100000;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential(senderEmail, senderPassword);

                MailMessage mailMessage = new MailMessage(senderEmail, toEmail, subject, emailBody);
                mailMessage.IsBodyHtml = true;
                mailMessage.BodyEncoding = UTF8Encoding.UTF8;

                client.Send(mailMessage);

                return true;
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return false;
            }
        }

        [HttpPost]
        public ActionResult UploadINV(HttpPostedFileBase file, FormCollection collection)
        {
            int id = Convert.ToInt32(collection[1]);
          
            string targetPath = Server.MapPath("~/Uploads/");
            if (!Directory.Exists(targetPath))
                Directory.CreateDirectory(targetPath);

            var fileName = file.FileName;
            file.SaveAs(targetPath + fileName);
            string FileName = Path.GetFileName(file.FileName);
            string Extension = Path.GetExtension(file.FileName);

            PurchaseInvoice p = db.PurchaseInvoices.Where(x => x.InvoiceID == id).FirstOrDefault();

            if (p != null)
            {
                p.fileINV = FileName;
                db.SaveChanges();
            }

            return RedirectToAction("Detail");
        }

        [HttpPost]
        public ActionResult UploadFP(HttpPostedFileBase file, FormCollection collection)
        {
            int id = Convert.ToInt32(collection[1]);
            string targetPath = Server.MapPath("~/Uploads/");
            if (!Directory.Exists(targetPath))
                Directory.CreateDirectory(targetPath);

            var fileName = file.FileName;
            file.SaveAs(targetPath + fileName);
            string FileName = Path.GetFileName(file.FileName);
            string Extension = Path.GetExtension(file.FileName);

            PurchaseInvoice p = db.PurchaseInvoices.Where(x => x.InvoiceID == id).FirstOrDefault();

            if (p != null)
            {
                p.fileFP = FileName;
                db.SaveChanges();
            }

            return RedirectToAction("Detail");
        }

        public ActionResult PostReportPartial(int[] ids)
        {
            DateTime dateTime = DateTime.Now;
            string FileName = string.Format("Invoice_" + dateTime.ToString("yyyyMMdd_HHmm") + ".xlsx");
            // Validate the Model is correct and contains valid data
            // Generate your report output based on the model parameters
            // This can be an Excel, PDF, Word file - whatever you need.

            // As an example lets assume we've generated an EPPlus ExcelPackage

            // Do something to populate your workbook

            // Generate a new unique identifier against which the file can be stored
            string handle = Guid.NewGuid().ToString();

            using (MemoryStream memoryStream = new MemoryStream())
            {
                using (ExcelPackage excel = new ExcelPackage())
                {
                    excel.Workbook.Worksheets.Add("Invoice");

                    var headerRow = new List<string[]>()
                    {
                        new string[] { "Supplier Code", "Invoice Number", "Delivery Order/Packing List", "PO Number", "Item Number", "Part Number Komatsu", "Ship Quantity", "Unit Price", "Country Of Origin" }
                    };

                    string headerRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";

                    var worksheet = excel.Workbook.Worksheets["Invoice"];

                    worksheet.Cells[headerRange].LoadFromArrays(headerRow);
                    worksheet.Cells[headerRange].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[headerRange].Style.Font.Bold = true;
                    worksheet.Cells[headerRange].Style.Font.Size = 12;
                    worksheet.Cells[headerRange].AutoFitColumns();

                    var purchaseData = (from i in db.PurchaseInvoiceDetails
                                        join inv in db.PurchaseInvoices on i.InvoiceNumber equals inv.InvoiceNumber
                                        where ids.Contains(i.PurchaseInvoiceDetailId)
                                        orderby i.PurchaseInvoiceDetailId ascending
                                        select new
                                        {
                                            i.SupplierCode,
                                            i.InvoiceNumber,
                                            i.DeliveryOrder,
                                            i.PurchaseNumber,
                                            i.ItemNumber,
                                            i.PartNumberKomatsu,
                                            i.ShipQty,
                                            i.UnitPrice,
                                            i.CountryOfOrigin,
                                        });

                    int rowNumber = 2;
                    foreach (var data in purchaseData)
                    {
                        worksheet.Cells[rowNumber, 1].Value = data.SupplierCode;
                        worksheet.Cells[rowNumber, 2].Value = data.InvoiceNumber;
                        worksheet.Cells[rowNumber, 3].Value = data.DeliveryOrder;
                        worksheet.Cells[rowNumber, 4].Value = data.PurchaseNumber;
                        worksheet.Cells[rowNumber, 5].Value = data.ItemNumber;
                        worksheet.Cells[rowNumber, 6].Value = data.PartNumberKomatsu;
                        worksheet.Cells[rowNumber, 7].Value = data.ShipQty;
                        worksheet.Cells[rowNumber, 8].Value = data.UnitPrice;
                        worksheet.Cells[rowNumber, 9].Value = data.CountryOfOrigin;

                        rowNumber++;
                    }

                    excel.SaveAs(memoryStream);
                }
                memoryStream.Position = 0;
                TempData[handle] = memoryStream.ToArray();
            }

            // Note we are returning a filename as well as the handle
            return new JsonResult()
            {
                Data = new { FileGuid = handle, FileName = FileName }
            };

        }

        public ActionResult StateOfAccount(int[] ids, string[] columns)
        {
            DateTime dateTime = DateTime.Now;
            string FileName = string.Format("Invoice_" + dateTime.ToString("yyyyMMdd_HHmm") + ".xlsx");
            // Validate the Model is correct and contains valid data
            // Generate your report output based on the model parameters
            // This can be an Excel, PDF, Word file - whatever you need.

            // As an example lets assume we've generated an EPPlus ExcelPackage

            // Do something to populate your workbook

            // Generate a new unique identifier against which the file can be stored
            string handle = Guid.NewGuid().ToString();

            using (MemoryStream memoryStream = new MemoryStream())
            {
                using (ExcelPackage excel = new ExcelPackage())
                {
                    excel.Workbook.Worksheets.Add("Invoice");

                    var headerRow = new List<string[]>()
                    {
                        new string[] { "Supplier Code", "Supplier Name", "Invoice Date", "Invoice Number", "Due Date", "Description", "Currency", "Principal Amount", "Principal Tax", "Total Amount" }
                    };

                    if (columns != null)
                    {
                        headerRow = new List<string[]>()
                        {
                            columns
                        };
                    }

                    string headerRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";

                    var worksheet = excel.Workbook.Worksheets["Invoice"];

                    worksheet.Cells[headerRange].LoadFromArrays(headerRow);
                    worksheet.Cells[headerRange].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[headerRange].Style.Font.Bold = true;
                    worksheet.Cells[headerRange].Style.Font.Size = 12;
                    worksheet.Cells[headerRange].AutoFitColumns();

                    var purchaseData = (from i in db.PurchaseInvoices
                                        join d in db.PurchaseInvoiceDetails on i.InvoiceNumber equals d.InvoiceNumber
                                        where ids.Contains(i.InvoiceID)
                                        orderby i.InvoiceID ascending
                                        group d by new
                                        {
                                            i.InvoiceID,
                                            i.SupplierID,
                                            i.SupplierCode,
                                            i.SupplierName,
                                            i.InvoiceDate,
                                            i.InvoiceNumber,
                                            i.InvoiceDueDate,
                                            i.Description,
                                            i.Currency
                                        } into invoiceGroup
                                        select new
                                        {
                                            InvoiceID = invoiceGroup.Key.InvoiceID,
                                            SupplierID = invoiceGroup.Key.SupplierID,
                                            SupplierCode = invoiceGroup.Key.SupplierCode,
                                            SupplierName = invoiceGroup.Key.SupplierName,
                                            InvoiceDate = invoiceGroup.Key.InvoiceDate,
                                            InvoiceNumber = invoiceGroup.Key.InvoiceNumber,
                                            InvoiceDueDate = invoiceGroup.Key.InvoiceDueDate,
                                            Description = invoiceGroup.Key.Description,
                                            Currency = invoiceGroup.Key.Currency,
                                            PrincipalAmount = invoiceGroup.Sum(a => a.UnitPrice * a.ShipQty),
                                            PrincipalTax = invoiceGroup.Sum(a => a.UnitPrice * a.ShipQty) * (decimal)0.1,
                                            TotalAmount = invoiceGroup.Sum(a => a.UnitPrice * a.ShipQty) + invoiceGroup.Sum(a => a.UnitPrice * a.ShipQty) * (decimal)0.1,
                                        });

                    int rowNumber = 2;
                    foreach (var data in purchaseData)
                    {
                        worksheet.Cells[rowNumber, 1].Value = data.SupplierCode;
                        worksheet.Cells[rowNumber, 2].Value = data.SupplierName;
                        worksheet.Cells[rowNumber, 3].Style.Numberformat.Format = "yyyy-mm-dd";
                        worksheet.Cells[rowNumber, 3].Value = data.InvoiceDate;
                        worksheet.Cells[rowNumber, 4].Value = data.InvoiceNumber;
                        worksheet.Cells[rowNumber, 5].Style.Numberformat.Format = "yyyy-mm-dd";
                        worksheet.Cells[rowNumber, 5].Value = data.InvoiceDueDate;
                        worksheet.Cells[rowNumber, 6].Value = data.Description;
                        worksheet.Cells[rowNumber, 7].Value = data.Currency;
                        worksheet.Cells[rowNumber, 8].Value = data.PrincipalAmount;
                        worksheet.Cells[rowNumber, 9].Value = data.PrincipalTax;
                        worksheet.Cells[rowNumber, 10].Value = data.TotalAmount;

                        rowNumber++;
                    }

                    excel.SaveAs(memoryStream);
                }
                memoryStream.Position = 0;
                TempData[handle] = memoryStream.ToArray();
            }

            // Note we are returning a filename as well as the handle
            return new JsonResult()
            {
                Data = new { FileGuid = handle, FileName = FileName }
            };

        }

        [HttpGet]
        public virtual ActionResult Download(string fileGuid, string fileName)
        {
            if (TempData[fileGuid] != null)
            {
                byte[] data = TempData[fileGuid] as byte[];
                return File(data, "application/vnd.ms-excel", fileName);
            }
            else
            {
                // Problem - Log the error, generate a blank file,
                //           redirect to another controller action - whatever fits with your application
                return new EmptyResult();
            }
        }

        public ActionResult DeleteFixed(string invNumber)
        {
            try
            {
                List<PurchaseInvoice> list = db.PurchaseInvoices.Where(x => x.InvoiceNumber == invNumber).ToList();
                db.PurchaseInvoices.RemoveRange(list);
                List<PurchaseInvoiceDetail> details = db.PurchaseInvoiceDetails.Where(x => x.InvoiceNumber == invNumber).ToList();
                db.PurchaseInvoiceDetails.RemoveRange(details);

                db.SaveChanges();

                return RedirectToAction("FixedInvoice", "Invoice");
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }
    }
}