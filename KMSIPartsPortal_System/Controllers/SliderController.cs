﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.IO;
using KMSIPartsPortal_System.Models;

namespace KMSIPartsPortal_System.Controllers
{
    public class SliderController : Controller
    {
        private kmsi_portalEntities db = new kmsi_portalEntities();

        // GET: Slider
        public ActionResult Index()
        {
            var subMenus = db.SubMenus.Include(s => s.MainMenu);
            ViewBag.Slider = db.Sliders.ToList();
            return View(subMenus.ToList());
        }

        // GET: Slider/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Slider/Create
        public ActionResult Create()
        {
            var subMenus = db.SubMenus.Include(s => s.MainMenu);
            return View(subMenus.ToList());
        }

        // POST: Slider/Create
        [HttpPost]
        public ActionResult Create(String[] Caption, HttpPostedFileBase[] Image)
        {
            try
            {
                // TODO: Add insert logic here
                var deleteSlider = db.Sliders.Where(x => x.Id >= 1).ToList();
                db.Sliders.RemoveRange(deleteSlider);
                db.SaveChanges();

                int i = 0;
                foreach (HttpPostedFileBase f in Image)
                {
                    if (f != null)
                    {
                        string path = Server.MapPath("~/Uploads/Slider/");
                        if (!Directory.Exists(path))
                            Directory.CreateDirectory(path);

                        string FileName = Path.GetFileName(f.FileName);

                        Slider Slider = new Slider();
                        Slider.Image = FileName;
                        Slider.Caption = Caption[i];
                        var SavePathAttachment = Path.Combine(path + FileName);
                        f.SaveAs(SavePathAttachment);
                        db.Sliders.Add(Slider);
                        db.SaveChanges();
                    }

                    i++;
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        // GET: Slider/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Slider/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Slider/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Slider/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
