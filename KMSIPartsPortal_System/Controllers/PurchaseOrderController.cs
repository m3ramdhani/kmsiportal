﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using KMSIPartsPortal_System.Helpers;
using KMSIPartsPortal_System.Models;
using Quartz;
using Quartz.Impl;

namespace KMSIPartsPortal_System.Controllers
{
    public class purchaseorderController : Controller
    {
        private kmsi_portalEntities db = new kmsi_portalEntities();

        // GET: PurchaseOrder/Index
        public ActionResult Index()
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    List<MenuModels> MenuMaster = (List<MenuModels>)Session["MenuMaster"];
                    Int32 RoleID = Convert.ToInt32(Session["RoleId"]);
                    //string RoleType = Session["RoleType"].ToString();
                    DateTime today = DateTime.Now.Date;
                    OspoLog log = db.OspoLogs.FirstOrDefault(x => DbFunctions.TruncateTime(x.TimeLogs) == today);
                    if (log == null)
                    {
                        string remote = ConfigurationManager.ConnectionStrings["OspoConnectionString"].ConnectionString;
                        string local = ConfigurationManager.ConnectionStrings["Constring"].ConnectionString;
                        Boolean fetchData = FileImport.ImportFromDB(remote, local);
                    }

                    var access = MenuMaster.Where(x => x.ControllerName == "PurchaseOrder" && x.ActionName == "Index" && x.RoleId == RoleID).FirstOrDefault();
                    if (access == null) {
                        var supplierId = Convert.ToInt32(Session["SupplierId"]);
                        if (supplierId > 0)
                            ViewData["Supplier"] = db.Suppliers.Where(p => p.SupplierId == supplierId).First();

                        ViewBag.IsInput = 1;
                        ViewBag.IsUpdate = 1;
                        ViewBag.IsDelete = 1;
                        ViewBag.IsUpload = 1;
                        ViewBag.IsDownload = 1;
                    }
                    else
                    {
                        if (access.IsView == 1)
                        {
                            var supplierId = Convert.ToInt32(Session["SupplierId"]);
                            if (supplierId > 0)
                                ViewData["Supplier"] = db.Suppliers.Where(p => p.SupplierId == supplierId).First();

                            ViewBag.IsInput = access.IsInput;
                            ViewBag.IsUpdate = access.IsUpdate;
                            ViewBag.IsDelete = access.IsDelete;
                            ViewBag.IsUpload = access.IsUpload;
                            ViewBag.IsDownload = access.IsDownload;
                        }
                        else
                        {
                            ViewBag.Message = "You don't have access to this page! Please contact administrator.";
                        }
                    }

                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult List()
        {
            try
            {
                // Creating instance of DatabaseContext class
                using (kmsi_portalEntities db = new kmsi_portalEntities())
                {
                    var draw = Request.Form.GetValues("draw").FirstOrDefault();
                    var start = Request.Form.GetValues("start").FirstOrDefault();
                    var length = Request.Form.GetValues("length").FirstOrDefault();
                    var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                    var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                    var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();

                    // Paging Size
                    int pageSize = length != null ? Convert.ToInt32(length) : 0;
                    int skip = start != null ? Convert.ToInt32(start) : 0;
                    int recordsTotal = 0;

                    // Getting all Purchase data
                    var purchaseData = db.vSummaryOspoes.AsEnumerable()
                        .Where(p => p.ShipQty < p.ReqQty || p.ShipQty == 0)
                        .Select(x => new
                        {
                            x.SupplierCode,
                            x.SupplierName,
                            PurchaseDate = x.PurchaseDate.Value.ToString("yyyy-MM-dd"),
                            x.PurchaseNumber,
                            x.Amount,
                            x.Currency,
                            x.Aging,
                            x.Remarks,
                            x.ShipQty,
                            x.ReqQty
                        });

                    var supplierCode = (string)Session["SupplierCode"];
                    if (!string.IsNullOrEmpty(supplierCode))
                        purchaseData = purchaseData.Where(p => p.SupplierCode.Trim().Equals(supplierCode.Trim()));

                    // Sorting
                    if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                    {
                        purchaseData = purchaseData.OrderBy(sortColumn + " " + sortColumnDir);
                    }
                    // Search
                    if (!string.IsNullOrEmpty(searchValue))
                    {
                        purchaseData = purchaseData.Where(p => p.SupplierCode.ToLower().Contains(searchValue.ToLower())
                            || (p.SupplierName != null && p.SupplierName.ToLower().Contains(searchValue.ToLower()))
                            || p.PurchaseNumber.ToLower().Contains(searchValue.ToLower())
                            || p.Amount.ToString().ToLower().Contains(searchValue.ToLower())
                            || p.Aging.ToString().ToLower().Contains(searchValue.ToLower()));
                    }

                    // total number of rows count
                    recordsTotal = purchaseData.Select(g => g.PurchaseNumber).Count();
                    // Paging
                    var data = purchaseData.Skip(skip).Take(pageSize).ToList();
                    // Returning Json Data
                    return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult DetailList()
        {
            try
            {
                // Creating instance of DatabaseContext class
                using (kmsi_portalEntities db = new kmsi_portalEntities())
                {
                    var draw = Request.Form.GetValues("draw").FirstOrDefault();
                    var start = Request.Form.GetValues("start").FirstOrDefault();
                    var length = Request.Form.GetValues("length").FirstOrDefault();
                    var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                    var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                    var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();

                    // Paging Size
                    int pageSize = length != null ? Convert.ToInt32(length) : 0;
                    int skip = start != null ? Convert.ToInt32(start) : 0;
                    int recordsTotal = 0;

                    // Getting all Purchase data
                    var purchaseData = (from pd in db.OutstandingPurchases
                                        select new
                                        {
                                            pd.ID,
                                            pd.PO_DATE,
                                            pd.PO_NUMBER,
                                            pd.PO_ITEM_NUMBER,
                                            pd.PART_NUMBER,
                                            pd.PART_NAME,
                                            RequestQuantity = pd.PO_QTY.HasValue ? pd.PO_QTY : 0,
                                            pd.PO_UNIT_PRICE,
                                            pd.SUPPLIER_CODE,
                                            SUPPLIER_NAME = db.Suppliers.FirstOrDefault(x => x.SupplierCode == pd.SUPPLIER_CODE.Trim()).SupplierName,
                                            TotalProvidedQuantity = ((pd.PROVIDED_QTY_1.HasValue) ? pd.PROVIDED_QTY_1 : 0) + ((pd.PROVIDED_QTY_2.HasValue) ? pd.PROVIDED_QTY_2 : 0) + ((pd.PROVIDED_QTY_3.HasValue) ? pd.PROVIDED_QTY_3 : 0) + ((pd.PROVIDED_QTY_4.HasValue) ? pd.PROVIDED_QTY_4 : 0) + ((pd.PROVIDED_QTY_5.HasValue) ? pd.PROVIDED_QTY_5 : 0),
                                            ProvidedQuantity = pd.PROVIDED_QTY_1,
                                            ProvidedQuantity_2 = pd.PROVIDED_QTY_2,
                                            ProvidedQuantity_3 = pd.PROVIDED_QTY_3,
                                            ProvidedQuantity_4 = pd.PROVIDED_QTY_4,
                                            ProvidedQuantity_5 = pd.PROVIDED_QTY_5,
                                            EstDelivered = pd.ETD_1,
                                            EstDelivered_2 = pd.ETD_2,
                                            EstDelivered_3 = pd.ETD_3,
                                            EstDelivered_4 = pd.ETD_4,
                                            EstDelivered_5 = pd.ETD_5,
                                            ShipQty = db.PurchaseInvoiceDetails.Where(x => x.PurchaseNumber == pd.PO_NUMBER.Trim() && x.PartNumberKomatsu == pd.PART_NUMBER.Trim()).Select(x => x.ShipQty).DefaultIfEmpty(0).Sum()
                                        }).ToList().Select(x => new
                                        {
                                            Ospo = x.RequestQuantity - x.TotalProvidedQuantity,
                                            SupplierCode = x.SUPPLIER_CODE,
                                            PurchaseDetailId = x.ID,
                                            SupplierName = x.SUPPLIER_NAME,
                                            PurchaseDate = x.PO_DATE,
                                            PurchaseNumber = x.PO_NUMBER,
                                            ItemNumber = x.PO_ITEM_NUMBER,
                                            PartNumber = x.PART_NUMBER,
                                            Description = x.PART_NAME,
                                            RequestQuantity = x.RequestQuantity,
                                            Price = x.PO_UNIT_PRICE,
                                            ShipQty = x.ShipQty,
                                            TotalProvidedQuantity = x.TotalProvidedQuantity,
                                            ProvidedQuantity = x.ProvidedQuantity,
                                            ProvidedQuantity_2 = x.ProvidedQuantity_2,
                                            ProvidedQuantity_3 = x.ProvidedQuantity_3,
                                            ProvidedQuantity_4 = x.ProvidedQuantity_4,
                                            ProvidedQuantity_5 = x.ProvidedQuantity_5,
                                            EstDelivered = x.EstDelivered,
                                            EstDelivered_2 = x.EstDelivered_2,
                                            EstDelivered_3 = x.EstDelivered_3,
                                            EstDelivered_4 = x.EstDelivered_4,
                                            EstDelivered_5 = x.EstDelivered_5
                                        });

                    var supplierCode = (string)Session["supplierCode"];
                    if (!string.IsNullOrEmpty(supplierCode))
                        purchaseData = purchaseData.Where(p => p.SupplierCode.Trim() == supplierCode.Trim());

                    // Sorting
                    if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                    {
                        purchaseData = purchaseData.OrderBy(sortColumn + " " + sortColumnDir);
                    }
                    // Search
                    if (!string.IsNullOrEmpty(searchValue))
                    {
                            purchaseData = purchaseData.Where(p => p.SupplierCode.ToLower().Contains(searchValue.ToLower())
                                || (p.SupplierName != null && p.SupplierName.ToLower().Contains(searchValue.ToLower()))
                                || p.PurchaseNumber.ToLower().Contains(searchValue.ToLower())
                                || p.ItemNumber.ToString().ToLower().Contains(searchValue.ToLower())
                                || p.PartNumber.ToLower().Contains(searchValue.ToLower())
                                || (p.Description != null && p.Description.ToLower().Contains(searchValue.ToLower()))
                                || p.RequestQuantity.ToString().ToLower().Contains(searchValue.ToLower())
                                || p.Price.ToString().ToLower().Contains(searchValue.ToLower())
                                || p.Ospo.ToString().Contains(searchValue.ToLower())
                                || p.TotalProvidedQuantity.ToString().ToLower().Contains(searchValue.ToLower()));
                    }

                    // total number of rows count
                    recordsTotal = purchaseData.Count();
                    // Paging
                    var data = purchaseData.Skip(skip).Take(pageSize).ToList();
                    
                    // Returning Json Data
                    return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        // GET: PurchaseOrder/Details/5
        public ActionResult Details(string poNumber)
        {
            if (poNumber == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OutstandingPurchase purchase = db.OutstandingPurchases.FirstOrDefault(x => x.PO_NUMBER.Equals(poNumber) && !x.STATUS.Equals("CLOSE"));
            if (purchase == null)
            {
                return HttpNotFound();
            }
            return View(purchase);
        }

        // GET: PurchaseOrder/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PurchaseOrder/Create
        [HttpPost]
        public ActionResult Create(PurchaseOrder purchaseOrders)
        {
            try
            {
                // TODO: Add insert logic here
                using (kmsi_portalEntities db = new kmsi_portalEntities())
                {
                    db.PurchaseOrders.Add(purchaseOrders);
                    db.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Detail()
        {
            if (Session["UserId"] != null)
            {
                var supplierId = Convert.ToInt32(Session["SupplierId"]);
                if (supplierId > 0)
                    ViewData["Supplier"] = db.Suppliers.Where(p => p.SupplierId == supplierId).First();

                return View();
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        // POST: PurchaseOrder/EditBySupplier
        [HttpPost]
        public ActionResult EditBySupplier(FormCollection collection)
        {
            kmsi_portalEntities db = new kmsi_portalEntities();
            var purID = collection["PurchaseDetailId"];
            var purName = collection["PurchaseNumber"];
            var purDate = collection["PurchaseDate"];
            var ShipQty = collection["ShipQty[0]"];
            var EtdDelivery = collection["EtdDelivery[0]"];

            try
            {
                var length = collection.Keys.Count/4;
                var index = 0;
                while(index <= length)
                {
                    // TODO: Add insert logic here
                    OutstandingPurchase outstanding;
                    if (collection["PurchaseDetailId"] != "")
                    {
                        int pId = Convert.ToInt32(collection["PurchaseDetailId"]);
                        outstanding = db.OutstandingPurchases.Find(pId);
                    } else
                    {
                        outstanding = new OutstandingPurchase();
                        outstanding = db.OutstandingPurchases.Where(x => x.PO_NUMBER.Trim() == purName.Trim() && x.PO_DATE == Convert.ToDateTime(purDate)).FirstOrDefault();
                    }

                    switch (index)
                    {
                        case 0:
                            outstanding.PROVIDED_QTY_1 = (collection["ShipQty[" + index + "]"] != "") ? Convert.ToInt32(collection["ShipQty[" + index + "]"]) : (int?) null;
                            outstanding.ETD_1 = (collection["EtdDelivery[" + index + "]"] != "") ? Convert.ToDateTime(collection["EtdDelivery[" + index + "]"]) : (DateTime?) null;
                            break;

                        case 1:
                            outstanding.PROVIDED_QTY_2 = (collection["ShipQty[" + index + "]"] != "") ? Convert.ToInt32(collection["ShipQty[" + index + "]"]) : (int?) null;
                            outstanding.ETD_2 = (collection["EtdDelivery[" + index + "]"] != "") ? Convert.ToDateTime(collection["EtdDelivery[" + index + "]"]) : (DateTime?)null;
                            break;

                        case 2:
                            outstanding.PROVIDED_QTY_3 = (collection["ShipQty[" + index + "]"] != "") ? Convert.ToInt32(collection["ShipQty[" + index + "]"]) : (int?)null;
                            outstanding.ETD_3 = (collection["EtdDelivery[" + index + "]"] != "") ? Convert.ToDateTime(collection["EtdDelivery[" + index + "]"]) : (DateTime?)null;
                            break;

                        case 3:
                            outstanding.PROVIDED_QTY_4 = (collection["ShipQty[" + index + "]"] != "") ? Convert.ToInt32(collection["ShipQty[" + index + "]"]) : (int?)null;
                            outstanding.ETD_4 = (collection["EtdDelivery[" + index + "]"] != "") ? Convert.ToDateTime(collection["EtdDelivery[" + index + "]"]) : (DateTime?)null;
                            break;

                        case 4:
                            outstanding.PROVIDED_QTY_5 = (collection["ShipQty[" + index + "]"] != "") ? Convert.ToInt32(collection["ShipQty[" + index + "]"]) : (int?)null;
                            outstanding.ETD_5 = (collection["EtdDelivery[" + index + "]"] != "") ? Convert.ToDateTime(collection["EtdDelivery[" + index + "]"]) : (DateTime?)null;
                            break;

                        default:
                            outstanding.PROVIDED_QTY_1 = (collection["ShipQty[" + index + "]"] != "") ? Convert.ToInt32(collection["ShipQty[" + index + "]"]) : (int?)null;
                            outstanding.ETD_1 = (collection["EtdDelivery[" + index + "]"] != "") ? Convert.ToDateTime(collection["EtdDelivery[" + index + "]"]) : (DateTime?)null;
                            break;
                    }

                    if(collection["PurchaseDetailId[" + index + "]"] == "")
                    {
                        db.OutstandingPurchases.Add(outstanding);
                    }

                    HistoryETDShipment history = db.HistoryETDShipments.SingleOrDefault(x => x.SUPPLIER_CODE == outstanding.SUPPLIER_CODE.Trim() && x.PO_NUMBER == outstanding.PO_NUMBER.Trim() && x.PO_DATE == outstanding.PO_DATE && x.PART_NUMBER == outstanding.PART_NUMBER.Trim());
                    if (history == null)
                    {
                        history = new HistoryETDShipment();
                        history.SUPPLIER_CODE = outstanding.SUPPLIER_CODE.Trim();
                        history.PO_NUMBER = outstanding.PO_NUMBER.Trim();
                        history.PO_ITEM_NUMBER = outstanding.PO_ITEM_NUMBER.Trim();
                        history.PART_NUMBER = outstanding.PART_NUMBER.Trim();
                        history.PART_NAME = outstanding.PART_NAME.Trim();
                        history.PO_QTY = outstanding.PO_QTY;
                        history.PO_AMOUNT = outstanding.PO_AMOUNT;
                        history.CURRENCY_CODE = outstanding.CURRENCY_CODE;
                        history.PO_DATE = outstanding.PO_DATE;
                        history.AGING = outstanding.AGING;
                        history.REMARK = outstanding.REMARK;
                        history.ETD_1 = outstanding.ETD_1;
                        history.ETD_2 = outstanding.ETD_2;
                        history.ETD_3 = outstanding.ETD_3;
                        history.ETD_4 = outstanding.ETD_4;
                        history.ETD_5 = outstanding.ETD_5;
                        history.PROVIDED_QTY_1 = outstanding.PROVIDED_QTY_1;
                        history.PROVIDED_QTY_2 = outstanding.PROVIDED_QTY_2;
                        history.PROVIDED_QTY_3 = outstanding.PROVIDED_QTY_3;
                        history.PROVIDED_QTY_4 = outstanding.PROVIDED_QTY_4;
                        history.PROVIDED_QTY_5 = outstanding.PROVIDED_QTY_5;
                        history.LastUpdateAt = DateTime.Now;
                        User updateBy = db.Users.Find(Convert.ToInt32(Session["UserId"]));
                        history.LastUpdateBy = updateBy.FirstName + " " + updateBy.LastName;
                        db.HistoryETDShipments.Add(history);
                    }
                    else
                    {
                        history.SUPPLIER_CODE = outstanding.SUPPLIER_CODE.Trim();
                        history.PO_NUMBER = outstanding.PO_NUMBER.Trim();
                        history.PO_ITEM_NUMBER = outstanding.PO_ITEM_NUMBER.Trim();
                        history.PART_NUMBER = outstanding.PART_NUMBER.Trim();
                        history.PART_NAME = outstanding.PART_NAME.Trim();
                        history.PO_QTY = outstanding.PO_QTY;
                        history.PO_AMOUNT = outstanding.PO_AMOUNT;
                        history.CURRENCY_CODE = outstanding.CURRENCY_CODE;
                        history.PO_DATE = outstanding.PO_DATE;
                        history.AGING = outstanding.AGING;
                        history.REMARK = outstanding.REMARK;
                        history.ETD_1 = outstanding.ETD_1;
                        history.ETD_2 = outstanding.ETD_2;
                        history.ETD_3 = outstanding.ETD_3;
                        history.ETD_4 = outstanding.ETD_4;
                        history.ETD_5 = outstanding.ETD_5;
                        history.PROVIDED_QTY_1 = outstanding.PROVIDED_QTY_1;
                        history.PROVIDED_QTY_2 = outstanding.PROVIDED_QTY_2;
                        history.PROVIDED_QTY_3 = outstanding.PROVIDED_QTY_3;
                        history.PROVIDED_QTY_4 = outstanding.PROVIDED_QTY_4;
                        history.PROVIDED_QTY_5 = outstanding.PROVIDED_QTY_5;
                        history.LastUpdateAt = DateTime.Now;
                        User updateBy = db.Users.Find(Convert.ToInt32(Session["UserId"]));
                        history.LastUpdateBy = updateBy.FirstName + " " + updateBy.LastName;
                    }

                    db.SaveChanges();

                    index++;
                }

                var user = db.Users.Where(x => x.UserRole == 1).ToList();
                foreach (var u in user)
                {
                    //string result = ViewRenderer.RenderRazorViewToString(this, "~/Views/Mail/Email.cshtml", new Email()
                    //{
                    //    To = "m3ramdhani@gmail.com",
                    //    Subject = "TEST MAIL"
                    //});

                    //SendEmail(u.Email, "Invoice", result);
                }

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return RedirectToAction("Detail");
            }
        }

        // GET: PurchaseOrder/GetData/[PurchaseDetailId]
        [HttpGet]
        public String GetData(int id)
        {
            kmsi_portalEntities db = new kmsi_portalEntities();
            var po = (from p in db.OutstandingPurchases
                      where p.ID == id
                      select new
                      {
                          PurchaseDetailId = p.ID,
                          PurchaseNumber = p.PO_NUMBER,
                          PartNumber = p.PART_NUMBER,
                          PurchaseDate = p.PO_DATE,
                          RequestQuantity = p.PO_QTY,
                          ProvidedQuantity = p.PROVIDED_QTY_1,
                          EstDelivered = p.ETD_1,
                          ProvidedQuantity_2 = p.PROVIDED_QTY_2,
                          EstDelivered_2 = p.ETD_2,
                          ProvidedQuantity_3 = p.PROVIDED_QTY_3,
                          EstDelivered_3 = p.ETD_3,
                          ProvidedQuantity_4 = p.PROVIDED_QTY_4,
                          EstDelivered_4 = p.ETD_4,
                          ProvidedQuantity_5 = p.PROVIDED_QTY_5,
                          EstDelivered_5 = p.ETD_5
                      });

            String json = new JavaScriptSerializer().Serialize(po);

            return json;
        }

        // GET: PurchaseOrder/Edit/5
        public ActionResult Edit(int id)
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    PurchaseOrder purchaseOrder = db.PurchaseOrders.Find(id);
                    List<PurchaseDetail> purchaseDetails = db.PurchaseDetails.Where(d => d.PurchaseId == id).ToList();

                    ViewBag.details = purchaseDetails;
                    ViewBag.purchase = purchaseOrder;
                    ViewData["Suppliers"] = new SelectList(db.Suppliers, "SupplierId", "SupplierCode", purchaseOrder.SupplierId);

                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        // POST: PurchaseOrder/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
                PurchaseOrder purchase = db.PurchaseOrders.Find(id);
                Supplier curSupp = db.Suppliers.Find(purchase.SupplierId);

                int SupplierId = Convert.ToInt32(collection[1]);
                DateTime PurchaseDate = Convert.ToDateTime(collection[2]);
                string PurchaseNumber = collection[3];

                Supplier supplier = db.Suppliers.Find(SupplierId);
                string SupplierCode = supplier.SupplierCode;
                string SupplierName = supplier.SupplierName;

                string baseUrl = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                ViewData["Approval"] = baseUrl + "purchaseorder/saveedit/" + id;

                ViewData["SupplierNameBefore"] = curSupp.SupplierName;
                ViewData["PurchaseDateBefore"] = purchase.PurchaseDate.Value.ToString("yyyy-MM-dd");
                ViewData["PurchaseNumberBefore"] = purchase.PurchaseNumber;

                ViewData["SupplierNameAfter"] = SupplierName;
                ViewData["PurchaseDateAfter"] = PurchaseDate.ToString("yyyy-MM-dd");
                ViewData["PurchaseNumberAfter"] = PurchaseNumber;

                string linkDetail = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                ViewData["LinkEmail"] = linkDetail;

                string body = ViewRenderer.RenderRazorViewToString(this, "~/Views/Mail/Submitted_EditPO.cshtml", new Email()
                {
                    To = "Admin",
                    Date = DateTime.Now,
                    WorkOrders = db.WorkOrders.ToList()
                });

                bool emailStat = EmailHelper.SendEmail("yazzy@global.komatsu", "Request Edit PO", body, "");
                
                if (emailStat == true)
                {
                    TempEditPO tempEdit = new TempEditPO();
                    tempEdit.PurchaseId = id;
                    tempEdit.PurchaseDate = PurchaseDate;
                    tempEdit.PurchaseNumber = PurchaseNumber;
                    tempEdit.SupplierId = SupplierId;
                    db.TempEditPOes.Add(tempEdit);
                    db.SaveChanges();

                    return RedirectToAction("Index");
                } else
                {
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult SaveEdit(int id)
        {
            try
            {
                TempEditPO temp = db.TempEditPOes.Where(x => x.PurchaseId == id).FirstOrDefault();
                int SupplierId = Convert.ToInt32(temp.SupplierId);
                DateTime PurchaseDate = Convert.ToDateTime(temp.PurchaseDate);
                string PurchaseNumber = temp.PurchaseNumber;

                Supplier supplier = db.Suppliers.Find(SupplierId);
                string SupplierCode = supplier.SupplierCode;
                string SupplierName = supplier.SupplierName;

                PurchaseOrder purchase = db.PurchaseOrders.Find(id);
                purchase.PurchaseNumber = PurchaseNumber;
                purchase.PurchaseDate = PurchaseDate;
                purchase.SupplierId = SupplierId;
                purchase.SupplierCode = SupplierCode;
                db.SaveChanges();

                List<PurchaseDetail> details = db.PurchaseDetails.Where(x => x.PurchaseId == id).ToList();
                if (details != null)
                {
                    foreach (var d in details)
                    {
                        d.PurchaseNumber = PurchaseNumber;
                        d.SupplierCode = SupplierCode;
                        d.SupplierName = SupplierName;
                    }
                }
                db.SaveChanges();

                db.TempEditPOes.Remove(temp);
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        // GET: PurchaseOrder/EditDetail/5
        public ActionResult EditDetail(int? id)
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    OutstandingPurchase outstanding = db.OutstandingPurchases.Find(id);

                    ViewBag.masterSupplier = db.Suppliers.FirstOrDefault(x => x.SupplierCode == outstanding.SUPPLIER_CODE.Trim());
                    return View(outstanding);
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [HttpPost]
        public ActionResult EditDetail(OutstandingPurchase detail)
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    if (ModelState.IsValid)
                    {
                        OutstandingPurchase currentData = db.OutstandingPurchases.Find(detail.ID);

                        string baseUrl = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                        ViewData["Approval"] = baseUrl + "purchaseorder/saveeditdetail/" + detail.ID;

                        ViewData["ItemNumberBefore"] = currentData.PO_ITEM_NUMBER;
                        ViewData["PartNumberBefore"] = currentData.PART_NUMBER;
                        ViewData["DescriptionBefore"] = currentData.PART_NAME;
                        ViewData["POQtyBefore"] = currentData.PO_QTY;
                        ViewData["PriceBefore"] = currentData.PO_UNIT_PRICE;
                        ViewData["StockPointBefore"] = currentData.STOCK_POINT;

                        ViewData["ItemNumberAfter"] = detail.PO_ITEM_NUMBER;
                        ViewData["PartNumberAfter"] = detail.PART_NUMBER;
                        ViewData["DescriptionAfter"] = detail.PART_NAME;
                        ViewData["POQtyAfter"] = detail.PO_QTY;
                        ViewData["PriceAfter"] = detail.PO_UNIT_PRICE;
                        ViewData["StockPointAfter"] = detail.STOCK_POINT;

                        string linkDetail = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                        ViewData["LinkEmail"] = linkDetail;

                        string body = ViewRenderer.RenderRazorViewToString(this, "~/Views/Mail/Submitted_EditPODetail.cshtml", new Email()
                        {
                            To = "Admin",
                            Date = DateTime.Now,
                            WorkOrders = db.WorkOrders.ToList()
                        });

                        bool emailStat = EmailHelper.SendEmail("yazzy@global.komatsu", "Request Edit PO Detail", body, "");

                        if (emailStat == true)
                        {
                            TempEditPODetail tempEditPO = new TempEditPODetail();
                            tempEditPO.DetailID = detail.ID;
                            tempEditPO.ItemNumber = Convert.ToInt32(detail.PO_ITEM_NUMBER);
                            tempEditPO.PartNumber = detail.PART_NUMBER;
                            tempEditPO.Descript = detail.PART_NAME;
                            tempEditPO.ReqQty = detail.PO_QTY;
                            tempEditPO.Price = Convert.ToDecimal(detail.PO_UNIT_PRICE);
                            tempEditPO.StockPoint = detail.STOCK_POINT;
                            db.TempEditPODetails.Add(tempEditPO);
                            db.SaveChanges();

                            return RedirectToAction("Detail");
                        }
                        else
                        {
                            return RedirectToAction("Detail");
                        }
                    }
                    return View(detail);
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult SaveEditDetail(int id)
        {
            try
            {
                TempEditPODetail temp = db.TempEditPODetails.Where(x => x.DetailID == id).FirstOrDefault();

                OutstandingPurchase detail = db.OutstandingPurchases.Find(id);
                detail.PO_ITEM_NUMBER = temp.ItemNumber.ToString().Trim();
                detail.PART_NUMBER = temp.PartNumber.Trim();
                detail.PART_NAME = temp.Descript.Trim();
                detail.PO_QTY = Convert.ToInt32(temp.ReqQty);
                detail.PO_UNIT_PRICE = Convert.ToDouble(temp.Price);
                detail.STOCK_POINT = temp.StockPoint;
                db.SaveChanges();

                db.TempEditPODetails.Remove(temp);
                db.SaveChanges();

                return RedirectToAction("Detail");
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        // GET: PurchaseOrder/Delete/5
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                kmsi_portalEntities db = new kmsi_portalEntities();
                PurchaseOrder po = db.PurchaseOrders.Find(id);
                db.PurchaseOrders.Remove(po);
                db.SaveChanges();

                var details = db.PurchaseDetails.Where(x => x.PurchaseId == id).ToList();
                db.PurchaseDetails.RemoveRange(details);
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }

        // POST: PurchaseOrder/RequestDelete/5
        [HttpPost]
        public ActionResult RequestDelete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                var comment = collection["comment"];

                kmsi_portalEntities db = new kmsi_portalEntities();
                PurchaseOrder po = db.PurchaseOrders.Find(id);
                po.Request = "Request Delete";
                po.ReqComment = comment;

                db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }

        // POST: PurchaseOrder/RequestEdit/5
        [HttpPost]
        public ActionResult RequestEdit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                var comment = collection["comment"];

                kmsi_portalEntities db = new kmsi_portalEntities();
                PurchaseOrder po = db.PurchaseOrders.Find(id);
                po.Request = "Request Edit";
                po.ReqComment = comment;

                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }

        public FileResult DownloadExcel()
        {
            string path = "/Docs/PO_Format.xlsx";
            return File(path, "application/vnd.ms-excel", "PO_Format.xlsx");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [HandleError]
        public ActionResult Import(HttpPostedFileBase file)
        {
            try
            {
                string filePath = string.Empty;
                if (file != null)
                {
                    string path = Server.MapPath("~/Uploads/");
                    if (!Directory.Exists(path))
                        Directory.CreateDirectory(path);

                    filePath = path + Path.GetFileName(file.FileName);
                    string extension = Path.GetExtension(file.FileName).ToLower();
                    file.SaveAs(filePath);

                    string conString = string.Empty;
                    switch (extension)
                    {
                        case ".xls": //Excel 97-03.
                            conString = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                            break;

                        case ".xlsx": //Excel 07 and above.
                            conString = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                            break;
                    }

                    DataTable dt = new DataTable();
                    DataTable dtSum = new DataTable();
                    conString = string.Format(conString, filePath);

                    using (OleDbConnection connExcel = new OleDbConnection(conString))
                    {
                        using (OleDbCommand cmdExcel = new OleDbCommand())
                        {
                            using (OleDbDataAdapter odaExcel = new OleDbDataAdapter())
                            {
                                cmdExcel.Connection = connExcel;

                                //Get the name of First Sheet.
                                connExcel.Open();
                                DataTable dtExcelSchema;
                                dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                                string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                                connExcel.Close();

                                //Read Data from First Sheet.
                                connExcel.Open();
                                cmdExcel.CommandText = "SELECT * FROM [" + sheetName + "]";
                                odaExcel.SelectCommand = cmdExcel;
                                odaExcel.Fill(dt);
                                connExcel.Close();
                            }
                        }

                        using (OleDbCommand cmdExcelSum = new OleDbCommand())
                        {
                            using (OleDbDataAdapter odaExcelSum = new OleDbDataAdapter())
                            {
                                cmdExcelSum.Connection = connExcel;

                                //Get the name of First Sheet.
                                connExcel.Open();
                                DataTable dtExcelSchema;
                                dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                                string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                                connExcel.Close();

                                //Read Data from First Sheet.
                                connExcel.Open();
                                cmdExcelSum.CommandText = "SELECT * FROM [" + sheetName + "]";
                                odaExcelSum.SelectCommand = cmdExcelSum;
                                odaExcelSum.Fill(dtSum);
                                connExcel.Close();
                            }
                        }
                    }

                    conString = ConfigurationManager.ConnectionStrings["Constring"].ConnectionString;
                    using (SqlConnection con = new SqlConnection(conString))
                    {
                        using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                        {
                            // Set the database table name.
                            sqlBulkCopy.DestinationTableName = "dbo.PurchaseDetails";

                            //[OPTIONAL]: Map teh Excel columns with that of the database table
                            sqlBulkCopy.ColumnMappings.Add("SUPPLIER CODE", "SupplierCode");
                            sqlBulkCopy.ColumnMappings.Add("SUPPLIER NAME", "SupplierName");
                            sqlBulkCopy.ColumnMappings.Add("PO DATE", "PurchaseDate");
                            sqlBulkCopy.ColumnMappings.Add("PO NUMBER", "PurchaseNumber");
                            sqlBulkCopy.ColumnMappings.Add("ITEM NUMBER PO", "ItemNumber");
                            sqlBulkCopy.ColumnMappings.Add("PART NUMBER", "PartNumber");
                            sqlBulkCopy.ColumnMappings.Add("DESCRIPTION", "Description");
                            sqlBulkCopy.ColumnMappings.Add("PO QUANTITY", "RequestQuantity");
                            sqlBulkCopy.ColumnMappings.Add("UNIT PRICE", "Price");
                            sqlBulkCopy.ColumnMappings.Add("CURRENCY", "Currency");
                            sqlBulkCopy.ColumnMappings.Add("STOCK POINT", "StockPoint");

                            con.Open();
                            sqlBulkCopy.WriteToServer(dt);
                            con.Close();
                        }

                        using (SqlBulkCopy sqlBulkCopySum = new SqlBulkCopy(con))
                        {
                            // Set the database table name.
                            sqlBulkCopySum.DestinationTableName = "dbo.PurchaseOrders";

                            //[OPTIONAL]: Map teh Excel columns with that of the database table
                            sqlBulkCopySum.ColumnMappings.Add("SUPPLIER CODE", "SupplierCode");
                            sqlBulkCopySum.ColumnMappings.Add("PO DATE", "PurchaseDate");
                            sqlBulkCopySum.ColumnMappings.Add("PO NUMBER", "PurchaseNumber");

                            con.Open();
                            sqlBulkCopySum.WriteToServer(dtSum);
                            con.Close();
                        }
                    }

                    ViewBag.Message = "Success Import";
                    ModelState.Clear();
                    return RedirectToAction("Index");
                }
                else
                {
                    var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                    var sw = new System.IO.StreamWriter(filename, true);
                    sw.WriteLine(DateTime.Now.ToString() + " " + "File Not Selected!");
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();
            }

            return RedirectToAction("Detail");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [HandleError]
        public ActionResult UploadExcel(PurchaseOrder purchaseOrder, HttpPostedFileBase file)
        {
            TempData["ActionMessage"] = "Upload Failed";
            try
            {
                var data = new List<string>();
                if (file != null)
                {
                    if (file.ContentType == "application/vnd.ms-excel" || file.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                    {
                        var fileName = file.FileName;
                        var targetPath = Server.MapPath("~/Uploads/");
                        file.SaveAs(targetPath + fileName);
                        var pathToExcelFile = targetPath + fileName;
                        string FileName = Path.GetFileName(file.FileName);
                        string Extension = Path.GetExtension(file.FileName);
                        DataTable dataTable = FileImport.ImportToGrid(pathToExcelFile, Extension, "Yes");
                        List<PurchaseOrder> dataPurchaseList = new List<PurchaseOrder>();
                        foreach (DataRow item in dataTable.Rows)
                        {
                            try
                            {
                                var PurchaseNumber = item["PO NUMBER"].ToString();
                                string poDateFormat = item["PO DATE (YYYYMMDD)"].ToString();
                                string day = poDateFormat.Substring(6, 2);
                                string month = poDateFormat.Substring(4, 2);
                                string year = poDateFormat.Substring(0, 4);
                                string poDate = year + '-' + month + '-' + day;
                                var PurchaseDate = Convert.ToDateTime(poDate);
                                var isExist = db.PurchaseOrders.Where(x => x.PurchaseNumber == PurchaseNumber && x.PurchaseDate == PurchaseDate).FirstOrDefault();
                                var PurchaseID = 0;
                                var SupplierID = 0;
                                if (isExist != null)
                                {
                                    PurchaseID = isExist.PurchaseId;
                                    SupplierID = Convert.ToInt32(isExist.SupplierId);
                                    var PartNumber = item["PART NUMBER"].ToString();
                                    var detailExist = db.PurchaseDetails.Where(x => x.PurchaseNumber == PurchaseNumber && x.PurchaseDate == PurchaseDate && x.PartNumber == PartNumber).FirstOrDefault();
                                    var DetailID = 0;
                                    if (detailExist != null)
                                    {
                                        DetailID = detailExist.PurchaseDetailId;
                                    }
                                    else
                                    {
                                        PurchaseDetail purchaseDetail = new PurchaseDetail();
                                        purchaseDetail.PurchaseId = PurchaseID;
                                        purchaseDetail.ItemNumber = Convert.ToInt32(item["ITEM NUMBER PO"]);
                                        purchaseDetail.Description = item["DESCRIPTION"].ToString();
                                        purchaseDetail.RequestQuantity = Convert.ToDecimal(item["PO QUANTITY"]);
                                        purchaseDetail.Price = Convert.ToDecimal(item["UNIT PRICE"]);
                                        purchaseDetail.Currency = item["CURRENCY"].ToString();
                                        purchaseDetail.CreatedDate = DateTime.Now;
                                        purchaseDetail.CreatedBy = Convert.ToInt32(Session["UserId"]);
                                        purchaseDetail.SupplierCode = item["SUPPLIER CODE"].ToString();
                                        purchaseDetail.SupplierName = item["SUPPLIER NAME"].ToString();
                                        purchaseDetail.PurchaseDate = PurchaseDate;
                                        purchaseDetail.PurchaseNumber = PurchaseNumber;
                                        purchaseDetail.PartNumber = PartNumber;
                                        purchaseDetail.StockPoint = item["STOCK POINT"].ToString();
                                        purchaseDetail.Remarks = item["REMARKS"].ToString();
                                        db.PurchaseDetails.Add(purchaseDetail);
                                        db.SaveChanges();

                                        TempData["ActionMessage"] = "Upload Success";
                                    }
                                }
                                else
                                {
                                    var SupplierCode = item["SUPPLIER CODE"].ToString();
                                    Supplier supplier = db.Suppliers.Where(x => x.SupplierCode == SupplierCode).First();
                                    SupplierID = supplier.SupplierId;

                                    PurchaseOrder objPurchase = new PurchaseOrder();
                                    objPurchase.SupplierCode = item["SUPPLIER CODE"].ToString();
                                    objPurchase.SupplierId = SupplierID;
                                    objPurchase.PurchaseDate = PurchaseDate;
                                    objPurchase.PurchaseNumber = PurchaseNumber;
                                    objPurchase.Remarks = item["REMARKS"].ToString();
                                    objPurchase.CreatedDate = DateTime.Now;
                                    objPurchase.CreatedBy = Convert.ToInt32(Session["UserId"]);
                                    db.PurchaseOrders.Add(objPurchase);
                                    db.SaveChanges();

                                    PurchaseDetail purchaseDetail = new PurchaseDetail();
                                    purchaseDetail.PurchaseId = objPurchase.PurchaseId;
                                    purchaseDetail.ItemNumber = Convert.ToInt32(item["ITEM NUMBER PO"]);
                                    purchaseDetail.Description = item["DESCRIPTION"].ToString();
                                    purchaseDetail.RequestQuantity = Convert.ToDecimal(item["PO QUANTITY"]);
                                    purchaseDetail.Price = Convert.ToDecimal(item["UNIT PRICE"]);
                                    purchaseDetail.Currency = item["CURRENCY"].ToString();
                                    purchaseDetail.CreatedDate = DateTime.Now;
                                    purchaseDetail.CreatedBy = Convert.ToInt32(Session["UserId"]);
                                    purchaseDetail.SupplierCode = item["SUPPLIER CODE"].ToString();
                                    purchaseDetail.SupplierName = item["SUPPLIER NAME"].ToString();
                                    purchaseDetail.PurchaseDate = PurchaseDate;
                                    purchaseDetail.PurchaseNumber = PurchaseNumber;
                                    purchaseDetail.PartNumber = item["PART NUMBER"].ToString();
                                    purchaseDetail.StockPoint = item["STOCK POINT"].ToString();
                                    purchaseDetail.Remarks = item["REMARKS"].ToString();
                                    db.PurchaseDetails.Add(purchaseDetail);
                                    db.SaveChanges();

                                    TempData["ActionMessage"] = "Upload Success";
                                }

                                var user = db.Users.Where(x => x.SupplierId == SupplierID).ToList();
                                string baseUrl = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                                foreach (var u in user)
                                {
                                    string BodyEmail = "New PO Inserted <br> " + baseUrl;
                                    EmailHelper.SendEmail(u.Email, "Purchase Order", BodyEmail, "");
                                }

                            }
                            catch (Exception ex)
                            {
                                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                                var sw = new System.IO.StreamWriter(filename, true);
                                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                                sw.Close();
                            }
                        }
                    }
                }
                else
                {
                    var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                    var sw = new System.IO.StreamWriter(filename, true);
                    sw.WriteLine(DateTime.Now.ToString() + " " + "File Not Selected!");
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();
            }

            return RedirectToAction("Detail");
        }

        // GET: PurchaseOrder/Index
        public ActionResult ClosePurchase()
        {
            if (Session["UserId"] != null)
            {
                using (kmsi_portalEntities db = new kmsi_portalEntities())
                {
                    var supplierId = Convert.ToInt32(Session["SupplierId"]);
                    if (supplierId > 0)
                        ViewData["Supplier"] = db.Suppliers.Where(p => p.SupplierId == supplierId).First();

                    return View();
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        public ActionResult CloseList()
        {
            try
            {
                // Creating instance of DatabaseContext class
                using (kmsi_portalEntities db = new kmsi_portalEntities())
                {
                    var draw = Request.Form.GetValues("draw").FirstOrDefault();
                    var start = Request.Form.GetValues("start").FirstOrDefault();
                    var length = Request.Form.GetValues("length").FirstOrDefault();
                    var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                    var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                    var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();

                    // Paging Size
                    int pageSize = length != null ? Convert.ToInt32(length) : 0;
                    int skip = start != null ? Convert.ToInt32(start) : 0;
                    int recordsTotal = 0;

                    // Getting all Purchase data
                    var purchaseData = db.vSummaryOspoes.AsEnumerable()
                        .Where(p => p.ShipQty >= p.ReqQty)
                        .Select(x => new
                        {
                            x.SupplierCode,
                            x.SupplierName,
                            PurchaseDate = x.PurchaseDate.Value.ToString("yyyy-MM-dd"),
                            x.PurchaseNumber,
                            x.Amount,
                            x.Currency,
                            x.Aging,
                            x.Remarks,
                            x.ShipQty,
                            x.ReqQty
                        });

                    var supplierCode = (string)Session["SupplierCode"];
                    if (!string.IsNullOrEmpty(supplierCode))
                        purchaseData = purchaseData.Where(p => p.SupplierCode.Trim().Equals(supplierCode.Trim()));

                    // Sorting
                    if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                    {
                        purchaseData = purchaseData.OrderBy(sortColumn + " " + sortColumnDir);
                    }
                    // Search
                    if (!string.IsNullOrEmpty(searchValue))
                    {
                        purchaseData = purchaseData.Where(p => p.SupplierCode.ToLower().Contains(searchValue.ToLower())
                            || p.SupplierName.ToLower().Contains(searchValue.ToLower())
                            || p.PurchaseNumber.ToLower().Contains(searchValue.ToLower())
                            || p.Amount.ToString().ToLower().Contains(searchValue.ToLower())
                            || p.Aging.ToString().ToLower().Contains(searchValue.ToLower()));
                    }

                    // total number of rows count
                    recordsTotal = purchaseData.Count();
                    // Paging
                    var data = purchaseData.Skip(skip).Take(pageSize).ToList();
                    // Returning Json Data
                    return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult HistoryETD()
        {
            if (Session["UserId"] != null)
            {
                var supplierId = Convert.ToInt32(Session["SupplierId"]);
                if (supplierId > 0)
                    ViewData["Supplier"] = db.Suppliers.Where(p => p.SupplierId == supplierId).First();

                return View();
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        public ActionResult HistoryList()
        {
            try
            {
                // Creating instance of DatabaseContext class
                using (kmsi_portalEntities db = new kmsi_portalEntities())
                {
                    var draw = Request.Form.GetValues("draw").FirstOrDefault();
                    var start = Request.Form.GetValues("start").FirstOrDefault();
                    var length = Request.Form.GetValues("length").FirstOrDefault();
                    var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                    var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                    var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();

                    // Paging Size
                    int pageSize = length != null ? Convert.ToInt32(length) : 0;
                    int skip = start != null ? Convert.ToInt32(start) : 0;
                    int recordsTotal = 0;

                    // Getting all Purchase data
                    var purchaseData = (from pd in db.HistoryETDShipments.AsEnumerable()
                                        select new
                                        {
                                            pd.ID,
                                            pd.PO_DATE,
                                            pd.PO_NUMBER,
                                            pd.PO_ITEM_NUMBER,
                                            pd.PART_NUMBER,
                                            pd.PART_NAME,
                                            RequestQuantity = pd.PO_QTY.HasValue ? pd.PO_QTY : 0,
                                            pd.SUPPLIER_CODE,
                                            SUPPLIER_NAME = db.Suppliers.FirstOrDefault(x => x.SupplierCode == pd.SUPPLIER_CODE.Trim()).SupplierName,
                                            TotalProvidedQuantity = ((pd.PROVIDED_QTY_1.HasValue) ? pd.PROVIDED_QTY_1 : 0) + ((pd.PROVIDED_QTY_2.HasValue) ? pd.PROVIDED_QTY_2 : 0) + ((pd.PROVIDED_QTY_3.HasValue) ? pd.PROVIDED_QTY_3 : 0) + ((pd.PROVIDED_QTY_4.HasValue) ? pd.PROVIDED_QTY_4 : 0) + ((pd.PROVIDED_QTY_5.HasValue) ? pd.PROVIDED_QTY_5 : 0),
                                            ProvidedQuantity = pd.PROVIDED_QTY_1,
                                            ProvidedQuantity_2 = pd.PROVIDED_QTY_2,
                                            ProvidedQuantity_3 = pd.PROVIDED_QTY_3,
                                            ProvidedQuantity_4 = pd.PROVIDED_QTY_4,
                                            ProvidedQuantity_5 = pd.PROVIDED_QTY_5,
                                            EstDelivered = pd.ETD_1,
                                            EstDelivered_2 = pd.ETD_2,
                                            EstDelivered_3 = pd.ETD_3,
                                            EstDelivered_4 = pd.ETD_4,
                                            EstDelivered_5 = pd.ETD_5,
                                            ShipQty = db.PurchaseInvoiceDetails.Where(x => x.PurchaseNumber == pd.PO_NUMBER.Trim() && x.PartNumberKomatsu == pd.PART_NUMBER.Trim()).Select(x => x.ShipQty).DefaultIfEmpty(0).Sum(),
                                            UpdateAt = pd.LastUpdateAt.Value.ToString("yyyy-MM-dd"),
                                            UpdateBy = pd.LastUpdateBy
                                        }).ToList().Select(x => new
                                        {
                                            Ospo = x.RequestQuantity - x.TotalProvidedQuantity,
                                            SupplierCode = x.SUPPLIER_CODE,
                                            PurchaseDetailId = x.ID,
                                            SupplierName = x.SUPPLIER_NAME,
                                            PurchaseDate = x.PO_DATE,
                                            PurchaseNumber = x.PO_NUMBER,
                                            ItemNumber = x.PO_ITEM_NUMBER,
                                            PartNumber = x.PART_NUMBER,
                                            Description = x.PART_NAME,
                                            RequestQuantity = x.RequestQuantity,
                                            ShipQty = x.ShipQty,
                                            TotalProvidedQuantity = x.TotalProvidedQuantity,
                                            ProvidedQuantity = x.ProvidedQuantity,
                                            ProvidedQuantity_2 = x.ProvidedQuantity_2,
                                            ProvidedQuantity_3 = x.ProvidedQuantity_3,
                                            ProvidedQuantity_4 = x.ProvidedQuantity_4,
                                            ProvidedQuantity_5 = x.ProvidedQuantity_5,
                                            EstDelivered = x.EstDelivered,
                                            EstDelivered_2 = x.EstDelivered_2,
                                            EstDelivered_3 = x.EstDelivered_3,
                                            EstDelivered_4 = x.EstDelivered_4,
                                            EstDelivered_5 = x.EstDelivered_5,
                                            UpdateAt = x.UpdateAt,
                                            UpdateBy = x.UpdateBy
                                        });

                    var supplierCode = (string)Session["supplierCode"];
                    if (!string.IsNullOrEmpty(supplierCode))
                        purchaseData = purchaseData.Where(p => p.SupplierCode.Trim() == supplierCode.Trim());

                    // Sorting
                    if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                    {
                        purchaseData = purchaseData.OrderBy(sortColumn + " " + sortColumnDir);
                    }
                    // Search
                    if (!string.IsNullOrEmpty(searchValue))
                    {
                        purchaseData = purchaseData.Where(p => p.SupplierCode.ToLower().Contains(searchValue.ToLower())
                            || p.PurchaseNumber.ToLower().Contains(searchValue.ToLower())
                            || p.ItemNumber.ToString().ToLower().Contains(searchValue.ToLower())
                            || p.PartNumber.ToLower().Contains(searchValue.ToLower())
                            || (p.Description != null && p.Description.ToLower().Contains(searchValue.ToLower()))
                            || p.RequestQuantity.ToString().ToLower().Contains(searchValue.ToLower())
                            || p.SupplierName.ToLower().Contains(searchValue.ToLower())
                            || (p.RequestQuantity - p.TotalProvidedQuantity).ToString().Contains(searchValue.ToLower())
                            || p.ShipQty.ToString().ToLower().Contains(searchValue.ToLower()));
                    }

                    // total number of rows count
                    recordsTotal = purchaseData.Count();
                    // Paging
                    var data = purchaseData.Skip(skip).Take(pageSize).ToList();

                    // Returning Json Data
                    return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [HttpPost]
        public ActionResult ReSync()
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    string Message = String.Empty;
                    bool Status = false;

                    bool isRunning = false;
                    StdSchedulerFactory schedulerFactory = new StdSchedulerFactory();
                    IScheduler scheduler = schedulerFactory.GetScheduler().Result;
                    var executingJobs = scheduler.GetCurrentlyExecutingJobs();
                    foreach (var job in executingJobs.Result)
                    {
                        if (job.Trigger.Key.Name.Equals("triggerOspo"))
                        {
                            DateTimeOffset? nextFireTime = job.Trigger.GetNextFireTimeUtc();
                            if (nextFireTime.HasValue)
                            {
                                DateTimeOffset currentTime = DateTimeOffset.UtcNow;

                                if (currentTime.Equals(nextFireTime))
                                {
                                    isRunning = true;
                                    break;
                                }
                            }
                        }
                    }

                    if (isRunning)
                    {
                        Message = "OSPO data is syncing, please wait a moment or try re-syncing later";
                        Status = false;
                    }
                    else
                    {
                        string remote = ConfigurationManager.ConnectionStrings["OspoConnectionString"].ConnectionString;
                        string local = ConfigurationManager.ConnectionStrings["Constring"].ConnectionString;

                        FileImport.ImportFromDB(remote, local);
                        Message = "Re-sync OSPO data success";
                        Status = true;
                    }

                    return new JsonResult() { Data = new { Success = Status, Message = Message } };
                }
                else
                {
                    return new JsonResult() { Data = new { Success = false, Message = "Your session is expired, please re-login!" } };
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new JsonResult() { Data = new { Success = false, Message = "Re-sync OSPO data failed" } };
            }
        }
    }
}
