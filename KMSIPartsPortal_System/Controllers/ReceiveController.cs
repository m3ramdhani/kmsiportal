﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using iTextSharp.text;
using iTextSharp.text.pdf;
using KMSIPartsPortal_System.Helpers;
using KMSIPartsPortal_System.Models;
using OfficeOpenXml;

namespace KMSIPartsPortal_System.Controllers
{
    public class ReceiveController : Controller
    {
        private kmsi_portalEntities db = new kmsi_portalEntities();

        public ActionResult List()
        {
            try
            {
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                var status = Request.Form.GetValues("status").FirstOrDefault();
                var role = Request.Form.GetValues("role").FirstOrDefault();
                var valid = Request.Form.GetValues("valid").FirstOrDefault();

                // Paging Size
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                int IsComplete = Convert.ToInt32(status);
                int IsValid = Convert.ToInt32(valid);
                int RoleID = Convert.ToInt32(role);

                string[] statusHub2 = new string[] { "HUB3W", "RHUB3" };
                string[] statusBJM = new string[] { "SHIPBJM", "RBJM", "FQCW" };
                string[] statusGGI = new string[] { "HUB3W", "RHUB3", "SHIPBJM", "RBJM" };
                string[] statusKMSI = new string[] { "HUB3W", "RHUB3", "SHIPBJM", "RBJM", "FQCW" };

                // Getting all Purchase data
                var receiveData = db.IncomingLists.AsEnumerable()
                                .Select(p => new
                                {
                                    ID = p.ID,
                                    DONO = p.DONO,
                                    ReceiveDate = p.ReceiveDate.HasValue ? p.ReceiveDate.Value.ToString("yyyy-MM-dd") : "",
                                    ForwardDate = p.ForwardDate.HasValue ? p.ForwardDate.Value.ToString("yyyy-MM-dd") : "",
                                    DeliveryDate = p.IncomingDate.HasValue ? p.IncomingDate.Value.ToString("yyyy-MM-dd") : "",
                                    Status = p.IncomingStatus != null ? p.IncomingStatus.Name : "",
                                    StatusName = p.IncomingStatus != null ? p.IncomingStatus.Value : "",
                                    Quantity = p.Quantity.HasValue ? p.Quantity.Value.ToString() : "",
                                    ActualQty = p.ActualQty.HasValue ? p.ActualQty.Value.ToString() : "",
                                    IsValid = p.IsValid,
                                    IsShipment = p.IsShipment
                                })
                                .GroupBy(x => x.DONO)
                                .Select(g => g.First());
                
                if (IsComplete == 0)
                {
                    if (RoleID == 12)
                    {
                        receiveData = receiveData.Where(x => statusHub2.Contains(x.StatusName));
                    }
                    else if (RoleID == 10)
                    {
                        receiveData = receiveData.Where(x => statusBJM.Contains(x.StatusName));
                    }
                    else if (RoleID == 7)
                    {
                        receiveData = receiveData.Where(x => statusGGI.Contains(x.StatusName));
                    }
                    else if (RoleID == 1 || RoleID == 6)
                    {
                        receiveData = receiveData.Where(x => statusKMSI.Contains(x.StatusName));
                    }
                }
                else if (IsComplete == 1)
                {
                    if (RoleID == 7 && IsValid == 0)
                    {
                        receiveData = receiveData.Where(x => x.StatusName == "FQCW");
                    }
                    else if(RoleID == 12 && IsValid == 1)
                    {
                        receiveData = receiveData.Where(x => x.StatusName == "SHIPBJM");
                    }
                    else
                    {
                        receiveData = receiveData.Where(x => x.StatusName == "GGI");
                    }
                }

                // Sorting
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    receiveData = receiveData.OrderBy(sortColumn + " " + sortColumnDir);
                }
                // Search
                if (!string.IsNullOrEmpty(searchValue))
                {
                    receiveData = receiveData.Where(p => p.DONO.ToLower().Contains(searchValue.ToLower())
                        || p.DeliveryDate.ToLower().Contains(searchValue.ToLower()));
                }

                // total number of rows count
                recordsTotal = receiveData.Count();
                // Paging
                var data = receiveData.Skip(skip).Take(pageSize).ToList();
                // Returning Json Data
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult Details(string dono)
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    List<IncomingList> incomings = db.IncomingLists.Where(x => x.DONO == dono).ToList();

                    ViewBag.summary = incomings.First();
                    ViewBag.lists = incomings;
                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult Edit(string dono)
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    List<IncomingList> incomings = db.IncomingLists.Where(x => x.DONO == dono && x.IsShipment == 1).ToList();

                    ViewBag.summary = incomings.First();
                    ViewBag.lists = incomings;
                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [HttpPost]
        public ActionResult Edit(string dono, IncomingList incomingList, FormCollection form)
        {
            try
            {
                List<IncomingList> currData = db.IncomingLists.Where(x => x.DONO == dono && x.IsShipment == 1).ToList();

                foreach (var item in currData)
                {
                    int ID = item.ID;
                    int StatusId = db.IncomingStatuses.SingleOrDefault(x => x.Value == "FQCW").ID;
                    IncomingList incoming = db.IncomingLists.Find(ID);
                    incoming.ActualQty = (form["Actuals[" + ID + "]"] != "") ? (int?)Convert.ToInt32(form["Actuals[" + ID + "]"]) : null;
                    incoming.StatusId = StatusId;
                    db.SaveChanges();
                }

                return RedirectToAction("Receive", "Incoming");
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult Confirm(string dono)
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    List<IncomingList> incomings = db.IncomingLists.Where(x => x.DONO == dono && x.IsShipment == 1).ToList();

                    ViewBag.summary = incomings.First();
                    ViewBag.lists = incomings;
                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult doConfirm(string dono)
        {
            try
            {
                int StatusId = db.IncomingStatuses.SingleOrDefault(x => x.Value == "GGI").ID;
                var incoming = db.IncomingLists.Where(x => x.DONO == dono).ToList();
                incoming.ForEach(x =>
                {
                    x.StatusId = StatusId;
                    x.IsValid = 1;
                    x.UpdatedBy = Convert.ToInt32(Session["UserId"]);
                    x.UpdatedAt = DateTime.Now;
                });
                db.SaveChanges();

                var list = db.IncomingLists.Where(x => x.DONO == dono).ToList();
                foreach (var row in list)
                {
                    //Stock stock = db.Stocks.Where(x => x.PartNumber == row.PartNumber && x.IncomingDeliveryDate == row.IncomingDate && x.Position == "GGI").FirstOrDefault();
                    Stock stock = db.Stocks.Where(x => x.PartNumber == row.PartNumber && x.Position == "GGI").FirstOrDefault();
                    if (stock != null)
                    {
                        stock.Quantity += row.ActualQty;
                        stock.UpdatedBy = Convert.ToInt32(Session["UserId"]);
                        stock.UpdatedAt = DateTime.Now;
                    }
                    else
                    {
                        Stock data = new Stock();
                        data.PartNumber = row.PartNumber;
                        data.Quantity = row.ActualQty;
                        data.Position = "GGI";
                        data.CreatedBy = Convert.ToInt32(Session["UserId"]);
                        data.CreatedAt = DateTime.Now;
                        data.UpdatedBy = Convert.ToInt32(Session["UserId"]);
                        data.UpdatedAt = DateTime.Now;
                        data.IncomingDeliveryDate = row.IncomingDate;
                        data.DONO = row.DONO;
                        data.PartName = row.Description;
                        db.Stocks.Add(data);
                    }
                    db.SaveChanges();
                }

                List<StockMailModel> email = db.IncomingLists.AsEnumerable()
                    .Where(x => x.DONO == dono && x.IsValid == 1)
                    .Join(db.Users, s => s.UpdatedBy, u => u.UserId, (s, u) => new StockMailModel
                    {
                        PartNumber = s.PartNumber,
                        QtyReceipt = s.Quantity.ToString(),
                        Position = "GGI",
                        ConfirmedAt = s.UpdatedAt.HasValue ? s.UpdatedAt.Value.ToString("dd/MM/yyyy") : "",
                        ConfirmedBy = u.FirstName + " " + u.LastName
                    })
                    .ToList();

                int userId = Convert.ToInt32(Session["UserId"]);
                User user = db.Users.Find(userId);
                var userMailLists = new[] { "KMSI_Stock", "GGI", "KMSI_Warehouse" };
                List<User> userLists = db.Users.Where(x => userMailLists.Contains(x.Role.RoleName)).ToList();
                ViewData["DataList"] = email;

                string linkDetail = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                ViewData["LinkEmail"] = linkDetail;

                foreach (var data in userLists)
                {
                    string body = ViewRenderer.RenderRazorViewToString(this, "~/Views/Mail/ConfirmationReceipt.cshtml", new Email()
                    {
                        To = data.FirstName + " " + data.LastName,
                        Date = DateTime.Now
                    });

                    bool emailStatus = EmailHelper.SendEmail(data.Email, "Confirmation Goods Receipt", body, "");

                    if (emailStatus)
                    {
                        Notification notification = new Notification();
                        notification.UserId = user.UserId;
                        notification.Name = user.FirstName + " " + user.LastName;
                        notification.Module = "VSS";
                        notification.Message = "Goods Receipt has been confirmed by " + notification.Name;
                        notification.Url = linkDetail;
                        notification.ReceiveId = data.UserId;
                        notification.CreatedAt = DateTime.Now;
                        db.Notifications.Add(notification);
                        db.SaveChanges();
                    }
                }

                return RedirectToAction("Receive", "Incoming");
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult Forward(string dono)
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    List<IncomingList> incomings = db.IncomingLists.Where(x => x.DONO == dono).ToList();

                    ViewBag.summary = incomings.First();
                    ViewBag.lists = incomings;
                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult doReceive(string dono)
        {
            try
            {
                int StatusHub = db.IncomingStatuses.SingleOrDefault(x => x.Value == "RHUB3").ID;
                int StatusBJM = db.IncomingStatuses.SingleOrDefault(x => x.Value == "RBJM").ID;
                var incoming = db.IncomingLists.Where(x => x.DONO == dono).ToList();
                int? LastStatus = incoming.FirstOrDefault().IsShipment;
                DateTime? receiveDate = DateTime.Now;
                incoming.ForEach(x =>
                {
                    x.StatusId = LastStatus == 1 ? StatusBJM : StatusHub;
                    x.IsShipment = LastStatus == 1 ? 1 : 0;
                    x.ReceiveDate = receiveDate;
                });
                db.SaveChanges();

                return RedirectToAction("Receive", "Incoming");
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult doForward(string dono)
        {
            try
            {
                int StatusId = db.IncomingStatuses.SingleOrDefault(x => x.Value == "SHIPBJM").ID;
                var incoming = db.IncomingLists.Where(x => x.DONO == dono).ToList();
                incoming.ForEach(x =>
                {
                    x.StatusId = StatusId;
                    x.IsShipment = 1;
                    x.ForwardDate = DateTime.Now;
                });
                db.SaveChanges();

                return RedirectToAction("Receive", "Incoming");
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult IncomingListReport()
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    List<MenuModels> MenuMaster = (List<MenuModels>)Session["MenuMaster"];
                    Int32 RoleID = Convert.ToInt32(Session["RoleId"]);

                    if (RoleID == 0)
                    {
                        ViewBag.IsInput = 1;
                        ViewBag.IsUpdate = 1;
                        ViewBag.IsDelete = 1;
                        ViewBag.IsUpload = 1;
                        ViewBag.IsDownload = 1;
                    }
                    else
                    {
                        var access = MenuMaster.Where(x => x.ControllerName == "Receive" && x.ActionName == "IncomingListReport" && x.RoleId == RoleID).FirstOrDefault();

                        if (access.IsView == 1)
                        {
                            ViewBag.IsInput = access.IsInput;
                            ViewBag.IsUpdate = access.IsUpdate;
                            ViewBag.IsDelete = access.IsDelete;
                            ViewBag.IsUpload = access.IsUpload;
                            ViewBag.IsDownload = access.IsDownload;
                        }
                        else
                        {
                            ViewBag.Message = "You don't have access to this page! Please contact administrator.";
                        }
                    }

                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult IncomingResultReport()
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    List<MenuModels> MenuMaster = (List<MenuModels>)Session["MenuMaster"];
                    Int32 RoleID = Convert.ToInt32(Session["RoleId"]);

                    if (RoleID == 0)
                    {
                        ViewBag.IsInput = 1;
                        ViewBag.IsUpdate = 1;
                        ViewBag.IsDelete = 1;
                        ViewBag.IsUpload = 1;
                        ViewBag.IsDownload = 1;
                    }
                    else
                    {
                        var access = MenuMaster.Where(x => x.ControllerName == "Receive" && x.ActionName == "IncomingResultReport" && x.RoleId == RoleID).FirstOrDefault();

                        if (access.IsView == 1)
                        {
                            ViewBag.IsInput = access.IsInput;
                            ViewBag.IsUpdate = access.IsUpdate;
                            ViewBag.IsDelete = access.IsDelete;
                            ViewBag.IsUpload = access.IsUpload;
                            ViewBag.IsDownload = access.IsDownload;
                        }
                        else
                        {
                            ViewBag.Message = "You don't have access to this page! Please contact administrator.";
                        }
                    }

                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult GenerateXls(string[] ids, int? status, int? valid)
        {
            try
            {
                DateTime dateTime = DateTime.Now;
                string FileName = String.Empty;
                if (status == 0 && valid == 0)
                {
                    FileName = string.Format("OUTSTANDING INCOMING LIST_" + dateTime.ToString("yyyyMMdd_HHmm") + ".xlsx");
                }
                else if (status == 1 && valid == 0)
                {
                    FileName = string.Format("WAITING CONFIRMATION LIST_" + dateTime.ToString("yyyyMMdd_HHmm") + ".xlsx");
                }
                else if (status == 1 && valid == 1)
                {
                    Int32 RoleID = Convert.ToInt32(Session["RoleId"]);
                    FileName = string.Format("COMPLETE RECEIVE AND FORWARD LIST_" + dateTime.ToString("yyyyMMdd_HHmm") + ".xlsx");
                    if (RoleID == 7)
                    {
                        FileName = string.Format("COMPLETE RECEIVE LIST_" + dateTime.ToString("yyyyMMdd_HHmm") + ".xlsx");
                    }
                }
                string handle = Guid.NewGuid().ToString();

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (ExcelPackage excel = new ExcelPackage())
                    {
                        excel.Workbook.Worksheets.Add("Incoming List");

                        var headerRow = new List<string[]>()
                        {
                            new string[] { "Incoming List No", "Delivery Date", "Status" }
                        };
                        string headerRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";

                        var worksheet = excel.Workbook.Worksheets["Incoming List"];
                        worksheet.Cells[headerRange].LoadFromArrays(headerRow);
                        worksheet.Cells[headerRange].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        worksheet.Cells[headerRange].Style.Font.Bold = true;
                        worksheet.Cells[headerRange].Style.Font.Size = 12;
                        worksheet.Cells[headerRange].AutoFitColumns();

                        var receiveData = db.IncomingLists.AsEnumerable()
                                .Where(x => ids.Contains(x.DONO))
                                .Select(p => new
                                {
                                    ID = p.ID,
                                    DONO = p.DONO,
                                    DeliveryDate = p.IncomingDate.HasValue ? p.IncomingDate.Value.ToString("yyyy-MM-dd") : "",
                                    Status = p.IncomingStatus != null ? p.IncomingStatus.Name : "",
                                    StatusName = p.IncomingStatus != null ? p.IncomingStatus.Value : "",
                                    IsValid = p.IsValid,
                                    IsShipment = p.IsShipment
                                })
                                .GroupBy(x => x.DONO)
                                .Select(g => g.First());

                        int rowNumber = 2;
                        foreach (var data in receiveData)
                        {
                            worksheet.Cells[rowNumber, 1].Value = data.DONO;
                            worksheet.Cells[rowNumber, 2].Value = data.DeliveryDate;
                            worksheet.Cells[rowNumber, 3].Value = data.StatusName;

                            rowNumber++;
                        }
                        excel.SaveAs(memoryStream);
                    }
                    memoryStream.Position = 0;
                    TempData[handle] = memoryStream.ToArray();
                }

                return new JsonResult()
                {
                    Data = new { FileGuid = handle, FileName = FileName }
                };
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult GenerateXlsResult(string[] ids)
        {
            try
            {
                DateTime dateTime = DateTime.Now;
                string FileName = string.Format("INCOMING RAW MATERIAL RESULT REPORT_" + dateTime.ToString("yyyyMMdd_HHmm") + ".xlsx");
                string handle = Guid.NewGuid().ToString();

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (ExcelPackage excel = new ExcelPackage())
                    {
                        excel.Workbook.Worksheets.Add("Incoming List");

                        var headerRow = new List<string[]>()
                        {
                            new string[] { "Incoming List No.", "Delivery Date", "Quantity", "Actual Quantity" }
                        };
                        string headerRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";

                        var worksheet = excel.Workbook.Worksheets["Incoming List"];
                        worksheet.Cells[headerRange].LoadFromArrays(headerRow);
                        worksheet.Cells[headerRange].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        worksheet.Cells[headerRange].Style.Font.Bold = true;
                        worksheet.Cells[headerRange].Style.Font.Size = 12;
                        worksheet.Cells[headerRange].AutoFitColumns();

                        var receiveData = db.IncomingLists.AsEnumerable()
                                .Where(x => ids.Contains(x.DONO))
                                .Select(p => new
                                {
                                    ID = p.ID,
                                    DONO = p.DONO,
                                    DeliveryDate = p.IncomingDate.HasValue ? p.IncomingDate.Value.ToString("yyyy-MM-dd") : "",
                                    Status = p.IncomingStatus != null ? p.IncomingStatus.Name : "",
                                    StatusName = p.IncomingStatus != null ? p.IncomingStatus.Value : "",
                                    Quantity = p.Quantity.HasValue ? p.Quantity.Value.ToString() : "",
                                    ActualQty = p.ActualQty.HasValue ? p.ActualQty.Value.ToString() : "",
                                    IsValid = p.IsValid,
                                    IsShipment = p.IsShipment
                                })
                                .GroupBy(x => x.DONO)
                                .Select(g => g.First());

                        int rowNumber = 2;
                        foreach (var data in receiveData)
                        {
                            worksheet.Cells[rowNumber, 1].Value = data.DONO;
                            worksheet.Cells[rowNumber, 2].Value = data.DeliveryDate;
                            worksheet.Cells[rowNumber, 3].Value = data.Quantity;
                            worksheet.Cells[rowNumber, 4].Value = data.ActualQty;

                            rowNumber++;
                        }
                        excel.SaveAs(memoryStream);
                    }
                    memoryStream.Position = 0;
                    TempData[handle] = memoryStream.ToArray();
                }

                return new JsonResult()
                {
                    Data = new { FileGuid = handle, FileName = FileName }
                };
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [HttpGet]
        public ActionResult DownloadXls(string fileGuid, string filename)
        {
            if (TempData[fileGuid] != null)
            {
                byte[] data = TempData[fileGuid] as byte[];
                return File(data, "application/vnd.ms-excel", filename);
            }
            else
            {
                return new EmptyResult();
            }
        }

        public ActionResult GeneratePdf(string[] ids, int? status, int? valid)
        {
            try
            {
                DateTime dateTime = DateTime.Now;
                string strPdfFileName = String.Empty;
                string titlePdf = String.Empty;
                if (status == 0 && valid == 0)
                {
                    strPdfFileName = string.Format("OUTSTANDING INCOMING LIST_" + dateTime.ToString("yyyyMMdd_HHmm") + ".pdf");
                    titlePdf = "OUTSTANDING INCOMING LIST";
                }
                else if (status == 1 && valid == 0)
                {
                    strPdfFileName = string.Format("WAITING CONFIRMATION LIST_" + dateTime.ToString("yyyyMMdd_HHmm") + ".pdf");
                    titlePdf = "WAITING CONFIRMATION LIST";
                }
                else if (status == 1 && valid == 1)
                {
                    Int32 RoleID = Convert.ToInt32(Session["RoleId"]);
                    strPdfFileName = string.Format("COMPLETE RECEIVE AND FORWARD LIST_" + dateTime.ToString("yyyyMMdd_HHmm") + ".pdf");
                    titlePdf = "COMPLETE RECEIVE AND FORWARD LIST";
                    if (RoleID == 7)
                    {
                        strPdfFileName = string.Format("COMPLETE RECEIVE LIST_" + dateTime.ToString("yyyyMMdd_HHmm") + ".pdf");
                        titlePdf = "COMPLETE RECEIVE LIST";
                    }
                }
                string handle = Guid.NewGuid().ToString();

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    Document pdfDoc = new Document(PageSize.A4.Rotate());
                    PdfWriter.GetInstance(pdfDoc, memoryStream).CloseStream = false;
                    pdfDoc.SetMargins(28f, 28f, 28f, 28f);

                    string strAttachment = Server.MapPath("~/PDFs/" + strPdfFileName);

                    pdfDoc.Open();
                    Phrase phrase = new Phrase();

                    PdfPTable table = new PdfPTable(1);
                    table.WidthPercentage = 100;
                    table.SpacingAfter = 10f;


                    //add a LEFT LABEL 
                    table.AddCell(new PdfPCell(new Phrase("K-PINTAR PARTS PORTAL SYSTEM", FontFactory.GetFont("Calibri", 7, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });

                    // invoke LEFT image
                    iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Content/img/logo_kmsi.png"));
                    jpg.ScaleAbsolute(120f, 33f);
                    PdfPCell imageCell = new PdfPCell(jpg);
                    imageCell.Colspan = 2; // either 1 if you need to insert one cell
                    imageCell.Border = 0;
                    imageCell.HorizontalAlignment = Element.ALIGN_LEFT;

                    //add a RIGHT LABEL 
                    table.AddCell(new PdfPCell(new Phrase("VENDOR STOCK SYSTEM", FontFactory.GetFont("Calibri", 7, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_RIGHT,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });

                    // invoke RIGHT image
                    iTextSharp.text.Image rightJpg = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Content/img/logo-komatsu.png"));
                    rightJpg.ScaleAbsolute(120f, 40f);
                    PdfPCell rightImageCell = new PdfPCell(rightJpg);
                    rightImageCell.Colspan = 2; // either 1 if you need to insert one cell
                    rightImageCell.Border = 0;
                    rightImageCell.HorizontalAlignment = Element.ALIGN_RIGHT;

                    // add a LEFT image to PdfPTables
                    table.AddCell(imageCell);
                    // add a RIGHT image to PdfPTables
                    //table.AddCell(rightImageCell);



                    table.AddCell(new PdfPCell(new Phrase(titlePdf, FontFactory.GetFont("Calibri", 20, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });
                    pdfDoc.Add(table);

                    var receiveData = db.IncomingLists.AsEnumerable()
                        .Where(x => ids.Contains(x.DONO))
                        .Select(p => new
                        {
                            ID = p.ID,
                            DONO = p.DONO,
                            DeliveryDate = p.IncomingDate.HasValue ? p.IncomingDate.Value.ToString("yyyy-MM-dd") : "",
                            Status = p.IncomingStatus != null ? p.IncomingStatus.Name : "",
                            StatusName = p.IncomingStatus != null ? p.IncomingStatus.Value : "",
                            IsValid = p.IsValid,
                            IsShipment = p.IsShipment
                        })
                        .GroupBy(x => x.DONO)
                        .Select(g => g.First());

                    //Table
                    //float[] widths = new float[] { 70f, 50f, 100f };
                    table = new PdfPTable(3);
                    //table.TotalWidth = 220f;
                    //table.LockedWidth = true;
                    //table.SetWidths(widths);
                    table.WidthPercentage = 100;
                    table.SpacingBefore = 5f;
                    table.HeaderRows = 1;

                    //header
                    string[] headers = new string[] { "Incoming List No.", "Incoming Date", "Status" };
                    for (int i = 0; i < headers.Count(); i++)
                    {
                        table.AddCell(new PdfPCell(new Phrase(headers[i], FontFactory.GetFont("Calibri", 10)))
                        {
                            PaddingTop = 3,
                            PaddingBottom = 8,
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                    }

                    foreach (var item in receiveData)
                    {
                        table.AddCell(new PdfPCell(new Phrase(item.DONO != null ? item.DONO : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.DeliveryDate != null ? item.DeliveryDate : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.Status != null ? item.Status : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                    }
                    pdfDoc.Add(table);
                    pdfDoc.Close();

                    var bytes = memoryStream.ToArray();
                    Session[strPdfFileName] = bytes;
                }

                return Json(new { success = true, strPdfFileName }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult GeneratePdfResult(string[] ids)
        {
            try
            {
                DateTime dateTime = DateTime.Now;
                string strPdfFileName = string.Format("INCOMING RAW MATERIAL RESULT REPORT_" + dateTime.ToString("yyyyMMdd_HHmm") + ".pdf");
                string handle = Guid.NewGuid().ToString();

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    Document pdfDoc = new Document(PageSize.A4.Rotate());
                    PdfWriter.GetInstance(pdfDoc, memoryStream).CloseStream = false;
                    pdfDoc.SetMargins(28f, 28f, 28f, 28f);

                    string strAttachment = Server.MapPath("~/PDFs/" + strPdfFileName);

                    pdfDoc.Open();
                    Phrase phrase = new Phrase();

                    PdfPTable table = new PdfPTable(1);
                    table.WidthPercentage = 100;
                    table.SpacingAfter = 10f;


                    //add a LEFT LABEL 
                    table.AddCell(new PdfPCell(new Phrase("K-PINTAR PARTS PORTAL SYSTEM", FontFactory.GetFont("Calibri", 7, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });

                    // invoke LEFT image
                    iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Content/img/logo_kmsi.png"));
                    jpg.ScaleAbsolute(120f, 33f);
                    PdfPCell imageCell = new PdfPCell(jpg);
                    imageCell.Colspan = 2; // either 1 if you need to insert one cell
                    imageCell.Border = 0;
                    imageCell.HorizontalAlignment = Element.ALIGN_LEFT;

                    //add a RIGHT LABEL 
                    table.AddCell(new PdfPCell(new Phrase("VENDOR STOCK SYSTEM", FontFactory.GetFont("Calibri", 7, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_RIGHT,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });

                    // invoke RIGHT image
                    iTextSharp.text.Image rightJpg = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Content/img/logo-komatsu.png"));
                    rightJpg.ScaleAbsolute(120f, 40f);
                    PdfPCell rightImageCell = new PdfPCell(rightJpg);
                    rightImageCell.Colspan = 2; // either 1 if you need to insert one cell
                    rightImageCell.Border = 0;
                    rightImageCell.HorizontalAlignment = Element.ALIGN_RIGHT;

                    // add a LEFT image to PdfPTables
                    table.AddCell(imageCell);
                    // add a RIGHT image to PdfPTables
                    //table.AddCell(rightImageCell);




                    table.AddCell(new PdfPCell(new Phrase("INCOMING RAW MATERIAL RESULT REPORT", FontFactory.GetFont("Calibri", 20, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });
                    pdfDoc.Add(table);

                    var receiveData = db.IncomingLists.AsEnumerable()
                        .Where(x => ids.Contains(x.DONO))
                        .Select(p => new
                        {
                            ID = p.ID,
                            DONO = p.DONO,
                            DeliveryDate = p.IncomingDate.HasValue ? p.IncomingDate.Value.ToString("yyyy-MM-dd") : "",
                            Status = p.IncomingStatus != null ? p.IncomingStatus.Name : "",
                            StatusName = p.IncomingStatus != null ? p.IncomingStatus.Value : "",
                            Quantity = p.Quantity.HasValue ? p.Quantity.Value.ToString("N0") : "",
                            ActualQty = p.ActualQty.HasValue ? p.ActualQty.Value.ToString("N0") : "",
                            IsValid = p.IsValid,
                            IsShipment = p.IsShipment
                        })
                        .GroupBy(x => x.DONO)
                        .Select(g => g.First());

                    //Table
                    //float[] widths = new float[] { 70f, 100f, 100f, 100f };
                    table = new PdfPTable(4);
                    ////table.TotalWidth = 320f;
                    //table.LockedWidth = true;
                    //table.SetWidths(widths);
                    table.WidthPercentage = 100;
                    table.SpacingBefore = 5f;
                    table.HeaderRows = 1;

                    //header
                    string[] headers = new string[] { "Incoming List No.", "Incoming Date", "Quantity", "Actual Quantity" };
                    for (int i = 0; i < headers.Count(); i++)
                    {
                        table.AddCell(new PdfPCell(new Phrase(headers[i], FontFactory.GetFont("Calibri", 10)))
                        {
                            PaddingTop = 3,
                            PaddingBottom = 8,
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                    }

                    foreach (var item in receiveData)
                    {
                        table.AddCell(new PdfPCell(new Phrase(item.DONO != null ? item.DONO : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.DeliveryDate, FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.Quantity, FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.ActualQty, FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                    }
                    pdfDoc.Add(table);
                    pdfDoc.Close();

                    var bytes = memoryStream.ToArray();
                    Session[strPdfFileName] = bytes;
                }

                return Json(new { success = true, strPdfFileName }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }


        //=============GGI incoming report ===========
        public ActionResult GenerateXlsGGIIncoming(string[] ids)
        {
            try
            {
                DateTime dateTime = DateTime.Now;
                string FileName = string.Format("INCOMING RAW MATERIAL REPORT_" + dateTime.ToString("yyyyMMdd_HHmm") + ".xlsx");
                string handle = Guid.NewGuid().ToString();

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (ExcelPackage excel = new ExcelPackage())
                    {
                        excel.Workbook.Worksheets.Add("IncomingRawMaterialReport");

                        var headerRow = new List<string[]>()
                        {
                            new string[] { "Incoming List No.", "Incoming Date", "Status" }
                        };
                        string headerRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";

                        var worksheet = excel.Workbook.Worksheets["IncomingRawMaterialReport"];
                        worksheet.Cells[headerRange].LoadFromArrays(headerRow);
                        worksheet.Cells[headerRange].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        worksheet.Cells[headerRange].Style.Font.Bold = true;
                        worksheet.Cells[headerRange].Style.Font.Size = 12;
                        worksheet.Cells[headerRange].AutoFitColumns();

                        var receiveData = db.IncomingLists.AsEnumerable()
                                .Where(x => ids.Contains(x.DONO))
                                .Select(p => new
                                {
                                    ID = p.ID,
                                    DONO = p.DONO,
                                    DeliveryDate = p.IncomingDate.HasValue ? p.IncomingDate.Value.ToString("yyyy-MM-dd") : "",
                                    Status = p.IncomingStatus != null ? p.IncomingStatus.Name : "",
                                    StatusName = p.IncomingStatus != null ? p.IncomingStatus.Value : "",
                                    IsValid = p.IsValid,
                                    IsShipment = p.IsShipment
                                })
                                .GroupBy(x => x.DONO)
                                .Select(g => g.First());

                        int rowNumber = 2;
                        foreach (var data in receiveData)
                        {
                            worksheet.Cells[rowNumber, 1].Value = data.DONO;
                            worksheet.Cells[rowNumber, 2].Value = data.DeliveryDate;
                            worksheet.Cells[rowNumber, 3].Value = data.StatusName;

                            rowNumber++;
                        }
                        excel.SaveAs(memoryStream);
                    }
                    memoryStream.Position = 0;
                    TempData[handle] = memoryStream.ToArray();
                }

                return new JsonResult()
                {
                    Data = new { FileGuid = handle, FileName = FileName }
                };
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }
        public ActionResult GeneratePdfGGIIncoming(string[] ids)
        {
            try
            {
                DateTime dateTime = DateTime.Now;
                string strPdfFileName = string.Format("INCOMING RAW MATERIAL REPORT_" + dateTime.ToString("yyyyMMdd_HHmm") + ".pdf");
                string handle = Guid.NewGuid().ToString();

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    Document pdfDoc = new Document(PageSize.A4.Rotate());
                    PdfWriter.GetInstance(pdfDoc, memoryStream).CloseStream = false;
                    pdfDoc.SetMargins(28f, 28f, 28f, 28f);

                    string strAttachment = Server.MapPath("~/PDFs/" + strPdfFileName);

                    pdfDoc.Open();
                    Phrase phrase = new Phrase();

                    PdfPTable table = new PdfPTable(1);
                    table.WidthPercentage = 100;
                    table.SpacingAfter = 10f;


                    //add a LEFT LABEL 
                    table.AddCell(new PdfPCell(new Phrase("K-PINTAR PARTS PORTAL SYSTEM", FontFactory.GetFont("Calibri", 7, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });

                    // invoke LEFT image
                    iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Content/img/logo_kmsi.png"));
                    jpg.ScaleAbsolute(120f, 33f);
                    PdfPCell imageCell = new PdfPCell(jpg);
                    imageCell.Colspan = 2; // either 1 if you need to insert one cell
                    imageCell.Border = 0;
                    imageCell.HorizontalAlignment = Element.ALIGN_LEFT;

                    //add a RIGHT LABEL 
                    table.AddCell(new PdfPCell(new Phrase("VENDOR STOCK SYSTEM", FontFactory.GetFont("Calibri", 7, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_RIGHT,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });

                    // invoke RIGHT image
                    iTextSharp.text.Image rightJpg = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Content/img/logo-komatsu.png"));
                    rightJpg.ScaleAbsolute(120f, 40f);
                    PdfPCell rightImageCell = new PdfPCell(rightJpg);
                    rightImageCell.Colspan = 2; // either 1 if you need to insert one cell
                    rightImageCell.Border = 0;
                    rightImageCell.HorizontalAlignment = Element.ALIGN_RIGHT;

                    // add a LEFT image to PdfPTables
                    table.AddCell(imageCell);
                    // add a RIGHT image to PdfPTables
                    //table.AddCell(rightImageCell);

                    table.AddCell(new PdfPCell(new Phrase("INCOMING RAW MATERIAL REPORT", FontFactory.GetFont("Calibri", 20, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });
                    pdfDoc.Add(table);

                    var receiveData = db.IncomingLists.AsEnumerable()
                        .Where(x => ids.Contains(x.DONO))
                        .Select(p => new
                        {
                            ID = p.ID,
                            DONO = p.DONO,
                            DeliveryDate = p.IncomingDate.HasValue ? p.IncomingDate.Value.ToString("yyyy-MM-dd") : "",
                            Status = p.IncomingStatus != null ? p.IncomingStatus.Name : "",
                            StatusName = p.IncomingStatus != null ? p.IncomingStatus.Value : "",
                            IsValid = p.IsValid,
                            IsShipment = p.IsShipment
                        })
                        .GroupBy(x => x.DONO)
                        .Select(g => g.First());

                    //Table
                    //float[] widths = new float[] { 70f, 50f, 100f };
                    table = new PdfPTable(3);
                    //table.TotalWidth = 220f;
                    //table.LockedWidth = true;
                    //table.SetWidths(widths);
                    table.WidthPercentage = 100;
                    table.SpacingBefore = 5f;
                    table.HeaderRows = 1;

                    //header
                    string[] headers = new string[] { "Incoming List No.", "Incoming Date", "Status" };
                    for (int i = 0; i < headers.Count(); i++)
                    {
                        table.AddCell(new PdfPCell(new Phrase(headers[i], FontFactory.GetFont("Calibri", 10)))
                        {
                            PaddingTop = 3,
                            PaddingBottom = 8,
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                    }

                    foreach (var item in receiveData)
                    {
                        table.AddCell(new PdfPCell(new Phrase(item.DONO != null ? item.DONO : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.DeliveryDate != null ? item.DeliveryDate : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.StatusName != null ? item.StatusName : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                    }
                    pdfDoc.Add(table);
                    pdfDoc.Close();

                    var bytes = memoryStream.ToArray();
                    Session[strPdfFileName] = bytes;
                }

                return Json(new { success = true, strPdfFileName }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }


        [HttpGet]
        public virtual ActionResult DownloadPdf(string fileName)
        {
            try
            {
                var ms = Session[fileName] as byte[];
                if (ms == null)
                    return new EmptyResult();
                Session[fileName] = null;
                return File(ms, "application/octet-stream", fileName);
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return HttpNotFound();
            }
        }
    }
}