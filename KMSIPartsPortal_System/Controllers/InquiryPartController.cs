﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using KMSIPartsPortal_System.Helpers;
using KMSIPartsPortal_System.Models;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace KMSIPartsPortal_System.Controllers
{
    public class InquiryPartController : Controller
    {
        private kmsi_portalEntities db = new kmsi_portalEntities();

        // GET: InquiryPart
        public ActionResult Index()
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    List<MenuModels> MenuMaster = (List<MenuModels>)Session["MenuMaster"];
                    Int32 RoleID = Convert.ToInt32(Session["RoleId"]);

                    List<Module> modules = db.Modules.Where(x => x.ModuleName == "Helpdesk" || x.ModuleName == "Information").ToList();
                    ViewBag.ModuleInternalLeft = modules;

                    if (RoleID == 0)
                    {
                        ViewBag.IsInput = 1;
                        ViewBag.IsUpdate = 1;
                        ViewBag.IsDelete = 1;
                        ViewBag.IsUpload = 1;
                        ViewBag.IsDownload = 1;
                    }
                    else
                    {
                        var access = MenuMaster.Where(x => x.ControllerName == "InquiryPart" && x.ActionName == "Index" && x.RoleId == RoleID).FirstOrDefault();

                        if (access.IsView == 1)
                        {
                            ViewBag.IsInput = access.IsInput;
                            ViewBag.IsUpdate = access.IsUpdate;
                            ViewBag.IsDelete = access.IsDelete;
                            ViewBag.IsUpload = access.IsUpload;
                            ViewBag.IsDownload = access.IsDownload;
                        }
                        else
                        {
                            ViewBag.Message = "You don't have access to this page! Please contact administrator.";
                        }
                    }

                    string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/');
                    ViewBag.baseUrl = baseUrl;
                    ViewBag.ModuleName = "Information";
                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult List()
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    db.Configuration.LazyLoadingEnabled = false;

                    // Server Side Parameter
                    int start = Convert.ToInt32(Request["start"]);
                    int length = Convert.ToInt32(Request["length"]);
                    string searchValue = Request["search[value]"];
                    string sortColumn = Request["columns[" + Request["order[0][column]"] + "][name]"];
                    string sortColumnDir = Request["order[0][dir]"];
                    string searchPartNumber = Request["searchByPNumber"];

                    var queryResult = db.Kpintar_Part_Info.Select(x => new
                    {
                        ID = x.ID,
                        PART_NUMBER = x.PART_NUMBER,
                        PART_NAME = x.PART_NAME,
                        LATEST_INTERCHANGE_CODE = x.LATEST_INTERCHANGE_CODE,
                        LATEST_INTERCHANGE_PART_NO = x.LATEST_INTERCHANGE_PART_NO,
                        LIST_PRICE_CURRENT_IDR = x.LIST_PRICE_CURRENT_IDR,
                        LIST_PRICE_CURRENT_USD = x.LIST_PRICE_CURRENT_USD,
                        DEPOT_MORTALITY = x.DEPOT_MORTALITY,
                        MATRIX_RANK = x.MATRIX_RANK,
                        NEW_COMMODITY_CODE = x.NEW_COMMODITY_CODE,
                        UNIT_WEIGHT = x.UNIT_WEIGHT,
                        NET_WEIGHT = x.NET_WEIGHT,
                        QTY_PER_MACHINE = x.QTY_PER_MACHINE,
                        SERVICE_NEWS_NUMBER = x.SERVICE_NEWS_NUMBER,
                        FOO = x.FOO,
                        FOH = x.FOH,
                        LT_PROD = x.LT_PROD,
                        LT_DELIVERY = x.LT_DELIVERY
                    }).AsEnumerable();
                    int recordsTotal = 0;

                    // total number of rows count
                    SqlCommand command = new SqlCommand("SELECT dbo.fGetPartInfoRecordCount() AS CountTotal");
                    command.CommandType = CommandType.Text;
                    using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["Constring"].ConnectionString))
                    {
                        connection.Open();
                        command.Connection = connection;
                        recordsTotal = int.Parse(command.ExecuteScalar().ToString());
                    }

                    // Search
                    if (!string.IsNullOrEmpty(searchValue))
                    {
                        queryResult = queryResult.Where(s => s.PART_NUMBER.ToLower().Contains(searchValue.ToLower())
                            || s.PART_NAME.ToLower().Contains(searchValue.ToLower())
                            || s.LATEST_INTERCHANGE_CODE.ToLower().Contains(searchValue.ToLower())
                            || s.LATEST_INTERCHANGE_PART_NO.ToLower().Contains(searchValue.ToLower())
                            || s.LIST_PRICE_CURRENT_IDR.ToString().ToLower().Contains(searchValue.ToLower())
                            || s.LIST_PRICE_CURRENT_USD.ToString().ToLower().Contains(searchValue.ToLower())
                            || s.DEPOT_MORTALITY.ToString().ToLower().Contains(searchValue.ToLower())
                            || s.MATRIX_RANK.ToLower().Contains(searchValue.ToLower())
                            || s.NEW_COMMODITY_CODE.ToLower().Contains(searchValue.ToLower())
                            || s.UNIT_WEIGHT.ToString().ToLower().Contains(searchValue.ToLower())
                            || s.NET_WEIGHT.ToString().ToLower().Contains(searchValue.ToLower())
                            || s.QTY_PER_MACHINE.ToString().ToLower().Contains(searchValue.ToLower())
                            || s.SERVICE_NEWS_NUMBER.ToLower().Contains(searchValue.ToLower())
                            || s.FOO.ToString().ToLower().Contains(searchValue.ToLower())
                            || s.FOH.ToString().ToLower().Contains(searchValue.ToLower())
                            || s.LT_PROD.ToString().ToLower().Contains(searchValue.ToLower())
                            || s.LT_DELIVERY.ToString().ToLower().Contains(searchValue.ToLower()));
                    }

                    int recordsFiltered = 0;
                    // Filtering
                    if (!string.IsNullOrEmpty(searchPartNumber))
                    {
                        List<string> ListPN = searchPartNumber.Split(',').Select(x => x.ToLower().Trim()).ToList();
                        queryResult = queryResult.Where(s => ListPN.Contains(s.PART_NUMBER.ToLower().Trim()));
                        recordsFiltered = queryResult.Count();
                    }
                    else
                    {
                        recordsFiltered = recordsTotal;
                    }

                    // Sorting
                    if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                    {
                        queryResult = queryResult.OrderBy(sortColumn + " " + sortColumnDir);
                    }

                    // Paging
                    queryResult = queryResult.Skip(start).Take(length).AsEnumerable();

                    return Json(new { draw = Request["draw"], recordsFiltered = recordsFiltered, recordsTotal = recordsTotal, data = queryResult },
                        JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (DbEntityValidationException ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);

                foreach (var eve in ex.EntityValidationErrors)
                {
                    sw.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sw.WriteLine("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                }
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult Detail(int? id)
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    List<Module> modules = db.Modules.Where(x => x.ModuleName == "Helpdesk" || x.ModuleName == "Information").ToList();
                    ViewBag.ModuleInternalLeft = modules;

                    Kpintar_Part_Info info = db.Kpintar_Part_Info.Find(id);

                    string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/');
                    ViewBag.baseUrl = baseUrl;
                    return View(info);
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public FileResult DownloadExcel()
        {
            string filename = "PartsNumber_Format";
            string path = "/Docs/" + filename + ".xlsx";
            return File(path, "application/vnd.ms-excel", filename + ".xlsx");
        }

        [HttpPost]
        public String UploadPartNumber()
        {
            string partNumberValues = String.Empty;
            try
            {
                if (Request.Files.Count > 0)
                {
                    HttpPostedFileBase file = Request.Files[0];
                    if (file.ContentType == "application/vnd.ms-excel" || file.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                    {
                        var filename = file.FileName;
                        var targetPath = Server.MapPath("~/Uploads/");
                        file.SaveAs(targetPath + filename);
                        var pathToExcelFile = targetPath + filename;
                        string FileName = Path.GetFileName(file.FileName);
                        string Extension = Path.GetExtension(file.FileName);
                        DataTable dataTable = FileImport.ImportToGrid(pathToExcelFile, Extension, "Yes");
                        List<PartNumberList> partNumberLists = new List<PartNumberList>();
                        partNumberLists = (from DataRow row in dataTable.Rows
                                           select new PartNumberList()
                                           {
                                               PartNumber = row[0].ToString()
                                           }).ToList();
                        partNumberValues = String.Join(", ", partNumberLists.Select(x => x.PartNumber));

                        TempData["status"] = true;
                        TempData["message"] = "upload success";
                        TempData["data"] = partNumberValues;

                        if (System.IO.File.Exists(pathToExcelFile))
                            System.IO.File.Delete(pathToExcelFile);
                    }
                    else
                    {
                        TempData["status"] = false;
                        TempData["message"] = "upload failed";
                    }
                }
                else
                {
                    TempData["status"] = false;
                    TempData["message"] = "upload failed";
                }

                return JsonConvert.SerializeObject(TempData);
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                TempData["status"] = false;
                TempData["message"] = "upload failed";
                return JsonConvert.SerializeObject(TempData);
            }
        }

        public ActionResult GenerateXls(string[] columns, string partnumber)
        {
            try
            {
                DateTime dateTime = DateTime.Now;
                string FileName = string.Format("PartInformation_" + dateTime.ToString("yyyyMMdd_HHmm") + ".xlsx");
                string handle = Guid.NewGuid().ToString();
                
                using (MemoryStream stream = new MemoryStream())
                {
                    using (ExcelPackage excel = new ExcelPackage())
                    {
                        int start = 0;
                        int maxRows = 30;
                        var queryResult = db.Kpintar_Part_Info.Select(x => new
                        {
                            ID = x.ID,
                            PART_NUMBER = x.PART_NUMBER,
                            PART_NAME = x.PART_NAME,
                            LATEST_INTERCHANGE_CODE = x.LATEST_INTERCHANGE_CODE,
                            LATEST_INTERCHANGE_PART_NO = x.LATEST_INTERCHANGE_PART_NO,
                            LIST_PRICE_CURRENT_IDR = x.LIST_PRICE_CURRENT_IDR,
                            LIST_PRICE_CURRENT_USD = x.LIST_PRICE_CURRENT_USD,
                            DEPOT_MORTALITY = x.DEPOT_MORTALITY,
                            MATRIX_RANK = x.MATRIX_RANK,
                            NEW_COMMODITY_CODE = x.NEW_COMMODITY_CODE,
                            UNIT_WEIGHT = x.UNIT_WEIGHT,
                            NET_WEIGHT = x.NET_WEIGHT,
                            QTY_PER_MACHINE = x.QTY_PER_MACHINE,
                            SERVICE_NEWS_NUMBER = x.SERVICE_NEWS_NUMBER,
                            FOO = x.FOO,
                            FOH = x.FOH,
                            LT_PROD = x.LT_PROD,
                            LT_DELIVERY = x.LT_DELIVERY
                        }).AsEnumerable();

                        if (!string.IsNullOrEmpty(partnumber))
                        {
                            List<string> ListPN = partnumber.Split(',').Select(x => x.Trim()).ToList();
                            queryResult = queryResult.Where(s => ListPN.Contains(s.PART_NUMBER.Trim()));
                        }

                        string sheetName = "Part_Information";
                        excel.Workbook.Worksheets.Add(sheetName);

                        int headerCount = 12 + columns.Length;

                        List<string> headerRow = new List<string>(new string[]
                        {
                            "PART_NUMBER", "PART_NAME", "LATEST_INTERCHANGE_CODE", "LATEST_INTERCHANGE_PART_NO", "LP IDR",
                            "LP USD", "MT", "MATRIX_RANK", "New Comm", "Net Weight",
                            "Net Height", "Machine Qty", "PSN", "FOO", "FOH",
                            "L/T PROD", "L/T DELIVERY"
                        });

                        var worksheet = excel.Workbook.Worksheets[sheetName];
                        worksheet.Cells[1, 1, 1, headerCount].Merge = true;
                        worksheet.Cells["A1"].Value = "PART INFORMATION";
                        worksheet.Cells["A1"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        worksheet.Cells["A1"].Style.Font.Bold = true;
                        worksheet.Cells["A1"].Style.Font.Size = 14;

                        int colNumber = 1;
                        foreach (var header in headerRow)
                        {
                            string Value = String.Empty;
                            if (header.Equals("PART_NUMBER") && columns.Contains(header) && colNumber == 1)
                            {
                                Value = "Part Number";
                            }
                            else if (header.Equals("PART_NAME") && columns.Contains(header) && colNumber == 2)
                            {
                                Value = "Part Name";
                            }
                            else if (header.Equals("LATEST_INTERCHANGE_CODE") && columns.Contains(header) && colNumber == 3)
                            {
                                Value = "ITC";
                            }
                            else if (header.Equals("LATEST_INTERCHANGE_PART_NO") && columns.Contains(header) && colNumber == 4)
                            {
                                Value = "New PN";
                            }
                            else if (header.Equals("MATRIX_RANK") && columns.Contains(header) && colNumber == 8)
                            {
                                Value = "Rank";
                            }
                            else
                            {
                                Value = header;
                            }

                            worksheet.Cells[3, colNumber].Value = Value;
                            worksheet.Cells[3, colNumber].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                            worksheet.Cells[3, colNumber].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                            worksheet.Cells[3, colNumber].Style.Font.Bold = true;
                            worksheet.Cells[3, colNumber].Style.Font.Size = 10;
                            worksheet.Cells[3, colNumber].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                            colNumber++;
                        }

                        queryResult = queryResult.Skip(start).Take(maxRows).AsEnumerable();

                        int rown = 4;
                        int coln = 1;
                        foreach (var data in queryResult)
                        {
                            string Value = String.Empty;
                            foreach (var header in headerRow)
                            {
                                if (header.Equals("PART_NUMBER") && columns.Contains(header) && coln == 1)
                                {
                                    Value = data.PART_NUMBER;
                                }
                                else if (header.Equals("PART_NAME") && columns.Contains(header) && coln == 2)
                                {
                                    Value = data.PART_NAME;
                                }
                                else if (header.Equals("LATEST_INTERCHANGE_CODE") && columns.Contains(header) && coln == 3)
                                {
                                    Value = data.LATEST_INTERCHANGE_CODE;
                                }
                                else if (header.Equals("LATEST_INTERCHANGE_PART_NO") && columns.Contains(header) && coln == 4)
                                {
                                    Value = data.LATEST_INTERCHANGE_PART_NO;
                                }
                                else if (header.Equals("MATRIX_RANK") && columns.Contains(header) && coln == 8)
                                {
                                    Value = data.MATRIX_RANK;
                                }
                                else if (coln == 5)
                                {
                                    Value = data.LIST_PRICE_CURRENT_IDR.HasValue ? data.LIST_PRICE_CURRENT_IDR.Value.ToString() : String.Empty;
                                }
                                else if (coln == 6)
                                {
                                    Value = data.LIST_PRICE_CURRENT_USD.HasValue ? data.LIST_PRICE_CURRENT_USD.Value.ToString() : String.Empty;
                                }
                                else if (coln == 7)
                                {
                                    Value = data.DEPOT_MORTALITY.HasValue ? data.DEPOT_MORTALITY.Value.ToString() : String.Empty;
                                }
                                else if (coln == 9)
                                {
                                    Value = data.NEW_COMMODITY_CODE;
                                }
                                else if (coln == 10)
                                {
                                    Value = data.UNIT_WEIGHT.HasValue ? data.UNIT_WEIGHT.Value.ToString() : String.Empty;
                                }
                                else if (coln == 11)
                                {
                                    Value = data.NET_WEIGHT.HasValue ? data.NET_WEIGHT.Value.ToString() : String.Empty;
                                }
                                else if (coln == 12)
                                {
                                    Value = data.QTY_PER_MACHINE.HasValue ? data.QTY_PER_MACHINE.Value.ToString() : String.Empty;
                                }
                                else if (coln == 13)
                                {
                                    Value = data.SERVICE_NEWS_NUMBER;
                                }
                                else if (coln == 14)
                                {
                                    Value = data.FOO.HasValue ? data.FOO.Value.ToString() : String.Empty;
                                }
                                else if (coln == 15)
                                {
                                    Value = data.FOH.HasValue ? data.FOH.Value.ToString() : String.Empty;
                                }
                                else if (coln == 16)
                                {
                                    Value = data.LT_PROD.HasValue ? data.LT_PROD.Value.ToString() : String.Empty;
                                }
                                else if (coln == 17)
                                {
                                    Value = data.LT_DELIVERY.HasValue ? data.LT_DELIVERY.Value.ToString() : String.Empty;
                                }

                                worksheet.Cells[rown, coln].Value = Value;
                                worksheet.Cells[rown, coln].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                worksheet.Cells[rown, coln].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                                worksheet.Cells[rown, coln].Style.Font.Size = 8;
                                worksheet.Cells[rown, coln].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                                coln++;
                            }

                            coln = 1;
                            rown++;

                        }

                        worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();
                        for (int i = 1; i <= worksheet.Dimension.End.Column; i++)
                        {
                            //worksheet.Column(i).Width = 7.71;
                            worksheet.Column(i).Style.WrapText = true;
                        }

                        excel.SaveAs(stream);
                    }
                    stream.Position = 0;
                    TempData[handle] = stream.ToArray();
                }

                return new JsonResult()
                {
                    Data = new { FileGuid = handle, FileName = FileName }
                };
            }
            catch (DbEntityValidationException ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);

                foreach (var eve in ex.EntityValidationErrors)
                {
                    sw.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sw.WriteLine("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                }
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult GeneratePdf(string[] columns, string partnumber)
        {
            try
            {
                DateTime dateTime = DateTime.Now;
                string FileName = string.Format("PartInformation_" + dateTime.ToString("yyyyMMdd_HHmm") + ".pdf");
                string handle = Guid.NewGuid().ToString();

                using (MemoryStream stream = new MemoryStream())
                {
                    int start = 0;
                    int maxRows = 30;
                    var queryResult = db.Kpintar_Part_Info.AsEnumerable();

                    if (!string.IsNullOrEmpty(partnumber))
                    {
                        List<string> ListPN = partnumber.Split(',').Select(x => x.ToLower().Trim()).ToList();
                        queryResult = queryResult.Where(s => ListPN.Contains(s.PART_NUMBER.ToLower().Trim()));
                    }

                    queryResult = queryResult.Skip(start).Take(maxRows).AsEnumerable();

                    Document pdfDoc = new Document(PageSize.A3.Rotate());
                    PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);
                    writer.CloseStream = false;
                    pdfDoc.SetMargins(20f, 20f, 20f, 20f);

                    pdfDoc.Open();
                    Phrase phrase = new Phrase();

                    Font fontCell = new Font(FontFactory.GetFont("Trebuchet MS", 8, Font.NORMAL));
                    Font fontHeader = new Font(FontFactory.GetFont("Trebuchet MS", 10, Font.BOLD));

                    PdfPTable table = new PdfPTable(1);
                    table.WidthPercentage = 100;
                    table.SpacingAfter = 10f;

                    // invoke LEFT image
                    Image jpg = Image.GetInstance(Server.MapPath("~/Content/img/logo-komatsu.png"));
                    jpg.ScalePercent(50);
                    PdfPCell imageCell = new PdfPCell(jpg);
                    imageCell.Colspan = 1; // either 1 if you need to insert one cell
                    imageCell.Border = 0;
                    imageCell.HorizontalAlignment = Element.ALIGN_LEFT;

                    // add a LEFT image to PdfPTables
                    table.AddCell(imageCell);

                    table.AddCell(new PdfPCell(new Phrase("STOCK INFORMATION", FontFactory.GetFont("Trebuchet MS", 20, Font.BOLD, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });
                    pdfDoc.Add(table);

                    int headerCount = 12 + columns.Length;

                    table = new PdfPTable(headerCount);
                    table.WidthPercentage = 100;
                    table.SpacingBefore = 5f;

                    if (columns.Contains("PART_NUMBER"))
                        addCellWithStyle(table, "PN", fontHeader);

                    if (columns.Contains("PART_NAME"))
                        addCellWithStyle(table, "Name", fontHeader);

                    if (columns.Contains("LATEST_INTERCHANGE_CODE"))
                        addCellWithStyle(table, "ITC", fontHeader);

                    if (columns.Contains("LATEST_INTERCHANGE_PART_NO"))
                        addCellWithStyle(table, "New PN", fontHeader);

                    addCellWithStyle(table, "LP IDR", fontHeader);
                    addCellWithStyle(table, "LP USD", fontHeader);
                    addCellWithStyle(table, "MT", fontHeader);

                    if (columns.Contains("MATRIX_RANK"))
                        addCellWithStyle(table, "Rank", fontHeader);

                    addCellWithStyle(table, "New Comm", fontHeader);
                    addCellWithStyle(table, "Net Weight", fontHeader);
                    addCellWithStyle(table, "Net Height", fontHeader);
                    addCellWithStyle(table, "Machine Qty", fontHeader);
                    addCellWithStyle(table, "PSN", fontHeader);
                    addCellWithStyle(table, "FOO", fontHeader);
                    addCellWithStyle(table, "FOH", fontHeader);
                    addCellWithStyle(table, "L/T PROD", fontHeader);
                    addCellWithStyle(table, "L/T DELIVERY", fontHeader);

                    foreach (var data in queryResult)
                    {
                        if (columns.Contains("PART_NUMBER"))
                            addCellWithStyle(table, data.PART_NUMBER, fontCell);

                        if (columns.Contains("PART_NAME"))
                            addCellWithStyle(table, data.PART_NAME, fontCell);

                        if (columns.Contains("LATEST_INTERCHANGE_CODE"))
                            addCellWithStyle(table, data.LATEST_INTERCHANGE_CODE, fontCell);

                        if (columns.Contains("LATEST_INTERCHANGE_PART_NO"))
                            addCellWithStyle(table, data.LATEST_INTERCHANGE_PART_NO, fontCell);

                        addCellWithStyle(table, data.LIST_PRICE_CURRENT_IDR.HasValue ? data.LIST_PRICE_CURRENT_IDR.Value.ToString() : String.Empty, fontCell);
                        addCellWithStyle(table, data.LIST_PRICE_CURRENT_USD.HasValue ? data.LIST_PRICE_CURRENT_USD.Value.ToString() : String.Empty, fontCell);
                        addCellWithStyle(table, data.DEPOT_MORTALITY.HasValue ? data.DEPOT_MORTALITY.Value.ToString() : String.Empty, fontCell);

                        if (columns.Contains("MATRIX_RANK"))
                            addCellWithStyle(table, data.MATRIX_RANK, fontCell);

                        addCellWithStyle(table, data.NEW_COMMODITY_CODE, fontCell);
                        addCellWithStyle(table, data.UNIT_WEIGHT.HasValue ? data.UNIT_WEIGHT.Value.ToString() : String.Empty, fontCell);
                        addCellWithStyle(table, data.NET_WEIGHT.HasValue ? data.NET_WEIGHT.Value.ToString() : String.Empty, fontCell);
                        addCellWithStyle(table, data.QTY_PER_MACHINE.HasValue ? data.QTY_PER_MACHINE.Value.ToString() : String.Empty, fontCell);
                        addCellWithStyle(table, data.SERVICE_NEWS_NUMBER, fontCell);
                        addCellWithStyle(table, data.FOO.HasValue ? data.FOO.Value.ToString() : String.Empty, fontCell);
                        addCellWithStyle(table, data.FOH.HasValue ? data.FOH.Value.ToString() : String.Empty, fontCell);
                        addCellWithStyle(table, data.LT_PROD.HasValue ? data.LT_PROD.Value.ToString() : String.Empty, fontCell);
                        addCellWithStyle(table, data.LT_DELIVERY.HasValue ? data.LT_DELIVERY.Value.ToString() : String.Empty, fontCell);
                    }
                    pdfDoc.Add(table);

                    pdfDoc.Close();

                    var bytes = stream.ToArray();
                    Session[FileName] = bytes;
                }

                return Json(new { success = true, FileName }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult GeneratePdfDetail(int id)
        {
            try
            {
                DateTime dateTime = DateTime.Now;
                string FileName = string.Format("DetailPartInformation_" + dateTime.ToString("yyyyMMdd_HHmm") + ".pdf");
                string handle = Guid.NewGuid().ToString();

                using (MemoryStream stream = new MemoryStream())
                {
                    Kpintar_Part_Info info = db.Kpintar_Part_Info.Find(id);

                    Document pdfDoc = new Document(PageSize.A4.Rotate());
                    PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);
                    writer.CloseStream = false;
                    pdfDoc.SetMargins(20f, 20f, 20f, 20f);

                    pdfDoc.Open();
                    Phrase phrase = new Phrase();

                    Font fontHeader = new Font(FontFactory.GetFont("Trebuchet MS", 14, Font.BOLD));
                    Font fontCell = new Font(FontFactory.GetFont("Trebuchet MS", 10, Font.NORMAL));

                    PdfPTable table = new PdfPTable(1);
                    table.WidthPercentage = 100;
                    table.SpacingAfter = 12f;

                    // invoke LEFT image
                    Image jpg = Image.GetInstance(Server.MapPath("~/Content/img/logo-komatsu.png"));
                    jpg.ScalePercent(50);
                    PdfPCell imageCell = new PdfPCell(jpg);
                    imageCell.Colspan = 1; // either 1 if you need to insert one cell
                    imageCell.Border = 0;
                    imageCell.HorizontalAlignment = Element.ALIGN_LEFT;

                    // add a LEFT image to PdfPTables
                    table.AddCell(imageCell);

                    table.AddCell(new PdfPCell(new Phrase("DETAIL PART INFORMATION", FontFactory.GetFont("Trebuchet MS", 20, Font.BOLD, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });
                    pdfDoc.Add(table);

                    table = new PdfPTable(1);
                    table.WidthPercentage = 100;
                    addCellTitle(table, "Part Identity", fontHeader);
                    pdfDoc.Add(table);

                    table = new PdfPTable(4);
                    table.WidthPercentage = 100;
                    table.SpacingAfter = 8f;
                    addCellData(table, "Part Number : " + info.PART_NUMBER, fontCell);
                    addCellData(table, "ITC Code : " + info.LATEST_INTERCHANGE_CODE, fontCell);
                    addCellData(table, "New Comm : " + info.NEW_COMMODITY_CODE, fontCell);
                    addCellData(table, "Rank : " + info.MATRIX_RANK, fontCell);

                    addCellData(table, "Part Name : " + info.PART_NAME, fontCell);
                    addCellData(table, "New PN : " + info.LATEST_INTERCHANGE_PART_NO, fontCell);
                    addCellData(table, "Machine Qty : " + (info.QTY_PER_MACHINE.HasValue ? info.QTY_PER_MACHINE.Value.ToString() : String.Empty), fontCell);
                    addCellData(table, "MT : " + (info.DEPOT_MORTALITY.HasValue ? info.DEPOT_MORTALITY.Value.ToString() : String.Empty), fontCell);
                    pdfDoc.Add(table);

                    table = new PdfPTable(1);
                    table.WidthPercentage = 100;
                    addCellTitle(table, "Pricing", fontHeader);
                    pdfDoc.Add(table);

                    table = new PdfPTable(4);
                    table.WidthPercentage = 100;
                    table.SpacingAfter = 8f;
                    addCellData(table, "List Price IDR : " + (info.LIST_PRICE_CURRENT_IDR.HasValue ? info.LIST_PRICE_CURRENT_IDR.Value.ToString() : String.Empty), fontCell);
                    addCellData(table, "List Price USD : " + (info.LIST_PRICE_CURRENT_USD.HasValue ? info.LIST_PRICE_CURRENT_USD.Value.ToString() : String.Empty), fontCell);
                    addCellData(table, "Currency : ", fontCell);
                    addCellData(table, String.Empty, fontCell);
                    pdfDoc.Add(table);

                    table = new PdfPTable(1);
                    table.WidthPercentage = 100;
                    addCellTitle(table, "Dimension", fontHeader);
                    pdfDoc.Add(table);

                    table = new PdfPTable(4);
                    table.WidthPercentage = 100;
                    table.SpacingAfter = 8f;
                    addCellData(table, "Gross Weight : ", fontCell);
                    addCellData(table, "Net Weight : " + (info.UNIT_WEIGHT.HasValue ? info.UNIT_WEIGHT.Value.ToString() : String.Empty), fontCell);
                    addCellData(table, "Height : ", fontCell);
                    addCellData(table, "Length : ", fontCell);
                    pdfDoc.Add(table);

                    table = new PdfPTable(1);
                    table.WidthPercentage = 100;
                    addCellTitle(table, "Stock", fontHeader);
                    pdfDoc.Add(table);

                    table = new PdfPTable(4);
                    table.WidthPercentage = 100;
                    table.SpacingAfter = 8f;
                    addCellData(table, "FOH : " + (info.FOH.HasValue ? info.FOH.Value.ToString() : String.Empty), fontCell);
                    addCellData(table, "FOO : " + (info.FOO.HasValue ? info.FOO.Value.ToString() : String.Empty), fontCell);
                    addCellData(table, "Lead Time : ", fontCell);
                    addCellData(table, String.Empty, fontCell);
                    pdfDoc.Add(table);

                    pdfDoc.Close();

                    var bytes = stream.ToArray();
                    Session[FileName] = bytes;
                }

                return Json(new { success = true, FileName }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        /* public ActionResult GeneratePdf(string[] columns, string partnumber)
        {
            try
            {
                DateTime dateTime = DateTime.Now;
                string FileName = string.Format("PartInformation_" + dateTime.ToString("yyyyMMdd_HHmm") + ".pdf");
                string handle = Guid.NewGuid().ToString();

                using (MemoryStream stream = new MemoryStream())
                {
                    int start = 0;
                    int maxRows = 30;
                    var queryResult = db.Kpintar_Part_Info.AsEnumerable();

                    if (!string.IsNullOrEmpty(partnumber))
                    {
                        List<string> ListPN = partnumber.Split(',').Select(x => x.ToLower().Trim()).ToList();
                        queryResult = queryResult.Where(s => ListPN.Contains(s.PART_NUMBER.ToLower().Trim()));
                    }

                    queryResult = queryResult.Skip(start).Take(maxRows).AsEnumerable();

                    string pHTML = ViewRenderer.RenderRazorViewToString(this, "~/Views/PDFs/PartInfo.cshtml", new PdfPartInfo()
                    {
                        Columns = columns,
                        Data = queryResult
                    });
                    StringReader reader = new StringReader(pHTML);

                    // 1: create object of a itextsharp document class  
                    Document doc = new Document(PageSize.A4.Rotate());

                    // 2: we create a itextsharp pdfwriter that listens to the document and directs a XML-stream to a file  
                    PdfWriter oPdfWriter = PdfWriter.GetInstance(doc, stream);

                    // 3: we create a worker parse the document  
                    //HTMLWorker htmlWorker = new HTMLWorker(doc);

                    // 4: we open document and start the worker on the document  
                    doc.Open();
                    doc.NewPage();
                    //htmlWorker.StartDocument();

                    // 5: parse the html into the document  
                    XMLWorkerHelper.GetInstance().ParseXHtml(oPdfWriter, doc, reader);
                    //htmlWorker.Parse(reader);

                    // 6: close the document and the worker  
                    //htmlWorker.EndDocument();
                    //htmlWorker.Close();
                    doc.Close();

                    var bytes = stream.ToArray();
                    Session[FileName] = bytes;
                }

                return Json(new { success = true, FileName }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        } */

        /* public ActionResult GeneratePdfDetail(int id)
        {
            try
            {
                DateTime dateTime = DateTime.Now;
                string FileName = string.Format("DetailPartInformation_" + dateTime.ToString("yyyyMMdd_HHmm") + ".pdf");
                string handle = Guid.NewGuid().ToString();

                using (MemoryStream stream = new MemoryStream())
                {
                    Kpintar_Part_Info info = db.Kpintar_Part_Info.Find(id);

                    string pHTML = ViewRenderer.RenderRazorViewToString(this, "~/Views/PDFs/DetailPartInfo.cshtml", new PdfDetailPart()
                    {
                        Detail = info
                    });
                    StringReader reader = new StringReader(pHTML);

                    Document doc = new Document(PageSize.A4.Rotate());

                    PdfWriter oPdfWriter = PdfWriter.GetInstance(doc, stream);

                    doc.Open();
                    doc.NewPage();

                    XMLWorkerHelper.GetInstance().ParseXHtml(oPdfWriter, doc, reader);

                    doc.Close();

                    var bytes = stream.ToArray();
                    Session[FileName] = bytes;
                }

                return Json(new { success = true, FileName }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        } */

        public ActionResult GenerateXlsDetail(int id)
        {
            try
            {
                DateTime dateTime = DateTime.Now;
                string FileName = string.Format("DetailPart_" + dateTime.ToString("yyyyMMdd_HHmm") + ".xlsx");
                string handle = Guid.NewGuid().ToString();

                using (MemoryStream stream = new MemoryStream())
                {
                    using (ExcelPackage excel = new ExcelPackage())
                    {
                        excel.Workbook.Worksheets.Add("DetailPart");
                        var worksheet = excel.Workbook.Worksheets["DetailPart"];

                        Kpintar_Part_Info info = db.Kpintar_Part_Info.Find(id);

                        worksheet.Cells[1, 1, 1, 11].Merge = true;
                        worksheet.Cells["A1"].Value = "DETAIL PART INFORMATION";
                        worksheet.Cells["A1"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        worksheet.Cells["A1"].Style.Font.Bold = true;
                        worksheet.Cells["A1"].Style.Font.Size = 14;

                        worksheet.Cells[3, 1, 3, 2].Merge = true;
                        worksheet.Cells["A3"].Value = "Parts Identity";
                        worksheet.Cells["A3"].Style.Font.Bold = true;
                        worksheet.Cells["A3"].Style.Font.Size = 12;

                        worksheet.Cells["A4"].Value = "Part Number";
                        worksheet.Cells["A4"].Style.Font.Size = 10;
                        string PART_NUMBER = info.PART_NUMBER != null ? info.PART_NUMBER : String.Empty;
                        worksheet.Cells["B4"].Value = ": " + PART_NUMBER;
                        worksheet.Cells["B4"].Style.Font.Size = 10;

                        worksheet.Cells["A5"].Value = "Part Name";
                        worksheet.Cells["A5"].Style.Font.Size = 10;
                        string PART_NAME = info.PART_NAME != null ? info.PART_NAME : String.Empty;
                        worksheet.Cells["B5"].Value = ": " + PART_NAME;
                        worksheet.Cells["B5"].Style.Font.Size = 10;

                        worksheet.Cells["D4"].Value = "ITC Code";
                        worksheet.Cells["D4"].Style.Font.Size = 10;
                        string LATEST_INTERCHANGE_CODE = info.LATEST_INTERCHANGE_CODE != null ? info.LATEST_INTERCHANGE_CODE : String.Empty;
                        worksheet.Cells["E4"].Value = ": " + LATEST_INTERCHANGE_CODE;
                        worksheet.Cells["E4"].Style.Font.Size = 10;

                        worksheet.Cells["D5"].Value = "New PN";
                        worksheet.Cells["D5"].Style.Font.Size = 10;
                        string LATEST_INTERCHANGE_PART_NO = info.LATEST_INTERCHANGE_PART_NO != null ? info.LATEST_INTERCHANGE_PART_NO : String.Empty;
                        worksheet.Cells["E5"].Value = ": " + LATEST_INTERCHANGE_PART_NO;
                        worksheet.Cells["E5"].Style.Font.Size = 10;

                        worksheet.Cells["G4"].Value = "New Comm";
                        worksheet.Cells["G4"].Style.Font.Size = 10;
                        string NEW_COMMODITY_CODE = info.NEW_COMMODITY_CODE != null ? info.NEW_COMMODITY_CODE : String.Empty;
                        worksheet.Cells["H4"].Value = ": " + NEW_COMMODITY_CODE;
                        worksheet.Cells["H4"].Style.Font.Size = 10;

                        worksheet.Cells["G5"].Value = "Machine Qty";
                        worksheet.Cells["G5"].Style.Font.Size = 10;
                        string QTY_PER_MACHINE = info.QTY_PER_MACHINE.HasValue ? info.QTY_PER_MACHINE.Value.ToString() : String.Empty;
                        worksheet.Cells["H5"].Value = ": " + QTY_PER_MACHINE;
                        worksheet.Cells["H5"].Style.Font.Size = 10;

                        worksheet.Cells["J4"].Value = "Rank";
                        worksheet.Cells["J4"].Style.Font.Size = 10;
                        string MATRIX_RANK = info.MATRIX_RANK != null ? info.MATRIX_RANK : String.Empty;
                        worksheet.Cells["K4"].Value = ": " + MATRIX_RANK;
                        worksheet.Cells["K4"].Style.Font.Size = 10;

                        worksheet.Cells["J5"].Value = "MT";
                        worksheet.Cells["J5"].Style.Font.Size = 10;
                        string DEPOT_MORTALITY = info.DEPOT_MORTALITY.HasValue ? info.DEPOT_MORTALITY.Value.ToString() : String.Empty;
                        worksheet.Cells["K5"].Value = ": " + DEPOT_MORTALITY;
                        worksheet.Cells["K5"].Style.Font.Size = 10;

                        worksheet.Cells[7, 1, 7, 2].Merge = true;
                        worksheet.Cells["A7"].Value = "Pricing";
                        worksheet.Cells["A7"].Style.Font.Bold = true;
                        worksheet.Cells["A7"].Style.Font.Size = 12;

                        worksheet.Cells["A8"].Value = "List Price IDR";
                        worksheet.Cells["A8"].Style.Font.Size = 10;
                        string LIST_PRICE_CURRENT_IDR = info.LIST_PRICE_CURRENT_IDR.HasValue ? info.LIST_PRICE_CURRENT_IDR.Value.ToString() : String.Empty;
                        worksheet.Cells["B8"].Value = ": " + LIST_PRICE_CURRENT_IDR;
                        worksheet.Cells["B8"].Style.Font.Size = 10;

                        worksheet.Cells["D8"].Value = "List Price USD";
                        worksheet.Cells["D8"].Style.Font.Size = 10;
                        string LIST_PRICE_CURRENT_USD = info.LIST_PRICE_CURRENT_USD.HasValue ? info.LIST_PRICE_CURRENT_USD.Value.ToString() : String.Empty;
                        worksheet.Cells["E8"].Value = ": " + LIST_PRICE_CURRENT_USD;
                        worksheet.Cells["E8"].Style.Font.Size = 10;

                        worksheet.Cells["G8"].Value = "Currency";
                        worksheet.Cells["G8"].Style.Font.Size = 10;
                        worksheet.Cells["H8"].Value = ": ";
                        worksheet.Cells["H8"].Style.Font.Size = 10;

                        worksheet.Cells[10, 1, 10, 2].Merge = true;
                        worksheet.Cells["A10"].Value = "Dimension";
                        worksheet.Cells["A10"].Style.Font.Bold = true;
                        worksheet.Cells["A10"].Style.Font.Size = 12;

                        worksheet.Cells["A11"].Value = "Gross Weight";
                        worksheet.Cells["A11"].Style.Font.Size = 10;
                        worksheet.Cells["B11"].Value = ": ";
                        worksheet.Cells["B11"].Style.Font.Size = 10;

                        worksheet.Cells["D11"].Value = "Net Weight";
                        worksheet.Cells["D11"].Style.Font.Size = 10;
                        string UNIT_WEIGHT = info.UNIT_WEIGHT.HasValue ? info.UNIT_WEIGHT.Value.ToString() : String.Empty;
                        worksheet.Cells["E11"].Value = ": " + UNIT_WEIGHT;
                        worksheet.Cells["E11"].Style.Font.Size = 10;

                        worksheet.Cells["G11"].Value = "Height";
                        worksheet.Cells["G11"].Style.Font.Size = 10;
                        worksheet.Cells["H11"].Value = ": ";
                        worksheet.Cells["H11"].Style.Font.Size = 10;

                        worksheet.Cells["J11"].Value = "Length";
                        worksheet.Cells["J11"].Style.Font.Size = 10;
                        worksheet.Cells["K11"].Value = ": ";
                        worksheet.Cells["K11"].Style.Font.Size = 10;

                        worksheet.Cells[13, 1, 13, 2].Merge = true;
                        worksheet.Cells["A13"].Value = "Stock";
                        worksheet.Cells["A13"].Style.Font.Bold = true;
                        worksheet.Cells["A13"].Style.Font.Size = 12;

                        worksheet.Cells["A14"].Value = "FOH";
                        worksheet.Cells["A14"].Style.Font.Size = 10;
                        string FOH = info.FOH.HasValue ? info.FOH.Value.ToString() : String.Empty;
                        worksheet.Cells["B14"].Value = ": " + FOH;
                        worksheet.Cells["B14"].Style.Font.Size = 10;

                        worksheet.Cells["D14"].Value = "FOO";
                        worksheet.Cells["D14"].Style.Font.Size = 10;
                        string FOO = info.FOO.HasValue ? info.FOO.Value.ToString() : String.Empty;
                        worksheet.Cells["E14"].Value = ": " + FOO;
                        worksheet.Cells["E14"].Style.Font.Size = 10;

                        worksheet.Cells["G14"].Value = "Lead Time";
                        worksheet.Cells["G14"].Style.Font.Size = 10;
                        worksheet.Cells["H14"].Value = ": ";
                        worksheet.Cells["H14"].Style.Font.Size = 10;

                        worksheet.Column(1).Width = 15.71;
                        worksheet.Column(2).Width = 15.71;
                        worksheet.Column(3).Width = 5.71;
                        worksheet.Column(4).Width = 15.71;
                        worksheet.Column(5).Width = 15.71;
                        worksheet.Column(6).Width = 5.71;
                        worksheet.Column(7).Width = 15.71;
                        worksheet.Column(8).Width = 15.71;
                        worksheet.Column(9).Width = 5.71;
                        worksheet.Column(10).Width = 15.71;
                        worksheet.Column(11).Width = 15.71;

                        excel.SaveAs(stream);
                    }
                    stream.Position = 0;
                    TempData[handle] = stream.ToArray();
                }

                return new JsonResult()
                {
                    Data = new { FileGuid = handle, FileName = FileName }
                };
            }
            catch (DbEntityValidationException ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);

                foreach (var eve in ex.EntityValidationErrors)
                {
                    sw.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sw.WriteLine("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                }
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [HttpGet]
        public virtual ActionResult DownloadXls(string fileGuid, string fileName)
        {
            if (TempData[fileGuid] != null)
            {
                byte[] data = TempData[fileGuid] as byte[];
                return File(data, "application/vnd.ms-excel", fileName);
            }
            else
            {
                return new EmptyResult();
            }
        }

        [HttpGet]
        public virtual ActionResult DownloadPdf(string fileName)
        {
            try
            {
                var ms = Session[fileName] as byte[];
                if (ms == null)
                    return new EmptyResult();
                Session[fileName] = null;
                return File(ms, "application/pdf", fileName);
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return HttpNotFound();
            }
        }

        private static void addCellWithStyle(PdfPTable table, string text, Font font)
        {
            PdfPCell cell = new PdfPCell(new Phrase(text, font));
            cell.PaddingTop = 3;
            cell.PaddingBottom = 8;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;

            table.AddCell(cell);
        }

        private static void addCellTitle(PdfPTable table, string text, Font font)
        {
            PdfPCell cell = new PdfPCell(new Phrase(text, font));
            cell.Border = 0;
            cell.PaddingTop = 3;
            cell.PaddingBottom = 8;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;

            table.AddCell(cell);
        }

        private static void addCellData(PdfPTable table, string text, Font font)
        {
            PdfPCell cell = new PdfPCell(new Phrase(text, font));
            cell.Border = 0;
            cell.PaddingTop = 3;
            cell.PaddingBottom = 8;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;

            table.AddCell(cell);
        }
    }
}