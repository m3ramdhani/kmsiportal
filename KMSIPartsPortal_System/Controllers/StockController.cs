﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Web;
using System.Web.Mvc;
using KMSIPartsPortal_System.Models;
using KMSIPartsPortal_System.Helpers;
using OfficeOpenXml;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace KMSIPartsPortal_System.Controllers
{
    public class StockController : Controller
    {
        private kmsi_portalEntities db = new kmsi_portalEntities();
        
        //Stock GGI ==========================================
        public ActionResult GGIReport()
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    List<MenuModels> MenuMaster = (List<MenuModels>)Session["MenuMaster"];
                    Int32 RoleID = Convert.ToInt32(Session["RoleId"]);

                    if (RoleID == 0)
                    {
                        ViewBag.IsInput = 1;
                        ViewBag.IsUpdate = 1;
                        ViewBag.IsDelete = 1;
                        ViewBag.IsUpload = 1;
                        ViewBag.IsDownload = 1;
                    }
                    else
                    {
                        var access = MenuMaster.Where(x => x.ControllerName == "Stock" && x.ActionName == "GGIReport" && x.RoleId == RoleID).FirstOrDefault();

                        if (access.IsView == 1)
                        {
                            ViewBag.IsInput = access.IsInput;
                            ViewBag.IsUpdate = access.IsUpdate;
                            ViewBag.IsDelete = access.IsDelete;
                            ViewBag.IsUpload = access.IsUpload;
                            ViewBag.IsDownload = access.IsDownload;
                        }
                        else
                        {
                            ViewBag.Message = "You don't have access to this page! Please contact administrator.";
                        }
                    }

                    var OnHand = db.Stocks.Where(x => x.Position == "GGI").Sum(x => x.Quantity);
                    ViewBag.OnHand = OnHand.HasValue ? OnHand.Value : 0;
                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult GGIStockList()
        {
            try
            {
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();

                // Paging Size
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                int totalStock = 0;

                // Getting all data
                var stockData = db.Stocks.Where(x => x.Position == "GGI").AsEnumerable()
                    .Select(p => new {
                        ID = p.ID,
                        PartNumber = p.PartNumber,
                        PartName = p.PartName != null ? p.PartName : "",
                        Quantity = p.Quantity,
                        UnitPrice = p.UnitPrice,
                        Position = p.Position,
                        IncomingDate = p.IncomingDeliveryDate.HasValue ? p.IncomingDeliveryDate.Value.ToString("yyyy-MM-dd") : "",
                        IncomingNo = p.DONO != null ? p.DONO : ""
                    });

                // Sorting
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    stockData = stockData.OrderBy(sortColumn + " " + sortColumnDir);
                }
                // Search
                if (!string.IsNullOrEmpty(searchValue))
                {
                    stockData = stockData.Where(p => p.IncomingNo.ToLower().Contains(searchValue.ToLower())
                        || p.IncomingDate.ToLower().Contains(searchValue.ToLower())
                        || p.PartNumber.ToLower().Contains(searchValue.ToLower())
                        || p.PartName.ToLower().Contains(searchValue.ToLower())
                        || p.Quantity.ToString().ToLower().Contains(searchValue.ToLower())
                    );
                }

                // total number of rows count
                recordsTotal = stockData.Count();
                totalStock = (int)stockData.Sum(x => x.Quantity);
                // Paging
                var data = stockData.Skip(skip).Take(pageSize).ToList();
                // Returning Json Data
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data, total = totalStock });
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult GenerateXlsGGIStock(int[] ids)
        {
            try
            {
                DateTime dateTime = DateTime.Now;
                string FileName = string.Format("STOCK On Hand Raw Material_" + dateTime.ToString("yyyyMMdd_HHmm") + ".xlsx");
                string handle = Guid.NewGuid().ToString();

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (ExcelPackage excel = new ExcelPackage())
                    {
                        excel.Workbook.Worksheets.Add("STOCK On Hand Raw Material");

                        var headerRow = new List<string[]>()
                        {
                            new string[] {
                                "Incoming List No.",
                                "Incoming Date",
                                "Part Number",
                                "Part Name",
                                "Quantity"
                            }
                        };
                        string headerRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";

                        var worksheet = excel.Workbook.Worksheets["STOCK On Hand Raw Material"];
                        worksheet.Cells[headerRange].LoadFromArrays(headerRow);
                        worksheet.Cells[headerRange].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        worksheet.Cells[headerRange].Style.Font.Bold = true;
                        worksheet.Cells[headerRange].Style.Font.Size = 12;
                        worksheet.Cells[headerRange].AutoFitColumns();

                        var stockData = db.Stocks.Where(x => x.Position == "GGI" && ids.Contains(x.ID)).AsEnumerable()
                            .Select(p => new {
                                PartNumber = p.PartNumber,
                                PartName = p.PartName != null ? p.PartName : "",
                                Quantity = p.Quantity,
                                UnitPrice = p.UnitPrice,
                                Position = p.Position,
                                IncomingDate = p.IncomingDeliveryDate.HasValue ? p.IncomingDeliveryDate.Value.ToString("yyyy-MM-dd") : "",
                                IncomingNo = p.DONO != null ? p.DONO : ""
                            }).ToList();

                        int rowNumber = 2;
                        foreach (var data in stockData)
                        {
                            worksheet.Cells[rowNumber, 1].Value = data.IncomingNo;
                            worksheet.Cells[rowNumber, 2].Value = data.IncomingDate;
                            worksheet.Cells[rowNumber, 3].Value = data.PartNumber;
                            worksheet.Cells[rowNumber, 4].Value = data.PartName;
                            worksheet.Cells[rowNumber, 5].Value = data.Quantity;

                            rowNumber++;
                        }
                        excel.SaveAs(memoryStream);
                    }
                    memoryStream.Position = 0;
                    TempData[handle] = memoryStream.ToArray();
                }

                return new JsonResult()
                {
                    Data = new { FileGuid = handle, FileName = FileName }
                };
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [HttpGet]
        public ActionResult DownloadXlsGGIStock(string fileGuid, string filename)
        {
            if (TempData[fileGuid] != null)
            {
                byte[] data = TempData[fileGuid] as byte[];
                return File(data, "application/vnd.ms-excel", filename);
            }
            else
            {
                return new EmptyResult();
            }
        }

        public ActionResult GeneratePdfGGIStock(int[] ids)
        {
            try
            {
                DateTime dateTime = DateTime.Now;
                string strPdfFileName = string.Format("STOCK On Hand Raw Material_" + dateTime.ToString("yyyyMMdd_HHmm") + ".pdf");
                string handle = Guid.NewGuid().ToString();

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    Document pdfDoc = new Document(PageSize.A4.Rotate());
                    PdfWriter.GetInstance(pdfDoc, memoryStream).CloseStream = false;
                    pdfDoc.SetMargins(28f, 28f, 28f, 28f);

                    string strAttachment = Server.MapPath("~/PDFs/" + strPdfFileName);

                    pdfDoc.Open();
                    Phrase phrase = new Phrase();

                    PdfPTable table = new PdfPTable(1);
                    table.WidthPercentage = 100;
                    table.SpacingAfter = 10f;

                    //add a LEFT LABEL 
                    table.AddCell(new PdfPCell(new Phrase("K-PINTAR PARTS PORTAL SYSTEM", FontFactory.GetFont("Calibri", 7, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });

                    // invoke LEFT image
                    iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Content/img/logo_kmsi.png"));
                    jpg.ScaleAbsolute(120f, 40f);
                    PdfPCell imageCell = new PdfPCell(jpg);
                    imageCell.Colspan = 2; // either 1 if you need to insert one cell
                    imageCell.Border = 0;
                    imageCell.HorizontalAlignment = Element.ALIGN_LEFT;

                    //add a RIGHT LABEL 
                    table.AddCell(new PdfPCell(new Phrase("VENDOR STOCK SYSTEM", FontFactory.GetFont("Calibri", 7, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_RIGHT,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });

                    // invoke RIGHT image
                    iTextSharp.text.Image rightJpg = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Content/img/logo-komatsu.png"));
                    rightJpg.ScaleAbsolute(120f, 40f);
                    PdfPCell rightImageCell = new PdfPCell(rightJpg);
                    rightImageCell.Colspan = 2; // either 1 if you need to insert one cell
                    rightImageCell.Border = 0;
                    rightImageCell.HorizontalAlignment = Element.ALIGN_RIGHT;

                    // add a LEFT image to PdfPTables
                    table.AddCell(imageCell);
                    // add a RIGHT image to PdfPTables
                    //table.AddCell(rightImageCell);

                    table.AddCell(new PdfPCell(new Phrase("STOCK On Hand Raw Material", FontFactory.GetFont("Calibri", 20, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });
                    pdfDoc.Add(table);

                    var stockData = db.Stocks.Where(x => x.Position == "GGI" && ids.Contains(x.ID)).AsEnumerable()
                        .Select(p => new {
                            PartNumber = p.PartNumber,
                            PartName = p.PartName != null ? p.PartName : "",
                            Quantity = p.Quantity,
                            UnitPrice = p.UnitPrice,
                            Position = p.Position,
                            IncomingDate = p.IncomingDeliveryDate.HasValue ? p.IncomingDeliveryDate.Value.ToString("yyyy-MM-dd") : "",
                            IncomingNo = p.DONO != null ? p.DONO : ""
                        }).ToList();

                    //Table
                    float[] widths = new float[] { 70f, 50f, 100f, 100f, 100f };
                    table = new PdfPTable(5);
                    ////table.WidthPercentage = 100;
                    table.TotalWidth = 520f;
                    table.LockedWidth = true;
                    table.SetWidths(widths);
                    table.SpacingBefore = 5f;
                    table.HeaderRows = 1;

                    //header
                    string[] headers = new string[] {
                        "Incoming List No.",
                        "Incoming Date",
                        "Part Number",
                        "Part Name",
                        "Quantity"
                    };
                    for (int i = 0; i < headers.Count(); i++)
                    {
                        table.AddCell(new PdfPCell(new Phrase(headers[i], FontFactory.GetFont("Calibri", 10)))
                        {
                            PaddingTop = 3,
                            PaddingBottom = 8,
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                    }

                    foreach (var item in stockData)
                    {
                        table.AddCell(new PdfPCell(new Phrase(item.IncomingNo != null ? item.IncomingNo : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.IncomingDate != null ? item.IncomingDate : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.PartNumber != null ? item.PartNumber : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.PartName != null ? item.PartName : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.Quantity.HasValue ? item.Quantity.Value.ToString("N0") : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                    }
                    pdfDoc.Add(table);
                    pdfDoc.Close();

                    var bytes = memoryStream.ToArray();
                    Session[strPdfFileName] = bytes;
                }

                return Json(new { success = true, strPdfFileName }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [HttpGet]
        public virtual ActionResult DownloadPdfGGIStock(string fileName)
        {
            try
            {
                var ms = Session[fileName] as byte[];
                if (ms == null)
                    return new EmptyResult();
                Session[fileName] = null;
                return File(ms, "application/octet-stream", fileName);
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return HttpNotFound();
            }
        }

        //Stock KMSI ==================================================
        public ActionResult KMSIReport()
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    List<MenuModels> MenuMaster = (List<MenuModels>)Session["MenuMaster"];
                    Int32 RoleID = Convert.ToInt32(Session["RoleId"]);

                    if (RoleID == 0)
                    {
                        ViewBag.IsInput = 1;
                        ViewBag.IsUpdate = 1;
                        ViewBag.IsDelete = 1;
                        ViewBag.IsUpload = 1;
                        ViewBag.IsDownload = 1;
                    }
                    else
                    {
                        var access = MenuMaster.Where(x => x.ControllerName == "Stock" && x.ActionName == "KMSIReport" && x.RoleId == RoleID).FirstOrDefault();

                        if (access.IsView == 1)
                        {
                            ViewBag.IsInput = access.IsInput;
                            ViewBag.IsUpdate = access.IsUpdate;
                            ViewBag.IsDelete = access.IsDelete;
                            ViewBag.IsUpload = access.IsUpload;
                            ViewBag.IsDownload = access.IsDownload;
                        }
                        else
                        {
                            ViewBag.Message = "You don't have access to this page! Please contact administrator.";
                        }
                    }

                    var OnHand = db.Stocks.Where(x => x.Position == "KMSI").Sum(x => x.Quantity);
                    ViewBag.OnHand = OnHand.HasValue ? OnHand.Value : 0;
                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult KMSIStockList()
        {
            try
            {
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();

                // Paging Size
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                int totalStock = 0;

                // Getting all data
                var stockData = db.Stocks.Where(x => x.Position == "KMSI").AsEnumerable()
                    .Select(p => new {
                        ID = p.ID,
                        PartNumber = p.PartNumber,
                        PartName = p.PartName != null ? p.PartName : "",
                        Quantity = p.Quantity,
                        UnitPrice = p.UnitPrice,
                        Position = p.Position,
                        IncomingDate = p.IncomingDeliveryDate.HasValue ? p.IncomingDeliveryDate.Value.ToString("yyyy-MM-dd") : "",
                        IncomingNo = p.DONO != null ? p.DONO : ""
                    });

                // Sorting
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    stockData = stockData.OrderBy(sortColumn + " " + sortColumnDir);
                }
                // Search
                if (!string.IsNullOrEmpty(searchValue))
                {
                    stockData = stockData.Where(p => p.IncomingNo.ToLower().Contains(searchValue.ToLower())
                        || p.IncomingDate.ToLower().Contains(searchValue.ToLower())
                        || p.PartNumber.ToLower().Contains(searchValue.ToLower())
                        || p.PartName.ToLower().Contains(searchValue.ToLower())
                        || p.Quantity.ToString().ToLower().Contains(searchValue.ToLower())
                    );
                }

                // total number of rows count
                recordsTotal = stockData.Count();
                totalStock = (int)stockData.Sum(x => x.Quantity);
                // Paging
                var data = stockData.Skip(skip).Take(pageSize).ToList();
                // Returning Json Data
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data, total = totalStock });
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult GenerateXlsKMSIStock(int[] ids)
        {
            try
            {
                DateTime dateTime = DateTime.Now;
                string FileName = string.Format("STOCK On Hand KMSI_" + dateTime.ToString("yyyyMMdd_HHmm") + ".xlsx");
                string handle = Guid.NewGuid().ToString();

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (ExcelPackage excel = new ExcelPackage())
                    {
                        excel.Workbook.Worksheets.Add("KMSI Stock");

                        var headerRow = new List<string[]>()
                        {
                            new string[] {
                                "Incoming List No.",
                                "Incoming Date",
                                "Part Number",
                                "Part Name",
                                "Quantity"
                            }
                        };
                        string headerRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";

                        var worksheet = excel.Workbook.Worksheets["KMSI Stock"];
                        worksheet.Cells[headerRange].LoadFromArrays(headerRow);
                        worksheet.Cells[headerRange].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        worksheet.Cells[headerRange].Style.Font.Bold = true;
                        worksheet.Cells[headerRange].Style.Font.Size = 12;
                        worksheet.Cells[headerRange].AutoFitColumns();

                        var stockData = db.Stocks.Where(x => x.Position == "KMSI" && ids.Contains(x.ID)).AsEnumerable()
                            .Select(p => new {
                                PartNumber = p.PartNumber,
                                PartName = p.PartName != null ? p.PartName : "",
                                Quantity = p.Quantity,
                                UnitPrice = p.UnitPrice,
                                Position = p.Position,
                                IncomingDate = p.IncomingDeliveryDate.HasValue ? p.IncomingDeliveryDate.Value.ToString("yyyy-MM-dd") : "",
                                IncomingNo = p.DONO != null ? p.DONO : ""
                            }).ToList();

                        int rowNumber = 2;
                        foreach (var data in stockData)
                        {
                            worksheet.Cells[rowNumber, 1].Value = data.IncomingNo;
                            worksheet.Cells[rowNumber, 2].Value = data.IncomingDate;
                            worksheet.Cells[rowNumber, 3].Value = data.PartNumber;
                            worksheet.Cells[rowNumber, 4].Value = data.PartName;
                            worksheet.Cells[rowNumber, 5].Value = data.Quantity;

                            rowNumber++;
                        }
                        excel.SaveAs(memoryStream);
                    }
                    memoryStream.Position = 0;
                    TempData[handle] = memoryStream.ToArray();
                }

                return new JsonResult()
                {
                    Data = new { FileGuid = handle, FileName = FileName }
                };
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [HttpGet]
        public ActionResult DownloadXlsKMSIStock(string fileGuid, string filename)
        {
            if (TempData[fileGuid] != null)
            {
                byte[] data = TempData[fileGuid] as byte[];
                return File(data, "application/vnd.ms-excel", filename);
            }
            else
            {
                return new EmptyResult();
            }
        }

        public ActionResult GeneratePdfKMSIStock(int[] ids)
        {
            try
            {
                DateTime dateTime = DateTime.Now;
                string strPdfFileName = string.Format("STOCK On Hand KMSI_" + dateTime.ToString("yyyyMMdd_HHmm") + ".pdf");
                string handle = Guid.NewGuid().ToString();

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    Document pdfDoc = new Document(PageSize.A4.Rotate());
                    PdfWriter.GetInstance(pdfDoc, memoryStream).CloseStream = false;
                    pdfDoc.SetMargins(28f, 28f, 28f, 28f);

                    string strAttachment = Server.MapPath("~/PDFs/" + strPdfFileName);

                    pdfDoc.Open();
                    Phrase phrase = new Phrase();

                    PdfPTable table = new PdfPTable(1);
                    table.WidthPercentage = 100;
                    table.SpacingAfter = 10f;

                    //add a LEFT LABEL 
                    table.AddCell(new PdfPCell(new Phrase("K-PINTAR PARTS PORTAL SYSTEM", FontFactory.GetFont("Calibri", 7, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });

                    // invoke LEFT image
                    iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Content/img/logo_kmsi.png"));
                    jpg.ScaleAbsolute(120f, 40f);
                    PdfPCell imageCell = new PdfPCell(jpg);
                    imageCell.Colspan = 2; // either 1 if you need to insert one cell
                    imageCell.Border = 0;
                    imageCell.HorizontalAlignment = Element.ALIGN_LEFT;

                    //add a RIGHT LABEL 
                    table.AddCell(new PdfPCell(new Phrase("VENDOR STOCK SYSTEM", FontFactory.GetFont("Calibri", 7, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_RIGHT,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });

                    // invoke RIGHT image
                    iTextSharp.text.Image rightJpg = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Content/img/logo-komatsu.png"));
                    rightJpg.ScaleAbsolute(120f, 40f);
                    PdfPCell rightImageCell = new PdfPCell(rightJpg);
                    rightImageCell.Colspan = 2; // either 1 if you need to insert one cell
                    rightImageCell.Border = 0;
                    rightImageCell.HorizontalAlignment = Element.ALIGN_RIGHT;

                    // add a LEFT image to PdfPTables
                    table.AddCell(imageCell);
                    // add a RIGHT image to PdfPTables
                    //table.AddCell(rightImageCell);

                    table.AddCell(new PdfPCell(new Phrase("STOCK On Hand KMSI", FontFactory.GetFont("Calibri", 20, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });
                    pdfDoc.Add(table);

                    var stockData = db.Stocks.Where(x => x.Position == "KMSI" && ids.Contains(x.ID)).AsEnumerable()
                        .Select(p => new {
                            PartNumber = p.PartNumber,
                            PartName = p.PartName != null ? p.PartName : "",
                            Quantity = p.Quantity,
                            UnitPrice = p.UnitPrice,
                            Position = p.Position,
                            IncomingDate = p.IncomingDeliveryDate.HasValue ? p.IncomingDeliveryDate.Value.ToString("yyyy-MM-dd") : "",
                            IncomingNo = p.DONO != null ? p.DONO : ""
                        }).ToList();

                    //Table
                    float[] widths = new float[] { 70f, 50f, 100f, 100f, 100f };
                    table = new PdfPTable(5);
                    ////table.WidthPercentage = 100;
                    table.TotalWidth = 520f;
                    table.LockedWidth = true;
                    table.SetWidths(widths);
                    table.SpacingBefore = 5f;
                    table.HeaderRows = 1;

                    //header
                    string[] headers = new string[] {
                        "Incoming List No.",
                        "Incoming Date",
                        "Part Number",
                        "Part Name",
                        "Quantity"
                    };
                    for (int i = 0; i < headers.Count(); i++)
                    {
                        table.AddCell(new PdfPCell(new Phrase(headers[i], FontFactory.GetFont("Calibri", 10)))
                        {
                            PaddingTop = 3,
                            PaddingBottom = 8,
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                    }

                    foreach (var item in stockData)
                    {
                        table.AddCell(new PdfPCell(new Phrase(item.IncomingNo != null ? item.IncomingNo : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.IncomingDate != null ? item.IncomingDate : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.PartNumber != null ? item.PartNumber : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.PartName != null ? item.PartName : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.Quantity != null ? item.Quantity.ToString() : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                    }
                    pdfDoc.Add(table);
                    pdfDoc.Close();

                    var bytes = memoryStream.ToArray();
                    Session[strPdfFileName] = bytes;
                }

                return Json(new { success = true, strPdfFileName }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [HttpGet]
        public virtual ActionResult DownloadPdfKMSIStock(string fileName)
        {
            try
            {
                var ms = Session[fileName] as byte[];
                if (ms == null)
                    return new EmptyResult();
                Session[fileName] = null;
                return File(ms, "application/octet-stream", fileName);
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return HttpNotFound();
            }
        }

    }
}
