﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.draw;
using KMSIPartsPortal_System.Helpers;
using KMSIPartsPortal_System.Models;

namespace KMSIPartsPortal_System.Controllers
{
    public class WorkOrderController : Controller
    {
        // GET: WorkOrder/Index
        public ActionResult Index(string code, string unit)
        {
            if (Session["UserId"] != null)
            {
                using (kmsi_portalEntities db = new kmsi_portalEntities())
                {
                    if (code == null)
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }
                    ViewBag.ContractCode = code;
                    ViewBag.UnitSerialNumber = unit;

                    return View();
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        public ActionResult List()
        {
            try
            {
                // Creating instance of DatabaseContext class
                using (kmsi_portalEntities db = new kmsi_portalEntities())
                {
                    var draw = Request.Form.GetValues("draw").FirstOrDefault();
                    var start = Request.Form.GetValues("start").FirstOrDefault();
                    var length = Request.Form.GetValues("length").FirstOrDefault();
                    var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                    var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                    var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();

                    // Paging Size
                    int pageSize = length != null ? Convert.ToInt32(length) : 0;
                    int skip = start != null ? Convert.ToInt32(start) : 0;
                    int recordsTotal = 0;

                    // Getting all Work Order data
                    var workOrderData = db.WorkOrders.AsEnumerable()
                                    .Where(x => x.ContractCode == Request.Form.GetValues("ContractCode").FirstOrDefault() && x.UnitSerialNumber == Request.Form.GetValues("UnitSerialNumber").FirstOrDefault() && x.ApprovalStatus == 1)
                                    .GroupBy(x => x.WorkOrderCode)
                                    .Select(group => group.First());

                    // Sorting
                    if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                    {
                        workOrderData = workOrderData.OrderBy(sortColumn + " " + sortColumnDir);
                    }
                    // Search
                    if (!string.IsNullOrEmpty(searchValue))
                    {
                        workOrderData = workOrderData.Where(w => w.WorkOrderCode.ToString().ToLower().Contains(searchValue.ToLower())
                            || w.WorkOrderDate.ToString().ToLower().Contains(searchValue.ToLower())
                            || w.UnitSerialNumber.ToLower().Contains(searchValue.ToLower())
                            || w.UnitCode.ToLower().Contains(searchValue.ToLower())
                            || w.UnitModel.ToString().ToLower().Contains(searchValue.ToLower())
                            || w.AgreementNumber.ToString().ToLower().Contains(searchValue.ToLower())
                            || w.CustomerCode.ToLower().Contains(searchValue.ToLower())
                            || w.CustomerName.ToLower().Contains(searchValue.ToLower())
                            || w.RequestDeliveryDate.ToString().ToLower().Contains(searchValue.ToLower())
                            || w.Site.ToLower().Contains(searchValue.ToLower())
                            || (w.Comments != null && w.Comments.ToLower().Contains(searchValue.ToLower()))
                        );
                    }

                    // total number of rows count
                    recordsTotal = workOrderData.Count();

                    // Paging
                    var data = workOrderData.Skip(skip).Take(pageSize).ToList();
                    // Returning Json Data
                    return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        // GET: WorkOrder/Details/5
        public ActionResult Details(string code)
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    using (kmsi_portalEntities db = new kmsi_portalEntities())
                    {
                        var workOrderData = db.WorkOrders
                            .Where(wo => wo.WorkOrderCode == code)
                            .OrderBy(wo => wo.WorkOrderID)
                            .ToList();
                        var woSummaryData = workOrderData.FirstOrDefault();

                        ViewBag.woSummaryData = woSummaryData;
                        ViewBag.pnData = workOrderData;
                        return View();
                    }
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        // GET: WorkOrder/Create
        public ActionResult Create(string code, string unit)
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    using (kmsi_portalEntities db = new kmsi_portalEntities())
                    {
                        if (code == null)
                        {
                            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                        }
                        List<Contract> contract = db.Contracts.Where(c => c.ContractCode == code && c.UnitSerialNumber == unit).ToList();
                        var contractData = contract.FirstOrDefault();
                        Customer customer = db.Customers.Where(c => c.CustomerCode == contractData.CustomerId).FirstOrDefault();
                        var getDataWorkOrder = (from wo in db.WorkOrders
                                                select wo.WorkOrderCode).Max();

                        string dtYear = DateTime.Now.ToString("yy");

                        ViewBag.contractData = contract.FirstOrDefault();
                        ViewBag.customerData = customer;
                        ViewBag.workOrderCode = getDataWorkOrder != null ? CodeGenerator.Generate("WO", "-", getDataWorkOrder) : "WO-" + "00" + dtYear + "-00001";
                        ViewBag.today = DateTime.Now.ToString("dddd, dd MMMM yyyy");

                        return View();
                    }
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            
        }

        // POST: WorkOrder/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(WorkOrder workOrder)
        {
            try
            {
                using (kmsi_portalEntities db = new kmsi_portalEntities())
                {
                    var errMsg = TempData["ErrorMessage"] as string;

                    if (ModelState.IsValid)
                    {
                        string[] PartsNumber = Request.Form.GetValues("PartsNumber[]");
                        string[] Description = Request.Form.GetValues("Description[]");
                        string[] Quantity = Request.Form.GetValues("Quantity[]");
                        string[] Remark = Request.Form.GetValues("Remark[]");

                        var i = 0;
                        foreach (var item in PartsNumber)
                        {
                            workOrder.WorkOrderDate = DateTime.Now;
                            workOrder.PartsNumber = item;
                            workOrder.Quantity = Convert.ToInt32(Quantity[i]);
                            workOrder.Description = Description[i];
                            workOrder.Remarks = Remark[i];
                            workOrder.ApprovalStatus = 1;
                            db.WorkOrders.Add(workOrder);
                            db.SaveChanges();
                            i++;
                        }

                        Int32 UserID = Convert.ToInt32(Session["UserId"]);
                        User user = new User();
                        user = db.Users.Find(UserID);
                        var woData = db.WorkOrders.Where(x => x.WorkOrderCode == workOrder.WorkOrderCode).ToList();
                        string baseUrl = string.Format("{0}://{1}{2}",Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                        ViewBag.baseUrl = baseUrl + "WorkOrder/Index?code=" + workOrder.ContractCode + "&unit=" + workOrder.UnitSerialNumber;

                        string linkDetail = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                        ViewData["LinkEmail"] = linkDetail;

                        string body = ViewRenderer.RenderRazorViewToString(this, "~/Views/Mail/Submitted_WorkOrder.cshtml", new Email()
                        {
                            To = user.FirstName + " " + user.LastName,
                            Date = DateTime.Now,
                            WorkOrders = woData
                        });

                        string attachment = CreateAttachment(workOrder.WorkOrderCode);
                        SendEmail((string)Session["Email"], "Work Order Document", body, attachment);

                        //return RedirectToAction("Index", new
                        //{
                        //    code = workOrder.ContractCode,
                        //    unit = workOrder.UnitSerialNumber
                        //});

                        return Json(new { success = true, code = workOrder.ContractCode, unit = workOrder.UnitSerialNumber }, JsonRequestBehavior.AllowGet);
                    }

                    //TempData["ErrorMessage"] = "This field is required.";
                    //return RedirectToAction("Create", new
                    //{
                    //    code = workOrder.ContractCode,
                    //    unit = workOrder.UnitSerialNumber
                    //});

                    return Json(new { success = false, message = "This field is required." }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return RedirectToAction("Create", new
                {
                    code = workOrder.ContractCode,
                    unit = workOrder.UnitSerialNumber
                });
            }
        }

        // GET: WorkOrder/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: WorkOrder/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: WorkOrder/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: WorkOrder/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public bool SendEmail(string toEmail, string subject, string emailBody, string attachment)
        {
            try
            {
                string senderEmail = System.Configuration.ConfigurationManager.AppSettings["senderEmail"].ToString();
                string senderPassword = System.Configuration.ConfigurationManager.AppSettings["senderPassword"].ToString();

                SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                client.EnableSsl = true;
                client.Timeout = 100000;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential(senderEmail, senderPassword);

                MailMessage mailMessage = new MailMessage(senderEmail, toEmail, subject, emailBody);
                mailMessage.IsBodyHtml = true;
                mailMessage.BodyEncoding = UTF8Encoding.UTF8;
                if (attachment != "")
                {
                    mailMessage.Attachments.Add(new Attachment(attachment));
                }

                client.Send(mailMessage);

                return true;
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return false;
            }
        }

        public ActionResult PostDownloadPartial(string[] codes)
        {
            try
            {
                using (kmsi_portalEntities db = new kmsi_portalEntities())
                {
                    DateTime dateTime = DateTime.Now;
                    string strCSVFileName = string.Format("FMCO_" + dateTime.ToString("yyyyMMdd_HHmm") + ".csv");
                    string handle = Guid.NewGuid().ToString();

                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        StreamWriter stringWriter = new StreamWriter(memoryStream);
                        stringWriter.WriteLine("\"WorkOrderCode\",\"PartsNumber\",\"Quantity\"");

                        var listWorkOrder = db.WorkOrders.Where(x => codes.Contains(x.WorkOrderCode)).OrderBy(w => w.WorkOrderCode);

                        foreach (var wo in listWorkOrder)
                        {
                            stringWriter.WriteLine(string.Format("\"{0}\",\"{1}\",\"{2}\"",
                                wo.WorkOrderCode, wo.PartsNumber, wo.Quantity));
                        }

                        stringWriter.Flush();
                        memoryStream.Seek(0, SeekOrigin.Begin);
                        memoryStream.Position = 0;
                        TempData[handle] = memoryStream.ToArray();
                    }

                    return Json(new { success = true, FileGuid = handle, FileName = strCSVFileName }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return HttpNotFound();
            }
        }

        [HttpGet]
        public virtual ActionResult DownloadCsv(string fileGuid, string fileName)
        {
            try
            {
                if (TempData[fileGuid] != null)
                {
                    byte[] data = TempData[fileGuid] as byte[];
                    return File(data, "text/plain", fileName);
                }
                else
                {
                    return new EmptyResult();
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return HttpNotFound();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Approval(WorkOrder workOrder)
        {
            try
            {
                using (kmsi_portalEntities db = new kmsi_portalEntities())
                {
                    string WorkOrderCode = Request.Form["WorkOrderCode"];
                    string ContractCode = Request.Form["ContractCode"];
                    string mode = Request.Form["mode"];
                    int approvalStatus = 1;
                    string body = String.Empty;

                    if (mode.Equals("approve"))
                    {
                        approvalStatus = 2;

                        Int32 UserID = Convert.ToInt32(Session["UserId"]);
                        User user = new User();
                        user = db.Users.Find(UserID);
                        string baseUrl = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                        ViewBag.baseUrl = baseUrl + "WorkOrder/Index?code=" + workOrder.ContractCode + "&unit=" + workOrder.UnitSerialNumber;

                        string linkDetail = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                        ViewData["LinkEmail"] = linkDetail;

                        body = ViewRenderer.RenderRazorViewToString(this, "~/Views/Mail/Approved_WorkOrder.cshtml", new Email()
                        {
                            To = user.FirstName + " " + user.LastName,
                            Date = DateTime.Now
                        });
                    }
                    else if (mode.Equals("reject"))
                    {
                        approvalStatus = 3;

                        Int32 UserID = Convert.ToInt32(Session["UserId"]);
                        User user = new User();
                        user = db.Users.Find(UserID);
                        string baseUrl = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                        ViewBag.baseUrl = baseUrl + "WorkOrder/Index?code=" + workOrder.ContractCode + "&unit=" + workOrder.UnitSerialNumber;

                        string linkDetail = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                        ViewData["LinkEmail"] = linkDetail;

                        body = ViewRenderer.RenderRazorViewToString(this, "~/Views/Mail/Rejected_WorkOrder.cshtml", new Email()
                        {
                            To = user.FirstName + " " + user.LastName,
                            Date = DateTime.Now
                        });
                    }

                    (from wo in db.WorkOrders
                     where wo.WorkOrderCode == WorkOrderCode
                     select wo).ToList()
                     .ForEach(x => x.ApprovalStatus = approvalStatus);
                    db.SaveChanges();

                    SendEmail((string)Session["Email"], "Work Order Document", body, "");

                    //if (mode.Equals("approve"))
                    //{
                    //    DownloadCSV(WorkOrderCode);
                    //}

                    return RedirectToAction("Index", new RouteValueDictionary(
                            new { controller = "WorkOrder", action = "Index", code = ContractCode, unit = workOrder.UnitSerialNumber }));
                }
            }
            catch
            {
                return View();
            }
        }

        public ActionResult DownloadPdf(string code)
        {
            try
            {
                using (kmsi_portalEntities db = new kmsi_portalEntities())
                {
                    MemoryStream memoryStream = new MemoryStream();
                    StringBuilder status = new StringBuilder("");
                    DateTime dateTime = DateTime.Now;
                    //file name to be created
                    string strPDFFileName = string.Format("WorkOrderForm_" + code + "_" + dateTime.ToString("yyyyMMdd_HHmm") + ".pdf");
                    Document pdfDoc = new Document(PageSize.A4);
                    pdfDoc.SetMargins(36f, 36f, 36f, 36f);
                    //Create PDF Table

                    //file will created in this path
                    string strAttachment = Server.MapPath("~/PDFs/" + strPDFFileName);

                    PdfWriter writer = PdfWriter.GetInstance(pdfDoc, memoryStream);
                    writer.CloseStream = false;
                    pdfDoc.Open();

                    PdfHeader header = new PdfHeader();
                    writer.PageEvent = header;

                    Phrase phrase = new Phrase();

                    //Table
                    PdfPTable table = new PdfPTable(2);
                    table.WidthPercentage = 100;
                    table.SpacingBefore = 0f;
                    table.SpacingAfter = 0f;

                    //Cell no 1
                    Image header1 = Image.GetInstance(Server.MapPath("~/Content/img/header-1.png"));
                    header1.ScaleAbsolute(154f, 35f);
                    header1.Alignment = Image.ALIGN_LEFT;
                    PdfPCell cell = new PdfPCell();
                    cell.Border = 0;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.AddElement(header1);
                    table.AddCell(cell);

                    //Cell no 2
                    Image header2 = Image.GetInstance(Server.MapPath("~/Content/img/header-2.png"));
                    header2.ScaleAbsolute(110f, 21f);
                    header2.Alignment = Image.ALIGN_RIGHT;
                    cell = new PdfPCell();
                    cell.Border = 0;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.AddElement(header2);
                    table.AddCell(cell);

                    //Add table to document
                    pdfDoc.Add(table);

                    //Horizontal Line
                    LineSeparator line = new LineSeparator(0.0F, 100.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1);
                    pdfDoc.Add(line);

                    //Title
                    table = new PdfPTable(1);
                    table.WidthPercentage = 100;
                    table.SpacingBefore = 10f;

                    table.AddCell(new PdfPCell(new Phrase("Work Order", FontFactory.GetFont("Arial", 20, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });
                    pdfDoc.Add(table);

                    List<WorkOrder> workOrders = db.WorkOrders.Where(wo => wo.WorkOrderCode == code).ToList();

                    if (workOrders.Count > 0)
                    {
                        WorkOrder workOrder = workOrders.FirstOrDefault();

                        //Table
                        table = new PdfPTable(4);
                        table.WidthPercentage = 100;
                        table.SpacingBefore = 20f;
                        table.SpacingAfter = 0f;

                        //Cell
                        table.AddCell(new PdfPCell(new Phrase("Work Order ID"))
                        {
                            Border = 0,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                        table.AddCell(new PdfPCell(new Phrase(": " + workOrder.WorkOrderCode))
                        {
                            Border = 0,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                        table.AddCell(new PdfPCell()
                        {
                            Border = 0,
                            Colspan = 2
                        });

                        table.AddCell(new PdfPCell(new Phrase("Work Order Date"))
                        {
                            Border = 0,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                        DateTime woDate = (DateTime)workOrder.WorkOrderDate;
                        table.AddCell(new PdfPCell(new Phrase(": " + woDate.ToString("yyyy-MM-dd")))
                        {
                            Border = 0,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                        table.AddCell(new PdfPCell(new Phrase("Customer ID"))
                        {
                            Border = 0,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                        table.AddCell(new PdfPCell(new Phrase(": " + workOrder.CustomerCode))
                        {
                            Border = 0,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });

                        table.AddCell(new PdfPCell(new Phrase("Unit Serial Number"))
                        {
                            Border = 0,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                        table.AddCell(new PdfPCell(new Phrase(": " + workOrder.UnitSerialNumber))
                        {
                            Border = 0,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                        table.AddCell(new PdfPCell(new Phrase("Customer Name"))
                        {
                            Border = 0,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                        table.AddCell(new PdfPCell(new Phrase(": " + workOrder.CustomerName))
                        {
                            Border = 0,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });

                        table.AddCell(new PdfPCell(new Phrase("Unit Code"))
                        {
                            Border = 0,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                        table.AddCell(new PdfPCell(new Phrase(": " + workOrder.UnitCode))
                        {
                            Border = 0,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                        table.AddCell(new PdfPCell(new Phrase("Req. Delivery Date"))
                        {
                            Border = 0,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                        //DateTime reqDate = (DateTime)workOrder.RequestDeliveryDate;
                        string reqDate = (workOrder.RequestDeliveryDate != null) ? workOrder.RequestDeliveryDate.Value.ToString("yyyy-MM-dd") : "";
                        table.AddCell(new PdfPCell(new Phrase(": " + reqDate))
                        {
                            Border = 0,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });

                        table.AddCell(new PdfPCell(new Phrase("Unit Model"))
                        {
                            Border = 0,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                        table.AddCell(new PdfPCell(new Phrase(": " + workOrder.UnitModel))
                        {
                            Border = 0,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                        table.AddCell(new PdfPCell(new Phrase("Site"))
                        {
                            Border = 0,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                        table.AddCell(new PdfPCell(new Phrase(": " + workOrder.Site))
                        {
                            Border = 0,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });

                        table.AddCell(new PdfPCell(new Phrase("Agreement No."))
                        {
                            Border = 0,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                        table.AddCell(new PdfPCell(new Phrase(": " + workOrder.ContractCode))
                        {
                            Border = 0,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                        table.AddCell(new PdfPCell()
                        {
                            Border = 0,
                            Colspan = 2
                        });

                        pdfDoc.Add(table);

                        //Table content
                        table = new PdfPTable(5);
                        table.WidthPercentage = 100;
                        table.SpacingBefore = 10f;
                        table.HeaderRows = 1;

                        //Cell content
                        //header
                        table.AddCell(new PdfPCell(new Phrase("No.", FontFactory.GetFont("Arial", 12, Font.UNDERLINE)))
                        {
                            Border = 0,
                            BorderWidthLeft = 1,
                            BorderWidthBottom = 1,
                            BorderWidthTop = 1,
                            PaddingTop = 3,
                            PaddingBottom = 8,
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP
                        });
                        table.AddCell(new PdfPCell(new Phrase("Parts Number", FontFactory.GetFont("Arial", 12, Font.UNDERLINE)))
                        {
                            Border = 0,
                            BorderWidthBottom = 1,
                            BorderWidthTop = 1,
                            PaddingTop = 3,
                            PaddingBottom = 8,
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                        table.AddCell(new PdfPCell(new Phrase("Description", FontFactory.GetFont("Arial", 12, Font.UNDERLINE)))
                        {
                            Border = 0,
                            BorderWidthBottom = 1,
                            BorderWidthTop = 1,
                            PaddingTop = 3,
                            PaddingBottom = 8,
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                        table.AddCell(new PdfPCell(new Phrase("Qty", FontFactory.GetFont("Arial", 12, Font.UNDERLINE)))
                        {
                            Border = 0,
                            BorderWidthBottom = 1,
                            BorderWidthTop = 1,
                            PaddingTop = 3,
                            PaddingBottom = 8,
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                        table.AddCell(new PdfPCell(new Phrase("Remarks", FontFactory.GetFont("Arial", 12, Font.UNDERLINE)))
                        {
                            Border = 0,
                            BorderWidthRight = 1,
                            BorderWidthBottom = 1,
                            BorderWidthTop = 1,
                            PaddingTop = 3,
                            PaddingBottom = 8,
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });

                        int counter = 1;
                        int size = workOrders.Count();
                        foreach (var item in workOrders)
                        {
                            table.AddCell(new PdfPCell(new Phrase(counter.ToString() + ".", FontFactory.GetFont("Arial", 11)))
                            {
                                Border = 0,
                                BorderWidthLeft = 1,
                                BorderWidthBottom = (counter == size || counter == 41) ? 1 : 0,
                                PaddingBottom = (counter == size || counter == 41) ? 8 : 0,
                                HorizontalAlignment = Element.ALIGN_CENTER,
                                VerticalAlignment = Element.ALIGN_MIDDLE
                            });
                            table.AddCell(new PdfPCell(new Phrase(item.PartsNumber, FontFactory.GetFont("Arial", 11)))
                            {
                                Border = 0,
                                BorderWidthBottom = (counter == size || counter == 41) ? 1 : 0,
                                PaddingBottom = (counter == size || counter == 41) ? 8 : 0,
                                HorizontalAlignment = Element.ALIGN_CENTER,
                                VerticalAlignment = Element.ALIGN_MIDDLE
                            });
                            table.AddCell(new PdfPCell(new Phrase(item.Description, FontFactory.GetFont("Arial", 11)))
                            {
                                Border = 0,
                                BorderWidthBottom = (counter == size || counter == 41) ? 1 : 0,
                                PaddingBottom = (counter == size || counter == 41) ? 8 : 0,
                                HorizontalAlignment = Element.ALIGN_CENTER,
                                VerticalAlignment = Element.ALIGN_MIDDLE
                            });
                            table.AddCell(new PdfPCell(new Phrase(item.Quantity.ToString(), FontFactory.GetFont("Arial", 11)))
                            {
                                Border = 0,
                                BorderWidthBottom = (counter == size || counter == 41) ? 1 : 0,
                                PaddingBottom = (counter == size || counter == 41) ? 8 : 0,
                                HorizontalAlignment = Element.ALIGN_CENTER,
                                VerticalAlignment = Element.ALIGN_MIDDLE
                            });
                            table.AddCell(new PdfPCell(new Phrase(item.Remarks, FontFactory.GetFont("Arial", 11)))
                            {
                                Border = 0,
                                BorderWidthRight = 1,
                                BorderWidthBottom = (counter == size || counter == 41) ? 1 : 0,
                                PaddingBottom = (counter == size || counter == 41) ? 8 : 0,
                                HorizontalAlignment = Element.ALIGN_CENTER,
                                VerticalAlignment = Element.ALIGN_MIDDLE
                            });
                            counter++;
                        }

                        pdfDoc.Add(table);

                        Paragraph para = new Paragraph();
                        phrase = new Phrase("*Notes : " + workOrder.Comments, FontFactory.GetFont("Arial", 10, Font.ITALIC));
                        para.Add(phrase);
                        pdfDoc.Add(para);
                    }

                    //Table footer
                    table = new PdfPTable(3);
                    table.WidthPercentage = 100;
                    table.SpacingBefore = 20f;

                    //Cell footer
                    table.AddCell(new PdfPCell(new Phrase("Disetujui Oleh,"))
                    {
                        Border = 0,
                        PaddingBottom = 72,
                        HorizontalAlignment = Element.ALIGN_CENTER
                    });
                    table.AddCell(new PdfPCell(new Phrase("Diterima Oleh,"))
                    {
                        Border = 0,
                        PaddingBottom = 72,
                        HorizontalAlignment = Element.ALIGN_CENTER
                    });
                    table.AddCell(new PdfPCell(new Phrase("Dibuat Oleh,"))
                    {
                        Border = 0,
                        PaddingBottom = 72,
                        HorizontalAlignment = Element.ALIGN_CENTER
                    });

                    table.AddCell(new PdfPCell(new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 56F, BaseColor.BLACK, Element.ALIGN_CENTER, 1))))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_CENTER
                    });
                    table.AddCell(new PdfPCell(new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 56F, BaseColor.BLACK, Element.ALIGN_CENTER, 1))))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_CENTER
                    });
                    table.AddCell(new PdfPCell(new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 56F, BaseColor.BLACK, Element.ALIGN_CENTER, 1))))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_CENTER
                    });

                    pdfDoc.Add(table);

                    // Closing the document
                    pdfDoc.Close();

                    byte[] byteInfo = memoryStream.ToArray();
                    memoryStream.Write(byteInfo, 0, byteInfo.Length);
                    memoryStream.Position = 0;

                    return File(memoryStream, "application/pdf", strPDFFileName);
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public string CreateAttachment(string code)
        {
            try
            {
                using (kmsi_portalEntities db = new kmsi_portalEntities())
                {
                    MemoryStream memoryStream = new MemoryStream();
                    StringBuilder status = new StringBuilder("");
                    DateTime dateTime = DateTime.Now;
                    //file name to be created
                    string strPDFFileName = string.Format("WorkOrderForm_" + code + "_" + dateTime.ToString("yyyyMMdd_HHmm") + ".pdf");
                    Document pdfDoc = new Document(PageSize.A4);
                    pdfDoc.SetMargins(36f, 36f, 72f, 72f);
                    //Create PDF Table

                    //file will created in this path
                    string strAttachment = Server.MapPath("~/PDFs/" + strPDFFileName);

                    PdfWriter.GetInstance(pdfDoc, memoryStream).CloseStream = false;
                    pdfDoc.Open();

                    Phrase phrase = new Phrase();

                    //Table
                    PdfPTable table = new PdfPTable(2);
                    table.WidthPercentage = 100;
                    table.SpacingBefore = 0f;
                    table.SpacingAfter = 0f;

                    //Cell no 1
                    Image header1 = Image.GetInstance(Server.MapPath("~/Content/img/header-1.png"));
                    header1.ScaleAbsolute(154f, 35f);
                    header1.Alignment = Image.ALIGN_LEFT;
                    PdfPCell cell = new PdfPCell();
                    cell.Border = 0;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.AddElement(header1);
                    table.AddCell(cell);

                    //Cell no 2
                    Image header2 = Image.GetInstance(Server.MapPath("~/Content/img/header-2.png"));
                    header2.ScaleAbsolute(110f, 21f);
                    header2.Alignment = Image.ALIGN_RIGHT;
                    cell = new PdfPCell();
                    cell.Border = 0;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.AddElement(header2);
                    table.AddCell(cell);

                    //Add table to document
                    pdfDoc.Add(table);

                    //Horizontal Line
                    Paragraph line = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1)));
                    pdfDoc.Add(line);

                    //Title
                    table = new PdfPTable(1);
                    table.WidthPercentage = 100;
                    table.SpacingBefore = 10f;

                    table.AddCell(new PdfPCell(new Phrase("Work Order", FontFactory.GetFont("Arial", 20, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });
                    pdfDoc.Add(table);

                    List<WorkOrder> workOrders = db.WorkOrders.Where(wo => wo.WorkOrderCode == code).ToList();

                    if (workOrders.Count > 0)
                    {
                        WorkOrder workOrder = workOrders.FirstOrDefault();

                        //Table
                        table = new PdfPTable(4);
                        table.WidthPercentage = 100;
                        table.SpacingBefore = 20f;
                        table.SpacingAfter = 0f;

                        //Cell
                        table.AddCell(new PdfPCell(new Phrase("Work Order ID"))
                        {
                            Border = 0,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                        table.AddCell(new PdfPCell(new Phrase(": " + workOrder.WorkOrderCode))
                        {
                            Border = 0,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                        table.AddCell(new PdfPCell()
                        {
                            Border = 0,
                            Colspan = 2
                        });

                        table.AddCell(new PdfPCell(new Phrase("Work Order Date"))
                        {
                            Border = 0,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                        DateTime woDate = (DateTime)workOrder.WorkOrderDate;
                        table.AddCell(new PdfPCell(new Phrase(": " + woDate.ToString("yyyy-MM-dd")))
                        {
                            Border = 0,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                        table.AddCell(new PdfPCell(new Phrase("Customer ID"))
                        {
                            Border = 0,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                        table.AddCell(new PdfPCell(new Phrase(": " + workOrder.CustomerCode))
                        {
                            Border = 0,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });

                        table.AddCell(new PdfPCell(new Phrase("Unit Serial Number"))
                        {
                            Border = 0,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                        table.AddCell(new PdfPCell(new Phrase(": " + workOrder.UnitSerialNumber))
                        {
                            Border = 0,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                        table.AddCell(new PdfPCell(new Phrase("Customer Name"))
                        {
                            Border = 0,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                        table.AddCell(new PdfPCell(new Phrase(": " + workOrder.CustomerName))
                        {
                            Border = 0,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });

                        table.AddCell(new PdfPCell(new Phrase("Unit Code"))
                        {
                            Border = 0,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                        table.AddCell(new PdfPCell(new Phrase(": " + workOrder.UnitCode))
                        {
                            Border = 0,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                        table.AddCell(new PdfPCell(new Phrase("Req. Delivery Date"))
                        {
                            Border = 0,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                        DateTime reqDate = (DateTime)workOrder.RequestDeliveryDate;
                        table.AddCell(new PdfPCell(new Phrase(": " + reqDate.ToString("yyyy-MM-dd")))
                        {
                            Border = 0,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });

                        table.AddCell(new PdfPCell(new Phrase("Unit Model"))
                        {
                            Border = 0,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                        table.AddCell(new PdfPCell(new Phrase(": " + workOrder.UnitModel))
                        {
                            Border = 0,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                        table.AddCell(new PdfPCell(new Phrase("Site"))
                        {
                            Border = 0,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                        table.AddCell(new PdfPCell(new Phrase(": " + workOrder.Site))
                        {
                            Border = 0,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });

                        table.AddCell(new PdfPCell(new Phrase("Agreement No."))
                        {
                            Border = 0,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                        table.AddCell(new PdfPCell(new Phrase(": " + workOrder.ContractCode))
                        {
                            Border = 0,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                        table.AddCell(new PdfPCell()
                        {
                            Border = 0,
                            Colspan = 2
                        });

                        pdfDoc.Add(table);

                        //Table content
                        table = new PdfPTable(5);
                        table.WidthPercentage = 100;
                        table.SpacingBefore = 10f;

                        //Cell content
                        //header
                        table.AddCell(new PdfPCell(new Phrase("No.", FontFactory.GetFont("Arial", 12, Font.UNDERLINE)))
                        {
                            Border = 0,
                            BorderWidthLeft = 1,
                            BorderWidthBottom = 1,
                            BorderWidthTop = 1,
                            PaddingTop = 3,
                            PaddingBottom = 8,
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP
                        });
                        table.AddCell(new PdfPCell(new Phrase("Parts Number", FontFactory.GetFont("Arial", 12, Font.UNDERLINE)))
                        {
                            Border = 0,
                            BorderWidthBottom = 1,
                            BorderWidthTop = 1,
                            PaddingTop = 3,
                            PaddingBottom = 8,
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                        table.AddCell(new PdfPCell(new Phrase("Description", FontFactory.GetFont("Arial", 12, Font.UNDERLINE)))
                        {
                            Border = 0,
                            BorderWidthBottom = 1,
                            BorderWidthTop = 1,
                            PaddingTop = 3,
                            PaddingBottom = 8,
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                        table.AddCell(new PdfPCell(new Phrase("Qty", FontFactory.GetFont("Arial", 12, Font.UNDERLINE)))
                        {
                            Border = 0,
                            BorderWidthBottom = 1,
                            BorderWidthTop = 1,
                            PaddingTop = 3,
                            PaddingBottom = 8,
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                        table.AddCell(new PdfPCell(new Phrase("Remarks", FontFactory.GetFont("Arial", 12, Font.UNDERLINE)))
                        {
                            Border = 0,
                            BorderWidthRight = 1,
                            BorderWidthBottom = 1,
                            BorderWidthTop = 1,
                            PaddingTop = 3,
                            PaddingBottom = 8,
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });

                        int counter = 1;
                        int size = workOrders.Count();
                        foreach (var item in workOrders)
                        {
                            table.AddCell(new PdfPCell(new Phrase(counter.ToString() + ".", FontFactory.GetFont("Arial", 11)))
                            {
                                Border = 0,
                                BorderWidthLeft = 1,
                                BorderWidthBottom = (counter == size) ? 1 : 0,
                                PaddingBottom = (counter == size) ? 8 : 0,
                                HorizontalAlignment = Element.ALIGN_CENTER,
                                VerticalAlignment = Element.ALIGN_MIDDLE
                            });
                            table.AddCell(new PdfPCell(new Phrase(item.PartsNumber, FontFactory.GetFont("Arial", 11)))
                            {
                                Border = 0,
                                BorderWidthBottom = (counter == size) ? 1 : 0,
                                PaddingBottom = (counter == size) ? 8 : 0,
                                HorizontalAlignment = Element.ALIGN_CENTER,
                                VerticalAlignment = Element.ALIGN_MIDDLE
                            });
                            table.AddCell(new PdfPCell(new Phrase(item.Description, FontFactory.GetFont("Arial", 11)))
                            {
                                Border = 0,
                                BorderWidthBottom = (counter == size) ? 1 : 0,
                                PaddingBottom = (counter == size) ? 8 : 0,
                                HorizontalAlignment = Element.ALIGN_CENTER,
                                VerticalAlignment = Element.ALIGN_MIDDLE
                            });
                            table.AddCell(new PdfPCell(new Phrase(item.Quantity.ToString(), FontFactory.GetFont("Arial", 11)))
                            {
                                Border = 0,
                                BorderWidthBottom = (counter == size) ? 1 : 0,
                                PaddingBottom = (counter == size) ? 8 : 0,
                                HorizontalAlignment = Element.ALIGN_CENTER,
                                VerticalAlignment = Element.ALIGN_MIDDLE
                            });
                            table.AddCell(new PdfPCell(new Phrase(item.Remarks, FontFactory.GetFont("Arial", 11)))
                            {
                                Border = 0,
                                BorderWidthRight = 1,
                                BorderWidthBottom = (counter == size) ? 1 : 0,
                                PaddingBottom = (counter == size) ? 8 : 0,
                                HorizontalAlignment = Element.ALIGN_CENTER,
                                VerticalAlignment = Element.ALIGN_MIDDLE
                            });
                            counter++;
                        }

                        pdfDoc.Add(table);

                        Paragraph para = new Paragraph();
                        phrase = new Phrase("*Notes : " + workOrder.Comments, FontFactory.GetFont("Arial", 10, Font.ITALIC));
                        para.Add(phrase);
                        pdfDoc.Add(para);
                    }

                    // Closing the document
                    pdfDoc.Close();

                    byte[] byteInfo = memoryStream.ToArray();
                    memoryStream.Write(byteInfo, 0, byteInfo.Length);
                    memoryStream.Position = 0;

                    FileStream file = new FileStream(strAttachment, FileMode.Create, System.IO.FileAccess.Write);
                    memoryStream.WriteTo(file);
                    file.Close();
                    memoryStream.Close();

                    return strAttachment;
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return filename;
            }
        }

        public FileResult DownloadPartsFormat()
        {
            string path = "/Docs/Parts_Format.xlsx";
            return File(path, "application/vnd.ms-excel", "Parts_Format.xlsx");
        }

        [HttpPost]
        public ActionResult Import(HttpPostedFileBase file)
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    string filePath = string.Empty;
                    if (file == null || file.ContentLength == 0)
                    {
                        return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        string path = Server.MapPath("~/Uploads/");
                        if (Directory.Exists(path))
                            Directory.CreateDirectory(path);

                        filePath = path + Path.GetFileName(file.FileName);
                        string extension = Path.GetExtension(file.FileName).ToLower();
                        file.SaveAs(filePath);

                        string conString = string.Empty;
                        switch (extension)
                        {
                            case ".xls": //Excel 97-03.
                                conString = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                                break;

                            case ".xlsx": //Excel 07 and above.
                                conString = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                                break;
                        }

                        DataTable dt = new DataTable();
                        conString = string.Format(conString, filePath);

                        using (OleDbConnection conExcel = new OleDbConnection(conString))
                        {
                            using (OleDbCommand cmdExcel = new OleDbCommand())
                            {
                                using (OleDbDataAdapter odaExcel = new OleDbDataAdapter())
                                {
                                    cmdExcel.Connection = conExcel;

                                    //Get the name of First Sheet.
                                    conExcel.Open();
                                    DataTable dtExcelSchema;
                                    dtExcelSchema = conExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                                    string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                                    conExcel.Close();

                                    //Read Data from First Sheet
                                    conExcel.Open();
                                    cmdExcel.CommandText = "SELECT * FROM [" + sheetName + "]";
                                    odaExcel.SelectCommand = cmdExcel;
                                    odaExcel.Fill(dt);
                                    conExcel.Close();
                                }
                            }
                        }

                        List<TempImportPart> parts = new List<TempImportPart>();
                        foreach (DataRow dtRow in dt.Rows)
                        {
                            TempImportPart part = new TempImportPart();
                            part.PartNumber = dtRow["PART NUMBER"].ToString();
                            part.Description = dtRow["DESCRIPTION"].ToString();
                            part.Quantity = Convert.ToInt32(dtRow["QUANTITY"]);
                            part.Remark = dtRow["REMARKS"].ToString();
                            parts.Add(part);
                        }

                        return Json(new { success = true, data = parts }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();
            }

            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }
    }
}
