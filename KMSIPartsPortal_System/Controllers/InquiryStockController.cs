﻿using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using KMSIPartsPortal_System.Helpers;
using KMSIPartsPortal_System.Models;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Linq.Expressions;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace KMSIPartsPortal_System.Controllers
{
    public class InquiryStockController : Controller
    {
        private kmsi_portalEntities db = new kmsi_portalEntities();

        // GET: InquiryStock
        public ActionResult Index()
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    List<MenuModels> MenuMaster = (List<MenuModels>)Session["MenuMaster"];
                    Int32 RoleID = Convert.ToInt32(Session["RoleId"]);

                    List<Module> modules = db.Modules.Where(x => x.ModuleName == "Helpdesk" || x.ModuleName == "Information").ToList();
                    ViewBag.ModuleInternalLeft = modules;

                    if (RoleID == 0)
                    {
                        ViewBag.IsInput = 1;
                        ViewBag.IsUpdate = 1;
                        ViewBag.IsDelete = 1;
                        ViewBag.IsUpload = 1;
                        ViewBag.IsDownload = 1;
                    }
                    else
                    {
                        var access = MenuMaster.Where(x => x.ControllerName == "InquiryStock" && x.ActionName == "Index" && x.RoleId == RoleID).FirstOrDefault();

                        if (access.IsView == 1)
                        {
                            ViewBag.IsInput = access.IsInput;
                            ViewBag.IsUpdate = access.IsUpdate;
                            ViewBag.IsDelete = access.IsDelete;
                            ViewBag.IsUpload = access.IsUpload;
                            ViewBag.IsDownload = access.IsDownload;
                        }
                        else
                        {
                            ViewBag.Message = "You don't have access to this page! Please contact administrator.";
                        }
                    }

                    string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/');
                    ViewBag.baseUrl = baseUrl;
                    ViewBag.ModuleName = "Information";
                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult List()
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    db.Configuration.LazyLoadingEnabled = false;

                    // Server Side Parameter
                    int start = Convert.ToInt32(Request["start"]);
                    int length = Convert.ToInt32(Request["length"]);
                    string searchValue = Request["search[value]"];
                    string sortColumn = Request["columns[" + Request["order[0][column]"] + "][name]"];
                    string sortColumnDir = Request["order[0][dir]"];
                    string searchPartNumber = Request["searchByPNumber"];

                    var queryResult = db.vStockInfoes.Select(x => new
                    {
                        ID = x.ID,
                        PART_NUMBER = x.PART_NUMBER,
                        PART_NAME = x.PART_NAME,
                        ITC_PART_NUMBER = x.ITC_PART_NUMBER,
                        ITC_CODE = x.ITC_CODE,
                        FOH_TOTAL = x.FOH_TOTAL,
                        FOO_TOTAL = x.FOO_TOTAL,
                        FS_TOTAL = x.FS_TOTAL,
                        FA_TOTAL = x.FA_TOTAL,
                        EORSVDP_TOTAL = x.EORSVDP_TOTAL,
                        FOH_JKT = x.FOH_JKT,
                        FOO_JKT = x.FOO_JKT,
                        FS_JKT = x.FS_JKT,
                        FA_JKT = x.FA_JKT,
                        EORSVDP_JKT = x.EORSVDP_JKT,
                        FOH_BPN = x.FOH_BPN,
                        FOO_BPN = x.FOO_BPN,
                        FS_BPN = x.FS_BPN,
                        FA_BPN = x.FA_BPN,
                        EORSVDP_BPN = x.EORSVDP_BPN,
                        FOH_PLB = x.FOH_PLB,
                        FOO_PLB = x.FOO_PLB,
                        FS_PLB = x.FS_PLB,
                        FA_PLB = x.FA_PLB,
                        EORSVDP_PLB = x.EORSVDP_PLB,
                        FOH_BJM = x.FOH_BJM,
                        FOO_BJM = x.FOO_BJM,
                        FS_BJM = x.FS_BJM,
                        FA_BJM = x.FA_BJM,
                        EORSVDP_BJM = x.EORSVDP_BJM
                    }).AsEnumerable();
                    int recordsTotal = 0;

                    // total number of rows count
                    SqlCommand command = new SqlCommand("SELECT dbo.fGetStockInfoRecordCount() AS CountTotal");
                    command.CommandType = CommandType.Text;
                    using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["Constring"].ConnectionString))
                    {
                        connection.Open();
                        command.Connection = connection;
                        recordsTotal = int.Parse(command.ExecuteScalar().ToString());
                    }

                    // Search
                    if (!string.IsNullOrEmpty(searchValue))
                    {
                        queryResult = queryResult.Where(s => s.PART_NUMBER.ToLower().Contains(searchValue.ToLower())
                            || s.PART_NAME.ToLower().Contains(searchValue.ToLower()));
                    }

                    int recordsFiltered = 0;
                    // Filtering
                    if (!string.IsNullOrEmpty(searchPartNumber))
                    {
                        List<string> ListPN = searchPartNumber.Split(',').Select(x => x.ToLower().Trim()).ToList();
                        queryResult = queryResult.Where(s => ListPN.Contains(s.PART_NUMBER.ToLower().Trim()));
                        recordsFiltered = queryResult.Count();
                    }
                    else
                    {
                        recordsFiltered = recordsTotal;
                    }

                    // Sorting
                    if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                    {
                        queryResult = queryResult.OrderBy(sortColumn + " " + sortColumnDir);
                    }

                    // Paging
                    queryResult = queryResult.Skip(start).Take(length).AsEnumerable();

                    return Json(new { draw = Request["draw"], recordsFiltered = recordsFiltered, recordsTotal = recordsTotal, data = queryResult },
                        JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (DbEntityValidationException ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);

                foreach (var eve in ex.EntityValidationErrors)
                {
                    sw.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sw.WriteLine("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                }
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public FileResult DownloadExcel()
        {
            string filename = "PartsNumber_Format";
            string path = "/Docs/" + filename + ".xlsx";
            return File(path, "application/vnd.ms-excel", filename + ".xlsx");
        }

        [HttpPost]
        public String UploadPartNumber()
        {
            string partNumberValues = String.Empty;
            try
            {
                if (Request.Files.Count > 0)
                {
                    HttpPostedFileBase file = Request.Files[0];
                    if (file.ContentType == "application/vnd.ms-excel" || file.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                    {
                        var filename = file.FileName;
                        var targetPath = Server.MapPath("~/Uploads/");
                        file.SaveAs(targetPath + filename);
                        var pathToExcelFile = targetPath + filename;
                        string FileName = Path.GetFileName(file.FileName);
                        string Extension = Path.GetExtension(file.FileName);
                        DataTable dataTable = FileImport.ImportToGrid(pathToExcelFile, Extension, "Yes");
                        List<PartNumberList> partNumberLists = new List<PartNumberList>();
                        partNumberLists = (from DataRow row in dataTable.Rows
                                           select new PartNumberList()
                                           {
                                               PartNumber = row[0].ToString()
                                           }).ToList();
                        partNumberValues = String.Join(", ", partNumberLists.Select(x => x.PartNumber));

                        TempData["status"] = true;
                        TempData["message"] = "upload success";
                        TempData["data"] = partNumberValues;

                        if (System.IO.File.Exists(pathToExcelFile))
                            System.IO.File.Delete(pathToExcelFile);
                    }
                    else
                    {
                        TempData["status"] = false;
                        TempData["message"] = "upload failed";
                    }
                }
                else
                {
                    TempData["status"] = false;
                    TempData["message"] = "upload failed";
                }

                return JsonConvert.SerializeObject(TempData);
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                TempData["status"] = false;
                TempData["message"] = "upload failed";
                return JsonConvert.SerializeObject(TempData);
            }
        }

        public ActionResult GenerateXls(string[] columns, string[] columnQty, string[] filter, string partnumber)
        {
            try
            {
                DateTime dateTime = DateTime.Now;
                string FileName = string.Format("StockInformation_" + dateTime.ToString("yyyyMMdd_HHmm") + ".xlsx");
                string handle = Guid.NewGuid().ToString();

                using (MemoryStream stream = new MemoryStream())
                {
                    using (ExcelPackage excel = new ExcelPackage())
                    {
                        int start = 0;
                        int maxRows = 30;
                        var queryResult = db.vStockInfoes.Select(x => new
                        {
                            ID = x.ID,
                            PART_NUMBER = x.PART_NUMBER,
                            PART_NAME = x.PART_NAME,
                            ITC_PART_NUMBER = x.ITC_PART_NUMBER,
                            ITC_CODE = x.ITC_CODE,
                            FOH_TOTAL = x.FOH_TOTAL,
                            FOO_TOTAL = x.FOO_TOTAL,
                            FS_TOTAL = x.FS_TOTAL,
                            FA_TOTAL = x.FA_TOTAL,
                            EORSVDP_TOTAL = x.EORSVDP_TOTAL,
                            FOH_JKT = x.FOH_JKT,
                            FOO_JKT = x.FOO_JKT,
                            FS_JKT = x.FS_JKT,
                            FA_JKT = x.FA_JKT,
                            EORSVDP_JKT = x.EORSVDP_JKT,
                            FOH_BPN = x.FOH_BPN,
                            FOO_BPN = x.FOO_BPN,
                            FS_BPN = x.FS_BPN,
                            FA_BPN = x.FA_BPN,
                            EORSVDP_BPN = x.EORSVDP_BPN,
                            FOH_PLB = x.FOH_PLB,
                            FOO_PLB = x.FOO_PLB,
                            FS_PLB = x.FS_PLB,
                            FA_PLB = x.FA_PLB,
                            EORSVDP_PLB = x.EORSVDP_PLB,
                            FOH_BJM = x.FOH_BJM,
                            FOO_BJM = x.FOO_BJM,
                            FS_BJM = x.FS_BJM,
                            FA_BJM = x.FA_BJM,
                            EORSVDP_BJM = x.EORSVDP_BJM
                        }).AsEnumerable();

                        if (!string.IsNullOrEmpty(partnumber))
                        {
                            List<string> ListPN = partnumber.Split(',').Select(x => x.ToLower().Trim()).ToList();
                            queryResult = queryResult.Where(s => ListPN.Contains(s.PART_NUMBER.ToLower().Trim()));
                        }

                        string sheetName = "Stock_Information";
                        excel.Workbook.Worksheets.Add(sheetName);

                        int headerCount = columns.Length + columnQty.Length;
                        if (filter != null)
                            headerCount = columns.Length + (filter.Length * columnQty.Length);

                        var worksheet = excel.Workbook.Worksheets[sheetName];
                        worksheet.Cells[1, 1, 1, headerCount].Merge = true;
                        worksheet.Cells["A1"].Value = "STOCK INFORMATION";
                        worksheet.Cells["A1"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        worksheet.Cells["A1"].Style.Font.Bold = true;
                        worksheet.Cells["A1"].Style.Font.Size = 14;

                        int colNumber = 1;
                        foreach (var col in columns)
                        {
                            worksheet.Cells[3, colNumber, 4, colNumber].Merge = true;
                            worksheet.Cells[3, colNumber, 4, colNumber].Value = (col.Equals("PART_NUMBER")) ? "PN" : (col.Equals("PART_NAME")) ? "Name" : (col.Equals("LATEST")) ? "Latest PN" : col;
                            worksheet.Cells[3, colNumber, 4, colNumber].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                            worksheet.Cells[3, colNumber, 4, colNumber].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                            worksheet.Cells[3, colNumber, 4, colNumber].Style.Font.Bold = true;
                            worksheet.Cells[3, colNumber, 4, colNumber].Style.Font.Size = 10;
                            worksheet.Cells[3, colNumber, 4, colNumber].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                            colNumber++;
                        }

                        if (filter != null)
                        {
                            foreach (var point in filter)
                            {
                                int startCol = colNumber;
                                int endCol = colNumber;
                                foreach (var qty in columnQty)
                                {
                                    worksheet.Cells[4, colNumber].Value = (qty.Equals("EORSVDP")) ? "EO RSVRD" : qty;
                                    worksheet.Cells[4, colNumber].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                    worksheet.Cells[4, colNumber].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                                    worksheet.Cells[4, colNumber].Style.Font.Bold = true;
                                    worksheet.Cells[4, colNumber].Style.Font.Size = 10;
                                    worksheet.Cells[4, colNumber].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                    colNumber++;

                                    endCol = colNumber - 1;
                                }
                                worksheet.Cells[3, startCol, 3, endCol].Merge = true;
                                worksheet.Cells[3, startCol, 3, endCol].Value = point;
                                worksheet.Cells[3, startCol, 3, endCol].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                worksheet.Cells[3, startCol, 3, endCol].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                                worksheet.Cells[3, startCol, 3, endCol].Style.Font.Bold = true;
                                worksheet.Cells[3, startCol, 3, endCol].Style.Font.Size = 10;
                                worksheet.Cells[3, startCol, 3, endCol].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                            }
                        }
                        else
                        {
                            int startCol = colNumber;
                            int endCol = colNumber;
                            foreach (var qty in columnQty)
                            {
                                worksheet.Cells[3, colNumber, 4, colNumber].Merge = true;
                                worksheet.Cells[3, colNumber, 4, colNumber].Value = qty;
                                worksheet.Cells[3, colNumber, 4, colNumber].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                worksheet.Cells[3, colNumber, 4, colNumber].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                                worksheet.Cells[3, colNumber, 4, colNumber].Style.Font.Bold = true;
                                worksheet.Cells[3, colNumber, 4, colNumber].Style.Font.Size = 10;
                                worksheet.Cells[3, colNumber, 4, colNumber].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                                colNumber++;
                            }
                        }

                        queryResult = queryResult.Skip(start).Take(maxRows).AsEnumerable();

                        int rown = 5;
                        int coln = 1;
                        foreach (var data in queryResult)
                        {
                            foreach (var c in columns)
                            {
                                worksheet.Cells[rown, coln].Value = (c.Equals("PART_NUMBER")) ? data.PART_NUMBER : (c.Equals("PART_NAME")) ? data.PART_NAME : (c.Equals("LATEST")) ? data.ITC_PART_NUMBER : (c.Equals("ITC")) ? data.ITC_CODE.HasValue ? data.ITC_CODE.Value.ToString() : String.Empty : String.Empty;
                                worksheet.Cells[rown, coln].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                worksheet.Cells[rown, coln].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                                worksheet.Cells[rown, coln].Style.Font.Size = 8;
                                worksheet.Cells[rown, coln].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                                coln++;
                            }

                            if (filter != null)
                            {
                                foreach (var p in filter)
                                {
                                    string value = String.Empty;
                                    foreach (var q in columnQty)
                                    {
                                        if (p.Equals("JKT"))
                                        {
                                            if (q.Equals("FOH"))
                                            {
                                                value = data.FOH_JKT.HasValue ? data.FOH_JKT.Value.ToString() : String.Empty;
                                            }
                                            else if (q.Equals("FOO"))
                                            {
                                                value = data.FOO_JKT.HasValue ? data.FOO_JKT.Value.ToString() : String.Empty;
                                            }
                                            else if (q.Equals("FS"))
                                            {
                                                value = data.FS_JKT.HasValue ? data.FS_JKT.Value.ToString() : String.Empty;
                                            }
                                            else if (q.Equals("FA"))
                                            {
                                                value = data.FA_JKT.HasValue ? data.FA_JKT.Value.ToString() : String.Empty;
                                            }
                                            else if (q.Equals("EORSVDP"))
                                            {
                                                value = data.EORSVDP_JKT.HasValue ? data.EORSVDP_JKT.Value.ToString() : String.Empty;
                                            }
                                        }
                                        else if (p.Equals("BPN"))
                                        {
                                            if (q.Equals("FOH"))
                                            {
                                                value = data.FOH_BPN.HasValue ? data.FOH_BPN.Value.ToString() : String.Empty;
                                            }
                                            else if (q.Equals("FOO"))
                                            {
                                                value = data.FOO_BPN.HasValue ? data.FOO_BPN.Value.ToString() : String.Empty;
                                            }
                                            else if (q.Equals("FS"))
                                            {
                                                value = data.FS_BPN.HasValue ? data.FS_BPN.Value.ToString() : String.Empty;
                                            }
                                            else if (q.Equals("FA"))
                                            {
                                                value = data.FA_BPN.HasValue ? data.FA_BPN.Value.ToString() : String.Empty;
                                            }
                                            else if (q.Equals("EORSVDP"))
                                            {
                                                value = data.EORSVDP_BPN.HasValue ? data.EORSVDP_BPN.Value.ToString() : String.Empty;
                                            }
                                        }
                                        else if (p.Equals("BJM"))
                                        {
                                            if (q.Equals("FOH"))
                                            {
                                                value = data.FOH_BJM.HasValue ? data.FOH_BJM.Value.ToString() : String.Empty;
                                            }
                                            else if (q.Equals("FOO"))
                                            {
                                                value = data.FOO_BJM.HasValue ? data.FOO_BJM.Value.ToString() : String.Empty;
                                            }
                                            else if (q.Equals("FS"))
                                            {
                                                value = data.FS_BJM.HasValue ? data.FS_BJM.Value.ToString() : String.Empty;
                                            }
                                            else if (q.Equals("FA"))
                                            {
                                                value = data.FA_BJM.HasValue ? data.FA_BJM.Value.ToString() : String.Empty;
                                            }
                                            else if (q.Equals("EORSVDP"))
                                            {
                                                value = data.EORSVDP_BJM.HasValue ? data.EORSVDP_BJM.Value.ToString() : String.Empty;
                                            }
                                        }
                                        else if (p.Equals("PLB"))
                                        {
                                            if (q.Equals("FOH"))
                                            {
                                                value = data.FOH_PLB.HasValue ? data.FOH_PLB.Value.ToString() : String.Empty;
                                            }
                                            else if (q.Equals("FOO"))
                                            {
                                                value = data.FOO_PLB.HasValue ? data.FOO_PLB.Value.ToString() : String.Empty;
                                            }
                                            else if (q.Equals("FS"))
                                            {
                                                value = data.FS_PLB.HasValue ? data.FS_PLB.Value.ToString() : String.Empty;
                                            }
                                            else if (q.Equals("FA"))
                                            {
                                                value = data.FA_PLB.HasValue ? data.FA_PLB.Value.ToString() : String.Empty;
                                            }
                                            else if (q.Equals("EORSVDP"))
                                            {
                                                value = data.EORSVDP_PLB.HasValue ? data.EORSVDP_PLB.Value.ToString() : String.Empty;
                                            }
                                        }
                                        worksheet.Cells[rown, coln].Value = value;
                                        worksheet.Cells[rown, coln].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                        worksheet.Cells[rown, coln].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                                        worksheet.Cells[rown, coln].Style.Font.Size = 8;
                                        worksheet.Cells[rown, coln].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                                        coln++;
                                    }
                                }
                            }
                            else
                            {
                                string value = String.Empty;
                                foreach (var q in columnQty)
                                {
                                    if (q.Equals("FOH"))
                                    {
                                        value = data.FOH_TOTAL.HasValue ? data.FOH_TOTAL.Value.ToString() : String.Empty;
                                    }
                                    else if (q.Equals("FOO"))
                                    {
                                        value = data.FOO_TOTAL.HasValue ? data.FOO_TOTAL.Value.ToString() : String.Empty;
                                    }
                                    else if (q.Equals("FS"))
                                    {
                                        value = data.FS_TOTAL.HasValue ? data.FS_TOTAL.Value.ToString() : String.Empty;
                                    }
                                    else if (q.Equals("FA"))
                                    {
                                        value = data.FA_TOTAL.HasValue ? data.FA_TOTAL.Value.ToString() : String.Empty;
                                    }
                                    else if (q.Equals("EORSVDP"))
                                    {
                                        value = data.EORSVDP_TOTAL.HasValue ? data.EORSVDP_TOTAL.Value.ToString() : String.Empty;
                                    }
                                    worksheet.Cells[rown, coln].Value = value;
                                    worksheet.Cells[rown, coln].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                    worksheet.Cells[rown, coln].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                                    worksheet.Cells[rown, coln].Style.Font.Size = 8;
                                    worksheet.Cells[rown, coln].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                                    coln++;
                                }
                            }

                            coln = 1;
                            rown++;
                        }
                        worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();
                        for (int i = columns.Length + 1; i <= worksheet.Dimension.End.Column; i++)
                        {
                            worksheet.Column(i).Width = 7.71;
                            worksheet.Column(i).Style.WrapText = true;
                        }

                        excel.SaveAs(stream);
                    }
                    stream.Position = 0;
                    TempData[handle] = stream.ToArray();
                }

                return new JsonResult()
                {
                    Data = new { FileGuid = handle, FileName = FileName }
                };
            }
            catch (DbEntityValidationException ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);

                foreach (var eve in ex.EntityValidationErrors)
                {
                    sw.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sw.WriteLine("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                }
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        /* public ActionResult GeneratePdf(string[] columns, string[] columnQty, string[] filter, string partnumber)
        {
            try
            {
                DateTime dateTime = DateTime.Now;
                string FileName = string.Format("StockInformation_" + dateTime.ToString("yyyyMMdd_HHmm") + ".pdf");
                string handle = Guid.NewGuid().ToString();

                using (MemoryStream stream = new MemoryStream())
                {
                    int start = 0;
                    int maxRows = 30;
                    var queryResult = db.vStockInfoes.AsEnumerable();

                    if (!string.IsNullOrEmpty(partnumber))
                    {
                        List<string> ListPN = partnumber.Split(',').Select(x => x.ToLower().Trim()).ToList();
                        queryResult = queryResult.Where(s => ListPN.Contains(s.PART_NUMBER.ToLower().Trim()));
                    }

                    queryResult = queryResult.Skip(start).Take(maxRows).AsEnumerable();

                    string[] nullFilter = new string[1];
                    if (filter == null)
                    {
                        nullFilter[0] = String.Empty;
                    }
                    string path = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/" + "Content/img/logo-komatsu.png";
                    var uri = new System.Uri(path);
                    var convertedUri = uri.AbsoluteUri;
                    string pHTML = ViewRenderer.RenderRazorViewToString(this, "~/Views/PDFs/StockInfo.cshtml", new PdfStockInfo()
                    {
                        Columns = columns,
                        ColumnsQty = columnQty,
                        Filter = (filter != null) ? filter : nullFilter,
                        Data = queryResult,
                        imgPath = convertedUri
                    });
                    StringReader reader = new StringReader(pHTML);

                    // 1: create object of a itextsharp document class  
                    Document doc = new Document(PageSize.A4.Rotate());

                    // 2: we create a itextsharp pdfwriter that listens to the document and directs a XML-stream to a file  
                    PdfWriter oPdfWriter = PdfWriter.GetInstance(doc, stream);

                    // 3: we create a worker parse the document  
                    HTMLWorker htmlWorker = new HTMLWorker(doc);

                    // 4: we open document and start the worker on the document  
                    doc.Open();
                    doc.NewPage();
                    htmlWorker.StartDocument();

                    // 5: parse the html into the document  
                    XMLWorkerHelper.GetInstance().ParseXHtml(oPdfWriter, doc, reader);
                    //htmlWorker.Parse(reader);

                    // 6: close the document and the worker  
                    //htmlWorker.EndDocument();
                    //htmlWorker.Close();
                    doc.Close();

                    var bytes = stream.ToArray();
                    Session[FileName] = bytes;
                }

                return Json(new { success = true, FileName }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        } */

        public ActionResult GeneratePdf(string[] columns, string[] columnQty, string[] filter, string partnumber)
        {
            try
            {
                DateTime dateTime = DateTime.Now;
                string FileName = string.Format("StockInformation_" + dateTime.ToString("yyyyMMdd_HHmm") + ".pdf");
                string handle = Guid.NewGuid().ToString();

                using (MemoryStream stream = new MemoryStream())
                {
                    int start = 0;
                    int maxRows = 30;
                    var queryResult = db.vStockInfoes.AsEnumerable();

                    if (!string.IsNullOrEmpty(partnumber))
                    {
                        List<string> ListPN = partnumber.Split(',').Select(x => x.ToLower().Trim()).ToList();
                        queryResult = queryResult.Where(s => ListPN.Contains(s.PART_NUMBER.ToLower().Trim()));
                    }

                    queryResult = queryResult.Skip(start).Take(maxRows).AsEnumerable();

                    Document pdfDoc = new Document(PageSize.A3.Rotate());
                    PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);
                    writer.CloseStream = false;
                    pdfDoc.SetMargins(20f, 20f, 20f, 20f);

                    pdfDoc.Open();
                    Phrase phrase = new Phrase();

                    Font fontCell = new Font(FontFactory.GetFont("Trebuchet MS", 8, Font.NORMAL));
                    Font fontHeader = new Font(FontFactory.GetFont("Trebuchet MS", 10, Font.BOLD));

                    PdfPTable table = new PdfPTable(1);
                    table.WidthPercentage = 100;
                    table.SpacingAfter = 10f;

                    // invoke LEFT image
                    Image jpg = Image.GetInstance(Server.MapPath("~/Content/img/logo-komatsu.png"));
                    jpg.ScalePercent(50);
                    PdfPCell imageCell = new PdfPCell(jpg);
                    imageCell.Colspan = 1; // either 1 if you need to insert one cell
                    imageCell.Border = 0;
                    imageCell.HorizontalAlignment = Element.ALIGN_LEFT;

                    // add a LEFT image to PdfPTables
                    table.AddCell(imageCell);

                    table.AddCell(new PdfPCell(new Phrase("STOCK INFORMATION", FontFactory.GetFont("Trebuchet MS", 20, Font.BOLD, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });
                    pdfDoc.Add(table);

                    int headerCount = columns.Length + columnQty.Length;
                    if (filter != null)
                        headerCount = columns.Length + (filter.Length * columnQty.Length);

                    table = new PdfPTable(headerCount);
                    table.WidthPercentage = 100;
                    table.SpacingBefore = 5f;
                    //table.HeaderRows = filter != null ? 2 : 1;

                    foreach (var c in columns)
                    {
                        String ColHeader = c.Equals("PART_NUMBER") ? "PN" : c.Equals("PART_NAME") ? "Name" : c.Equals("LATEST") ? "Latest PN" : c.Equals("ITC") ? "ITC" : c;
                        addCellWithRowSpan(table, ColHeader, filter != null ? 2 : 1);
                    }

                    if (filter != null)
                    {
                        for (int i = 0; i < filter.Length; i++)
                        {
                            addCellWithColSpan(table, filter[i], columnQty.Length);
                        }

                        for (int i = 0; i < filter.Length; i++)
                        {
                            foreach (var q in columnQty)
                            {
                                String QtyHeader = q.Equals("EORSVDP") ? "EO RSVRD" : q;
                                table.AddCell(new PdfPCell(new Phrase(QtyHeader, fontHeader))
                                {
                                    PaddingTop = 3,
                                    PaddingBottom = 8,
                                    HorizontalAlignment = Element.ALIGN_CENTER,
                                    VerticalAlignment = Element.ALIGN_MIDDLE
                                });
                            }
                        }
                    }
                    else
                    {
                        foreach (var q in columnQty)
                        {
                            String QtyHeader = String.Empty;
                            QtyHeader = q.Equals("EORSVDP") ? "EO RSVRD" : q;
                            table.AddCell(new PdfPCell(new Phrase(QtyHeader, fontHeader))
                            {
                                PaddingTop = 3,
                                PaddingBottom = 8,
                                HorizontalAlignment = Element.ALIGN_CENTER,
                                VerticalAlignment = Element.ALIGN_MIDDLE
                            }).Rowspan = filter != null ? 2 : 1;
                        }
                    }

                    foreach (var data in queryResult)
                    {
                        foreach (var c in columns)
                        {
                            String Value = c.Equals("PART_NUMBER") ? data.PART_NUMBER != null ? data.PART_NUMBER.Trim() : String.Empty : c.Equals("PART_NAME") ? data.PART_NAME != null ? data.PART_NAME.Trim() : String.Empty : c.Equals("LATEST") ? data.ITC_PART_NUMBER != null ? data.ITC_PART_NUMBER.Trim() : String.Empty : c.Equals("ITC") ? data.ITC_CODE.HasValue ? data.ITC_CODE.ToString().Trim() : String.Empty : String.Empty;
                            table.AddCell(new PdfPCell(new Phrase(Value, fontCell))
                            {
                                PaddingTop = 3,
                                PaddingBottom = 8,
                                HorizontalAlignment = Element.ALIGN_CENTER,
                                VerticalAlignment = Element.ALIGN_MIDDLE
                            });
                        }

                        if (filter != null)
                        {
                            foreach (var p in filter)
                            {
                                String Value = String.Empty;
                                foreach (var q in columnQty)
                                {
                                    if (p.Equals("JKT"))
                                    {
                                        if (q.Equals("FOH"))
                                        {
                                            Value = data.FOH_JKT.HasValue ? data.FOH_JKT.ToString() : String.Empty;
                                        }
                                        else if (q.Equals("FOO"))
                                        {
                                            Value = data.FOO_JKT.HasValue ? data.FOO_JKT.ToString() : String.Empty;
                                        }
                                        else if (q.Equals("FS"))
                                        {
                                            Value = data.FS_JKT.HasValue ? data.FS_JKT.ToString() : String.Empty;
                                        }
                                        else if (q.Equals("FA"))
                                        {
                                            Value = data.FA_JKT.HasValue ? data.FA_JKT.ToString() : String.Empty;
                                        }
                                        else if (q.Equals("EORSVDP"))
                                        {
                                            Value = data.EORSVDP_JKT.HasValue ? data.EORSVDP_JKT.ToString() : String.Empty;
                                        }
                                    }
                                    else if (p.Equals("BPN"))
                                    {
                                        if (q.Equals("FOH"))
                                        {
                                            Value = data.FOH_BJM.HasValue ? data.FOH_BPN.ToString() : String.Empty;
                                        }
                                        else if (q.Equals("FOO"))
                                        {
                                            Value = data.FOO_BPN.HasValue ? data.FOO_BPN.ToString() : String.Empty;
                                        }
                                        else if (q.Equals("FS"))
                                        {
                                            Value = data.FS_BPN.HasValue ? data.FS_BPN.ToString() : String.Empty;
                                        }
                                        else if (q.Equals("FA"))
                                        {
                                            Value = data.FA_BPN.HasValue ? data.FA_BPN.ToString() : String.Empty;
                                        }
                                        else if (q.Equals("EORSVDP"))
                                        {
                                            Value = data.EORSVDP_BPN.HasValue ? data.EORSVDP_BPN.ToString() : String.Empty;
                                        }
                                    }
                                    else if (p.Equals("BJM"))
                                    {
                                        if (q.Equals("FOH"))
                                        {
                                            Value = data.FOH_BJM.HasValue ? data.FOH_BJM.ToString() : String.Empty;
                                        }
                                        else if (q.Equals("FOO"))
                                        {
                                            Value = data.FOO_BJM.HasValue ? data.FOO_BJM.ToString() : String.Empty;
                                        }
                                        else if (q.Equals("FS"))
                                        {
                                            Value = data.FS_BJM.HasValue ? data.FS_BJM.ToString() : String.Empty;
                                        }
                                        else if (q.Equals("FA"))
                                        {
                                            Value = data.FA_BJM.HasValue ? data.FA_BJM.ToString() : String.Empty;
                                        }
                                        else if (q.Equals("EORSVDP"))
                                        {
                                            Value = data.EORSVDP_BJM.HasValue ? data.EORSVDP_BJM.ToString() : String.Empty;
                                        }
                                    }
                                    else if (p.Equals("PLB"))
                                    {
                                        if (q.Equals("FOH"))
                                        {
                                            Value = data.FOH_PLB.HasValue ? data.FOH_PLB.ToString() : String.Empty;
                                        }
                                        else if (q.Equals("FOO"))
                                        {
                                            Value = data.FOO_PLB.HasValue ? data.FOO_PLB.ToString() : String.Empty;
                                        }
                                        else if (q.Equals("FS"))
                                        {
                                            Value = data.FS_PLB.HasValue ? data.FS_PLB.ToString() : String.Empty;
                                        }
                                        else if (q.Equals("FA"))
                                        {
                                            Value = data.FA_PLB.HasValue ? data.FA_PLB.ToString() : String.Empty;
                                        }
                                        else if (q.Equals("EORSVDP"))
                                        {
                                            Value = data.EORSVDP_PLB.HasValue ? data.EORSVDP_PLB.ToString() : String.Empty;
                                        }
                                    }

                                    table.AddCell(new PdfPCell(new Phrase(Value, fontCell))
                                    {
                                        PaddingTop = 3,
                                        PaddingBottom = 8,
                                        HorizontalAlignment = Element.ALIGN_CENTER,
                                        VerticalAlignment = Element.ALIGN_MIDDLE
                                    });
                                }
                            }
                        }
                        else
                        {
                            String Value = String.Empty;
                            foreach (var q in columnQty)
                            {
                                if (q.Equals("FOH"))
                                {
                                    Value = data.FOH_TOTAL.HasValue ? data.FOH_TOTAL.ToString() : String.Empty;
                                }
                                else if (q.Equals("FOO"))
                                {
                                    Value = data.FOO_TOTAL.HasValue ? data.FOO_TOTAL.ToString() : String.Empty;
                                }
                                else if (q.Equals("FS"))
                                {
                                    Value = data.FS_TOTAL.HasValue ? data.FS_TOTAL.ToString() : String.Empty;
                                }
                                else if (q.Equals("FA"))
                                {
                                    Value = data.FA_TOTAL.HasValue ? data.FA_TOTAL.ToString() : String.Empty;
                                }
                                else if (q.Equals("EORSVDP"))
                                {
                                    Value = data.EORSVDP_TOTAL.HasValue ? data.EORSVDP_TOTAL.ToString() : String.Empty;
                                }

                                table.AddCell(new PdfPCell(new Phrase(Value, fontCell))
                                {
                                    PaddingTop = 3,
                                    PaddingBottom = 8,
                                    HorizontalAlignment = Element.ALIGN_CENTER,
                                    VerticalAlignment = Element.ALIGN_MIDDLE
                                });
                            }
                        }
                    }
                    pdfDoc.Add(table);

                    pdfDoc.Close();

                    var bytes = stream.ToArray();
                    Session[FileName] = bytes;
                }

                return Json(new { success = true, FileName }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [HttpGet]
        public virtual ActionResult DownloadXls(string fileGuid, string fileName)
        {
            if (TempData[fileGuid] != null)
            {
                byte[] data = TempData[fileGuid] as byte[];
                return File(data, "application/vnd.ms-excel", fileName);
            }
            else
            {
                return new EmptyResult();
            }
        }

        [HttpGet]
        public virtual ActionResult DownloadPdf(string fileName)
        {
            try
            {
                var ms = Session[fileName] as byte[];
                if (ms == null)
                    return new EmptyResult();
                Session[fileName] = null;
                return File(ms, "application/pdf", fileName);
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return HttpNotFound();
            }
        }

        private static void addCellWithRowSpan(PdfPTable table, string text, int rowspan)
        {
            Font fontHeader = new Font(FontFactory.GetFont("Trebuchet MS", 10, Font.BOLD));
            PdfPCell cell = new PdfPCell(new Phrase(text, fontHeader));
            cell.Rowspan = rowspan;

            cell.PaddingTop = 3;
            cell.PaddingBottom = 8;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;

            table.AddCell(cell);
        }

        private static void addCellWithColSpan(PdfPTable table, string text, int colspan)
        {
            Font fontHeader = new Font(FontFactory.GetFont("Trebuchet MS", 10, Font.BOLD));
            PdfPCell cell = new PdfPCell(new Phrase(text, fontHeader));
            cell.Colspan = colspan;

            cell.PaddingTop = 3;
            cell.PaddingBottom = 8;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;

            table.AddCell(cell);
        }
    }
}