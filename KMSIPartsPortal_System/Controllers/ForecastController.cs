﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Web;
using System.Web.Mvc;
using iTextSharp.text;
using iTextSharp.text.pdf;
using KMSIPartsPortal_System.Helpers;
using KMSIPartsPortal_System.Models;
using OfficeOpenXml;

namespace KMSIPartsPortal_System.Controllers
{
    public class ForecastController : Controller
    {
        private kmsi_portalEntities db = new kmsi_portalEntities();

        public ActionResult Details(string fNumber)
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    ViewBag.fNumber = fNumber;
                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult DetailList()
        {
            try
            {
                // Creating instance of DatabaseContext class
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                var number = Request.Form.GetValues("param").FirstOrDefault();

                // Paging Size
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                // Getting all data
                var forecastData = db.ForecastLists.AsEnumerable()
                                .Where(x => x.ForecastNumber == number)
                                .Join(db.Users, f => f.CreatedBy, u => u.UserId, (f, u) => new
                                {
                                    ID = f.ID,
                                    ForecastNumber = f.ForecastNumber,
                                    ForecastDate = f.ForecastDate.HasValue ? f.ForecastDate.Value.ToString("yyyy-MM-dd") : "",
                                    PartNumber = f.PartNumber,
                                    Quantity = f.Quantity,
                                    Requester = u.FirstName + " " + u.LastName,
                                    Remarks = f.Remarks
                                });

                // Sorting
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    forecastData = forecastData.OrderBy(sortColumn + " " + sortColumnDir);
                }
                // Search
                if (!string.IsNullOrEmpty(searchValue))
                {
                    forecastData = forecastData.Where(p => p.ForecastNumber.ToLower().Contains(searchValue.ToLower())
                        || p.ForecastDate.ToLower().Contains(searchValue.ToLower())
                        || p.PartNumber.ToLower().Contains(searchValue.ToLower())
                        || p.Quantity.ToString().ToLower().Contains(searchValue.ToLower())
                        || p.Requester.ToLower().Contains(searchValue.ToLower())
                        || p.Remarks.ToString().ToLower().Contains(searchValue.ToLower()));
                }

                // total number of rows count
                recordsTotal = forecastData.Count();
                // Paging
                var data = forecastData.Skip(skip).Take(pageSize).ToList();

                // Returning Json Data
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult Edit(string fNumber)
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    List<ForecastList> forecast = db.ForecastLists.Where(x => x.ForecastNumber == fNumber).ToList();

                    ViewBag.summary = forecast.First();
                    ViewBag.lists = forecast;
                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [HttpPost]
        public ActionResult Edit(string fNumber, ForecastList forecastList, FormCollection form)
        {
            try
            {
                List<ForecastList> currData = db.ForecastLists.Where(x => x.ForecastNumber == fNumber).ToList();

                foreach (var item in currData)
                {
                    int ID = item.ID;
                    ForecastList forecast = db.ForecastLists.Find(ID);
                    forecast.ForecastNumber = forecastList.ForecastNumber;
                    forecast.ForecastDate = forecastList.ForecastDate;
                    forecast.Remarks = forecastList.Remarks;
                    forecast.PartNumber = form["PartNumbers[" + ID + "]"].ToString();
                    forecast.Quantity = Convert.ToInt32(form["Quantities[" + ID + "]"]);
                    db.SaveChanges();
                }

                return RedirectToAction("Forecast", "Incoming");
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult Delete(string fNumber)
        {
            try
            {
                List<ForecastList> list = db.ForecastLists.Where(x => x.ForecastNumber == fNumber).ToList();
                db.ForecastLists.RemoveRange(list);
                db.SaveChanges();

                return RedirectToAction("Forecast", "Incoming");
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult Process(string fNumber)
        {
            try
            {
                List<ForecastList> list = db.ForecastLists.Where(x => x.ForecastNumber == fNumber).ToList();
                list.ForEach(x => x.IsValid = 1);
                db.SaveChanges();

                foreach (var f in list)
                {
                    User user = db.Users.Find(f.CreatedBy);
                    f.Description = db.MasterParts.FirstOrDefault(x => x.PartNumber == f.PartNumber).PartName;
                    f.Requester = user.FirstName + " " + user.LastName;
                }

                int userId = Convert.ToInt32(Session["UserId"]);
                User u = db.Users.Find(userId);
                var userMailLists = new[] { "KMSI_Stock", "GGI" };
                List<User> userLists = db.Users.Where(x => userMailLists.Contains(x.Role.RoleName)).ToList();

                string linkDetail = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                foreach (var data in userLists)
                {
                    string body = ViewRenderer.RenderRazorViewToString(this, "~/Views/Mail/ForecastInfo.cshtml", new EmailForecast()
                    {
                        ForecastNo = list.FirstOrDefault().ForecastNumber,
                        Date = list.FirstOrDefault().ForecastDate.Value.ToString("dd/MM/yyyy"),
                        ForecastLists = list,
                        Remarks = list.FirstOrDefault().Remarks,
                        Requester = list.FirstOrDefault().Requester,
                        BaseUrl = linkDetail
                    });

                    bool emailStatus = EmailHelper.SendEmail(data.Email, "KMSI FORECAST INFORMATION", body, "");

                    if (emailStatus)
                    {
                        Notification notification = new Notification();
                        notification.UserId = u.UserId;
                        notification.Name = u.FirstName + " " + u.LastName;
                        notification.Module = "VSS";
                        notification.Message = "Forecast data has been processed by " + notification.Name;
                        notification.Url = linkDetail;
                        notification.ReceiveId = data.UserId;
                        notification.CreatedAt = DateTime.Now;
                        db.Notifications.Add(notification);
                        db.SaveChanges();
                    }
                }

                return RedirectToAction("Forecast", "Incoming");
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult GenerateXls(string[] ids, int? status)
        {
            try
            {
                DateTime dateTime = DateTime.Now;
                string FileName = String.Empty;
                if (status == 0)
                {
                    FileName = string.Format("UNPROCESSED FORECAST_" + dateTime.ToString("yyyyMMdd_HHmm") + ".xlsx");
                }
                else if (status == 1)
                {
                    if (Session["RoleType"].ToString() == "Internal")
                    {
                        FileName = string.Format("PROCESSED FORECAST_" + dateTime.ToString("yyyyMMdd_HHmm") + ".xlsx");
                    }
                    else
                    {
                        FileName = string.Format("KMSI FORECAST_" + dateTime.ToString("yyyyMMdd_HHmm") + ".xlsx");
                    }
                }
                string handle = Guid.NewGuid().ToString();

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (ExcelPackage excel = new ExcelPackage())
                    {
                        excel.Workbook.Worksheets.Add("Forecast");

                        var headerRow = new List<string[]>()
                        {
                            new string[] { "Forecast No", "Forecast Date", "Remarks", "Submitted By" }
                        };
                        string headerRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";

                        var worksheet = excel.Workbook.Worksheets["Forecast"];
                        worksheet.Cells[headerRange].LoadFromArrays(headerRow);
                        worksheet.Cells[headerRange].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        worksheet.Cells[headerRange].Style.Font.Bold = true;
                        worksheet.Cells[headerRange].Style.Font.Size = 12;
                        worksheet.Cells[headerRange].AutoFitColumns();

                        var forecastData = db.ForecastLists.AsEnumerable()
                            .Where(x => ids.Contains(x.ForecastNumber))
                            .Join(db.Users, f => f.CreatedBy, u => u.UserId, (f, u) => new
                            {
                                ForecastNumber = f.ForecastNumber,
                                ForecastDate = f.ForecastDate.HasValue ? f.ForecastDate.Value.ToString("yyyy-MM-dd") : "",
                                Remarks = f.Remarks,
                                SubmittedBy = u.FirstName + " " + u.LastName
                            })
                            .GroupBy(x => x.ForecastNumber)
                            .Select(g => g.First())
                            .ToList();

                        int rowNumber = 2;
                        foreach (var data in forecastData)
                        {
                            worksheet.Cells[rowNumber, 1].Value = data.ForecastNumber;
                            worksheet.Cells[rowNumber, 2].Value = data.ForecastDate;
                            worksheet.Cells[rowNumber, 3].Value = data.Remarks;
                            worksheet.Cells[rowNumber, 4].Value = data.SubmittedBy;

                            rowNumber++;
                        }
                        excel.SaveAs(memoryStream);
                    }
                    memoryStream.Position = 0;
                    TempData[handle] = memoryStream.ToArray();
                }

                return new JsonResult()
                {
                    Data = new { FileGuid = handle, FileName = FileName }
                };
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [HttpGet]
        public ActionResult DownloadXls(string fileGuid, string filename)
        {
            if (TempData[fileGuid] != null)
            {
                byte[] data = TempData[fileGuid] as byte[];
                return File(data, "application/vnd.ms-excel", filename);
            }
            else
            {
                return new EmptyResult();
            }
        }

        public ActionResult GeneratePdf(string[] ids, int? status)
        {
            try
            {
                DateTime dateTime = DateTime.Now;
                string strPdfFileName = String.Empty;
                string titlePdf = String.Empty;
                if (status == 0)
                {
                    strPdfFileName = string.Format("UNPROCESSED FORECAST_" + dateTime.ToString("yyyyMMdd_HHmm") + ".pdf");
                    titlePdf = "UNPROCESSED FORECAST";
                }
                else if (status == 1)
                {
                    if (Session["RoleType"].ToString() == "Internal")
                    {
                        strPdfFileName = string.Format("PROCESSED FORECAST_" + dateTime.ToString("yyyyMMdd_HHmm") + ".pdf");
                        titlePdf = "PROCESSED FORECAST";
                    }
                    else
                    {
                        strPdfFileName = string.Format("KMSI FORECAST_" + dateTime.ToString("yyyyMMdd_HHmm") + ".pdf");
                        titlePdf = "KMSI FORECAST";
                    }
                }
                string handle = Guid.NewGuid().ToString();

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    Document pdfDoc = new Document(PageSize.A4.Rotate());
                    PdfWriter.GetInstance(pdfDoc, memoryStream).CloseStream = false;
                    pdfDoc.SetMargins(28f, 28f, 28f, 28f);

                    string strAttachment = Server.MapPath("~/PDFs/" + strPdfFileName);

                    pdfDoc.Open();
                    Phrase phrase = new Phrase();

                    PdfPTable table = new PdfPTable(1);
                    table.WidthPercentage = 100;
                    table.SpacingAfter = 10f;

                    //add a LEFT LABEL 
                    table.AddCell(new PdfPCell(new Phrase("K-PINTAR PARTS PORTAL SYSTEM", FontFactory.GetFont("Calibri", 7, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });

                    // invoke LEFT image
                    iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Content/img/logo_kmsi.png" ));
                    jpg.ScaleAbsolute(120f,33f);
                    PdfPCell imageCell = new PdfPCell(jpg);
                    imageCell.Colspan = 2; // either 1 if you need to insert one cell
                    imageCell.Border = 0;
                    imageCell.HorizontalAlignment = Element.ALIGN_LEFT;

                    //add a RIGHT LABEL 
                    table.AddCell(new PdfPCell(new Phrase("VENDOR STOCK SYSTEM", FontFactory.GetFont("Calibri", 7, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_RIGHT,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });

                    // invoke RIGHT image
                    iTextSharp.text.Image rightJpg = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Content/img/logo-komatsu.png"));
                    rightJpg.ScaleAbsolute(120f, 40f);
                    PdfPCell rightImageCell = new PdfPCell(rightJpg);
                    rightImageCell.Colspan = 2; // either 1 if you need to insert one cell
                    rightImageCell.Border = 0;
                    rightImageCell.HorizontalAlignment = Element.ALIGN_RIGHT;

                    // add a LEFT image to PdfPTables
                    table.AddCell(imageCell);
                    // add a RIGHT image to PdfPTables
                    //table.AddCell(rightImageCell);

                    table.AddCell(new PdfPCell(new Phrase(titlePdf, FontFactory.GetFont("Calibri", 20, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });
                    pdfDoc.Add(table);

                    var forecastData = db.ForecastLists.AsEnumerable()
                            .Where(x => ids.Contains(x.ForecastNumber))
                            .Join(db.Users, f => f.CreatedBy, u => u.UserId, (f, u) => new
                            {
                                ForecastNumber = f.ForecastNumber,
                                ForecastDate = f.ForecastDate.HasValue ? f.ForecastDate.Value.ToString("yyyy-MM-dd") : "",
                                Remarks = f.Remarks,
                                SubmittedBy = u.FirstName + " " + u.LastName
                            })
                            .GroupBy(x => x.ForecastNumber)
                            .Select(g => g.First())
                            .ToList();

                    //Table
                    //float[] widths = new float[] { 70f, 50f, 100f, 100f };
                    table = new PdfPTable(4);
                    ////table.TotalWidth = 520f;
                    //table.LockedWidth = true;
                    //table.SetWidths(widths);
                    table.WidthPercentage = 100;
                    table.SpacingBefore = 5f;
                    table.HeaderRows = 1;

                    //header
                    string[] headers = new string[] { "Forecast No", "Forecast Date", "Remarks", "Submitted By" };
                    for (int i = 0; i < headers.Count(); i++)
                    {
                        table.AddCell(new PdfPCell(new Phrase(headers[i], FontFactory.GetFont("Calibri", 10)))
                        {
                            PaddingTop = 3,
                            PaddingBottom = 8,
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                    }

                    foreach (var item in forecastData)
                    {
                        table.AddCell(new PdfPCell(new Phrase(item.ForecastNumber != null ? item.ForecastNumber : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.ForecastDate != null ? item.ForecastDate : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.Remarks != null ? item.Remarks : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.SubmittedBy != null ? item.SubmittedBy : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                    }
                    pdfDoc.Add(table);
                    pdfDoc.Close();

                    var bytes = memoryStream.ToArray();
                    Session[strPdfFileName] = bytes;
                }

                return Json(new { success = true, strPdfFileName }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [HttpGet]
        public virtual ActionResult DownloadPdf(string fileName)
        {
            try
            {
                var ms = Session[fileName] as byte[];
                if (ms == null)
                    return new EmptyResult();
                Session[fileName] = null;
                return File(ms, "application/octet-stream", fileName);
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return HttpNotFound();
            }
        }

        public ActionResult GenerateDoc(string fNumber)
        {
            try
            {
                MemoryStream memoryStream = new MemoryStream();
                DateTime dateTime = DateTime.Now;

                string strPdfFileName = string.Format("Forecast_" + dateTime.ToString("yyyyMMdd_HHmm") + ".pdf");

                Document pdfDoc = new Document(PageSize.A4.Rotate());
                pdfDoc.SetMargins(28f, 28f, 28f, 28f);

                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, memoryStream);
                writer.CloseStream = false;
                pdfDoc.Open();

                Phrase phrase = new Phrase();

                PdfPTable table = new PdfPTable(1);
                table.WidthPercentage = 100;
                table.SpacingAfter = 10f;

                table.AddCell(new PdfPCell(new Phrase("FORECAST DOCUMENT", FontFactory.GetFont("Calibri", 20, BaseColor.BLACK)))
                {
                    Border = 0,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    VerticalAlignment = Element.ALIGN_MIDDLE
                });
                pdfDoc.Add(table);

                var forecastData = db.ForecastLists.AsEnumerable()
                        .Where(x => x.ForecastNumber == fNumber)
                        .Join(db.Users, f => f.CreatedBy, u => u.UserId, (f, u) => new
                        {
                            ForecastNumber = f.ForecastNumber,
                            ForecastDate = f.ForecastDate.HasValue ? f.ForecastDate.Value.ToString("dd/MM/yyyy") : "",
                            Remarks = f.Remarks,
                            SubmittedBy = u.FirstName + " " + u.LastName,
                            PartNumber = f.PartNumber,
                            Quantity = f.Quantity.HasValue ? f.Quantity.ToString() : ""
                        })
                        .ToList();

                var summary = forecastData.FirstOrDefault();
                float[] widths1 = new float[] { 100f, 150f, 150f, 100f };
                table = new PdfPTable(4);
                table.TotalWidth = 500f;
                table.LockedWidth = true;
                table.SetWidths(widths1);
                table.SpacingBefore = 20f;
                table.SpacingAfter = 0f;

                table.AddCell(new PdfPCell(new Phrase("FORECAST NO.", FontFactory.GetFont("Calibri", 10)))
                {
                    Border = 0,
                    VerticalAlignment = Element.ALIGN_MIDDLE
                });
                table.AddCell(new PdfPCell(new Phrase(": " + summary.ForecastNumber, FontFactory.GetFont("Calibri", 10)))
                {
                    Border = 0,
                    VerticalAlignment = Element.ALIGN_MIDDLE
                });
                table.AddCell(new PdfPCell(new Phrase("DATE"))
                {
                    Border = 0,
                    VerticalAlignment = Element.ALIGN_MIDDLE,
                    HorizontalAlignment = Element.ALIGN_RIGHT
                });
                table.AddCell(new PdfPCell(new Phrase(": " + summary.ForecastDate, FontFactory.GetFont("Calibri", 10)))
                {
                    Border = 0,
                    VerticalAlignment = Element.ALIGN_MIDDLE,
                    HorizontalAlignment = Element.ALIGN_RIGHT
                });
                pdfDoc.Add(table);

                //Table
                float[] widths = new float[] { 100f, 250f, 150f };
                table = new PdfPTable(3);
                table.TotalWidth = 500f;
                table.LockedWidth = true;
                table.SetWidths(widths);
                table.SpacingBefore = 5f;
                table.HeaderRows = 1;

                //header
                string[] headers = new string[] { "No.", "Part Number", "PO Quantity" };
                for (int i = 0; i < headers.Count(); i++)
                {
                    table.AddCell(new PdfPCell(new Phrase(headers[i], FontFactory.GetFont("Calibri", 10, Font.BOLD)))
                    {
                        PaddingTop = 3,
                        PaddingBottom = 8,
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });
                }

                int number = 1;
                foreach (var item in forecastData)
                {
                    table.AddCell(new PdfPCell(new Phrase(number.ToString(), FontFactory.GetFont("Calibri", 8)))
                    {
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_TOP,
                        PaddingBottom = 8
                    });
                    table.AddCell(new PdfPCell(new Phrase(item.PartNumber != null ? item.PartNumber.ToUpper() : "", FontFactory.GetFont("Calibri", 8)))
                    {
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_TOP,
                        PaddingBottom = 8
                    });
                    table.AddCell(new PdfPCell(new Phrase(item.Quantity != null ? item.Quantity : "", FontFactory.GetFont("Calibri", 8)))
                    {
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_TOP,
                        PaddingBottom = 8
                    });

                    number++;
                }
                pdfDoc.Add(table);

                float[] widths3 = new float[] { 100, 400f };
                table = new PdfPTable(2);
                table.TotalWidth = 500f;
                table.LockedWidth = true;
                table.SetWidths(widths3);
                table.SpacingBefore = 5f;
                table.SpacingAfter = 0f;

                table.AddCell(new PdfPCell(new Phrase("REMARKS", FontFactory.GetFont("Calibri", 10)))
                {
                    Border = 0,
                    VerticalAlignment = Element.ALIGN_MIDDLE
                });
                table.AddCell(new PdfPCell(new Phrase(": " + summary.Remarks, FontFactory.GetFont("Calibri", 10)))
                {
                    Border = 0,
                    VerticalAlignment = Element.ALIGN_MIDDLE
                });
                pdfDoc.Add(table);

                float[] Fwidths = new float[] { 500f };
                table = new PdfPTable(1);
                table.TotalWidth = 500f;
                table.LockedWidth = true;
                table.SetWidths(Fwidths);
                table.SpacingBefore = 5f;

                table.AddCell(new PdfPCell(new Phrase("Submitted By,", FontFactory.GetFont("Calibri", 10, Font.ITALIC)))
                {
                    Border = 0,
                    PaddingBottom = 62,
                    HorizontalAlignment = Element.ALIGN_RIGHT
                });
                table.AddCell(new PdfPCell(new Phrase("(" + summary.SubmittedBy + ")", FontFactory.GetFont("Calibri", 10)))
                {
                    Border = 0,
                    HorizontalAlignment = Element.ALIGN_RIGHT
                });
                pdfDoc.Add(table);

                pdfDoc.Close();

                byte[] byteInfo = memoryStream.ToArray();
                memoryStream.Write(byteInfo, 0, byteInfo.Length);
                memoryStream.Position = 0;

                return File(memoryStream, "application/pdf", strPdfFileName);
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult GenerateExcel(string fNumber)
        {
            try
            {
                DateTime dateTime = DateTime.Now;
                string FileName = string.Format("Forecast_" + dateTime.ToString("yyyyMMdd_HHmm") + ".xlsx");
                string handle = Guid.NewGuid().ToString();

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (ExcelPackage excel = new ExcelPackage())
                    {
                        excel.Workbook.Worksheets.Add("Forecast");

                        var headerRow = new List<string[]>()
                        {
                            new string[] { "No.", "Forecast No.", "Date", "Part Number", "Qty", "Requester", "Remarks" }
                        };
                        string headerRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";

                        var worksheet = excel.Workbook.Worksheets["Forecast"];
                        worksheet.Cells[headerRange].LoadFromArrays(headerRow);
                        worksheet.Cells[headerRange].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        worksheet.Cells[headerRange].Style.Font.Bold = true;
                        worksheet.Cells[headerRange].Style.Font.Size = 12;
                        worksheet.Cells[headerRange].AutoFitColumns();

                        var forecastData = db.ForecastLists.AsEnumerable()
                            .Where(x => x.ForecastNumber == fNumber)
                            .Join(db.Users, f => f.CreatedBy, u => u.UserId, (f, u) => new
                            {
                                ForecastNumber = f.ForecastNumber,
                                ForecastDate = f.ForecastDate.HasValue ? f.ForecastDate.Value.ToString("dd/MM/yyyy") : "",
                                Remarks = f.Remarks,
                                SubmittedBy = u.FirstName + " " + u.LastName,
                                PartNumber = f.PartNumber,
                                Quantity = f.Quantity.HasValue ? f.Quantity.ToString() : "",
                                Requester = u.FirstName + " " + u.LastName
                            })
                            .ToList();

                        int rowNumber = 2;
                        int number = 1;
                        foreach (var data in forecastData)
                        {
                            worksheet.Cells[rowNumber, 1].Value = number;
                            worksheet.Cells[rowNumber, 2].Value = data.ForecastNumber;
                            worksheet.Cells[rowNumber, 3].Value = data.ForecastDate;
                            worksheet.Cells[rowNumber, 4].Value = data.PartNumber;
                            worksheet.Cells[rowNumber, 5].Value = data.Quantity;
                            worksheet.Cells[rowNumber, 6].Value = data.Requester;
                            worksheet.Cells[rowNumber, 7].Value = data.Remarks;

                            rowNumber++;
                            number++;
                        }
                        excel.SaveAs(memoryStream);
                    }
                    memoryStream.Position = 0;
                    TempData[handle] = memoryStream.ToArray();
                }

                return new JsonResult()
                {
                    Data = new { FileGuid = handle, FileName = FileName }
                };
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult Report()
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    List<MenuModels> MenuMaster = (List<MenuModels>)Session["MenuMaster"];
                    Int32 RoleID = Convert.ToInt32(Session["RoleId"]);

                    if (RoleID == 0)
                    {
                        ViewBag.IsInput = 1;
                        ViewBag.IsUpdate = 1;
                        ViewBag.IsDelete = 1;
                        ViewBag.IsUpload = 1;
                        ViewBag.IsDownload = 1;
                    }
                    else
                    {
                        var access = MenuMaster.Where(x => x.ControllerName == "Forecast" && x.ActionName == "Report" && x.RoleId == RoleID).FirstOrDefault();

                        if (access.IsView == 1)
                        {
                            ViewBag.IsInput = access.IsInput;
                            ViewBag.IsUpdate = access.IsUpdate;
                            ViewBag.IsDelete = access.IsDelete;
                            ViewBag.IsUpload = access.IsUpload;
                            ViewBag.IsDownload = access.IsDownload;
                        }
                        else
                        {
                            ViewBag.Message = "You don't have access to this page! Please contact administrator.";
                        }
                    }

                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }
    }
}