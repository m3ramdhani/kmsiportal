﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using KMSIPartsPortal_System.Helpers;
using KMSIPartsPortal_System.Models;
using OfficeOpenXml;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace KMSIPartsPortal_System.Controllers
{
    public class DeliveryController : Controller
    {
        private kmsi_portalEntities db = new kmsi_portalEntities();

        public FileResult DownloadExcel()
        {
            string filename = "KMSI_PO_Delivery_Order";
            string path = "/Docs/" + filename + ".xlsx";
            return File(path, "application/vnd.ms-excel", filename + ".xlsx");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [HandleError]
        public ActionResult Upload(HttpPostedFileBase file)
        {
            if (Session["UserId"] != null)
            {
                TempData["ActionMessage"] = "Upload Failed";
                try
                {
                    if (file != null)
                    {
                        if (file.ContentType == "application/vnd.ms-excel" || file.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                        {
                            var fileName = file.FileName;
                            var targetPath = Server.MapPath("~/Uploads/");
                            file.SaveAs(targetPath + fileName);
                            var pathToExcelFile = targetPath + fileName;
                            string FileName = Path.GetFileName(file.FileName);
                            string Extension = Path.GetExtension(file.FileName);
                            DataTable dataTable = FileImport.ImportToGrid(pathToExcelFile, Extension, "Yes");
                            List<EmailViewModel> email = new List<EmailViewModel>();
                            foreach (DataRow row in dataTable.Rows)
                            {
                                try
                                {
                                    string DONO = row["DONO"].ToString().Trim();
                                    string PurchaseNumber = row["PO NUMBER"].ToString().Trim();
                                    string delivDateFormat = row["DELIVERY DATE (YYYYMMDD)"].ToString().Trim();
                                    DateTime? DeliveryDate = null;
                                    if (delivDateFormat != "")
                                    {
                                        string day = delivDateFormat.Substring(6, 2);
                                        string month = delivDateFormat.Substring(4, 2);
                                        string year = delivDateFormat.Substring(0, 4);
                                        string deliveryDate = year + '-' + month + '-' + day;
                                        DeliveryDate = Convert.ToDateTime(deliveryDate);
                                    }
                                    string PartNumber = row["PART NO"].ToString().Trim();
                                    string PartName = row["PART NAME"].ToString().Trim();
                                    int Quantity = Convert.ToInt32(row["QTY"]);
                                    string Unit = row["UNIT"].ToString().Trim();
                                    string CNO = row["C/NO"].ToString().Trim();

                                    string orderDateFormat = row["ORDER DATE (YYYYMMDD)"].ToString();
                                    DateTime? OrderDate = null;
                                    if (orderDateFormat != "")
                                    {
                                        string day = orderDateFormat.Substring(6, 2);
                                        string month = orderDateFormat.Substring(4, 2);
                                        string year = orderDateFormat.Substring(0, 4);
                                        string orderDate = year + '-' + month + '-' + day;
                                        OrderDate = Convert.ToDateTime(orderDate);
                                    }

                                    var isExist = db.OutgoingPurchases.FirstOrDefault(x => x.PurchaseNumber == PurchaseNumber && x.PartNumber == PartNumber && x.PurchaseDate == OrderDate);
                                    string status = "";
                                    if (isExist != null)
                                    {
                                        if (Quantity < isExist.Quantity)
                                        {
                                            status = "Less";
                                        }
                                        else if (Quantity > isExist.Quantity)
                                        {
                                            status = "More";
                                        }
                                        else if (Quantity == isExist.Quantity)
                                        {
                                            status = "Matched";
                                        }
                                    }
                                    else
                                    {
                                        status = "N/A";
                                    }

                                    if (isExist != null)
                                    {
                                        DeliveryOrder doExist = db.DeliveryOrders.FirstOrDefault(x => x.DONO == DONO
                                            && x.PurchaseNumber == PurchaseNumber
                                            && x.DODate == DeliveryDate
                                            && x.PartNumber == PartNumber
                                            && x.Description == PartName
                                            && x.Quantity == Quantity
                                            && x.Unit == Unit
                                            && x.CNO == CNO);

                                        DeliveryOrder order = null;
                                        if (doExist == null)
                                        {
                                            order = new DeliveryOrder();
                                            order.DONO = DONO;
                                            order.PurchaseNumber = PurchaseNumber;
                                            order.OrderDate = OrderDate;
                                            order.DODate = DeliveryDate;
                                            order.SalesName = row["SALES NAME"].ToString();
                                            order.AttnOrderBy = row["ATTN (ORDER)"].ToString();
                                            order.CompanyNameOrderBy = row["COMPANY (ORDER)"].ToString();
                                            order.AddressOrderBy = row["ADDR (ORDER)"].ToString();
                                            order.PhoneOrderBy = row["PHONE (ORDER)"].ToString();
                                            order.EmailOrderBy = row["EMAIL (ORDER)"].ToString();
                                            order.AttnDeliverTo = row["ATTN (DELIVER)"].ToString();
                                            order.CompanyNameDeliverTo = row["COMPANY (DELIVER)"].ToString();
                                            order.AddressDeliverTo = row["ADDR (DELIVER)"].ToString();
                                            order.PhoneDeliverTo = row["PHONE (DELIVER)"].ToString();
                                            order.EmailDeliverTo = row["EMAIL (DELIVER)"].ToString();
                                            order.PartNumber = PartNumber;
                                            order.Description = PartName;
                                            order.Quantity = Quantity;
                                            order.Unit = Unit;
                                            order.Remarks = row["REMARKS"].ToString();
                                            order.CNO = CNO;
                                            order.StockPoint = row["STOCK POINT"].ToString();
                                            order.StatusId = db.DeliveryStatuses.SingleOrDefault(x => x.Value == "WAIT").ID;
                                            order.IssuedBy = Convert.ToInt32(Session["UserId"]);
                                            order.CreatedAt = DateTime.Now;
                                            db.DeliveryOrders.Add(order);
                                            db.SaveChanges();
                                        }
                                        else
                                        {
                                            doExist.Quantity += Quantity;
                                            db.SaveChanges();
                                        }

                                        int? doTotal = db.DeliveryOrders.Where(x => x.PurchaseNumber == PurchaseNumber && x.PartNumber == PartNumber).Sum(x => x.Quantity);
                                        if (doTotal < isExist.Quantity)
                                        {
                                            isExist.DeliveryStatus = 2;
                                            db.SaveChanges();
                                        }
                                        else if (doTotal >= isExist.Quantity)
                                        {
                                            isExist.DeliveryStatus = 1;
                                            db.SaveChanges();
                                        }

                                        TempData["ActionMessage"] = "Upload Success";

                                        EmailViewModel model = new EmailViewModel();
                                        model.PartNumber = PartNumber;
                                        model.Status = status;
                                        email.Add(model);
                                    }

                                    if (System.IO.File.Exists(pathToExcelFile))
                                    {
                                        System.IO.File.Delete(pathToExcelFile);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                                    var sw = new System.IO.StreamWriter(filename, true);
                                    sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                                    sw.Close();

                                    TempData["ActionMessage"] = "Upload Failed";
                                }
                            }

                            // preparing email body
                            int userId = Convert.ToInt32(Session["UserId"]);
                            User user = db.Users.Find(userId);
                            var userMailLists = new[] { "KMSI_Stock", "KMSI_Warehouse" };
                            List<User> userLists = db.Users.Where(x => userMailLists.Contains(x.Role.RoleName)).ToList();
                            ViewData["DataList"] = email;

                            string linkDetail = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                            ViewData["LinkEmail"] = linkDetail;

                            foreach (var data in userLists)
                            {
                                string body = ViewRenderer.RenderRazorViewToString(this, "~/Views/Mail/ValidationPODO.cshtml", new Email()
                                {
                                    To = data.FirstName + " " + data.LastName,
                                    Date = DateTime.Now
                                });

                                bool emailStatus = EmailHelper.SendEmail(data.Email, "Data Validation PO by DO", body, "");

                                if (emailStatus)
                                {
                                    Notification notification = new Notification();
                                    notification.UserId = user.UserId;
                                    notification.Name = user.FirstName + " " + user.LastName;
                                    notification.Module = "VSS";
                                    notification.Message = "New DO Document has been uploaded by " + notification.Name;
                                    notification.Url = linkDetail;
                                    notification.ReceiveId = data.UserId;
                                    notification.CreatedAt = DateTime.Now;
                                    db.Notifications.Add(notification);
                                    db.SaveChanges();
                                }
                            }
                        }
                    }
                    else
                    {
                        var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                        var sw = new System.IO.StreamWriter(filename, true);
                        sw.WriteLine(DateTime.Now.ToString() + " " + "File Not Selected!");
                        sw.Close();

                        TempData["ActionMessage"] = "Upload Failed";
                    }
                }
                catch (Exception ex)
                {
                    var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                    var sw = new System.IO.StreamWriter(filename, true);
                    sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                    sw.Close();

                    TempData["ActionMessage"] = "Upload Failed";
                }

                return RedirectToAction("Purchase", "Outgoing");
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        public ActionResult List()
        {
            try
            {
                // Creating instance of DatabaseContext class
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                var status = Request.Form.GetValues("status").FirstOrDefault();

                // Paging Size
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                int IsComplete = Convert.ToInt32(status);

                string[] statusBJM = new string[] { "WAIT", "RECEIVE", "FQC" };
                string[] statusKMSI = new string[] { "KMSI" };

                // Getting all data
                var deliveryData = db.DeliveryOrders.AsEnumerable()
                    .Select(d => new
                    {
                        ID = d.ID,
                        DONO = d.DONO,
                        AttnOrderBy = d.AttnOrderBy,
                        AttnDeliverTo = d.AttnDeliverTo,
                        IssuedBy = d.IssuedBy,
                        PurchaseNumber = d.PurchaseNumber,
                        DODate = d.DODate.HasValue ? d.DODate.Value.ToString("yyyy-MM-dd") : "",
                        CreatedAt = d.CreatedAt.HasValue ? d.CreatedAt.Value.ToString("yyyy-MM-dd") : "",
                        UpdatedAt = d.UpdatedAt.HasValue ? d.UpdatedAt.Value.ToString("yyyy-MM-dd") : "",
                        OrderDate = d.OrderDate.HasValue ? d.OrderDate.Value.ToString("yyyy-MM-dd") : "",
                        Status = d.DeliveryStatus != null ? d.DeliveryStatus.Name : "",
                        StatusName = d.DeliveryStatus != null ? d.DeliveryStatus.Value : "",
                        IsValid = d.IsValid,
                        OrderAt = d.OrderDate,
                        Delivery = db.OutgoingPurchases.FirstOrDefault(x => x.PurchaseNumber == d.PurchaseNumber && x.PartNumber == d.PartNumber).DeliveryStatus,
                        Invoice = db.OutgoingPurchases.FirstOrDefault(x => x.PurchaseNumber == d.PurchaseNumber && x.PartNumber == d.PartNumber).InvoiceStatus
                    })
                    .OrderBy("Delivery asc")
                    .OrderBy("Invoice asc")
                    .GroupBy(x => x.DONO)
                    .Select(g => g.First());

                if (IsComplete == 0)
                {
                    deliveryData = deliveryData.Where(x => statusBJM.Contains(x.StatusName));
                }
                else if (IsComplete == 1)
                {
                    deliveryData = deliveryData.Where(x => statusKMSI.Contains(x.StatusName));
                }

                // Sorting
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    deliveryData = deliveryData.OrderBy(sortColumn + " " + sortColumnDir);
                }
                // Search
                if (!string.IsNullOrEmpty(searchValue))
                {
                    deliveryData = deliveryData.Where(p => p.DONO.ToLower().Contains(searchValue.ToLower())
                        || p.DODate.ToLower().Contains(searchValue.ToLower())
                        || p.Status.ToLower().Contains(searchValue.ToLower())
                        || p.StatusName.ToLower().Contains(searchValue.ToLower()));
                }

                // total number of rows count
                recordsTotal = deliveryData.Count();
                // Paging
                var data = deliveryData.Skip(skip).Take(pageSize).ToList();

                // Returning Json Data
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult Details(string poNumber)
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    var summary = db.DeliveryOrders.AsEnumerable()
                                .Where(x => x.PurchaseNumber == poNumber)
                                .Join(db.Users, d => d.IssuedBy, u => u.UserId, (d, u) => new
                                {
                                    ID = d.ID,
                                    DONO = d.DONO,
                                    PurchaseNumber = d.PurchaseNumber,
                                    OrderDate = d.OrderDate.HasValue ? d.OrderDate.Value.ToString("yyyy-MM-dd") : "",
                                    DODate = d.DODate.HasValue ? d.DODate.Value.ToString("yyyy-MM-dd") : "",
                                    SalesName = d.SalesName,
                                    CustomerOrderBy = d.CustomerOrderBy,
                                    AttnOrderBy = d.AttnOrderBy,
                                    CompanyNameOrderBy = d.CompanyNameOrderBy,
                                    AddressOrderBy = d.AddressOrderBy,
                                    PhoneOrderBy = d.PhoneOrderBy,
                                    EmailOrderBy = d.EmailOrderBy,
                                    CustomerDeliverTo = d.CustomerDeliverTo,
                                    AttnDeliverTo = d.AttnDeliverTo,
                                    CompanyNameDeliverTo = d.CompanyNameDeliverTo,
                                    AddressDeliverTo = d.AddressDeliverTo,
                                    PhoneDeliverTo = d.PhoneDeliverTo,
                                    EmailDeliverTo = d.EmailDeliverTo,
                                    IssuedBy = u.FirstName + " " + u.LastName
                                })
                                .GroupBy(x => x.DONO)
                                .Select(g => g.First()).First();
                    if (summary != null) {
                        ViewBag.poNumber = poNumber;
                        ViewData["DONO"] = summary.DONO;
                        ViewData["PurchaseNumber"] = summary.PurchaseNumber;
                        ViewData["OrderDate"] = summary.OrderDate;
                        ViewData["DODate"] = summary.DODate;
                        ViewData["SalesName"] = summary.SalesName;
                        ViewData["AttnOrderBy"] = summary.AttnOrderBy;
                        ViewData["CompanyNameOrderBy"] = summary.CompanyNameOrderBy;
                        ViewData["AddressOrderBy"] = summary.AddressOrderBy;
                        ViewData["PhoneOrderBy"] = summary.PhoneOrderBy;
                        ViewData["EmailOrderBy"] = summary.EmailOrderBy;
                        ViewData["AttnDeliverTo"] = summary.AttnDeliverTo;
                        ViewData["CompanyNameDeliverTo"] = summary.CompanyNameDeliverTo;
                        ViewData["AddressDeliverTo"] = summary.AddressDeliverTo;
                        ViewData["PhoneDeliverTo"] = summary.PhoneDeliverTo;
                        ViewData["EmailDeliverTo"] = summary.EmailDeliverTo;
                        ViewData["IssuedBy"] = summary.IssuedBy;
                        return View();
                    }
                    else
                    {
                        TempData["ActionMessage"] = "DO not found";
                        return RedirectToAction("Purchase", "Outgoing");
                    }
                   
                }
                else
                {

                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                TempData["ActionMessage"] = "DO not found";
                return RedirectToAction("Purchase", "Outgoing");
            }
        }

        public ActionResult Open(string dono)
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    var summary = db.DeliveryOrders.AsEnumerable()
                                .Where(x => x.DONO == dono)
                                .Join(db.Users, d => d.IssuedBy, u => u.UserId, (d, u) => new
                                {
                                    ID = d.ID,
                                    DONO = d.DONO,
                                    PurchaseNumber = d.PurchaseNumber,
                                    OrderDate = d.OrderDate.HasValue ? d.OrderDate.Value.ToString("yyyy-MM-dd") : "",
                                    DODate = d.DODate.HasValue ? d.DODate.Value.ToString("yyyy-MM-dd") : "",
                                    SalesName = d.SalesName,
                                    CustomerOrderBy = d.CustomerOrderBy,
                                    AttnOrderBy = d.AttnOrderBy,
                                    CompanyNameOrderBy = d.CompanyNameOrderBy,
                                    AddressOrderBy = d.AddressOrderBy,
                                    PhoneOrderBy = d.PhoneOrderBy,
                                    EmailOrderBy = d.EmailOrderBy,
                                    CustomerDeliverTo = d.CustomerDeliverTo,
                                    AttnDeliverTo = d.AttnDeliverTo,
                                    CompanyNameDeliverTo = d.CompanyNameDeliverTo,
                                    AddressDeliverTo = d.AddressDeliverTo,
                                    PhoneDeliverTo = d.PhoneDeliverTo,
                                    EmailDeliverTo = d.EmailDeliverTo,
                                    IssuedBy = u.FirstName + " " + u.LastName
                                })
                                .GroupBy(x => x.DONO)
                                .Select(g => g.First()).First();

                    ViewBag.DONO = dono;
                    ViewData["DONO"] = summary.DONO;
                    ViewData["PurchaseNumber"] = summary.PurchaseNumber;
                    ViewData["OrderDate"] = summary.OrderDate;
                    ViewData["DODate"] = summary.DODate;
                    ViewData["SalesName"] = summary.SalesName;
                    ViewData["AttnOrderBy"] = summary.AttnOrderBy;
                    ViewData["CompanyNameOrderBy"] = summary.CompanyNameOrderBy;
                    ViewData["AddressOrderBy"] = summary.AddressOrderBy;
                    ViewData["PhoneOrderBy"] = summary.PhoneOrderBy;
                    ViewData["EmailOrderBy"] = summary.EmailOrderBy;
                    ViewData["AttnDeliverTo"] = summary.AttnDeliverTo;
                    ViewData["CompanyNameDeliverTo"] = summary.CompanyNameDeliverTo;
                    ViewData["AddressDeliverTo"] = summary.AddressDeliverTo;
                    ViewData["PhoneDeliverTo"] = summary.PhoneDeliverTo;
                    ViewData["EmailDeliverTo"] = summary.EmailDeliverTo;
                    ViewData["IssuedBy"] = summary.IssuedBy;
                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                return RedirectToAction("Delivery", "Outgoing");
            }
        }

        public ActionResult DetailList()
        {
            try
            {
                // Creating instance of DatabaseContext class
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                var number = Request.Form.GetValues("param").FirstOrDefault();

                // Paging Size
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                // Getting all data
                var purchaseData = db.DeliveryOrders.AsEnumerable()
                                .Where(x => x.DONO == number)
                                .Select(d => new
                                {
                                    ID = d.ID,
                                    PartNumber = d.PartNumber,
                                    PurchaseNumber = d.PurchaseNumber,
                                    Description = d.Description,
                                    Quantity = d.Quantity,
                                    ActualQty = d.ActualQty,
                                    Unit = d.Unit,
                                    Remarks = d.Remarks,
                                    CNO = d.CNO,
                                    StockPoint = d.StockPoint
                                });
                // Sorting
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    purchaseData = purchaseData.OrderBy(sortColumn + " " + sortColumnDir);
                }
                // Search
                if (!string.IsNullOrEmpty(searchValue))
                {
                    purchaseData = purchaseData.Where(p => p.PartNumber.ToLower().Contains(searchValue.ToLower())
                        || p.Description.ToLower().Contains(searchValue.ToLower())
                        || p.Quantity.ToString().ToLower().Contains(searchValue.ToLower())
                        || p.Unit.ToLower().Contains(searchValue.ToLower())
                        || p.Remarks.ToString().ToLower().Contains(searchValue.ToLower())
                        || p.CNO.ToLower().Contains(searchValue.ToLower())
                        || p.StockPoint.ToLower().Contains(searchValue.ToLower()));
                }

                // total number of rows count
                recordsTotal = purchaseData.Count();
                // Paging
                var data = purchaseData.Skip(skip).Take(pageSize).ToList();

                // Returning Json Data
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }


        public ActionResult DetailListByPo()
        {
            try
            {
                // Creating instance of DatabaseContext class
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                var number = Request.Form.GetValues("param").FirstOrDefault();

                // Paging Size
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                // Getting all data
                var purchaseData = db.DeliveryOrders.AsEnumerable()
                                .Where(x => x.PurchaseNumber == number)
                                .Select(d => new
                                {
                                    ID = d.ID,
                                    PartNumber = d.PartNumber,
                                    DONO = d.DONO,
                                    DODate = d.DODate.HasValue ? d.DODate.Value.ToString("yyyy-MM-dd") : "",
                                    PurchaseNumber = d.PurchaseNumber,
                                    Description = d.Description,
                                    Quantity = d.Quantity,
                                    Unit = d.Unit,
                                    Remarks = d.Remarks,
                                    CNO = d.CNO,
                                    StockPoint = d.StockPoint
                                });
                // Sorting
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    purchaseData = purchaseData.OrderBy(sortColumn + " " + sortColumnDir);
                }
                // Search
                if (!string.IsNullOrEmpty(searchValue))
                {
                    purchaseData = purchaseData.Where(p => p.PartNumber.ToLower().Contains(searchValue.ToLower())
                        || p.Description.ToLower().Contains(searchValue.ToLower())
                        || p.Quantity.ToString().ToLower().Contains(searchValue.ToLower())
                        || p.Unit.ToLower().Contains(searchValue.ToLower())
                        || p.Remarks.ToString().ToLower().Contains(searchValue.ToLower())
                        || p.CNO.ToLower().Contains(searchValue.ToLower())
                        || p.StockPoint.ToLower().Contains(searchValue.ToLower()));
                }

                // total number of rows count
                recordsTotal = purchaseData.Count();
                // Paging
                var data = purchaseData.Skip(skip).Take(pageSize).ToList();

                // Returning Json Data
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }


        public ActionResult Edit(string dono)
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    List<DeliveryOrder> orders = db.DeliveryOrders.Where(x => x.DONO == dono).ToList();

                    ViewBag.summary = orders.First();
                    ViewBag.lists = orders;
                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [HttpPost]
        public ActionResult Edit(string dono, DeliveryOrder deliveryOrder, FormCollection form)
        {
            TempData["ActionMessage"] = "Failed Input Receipt";
            try
            {
                List<DeliveryOrder> currData = db.DeliveryOrders.Where(x => x.DONO == dono).ToList();
                List<EmailViewModel> email = new List<EmailViewModel>();
                foreach (var item in currData)
                {
                    int ID = item.ID;
                    int StatusId = db.DeliveryStatuses.SingleOrDefault(x => x.Value == "KMSI").ID;
                    DeliveryOrder delivery = db.DeliveryOrders.Find(ID);
                    string PartNumber = delivery.PartNumber;
                    int? Qty = (form["Actuals[" + ID + "]"] != "") ? (int?)Convert.ToInt32(form["Actuals[" + ID + "]"]) : null;
                    string status = "";
                    if (Qty != null)
                    {
                        if (Qty < delivery.Quantity)
                        {
                            status = "Less";
                        }
                        else if (Qty > delivery.Quantity)
                        {
                            status = "More";
                        }
                        else if (Qty == delivery.Quantity)
                        {
                            status = "Matched";
                        }
                    }
                    else
                    {
                        status = "Less";
                    }

                    delivery.ActualQty = (form["Actuals[" + ID + "]"] != "") ? (int?)Convert.ToInt32(form["Actuals[" + ID + "]"]) : null;
                    delivery.StatusId = StatusId;
                    delivery.UpdatedBy = Convert.ToInt32(Session["UserId"]);
                    delivery.UpdatedAt = DateTime.Now;
                    db.SaveChanges();

                    Stock stock = db.Stocks.Where(x => x.PartNumber == PartNumber && x.Position == "KMSI").FirstOrDefault();
                    if (stock != null)
                    {
                        stock.Quantity += (form["Actuals[" + ID + "]"] != "") ? (int?)Convert.ToInt32(form["Actuals[" + ID + "]"]) : 0;
                        stock.UpdatedBy = Convert.ToInt32(Session["UserId"]);
                        stock.UpdatedAt = DateTime.Now;
                    }
                    else
                    {
                        Stock data = new Stock();
                        data.PartNumber = PartNumber;
                        data.PartName = form["Description"];
                        data.Unit = form["Unit"];
                        data.Position = "KMSI";
                        data.Quantity = (form["Actuals[" + ID + "]"] != "") ? (int?)Convert.ToInt32(form["Actuals[" + ID + "]"]) : 0;
                        data.CreatedBy = Convert.ToInt32(Session["UserId"]);
                        data.CreatedAt = DateTime.Now;
                        data.UpdatedBy = Convert.ToInt32(Session["UserId"]);
                        data.UpdatedAt = DateTime.Now;
                        data.IncomingDeliveryDate = delivery.DODate;
                        data.DONO = delivery.DONO;
                        db.Stocks.Add(data);
                    }
                    db.SaveChanges();

                    Stock ggiStock = db.Stocks.Where(x => x.PartNumber == PartNumber && x.Position == "GGI").FirstOrDefault();
                    if (ggiStock != null)
                    {
                        ggiStock.Quantity -= (form["Actuals[" + ID + "]"] != "") ? (int?)Convert.ToInt32(form["Actuals[" + ID + "]"]) : 0;
                        ggiStock.UpdatedBy = Convert.ToInt32(Session["UserId"]);
                        ggiStock.UpdatedAt = DateTime.Now;
                    }
                    db.SaveChanges();

                    EmailViewModel model = new EmailViewModel();
                    model.PartNumber = PartNumber;
                    model.Status = status;
                    email.Add(model);
                }

                // preparing email body
                int userId = Convert.ToInt32(Session["UserId"]);
                User user = db.Users.Find(userId);
                var userMailLists = new[] { "KMSI_Stock" };
                List<User> userLists = db.Users.Where(x => userMailLists.Contains(x.Role.RoleName)).ToList();
                ViewData["DataList"] = email;

                string linkDetail = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                ViewData["LinkEmail"] = linkDetail;

                foreach (var data in userLists)
                {
                    string body = ViewRenderer.RenderRazorViewToString(this, "~/Views/Mail/ValidationDOResult.cshtml", new Email()
                    {
                        To = data.FirstName + " " + data.LastName,
                        Date = DateTime.Now
                    });

                    bool emailStatus = EmailHelper.SendEmail(data.Email, "Data Validation DO by Receipt Results KMSI", body, "");

                    if (emailStatus)
                    {
                        Notification notification = new Notification();
                        notification.UserId = user.UserId;
                        notification.Name = user.FirstName + " " + user.LastName;
                        notification.Module = "VSS";
                        notification.Message = "Incoming Result data has been updated by " + notification.Name;
                        notification.Url = linkDetail;
                        notification.ReceiveId = data.UserId;
                        notification.CreatedAt = DateTime.Now;
                        db.Notifications.Add(notification);
                        db.SaveChanges();
                    }
                }

                TempData["ActionMessage"] = "Success Input Receipt";

                return RedirectToAction("Delivery", "Outgoing");
            }
            catch (Exception ex)
            {
                TempData["ActionMessage"] = "Failed Input Receipt. Please check your input";

                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                return RedirectToAction("Delivery", "Outgoing");
            }
        }

        //PDF dan EXCELL delivery order Report ===============================================================================
        public ActionResult GenerateXls(string[] ids)
        {
            try
            {
                DateTime dateTime = DateTime.Now;
                string FileName = string.Format("Delivery_Order_Report_" + dateTime.ToString("yyyyMMdd_HHmm") + ".xlsx");
                string handle = Guid.NewGuid().ToString();

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (ExcelPackage excel = new ExcelPackage())
                    {
                        excel.Workbook.Worksheets.Add("Delivery Order Report");

                        var headerRow = new List<string[]>()
                        {
                            new string[] {
                            "Delivery Order No",
                            "Purchase Number",
                            "Order Date",
                            "DO Date",
                            "Attn Order By",
                            "Attn Deliver To",
                            "Issued By"
                            }
                        };
                        string headerRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";

                        var worksheet = excel.Workbook.Worksheets["Delivery Order Report"];
                        worksheet.Cells[headerRange].LoadFromArrays(headerRow);
                        worksheet.Cells[headerRange].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        worksheet.Cells[headerRange].Style.Font.Bold = true;
                        worksheet.Cells[headerRange].Style.Font.Size = 12;
                        worksheet.Cells[headerRange].AutoFitColumns();

                        // Getting all data
                        var deliveryData = db.DeliveryOrders.AsEnumerable()
                            .Select(d => new
                            {
                                ID = d.ID,
                                DONO = d.DONO,
                                AttnOrderBy = d.AttnOrderBy,
                                AttnDeliverTo = d.AttnDeliverTo,
                                IssuedBy = d.IssuedBy,
                                PurchaseNumber = d.PurchaseNumber,
                                DODate = d.DODate.HasValue ? d.DODate.Value.ToString("yyyy-MM-dd") : "",
                                OrderDate = d.OrderDate.HasValue ? d.OrderDate.Value.ToString("yyyy-MM-dd") : "",
                                Status = d.DeliveryStatus != null ? d.DeliveryStatus.Name : "",
                                StatusName = d.DeliveryStatus != null ? d.DeliveryStatus.Value : "",
                                IsValid = d.IsValid
                            })
                            .GroupBy(x => x.DONO)
                            .Select(g => g.First());


                        deliveryData = deliveryData.ToList();

                        int rowNumber = 2;
                        foreach (var data in deliveryData)
                        {
                            worksheet.Cells[rowNumber, 1].Value = data.DONO;
                            worksheet.Cells[rowNumber, 2].Value = data.PurchaseNumber;
                            worksheet.Cells[rowNumber, 3].Value = data.OrderDate;
                            worksheet.Cells[rowNumber, 4].Value = data.DODate;
                            worksheet.Cells[rowNumber, 5].Value = data.AttnOrderBy;
                            worksheet.Cells[rowNumber, 6].Value = data.AttnDeliverTo;
                            worksheet.Cells[rowNumber, 7].Value = data.IssuedBy;

                            rowNumber++;
                        }
                        excel.SaveAs(memoryStream);
                    }
                    memoryStream.Position = 0;
                    TempData[handle] = memoryStream.ToArray();
                }

                return new JsonResult()
                {
                    Data = new { FileGuid = handle, FileName = FileName }
                };
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [HttpGet]
        public ActionResult DownloadXls(string fileGuid, string filename)
        {
            if (TempData[fileGuid] != null)
            {
                byte[] data = TempData[fileGuid] as byte[];
                return File(data, "application/vnd.ms-excel", filename);
            }
            else
            {
                return new EmptyResult();
            }
        }

        public ActionResult GeneratePdf(int[] ids)
        {
            try
            {
                DateTime dateTime = DateTime.Now;
                string strPdfFileName = string.Format("Delivery_Order_Report_" + dateTime.ToString("yyyyMMdd_HHmm") + ".pdf");
                string handle = Guid.NewGuid().ToString();

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    Document pdfDoc = new Document(PageSize.A4.Rotate());
                    PdfWriter.GetInstance(pdfDoc, memoryStream).CloseStream = false;
                    pdfDoc.SetMargins(28f, 28f, 28f, 28f);

                    string strAttachment = Server.MapPath("~/PDFs/" + strPdfFileName);

                    pdfDoc.Open();
                    Phrase phrase = new Phrase();

                    PdfPTable table = new PdfPTable(1);
                    table.WidthPercentage = 100;
                    table.SpacingAfter = 10f;

                    //add a LEFT LABEL 
                    table.AddCell(new PdfPCell(new Phrase("K-PINTAR PARTS PORTAL SYSTEM", FontFactory.GetFont("Calibri", 7, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });

                    // invoke LEFT image
                    iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Content/img/logo_kmsi.png"));
                    jpg.ScaleAbsolute(120f, 33f);
                    PdfPCell imageCell = new PdfPCell(jpg);
                    imageCell.Colspan = 2; // either 1 if you need to insert one cell
                    imageCell.Border = 0;
                    imageCell.HorizontalAlignment = Element.ALIGN_LEFT;

                    //add a RIGHT LABEL 
                    table.AddCell(new PdfPCell(new Phrase("VENDOR STOCK SYSTEM", FontFactory.GetFont("Calibri", 7, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_RIGHT,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });

                    // invoke RIGHT image
                    iTextSharp.text.Image rightJpg = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Content/img/logo-komatsu.png"));
                    rightJpg.ScaleAbsolute(120f, 40f);
                    PdfPCell rightImageCell = new PdfPCell(rightJpg);
                    rightImageCell.Colspan = 2; // either 1 if you need to insert one cell
                    rightImageCell.Border = 0;
                    rightImageCell.HorizontalAlignment = Element.ALIGN_RIGHT;

                    // add a LEFT image to PdfPTables
                    table.AddCell(imageCell);
                    // add a RIGHT image to PdfPTables
                    //table.AddCell(rightImageCell);

                    table.AddCell(new PdfPCell(new Phrase("DELIVERY ORDER REPORT", FontFactory.GetFont("Calibri", 20, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });
                    pdfDoc.Add(table);

                    // Getting all data
                    var deliveryData = db.DeliveryOrders.AsEnumerable()
                        .Where(x=> ids.Contains(x.ID))
                        .Select(d => new
                        {
                            ID = d.ID,
                            DONO = d.DONO,
                            AttnOrderBy = d.AttnOrderBy,
                            AttnDeliverTo = d.AttnDeliverTo,
                            IssuedBy = d.IssuedBy,
                            PurchaseNumber = d.PurchaseNumber,
                            DODate = d.DODate.HasValue ? d.DODate.Value.ToString("yyyy-MM-dd") : "",
                            OrderDate = d.OrderDate.HasValue ? d.OrderDate.Value.ToString("yyyy-MM-dd") : "",
                            Status = d.DeliveryStatus != null ? d.DeliveryStatus.Name : "",
                            StatusName = d.DeliveryStatus != null ? d.DeliveryStatus.Value : "",
                            IsValid = d.IsValid
                        })
                        .GroupBy(x => x.DONO)
                        .Select(g => g.First());


                    deliveryData = deliveryData.ToList();

                    //Table
                    //float[] widths = new float[] { 100f, 100f, 80f, 100f, 80f, 100f, 100f, };
                    table = new PdfPTable(7);
                    table.WidthPercentage = 100;
                    //table.TotalWidth = 520f;
                    //table.LockedWidth = true;
                    //table.SetWidths(widths);
                    table.SpacingBefore = 5f;
                    table.HeaderRows = 1;

                    //header
                    string[] headers = new string[] {
                            "Delivery Order No",
                            "Purchase Number",
                            "Order Date",
                            "DO Date",
                            "Attn Order By",
                            "Attn Deliver To",
                            "Issued By"
                    };
                    for (int i = 0; i < headers.Count(); i++)
                    {
                        table.AddCell(new PdfPCell(new Phrase(headers[i], FontFactory.GetFont("Calibri", 10)))
                        {
                            PaddingTop = 3,
                            PaddingBottom = 8,
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                    }

                    foreach (var item in deliveryData)
                    {
                        table.AddCell(new PdfPCell(new Phrase(item.DONO != null ? item.DONO : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.PurchaseNumber != null ? item.PurchaseNumber : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.OrderDate != null ? item.OrderDate : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.DODate != null ? item.DODate : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.AttnOrderBy != null ? item.AttnOrderBy : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.AttnDeliverTo != null ? item.AttnDeliverTo : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.IssuedBy != null ? item.IssuedBy.ToString() : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                    }
                    pdfDoc.Add(table);
                    pdfDoc.Close();

                    var bytes = memoryStream.ToArray();
                    Session[strPdfFileName] = bytes;
                }

                return Json(new { success = true, strPdfFileName }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [HttpGet]
        public virtual ActionResult DownloadPdf(string fileName)
        {
            try
            {
                var ms = Session[fileName] as byte[];
                if (ms == null)
                    return new EmptyResult();
                Session[fileName] = null;
                return File(ms, "application/octet-stream", fileName);
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return HttpNotFound();
            }
        }




        public ActionResult Confirm(string dono)
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    List<DeliveryOrder> deliveries = db.DeliveryOrders.Where(x => x.DONO == dono).ToList();

                    ViewBag.summary = deliveries.First();
                    ViewBag.lists = deliveries;
                    return View();
                }
                else
                {
                    return RedirectToAction("Delivery", "Outgoing");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult doConfirm(int id)
        {
            try
            {
                DeliveryOrder delivery = db.DeliveryOrders.Find(id);
                delivery.IsValid = 1;
                db.SaveChanges();

                Stock stock = db.Stocks.Where(x => x.PartNumber == delivery.PartNumber && x.Position == "KMSI").FirstOrDefault();
                if (stock != null)
                {
                    stock.Quantity += delivery.Quantity;
                    stock.UpdatedBy = Convert.ToInt32(Session["UserId"]);
                    stock.UpdatedAt = DateTime.Now;
                }
                else
                {
                    Stock data = new Stock();
                    data.PartNumber = delivery.PartNumber;
                    data.Quantity = delivery.ActualQty;
                    data.Position = "KMSI";
                    data.CreatedBy = Convert.ToInt32(Session["UserId"]);
                    data.CreatedAt = DateTime.Now;
                    data.UpdatedBy = Convert.ToInt32(Session["UserId"]);
                    data.UpdatedAt = DateTime.Now;
                    data.IncomingDeliveryDate = delivery.DODate;
                    data.DONO = delivery.DONO;
                    data.PartName = delivery.Description;
                    db.Stocks.Add(data);
                }

                Stock ggiStock = db.Stocks.Where(x => x.PartNumber == delivery.PartNumber && x.Position == "GGI").FirstOrDefault();
                if (ggiStock != null)
                {
                    ggiStock.Quantity -= delivery.Quantity;
                    ggiStock.UpdatedBy = Convert.ToInt32(Session["UserId"]);
                    ggiStock.UpdatedAt = DateTime.Now;
                }

                db.SaveChanges();

                List<StockMailModel> email = db.Stocks.AsEnumerable()
                    .Where(x => x.PartNumber == delivery.PartNumber && x.Position == "KMSI")
                    .Join(db.Users, s => s.UpdatedBy, u => u.UserId, (s, u) => new StockMailModel
                    {
                        PartNumber = s.PartNumber,
                        QtyReceipt = s.Quantity.ToString(),
                        Position = s.Position,
                        ConfirmedAt = s.UpdatedAt.HasValue ? s.UpdatedAt.Value.ToString("dd/MM/yyyy") : "",
                        ConfirmedBy = u.FirstName + " " + u.LastName
                    })
                    .ToList();

                int userId = Convert.ToInt32(Session["UserId"]);
                User user = db.Users.Find(userId);
                var userMailLists = new[] { "KMSI_Stock", "KMSI_Warehouse" };
                List<User> userLists = db.Users.Where(x => userMailLists.Contains(x.Role.RoleName)).ToList();
                ViewData["DataList"] = email;

                string linkDetail = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                ViewData["LinkEmail"] = linkDetail;

                foreach (var data in userLists)
                {
                    string body = ViewRenderer.RenderRazorViewToString(this, "~/Views/Mail/ConfirmationReceipt.cshtml", new Email()
                    {
                        To = data.FirstName + " " + data.LastName,
                        Date = DateTime.Now
                    });

                    bool emailStatus = EmailHelper.SendEmail(data.Email, "Confirmation Goods Receipt", body, "");

                    if (emailStatus)
                    {
                        Notification notification = new Notification();
                        notification.UserId = user.UserId;
                        notification.Name = user.FirstName + " " + user.LastName;
                        notification.Module = "VSS";
                        notification.Message = "Goods Receipt has been confirmed by " + notification.Name;
                        notification.Url = linkDetail;
                        notification.ReceiveId = data.UserId;
                        notification.CreatedAt = DateTime.Now;
                        db.Notifications.Add(notification);
                        db.SaveChanges();
                    }
                }

                return RedirectToAction("Confirm", new RouteValueDictionary(
                    new { controller = "Delivery", action = "Confirm", dono = delivery.DONO }));
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult Report()
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    List<MenuModels> MenuMaster = (List<MenuModels>)Session["MenuMaster"];
                    Int32 RoleID = Convert.ToInt32(Session["RoleId"]);

                    if (RoleID == 0)
                    {
                        ViewBag.IsView = 1;
                        ViewBag.IsInput = 1;
                        ViewBag.IsUpdate = 1;
                        ViewBag.IsDelete = 1;
                        ViewBag.IsUpload = 1;
                        ViewBag.IsDownload = 1;
                    }
                    else
                    {
                        var access = MenuMaster.Where(x => x.ControllerName == "Delivery" && x.ActionName == "Report" && x.RoleId == RoleID).FirstOrDefault();

                        ViewBag.IsView = access.IsView;
                        ViewBag.IsInput = access.IsInput;
                        ViewBag.IsUpdate = access.IsUpdate;
                        ViewBag.IsDelete = access.IsDelete;
                        ViewBag.IsUpload = access.IsUpload;
                        ViewBag.IsDownload = access.IsDownload;
                    }

                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult doReceive(string dono)
        {
            string ActionMessage = "Failed Receive";
            TempData["ActionMessage"] = ActionMessage;
            int checkInvoice = 0;

            try
            {
                int StatusBJM = db.DeliveryStatuses.SingleOrDefault(x => x.Value == "RECEIVE").ID;
                var incoming = db.DeliveryOrders.Where(x => x.DONO == dono).ToList();

                foreach (var i in incoming)
                {
                    var invoice = db.OutgoingInvoices.Where(x => x.PurchaseNumber == i.PurchaseNumber && x.PartNumber == i.PartNumber).ToList();

                    if (invoice.Count() == 0)
                    {
                        ActionMessage += "<br />Invoice for Part Number: " + i.PartNumber + " in Purchase with number: " + i.PurchaseNumber + " not available";
                        checkInvoice++;
                    }
                }

                if (checkInvoice == 0)
                {
                    incoming.ForEach(x =>
                    {
                        x.StatusId = StatusBJM;
                    });
                    db.SaveChanges();

                    TempData["ActionMessage"] = "Success Receive";
                }
                else
                {
                    ActionMessage += "<br />Please complete the invoice! And then continue receiving this Delivery Order.";
                    TempData["ActionMessage"] = ActionMessage;
                }

                return RedirectToAction("Delivery", "Outgoing");
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return RedirectToAction("Delivery", "Outgoing");
            }
        }

        //kmsi incoming result report=================================
        public ActionResult ResultReport()
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    List<MenuModels> MenuMaster = (List<MenuModels>)Session["MenuMaster"];
                    Int32 RoleID = Convert.ToInt32(Session["RoleId"]);

                    if (RoleID == 0)
                    {
                        ViewBag.IsInput = 1;
                        ViewBag.IsUpdate = 1;
                        ViewBag.IsDelete = 1;
                        ViewBag.IsUpload = 1;
                        ViewBag.IsDownload = 1;
                    }
                    else
                    {
                        var access = MenuMaster.Where(x => x.ControllerName == "Delivery" && x.ActionName == "ResultReport" && x.RoleId == RoleID).FirstOrDefault();

                        if (access.IsView == 1)
                        {
                            ViewBag.IsInput = access.IsInput;
                            ViewBag.IsUpdate = access.IsUpdate;
                            ViewBag.IsDelete = access.IsDelete;
                            ViewBag.IsUpload = access.IsUpload;
                            ViewBag.IsDownload = access.IsDownload;
                        }
                        else
                        {
                            ViewBag.Message = "You don't have access to this page! Please contact administrator.";
                        }
                    }

                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult KMSISIncomingResultList()
        {
            try
            {
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                var status = Request.Form.GetValues("status").FirstOrDefault();

                // Paging Size
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                Int32 IsValid = Convert.ToInt32(status);

                // Getting all Purchase data
                var stockData = db.DeliveryOrders.Where(x => x.ActualQty != null).AsEnumerable().Select(p => new {
                    ID = p.ID,
                    DONO = p.DONO,
                    PurchaseNumber = p.PurchaseNumber,
                    PartNumber = p.PartNumber,
                    Quantity = p.Quantity,
                    OrderDate = p.OrderDate.HasValue ? p.OrderDate.Value.ToString("yyyy-MM-dd") : "",
                    DODate = p.DODate.HasValue ? p.DODate.Value.ToString("yyyy-MM-dd") : "",
                    SalesName = p.SalesName,
                    CustomerOrderBy = p.CustomerOrderBy,
                    AttnOrderBy = p.AttnOrderBy,
                    CompanyNameOrderBy = p.CompanyNameOrderBy,
                    AddressOrderBy = p.AddressOrderBy,
                    PhoneOrderBy = p.PhoneOrderBy,
                    EmailOrderBy = p.EmailOrderBy,
                    CustomerDeliverTo = p.CustomerDeliverTo,
                    AttnDeliverTo = p.AttnDeliverTo,
                    CompanyNameDeliverTo = p.CompanyNameDeliverTo,
                    AddressDeliverTo = p.AddressDeliverTo,
                    PhoneDeliverTo = p.PhoneDeliverTo,
                    EmailDeliverTo = p.EmailDeliverTo,
                    Number = p.Number,
                    Description = p.Description,
                    Unit = p.Unit,
                    Remarks = p.Remarks,
                    CNO = p.CNO,
                    StockPoint = p.StockPoint,
                    IssuedBy = p.IssuedBy,
                    ReceivedBy = p.ReceivedBy,
                    CreatedAt = p.CreatedAt,
                    ActualQty = p.ActualQty,
                    UpdatedBy = p.UpdatedBy,
                    UpdatedAt = p.UpdatedAt,
                    IsValid = p.IsValid,
                    StatusId = p.StatusId,
                });


                // Sorting
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    stockData = stockData.OrderBy(sortColumn + " " + sortColumnDir);
                }
                // Search
                if (!string.IsNullOrEmpty(searchValue))
                {
                    stockData = stockData.Where(p => p.PartNumber.ToLower().Contains(searchValue.ToLower())
                    || p.DODate.ToString().ToLower().Contains(searchValue.ToLower())
                    || p.DONO.ToLower().Contains(searchValue.ToLower()));
                }

                // total number of rows count
                recordsTotal = stockData.Count();
                // Paging
                var data = stockData.Skip(skip).Take(pageSize).ToList();
                // Returning Json Data
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult GenerateXlsKMSIIncomingResult(string[] ids)
        {
            try
            {
                DateTime dateTime = DateTime.Now;
                string FileName = string.Format("KMSIIncomingResult_" + dateTime.ToString("yyyyMMdd_HHmm") + ".xlsx");
                string handle = Guid.NewGuid().ToString();

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (ExcelPackage excel = new ExcelPackage())
                    {
                        excel.Workbook.Worksheets.Add("KMSI Stock");

                        var headerRow = new List<string[]>()
                        {
                            new string[] {
                            "DONO",
                            "DODate",
                            "Quantity",
                            "ActualQty",
                            }
                        };
                        string headerRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";

                        var worksheet = excel.Workbook.Worksheets["KMSI Stock"];
                        worksheet.Cells[headerRange].LoadFromArrays(headerRow);
                        worksheet.Cells[headerRange].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        worksheet.Cells[headerRange].Style.Font.Bold = true;
                        worksheet.Cells[headerRange].Style.Font.Size = 12;
                        worksheet.Cells[headerRange].AutoFitColumns();

                        var stockData = db.DeliveryOrders.Where(x => x.ActualQty != null).AsEnumerable().Select(p => new {
                            ID = p.ID,
                            DONO = p.DONO,
                            PurchaseNumber = p.PurchaseNumber,
                            PartNumber = p.PartNumber,
                            Quantity = p.Quantity,
                            OrderDate = p.OrderDate.HasValue ? p.OrderDate.Value.ToString("yyyy-MM-dd") : "",
                            DODate = p.DODate.HasValue ? p.DODate.Value.ToString("yyyy-MM-dd") : "",
                            SalesName = p.SalesName,
                            CustomerOrderBy = p.CustomerOrderBy,
                            AttnOrderBy = p.AttnOrderBy,
                            CompanyNameOrderBy = p.CompanyNameOrderBy,
                            AddressOrderBy = p.AddressOrderBy,
                            PhoneOrderBy = p.PhoneOrderBy,
                            EmailOrderBy = p.EmailOrderBy,
                            CustomerDeliverTo = p.CustomerDeliverTo,
                            AttnDeliverTo = p.AttnDeliverTo,
                            CompanyNameDeliverTo = p.CompanyNameDeliverTo,
                            AddressDeliverTo = p.AddressDeliverTo,
                            PhoneDeliverTo = p.PhoneDeliverTo,
                            EmailDeliverTo = p.EmailDeliverTo,
                            Number = p.Number,
                            Description = p.Description,
                            Unit = p.Unit,
                            Remarks = p.Remarks,
                            CNO = p.CNO,
                            StockPoint = p.StockPoint,
                            IssuedBy = p.IssuedBy,
                            ReceivedBy = p.ReceivedBy,
                            CreatedAt = p.CreatedAt,
                            ActualQty = p.ActualQty,
                            UpdatedBy = p.UpdatedBy,
                            UpdatedAt = p.UpdatedAt,
                            IsValid = p.IsValid,
                            StatusId = p.StatusId,
                        }).ToList();

                        int rowNumber = 2;
                        foreach (var data in stockData)
                        {
                            worksheet.Cells[rowNumber, 1].Value = data.DONO;
                            worksheet.Cells[rowNumber, 2].Value = data.DODate;
                            worksheet.Cells[rowNumber, 3].Value = data.Quantity;
                            worksheet.Cells[rowNumber, 4].Value = data.ActualQty;

                            rowNumber++;
                        }
                        excel.SaveAs(memoryStream);
                    }
                    memoryStream.Position = 0;
                    TempData[handle] = memoryStream.ToArray();
                }

                return new JsonResult()
                {
                    Data = new { FileGuid = handle, FileName = FileName }
                };
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [HttpGet]
        public ActionResult DownloadXlsKMSIIncomingResult(string fileGuid, string filename)
        {
            if (TempData[fileGuid] != null)
            {
                byte[] data = TempData[fileGuid] as byte[];
                return File(data, "application/vnd.ms-excel", filename);
            }
            else
            {
                return new EmptyResult();
            }
        }

        public ActionResult GeneratePdfKMSIIncomingResult(string[] ids)
        {
            try
            {
                DateTime dateTime = DateTime.Now;
                string strPdfFileName = string.Format("KMSIIncomingResult_" + dateTime.ToString("yyyyMMdd_HHmm") + ".pdf");
                string handle = Guid.NewGuid().ToString();

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    Document pdfDoc = new Document(PageSize.A4.Rotate());
                    PdfWriter.GetInstance(pdfDoc, memoryStream).CloseStream = false;
                    pdfDoc.SetMargins(28f, 28f, 28f, 28f);

                    string strAttachment = Server.MapPath("~/PDFs/" + strPdfFileName);

                    pdfDoc.Open();
                    Phrase phrase = new Phrase();

                    PdfPTable table = new PdfPTable(1);
                    table.WidthPercentage = 100;
                    table.SpacingAfter = 10f;


                    ////add a LEFT LABEL 
                    //table.AddCell(new PdfPCell(new Phrase("K-PINTAR PARTS PORTAL SYSTEM", FontFactory.GetFont("Calibri", 7, BaseColor.BLACK)))
                    //{
                    //    Border = 0,
                    //    HorizontalAlignment = Element.ALIGN_LEFT,
                    //    VerticalAlignment = Element.ALIGN_MIDDLE
                    //});

                    //// invoke LEFT image
                    //iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Content/img/logo_kmsi.png"));
                    //jpg.ScaleAbsolute(120f, 33f);
                    //PdfPCell imageCell = new PdfPCell(jpg);
                    //imageCell.Colspan = 2; // either 1 if you need to insert one cell
                    //imageCell.Border = 0;
                    //imageCell.HorizontalAlignment = Element.ALIGN_LEFT;

                    ////add a RIGHT LABEL 
                    //table.AddCell(new PdfPCell(new Phrase("VENDOR STOCK SYSTEM", FontFactory.GetFont("Calibri", 7, BaseColor.BLACK)))
                    //{
                    //    Border = 0,
                    //    HorizontalAlignment = Element.ALIGN_RIGHT,
                    //    VerticalAlignment = Element.ALIGN_MIDDLE
                    //});

                    //// invoke RIGHT image
                    //iTextSharp.text.Image rightJpg = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Content/img/logo-komatsu.png"));
                    //rightJpg.ScaleAbsolute(120f, 40f);
                    //PdfPCell rightImageCell = new PdfPCell(rightJpg);
                    //rightImageCell.Colspan = 2; // either 1 if you need to insert one cell
                    //rightImageCell.Border = 0;
                    //rightImageCell.HorizontalAlignment = Element.ALIGN_RIGHT;

                    //// add a LEFT image to PdfPTables
                    //table.AddCell(imageCell);
                    //// add a RIGHT image to PdfPTables
                    ////table.AddCell(rightImageCell);


                    //add a LEFT LABEL 
                    table.AddCell(new PdfPCell(new Phrase("K-PINTAR PARTS PORTAL SYSTEM", FontFactory.GetFont("Calibri", 7, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });

                    // invoke LEFT image
                    iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Content/img/logo_kmsi.png"));
                    jpg.ScaleAbsolute(120f, 40f);
                    PdfPCell imageCell = new PdfPCell(jpg);
                    imageCell.Colspan = 2; // either 1 if you need to insert one cell
                    imageCell.Border = 0;
                    imageCell.HorizontalAlignment = Element.ALIGN_LEFT;

                    //add a RIGHT LABEL 
                    table.AddCell(new PdfPCell(new Phrase("VENDOR STOCK SYSTEM", FontFactory.GetFont("Calibri", 7, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_RIGHT,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });

                    // invoke RIGHT image
                    iTextSharp.text.Image rightJpg = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Content/img/logo-komatsu.png"));
                    rightJpg.ScaleAbsolute(120f, 40f);
                    PdfPCell rightImageCell = new PdfPCell(rightJpg);
                    rightImageCell.Colspan = 2; // either 1 if you need to insert one cell
                    rightImageCell.Border = 0;
                    rightImageCell.HorizontalAlignment = Element.ALIGN_RIGHT;

                    // add a LEFT image to PdfPTables
                    table.AddCell(imageCell);
                    // add a RIGHT image to PdfPTables
                    //table.AddCell(rightImageCell);

                    table.AddCell(new PdfPCell(new Phrase("KMSI INCOMING RESULT REPORT", FontFactory.GetFont("Calibri", 20, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });
                    pdfDoc.Add(table);

                    var stockData = db.DeliveryOrders.Where(x => x.ActualQty != null).AsEnumerable().Select(p => new {
                        ID = p.ID,
                        DONO = p.DONO,
                        PurchaseNumber = p.PurchaseNumber,
                        PartNumber = p.PartNumber,
                        Quantity = p.Quantity,
                        OrderDate = p.OrderDate,
                        DODate = p.DODate,
                        SalesName = p.SalesName,
                        CustomerOrderBy = p.CustomerOrderBy,
                        AttnOrderBy = p.AttnOrderBy,
                        CompanyNameOrderBy = p.CompanyNameOrderBy,
                        AddressOrderBy = p.AddressOrderBy,
                        PhoneOrderBy = p.PhoneOrderBy,
                        EmailOrderBy = p.EmailOrderBy,
                        CustomerDeliverTo = p.CustomerDeliverTo,
                        AttnDeliverTo = p.AttnDeliverTo,
                        CompanyNameDeliverTo = p.CompanyNameDeliverTo,
                        AddressDeliverTo = p.AddressDeliverTo,
                        PhoneDeliverTo = p.PhoneDeliverTo,
                        EmailDeliverTo = p.EmailDeliverTo,
                        Number = p.Number,
                        Description = p.Description,
                        Unit = p.Unit,
                        Remarks = p.Remarks,
                        CNO = p.CNO,
                        StockPoint = p.StockPoint,
                        IssuedBy = p.IssuedBy,
                        ReceivedBy = p.ReceivedBy,
                        CreatedAt = p.CreatedAt,
                        ActualQty = p.ActualQty,
                        UpdatedBy = p.UpdatedBy,
                        UpdatedAt = p.UpdatedAt,
                        IsValid = p.IsValid,
                        StatusId = p.StatusId
                    }).ToList();

                    //Table
                    //float[] widths = new float[] { 100f, 100f, 100f, 100f };
                    table = new PdfPTable(4);
                    table.WidthPercentage = 100;
                    //table.TotalWidth = 520f;
                    //table.LockedWidth = true;
                    //table.SetWidths(widths);
                    table.SpacingBefore = 5f;
                    table.HeaderRows = 1;

                    //header
                    string[] headers = new string[] {
                         "DONO",
                         "DODate",
                         "Quantity",
                         "ActualQty",
                    };
                    for (int i = 0; i < headers.Count(); i++)
                    {
                        table.AddCell(new PdfPCell(new Phrase(headers[i], FontFactory.GetFont("Calibri", 10)))
                        {
                            PaddingTop = 3,
                            PaddingBottom = 8,
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                    }

                    foreach (var item in stockData)
                    {
                        table.AddCell(new PdfPCell(new Phrase(item.DONO != null ? item.DONO : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.DODate != null ? item.DODate.Value.ToString("yyyy-MM-dd") : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.Quantity != null ? item.Quantity.ToString() : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.ActualQty != null ? item.ActualQty.ToString() : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                    }
                    pdfDoc.Add(table);
                    pdfDoc.Close();

                    var bytes = memoryStream.ToArray();
                    Session[strPdfFileName] = bytes;
                }

                return Json(new { success = true, strPdfFileName }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [HttpGet]
        public virtual ActionResult DownloadPdfKMSIIncomingResult(string fileName)
        {
            try
            {
                var ms = Session[fileName] as byte[];
                if (ms == null)
                    return new EmptyResult();
                Session[fileName] = null;
                return File(ms, "application/octet-stream", fileName);
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return HttpNotFound();
            }
        }




        //=======Outstanding incoming ==========================================
        public ActionResult GenerateXlsOutstandingIncoming(int[] ids)
        {
            try
            {
                DateTime dateTime = DateTime.Now;
                string FileName = string.Format("Outstanding_Incoming_information_" + dateTime.ToString("yyyyMMdd_HHmm") + ".xlsx");
                string handle = Guid.NewGuid().ToString();

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (ExcelPackage excel = new ExcelPackage())
                    {
                        excel.Workbook.Worksheets.Add("Delivery Order Report");

                        var headerRow = new List<string[]>()
                        {
                            new string[] {
                            "Delivery Order No",
                            "Purchase Number",
                            "Order Date",
                            "DO Date",
                            "Attn Order By",
                            "Attn Deliver To",
                            "Issued By"
                            }
                        };
                        string headerRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";

                        var worksheet = excel.Workbook.Worksheets["Delivery Order Report"];
                        worksheet.Cells[headerRange].LoadFromArrays(headerRow);
                        worksheet.Cells[headerRange].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        worksheet.Cells[headerRange].Style.Font.Bold = true;
                        worksheet.Cells[headerRange].Style.Font.Size = 12;
                        worksheet.Cells[headerRange].AutoFitColumns();

                        // Getting all data
                        var deliveryData = db.DeliveryOrders.AsEnumerable()
                            .Where(x => ids.Contains(x.ID))
                            .Select(d => new
                            {
                                ID = d.ID,
                                DONO = d.DONO,
                                AttnOrderBy = d.AttnOrderBy,
                                AttnDeliverTo = d.AttnDeliverTo,
                                IssuedBy = d.IssuedBy,
                                PurchaseNumber = d.PurchaseNumber,
                                DODate = d.DODate.HasValue ? d.DODate.Value.ToString("yyyy-MM-dd") : "",
                                OrderDate = d.OrderDate.HasValue ? d.OrderDate.Value.ToString("yyyy-MM-dd") : "",
                                Status = d.DeliveryStatus != null ? d.DeliveryStatus.Name : "",
                                StatusName = d.DeliveryStatus != null ? d.DeliveryStatus.Value : "",
                                IsValid = d.IsValid
                            })
                            .GroupBy(x => x.DONO)
                            .Select(g => g.First());


                        deliveryData = deliveryData.ToList();

                        int rowNumber = 2;
                        foreach (var data in deliveryData)
                        {
                            worksheet.Cells[rowNumber, 1].Value = data.DONO;
                            worksheet.Cells[rowNumber, 2].Value = data.PurchaseNumber;
                            worksheet.Cells[rowNumber, 3].Value = data.OrderDate;
                            worksheet.Cells[rowNumber, 4].Value = data.DODate;
                            worksheet.Cells[rowNumber, 5].Value = data.AttnOrderBy;
                            worksheet.Cells[rowNumber, 6].Value = data.AttnDeliverTo;
                            worksheet.Cells[rowNumber, 7].Value = data.IssuedBy;

                            rowNumber++;
                        }
                        excel.SaveAs(memoryStream);
                    }
                    memoryStream.Position = 0;
                    TempData[handle] = memoryStream.ToArray();
                }

                return new JsonResult()
                {
                    Data = new { FileGuid = handle, FileName = FileName }
                };
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [HttpGet]
        public ActionResult DownloadXlsOutstandingIncoming(string fileGuid, string filename)
        {
            if (TempData[fileGuid] != null)
            {
                byte[] data = TempData[fileGuid] as byte[];
                return File(data, "application/vnd.ms-excel", filename);
            }
            else
            {
                return new EmptyResult();
            }
        }

        public ActionResult GeneratePdfOutstandingIncoming(int[] ids)
        {
            try
            {
                DateTime dateTime = DateTime.Now;
                string strPdfFileName = string.Format("Outstanding_Incoming_information_" + dateTime.ToString("yyyyMMdd_HHmm") + ".pdf");
                string handle = Guid.NewGuid().ToString();

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    Document pdfDoc = new Document(PageSize.A4.Rotate());
                    PdfWriter.GetInstance(pdfDoc, memoryStream).CloseStream = false;
                    pdfDoc.SetMargins(28f, 28f, 28f, 28f);

                    string strAttachment = Server.MapPath("~/PDFs/" + strPdfFileName);

                    pdfDoc.Open();
                    Phrase phrase = new Phrase();

                    PdfPTable table = new PdfPTable(1);
                    table.WidthPercentage = 100;
                    table.SpacingAfter = 10f;

                    //add a LEFT LABEL 
                    table.AddCell(new PdfPCell(new Phrase("K-PINTAR PARTS PORTAL SYSTEM", FontFactory.GetFont("Calibri", 7, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });

                    // invoke LEFT image
                    iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Content/img/logo_kmsi.png"));
                    jpg.ScaleAbsolute(120f, 33f);
                    PdfPCell imageCell = new PdfPCell(jpg);
                    imageCell.Colspan = 2; // either 1 if you need to insert one cell
                    imageCell.Border = 0;
                    imageCell.HorizontalAlignment = Element.ALIGN_LEFT;

                    //add a RIGHT LABEL 
                    table.AddCell(new PdfPCell(new Phrase("VENDOR STOCK SYSTEM", FontFactory.GetFont("Calibri", 7, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_RIGHT,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });

                    // invoke RIGHT image
                    iTextSharp.text.Image rightJpg = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Content/img/logo-komatsu.png"));
                    rightJpg.ScaleAbsolute(120f, 40f);
                    PdfPCell rightImageCell = new PdfPCell(rightJpg);
                    rightImageCell.Colspan = 2; // either 1 if you need to insert one cell
                    rightImageCell.Border = 0;
                    rightImageCell.HorizontalAlignment = Element.ALIGN_RIGHT;

                    // add a LEFT image to PdfPTables
                    table.AddCell(imageCell);
                    // add a RIGHT image to PdfPTables
                    //table.AddCell(rightImageCell);

                    table.AddCell(new PdfPCell(new Phrase("DELIVERY ORDER REPORT", FontFactory.GetFont("Calibri", 20, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });
                    pdfDoc.Add(table);

                    // Getting all data
                    var deliveryData = db.DeliveryOrders.AsEnumerable()
                        .Where(x => ids.Contains(x.ID))
                        .Select(d => new
                        {
                            ID = d.ID,
                            DONO = d.DONO,
                            AttnOrderBy = d.AttnOrderBy,
                            AttnDeliverTo = d.AttnDeliverTo,
                            IssuedBy = d.IssuedBy,
                            PurchaseNumber = d.PurchaseNumber,
                            DODate = d.DODate.HasValue ? d.DODate.Value.ToString("yyyy-MM-dd") : "",
                            OrderDate = d.OrderDate.HasValue ? d.OrderDate.Value.ToString("yyyy-MM-dd") : "",
                            Status = d.DeliveryStatus != null ? d.DeliveryStatus.Name : "",
                            StatusName = d.DeliveryStatus != null ? d.DeliveryStatus.Value : "",
                            IsValid = d.IsValid
                        })
                        .GroupBy(x => x.DONO)
                        .Select(g => g.First());


                    deliveryData = deliveryData.ToList();

                    //Table
                    //float[] widths = new float[] { 100f, 100f, 80f, 100f, 80f, 100f, 100f, };
                    table = new PdfPTable(7);
                    table.WidthPercentage = 100;
                    //table.TotalWidth = 520f;
                    //table.LockedWidth = true;
                    //table.SetWidths(widths);
                    table.SpacingBefore = 5f;
                    table.HeaderRows = 1;

                    //header
                    string[] headers = new string[] {
                            "Delivery Order No",
                            "Purchase Number",
                            "Order Date",
                            "DO Date",
                            "Attn Order By",
                            "Attn Deliver To",
                            "Issued By"
                    };
                    for (int i = 0; i < headers.Count(); i++)
                    {
                        table.AddCell(new PdfPCell(new Phrase(headers[i], FontFactory.GetFont("Calibri", 10)))
                        {
                            PaddingTop = 3,
                            PaddingBottom = 8,
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                    }

                    foreach (var item in deliveryData)
                    {
                        table.AddCell(new PdfPCell(new Phrase(item.DONO != null ? item.DONO : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.PurchaseNumber != null ? item.PurchaseNumber : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.OrderDate != null ? item.OrderDate : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.DODate != null ? item.DODate : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.AttnOrderBy != null ? item.AttnOrderBy : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.AttnDeliverTo != null ? item.AttnDeliverTo : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.IssuedBy != null ? item.IssuedBy.ToString() : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                    }
                    pdfDoc.Add(table);
                    pdfDoc.Close();

                    var bytes = memoryStream.ToArray();
                    Session[strPdfFileName] = bytes;
                }

                return Json(new { success = true, strPdfFileName }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [HttpGet]
        public virtual ActionResult DownloadPdfOutstandingIncoming(string fileName)
        {
            try
            {
                var ms = Session[fileName] as byte[];
                if (ms == null)
                    return new EmptyResult();
                Session[fileName] = null;
                return File(ms, "application/octet-stream", fileName);
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return HttpNotFound();
            }
        }

    }
}