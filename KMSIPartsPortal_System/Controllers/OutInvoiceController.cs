﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Web;
using System.Web.Mvc;
using KMSIPartsPortal_System.Helpers;
using KMSIPartsPortal_System.Models;
using OfficeOpenXml;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace KMSIPartsPortal_System.Controllers
{
    public class OutInvoiceController : Controller
    {
        private kmsi_portalEntities db = new kmsi_portalEntities();

        public FileResult DownloadExcel()
        {
            string filename = "KMSI_PO_Invoice_Document";
            string path = "/Docs/" + filename + ".xlsx";
            return File(path, "application/vnd.ms-excel", filename + ".xlsx");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [HandleError]
        public ActionResult Upload(HttpPostedFileBase file)
        {
            TempData["ActionMessage"] = "Failed Upload";
            try
            {
                if (file != null)
                {
                    if (file.ContentType == "application/vnd.ms-excel" || file.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                    {
                        var fileName = file.FileName;
                        var targetPath = Server.MapPath("~/Uploads/");
                        file.SaveAs(targetPath + fileName);
                        var pathToExcelFile = targetPath + fileName;
                        string FileName = Path.GetFileName(file.FileName);
                        string Extension = Path.GetExtension(file.FileName);
                        DataTable dataTable = FileImport.ImportToGrid(pathToExcelFile, Extension, "Yes");
                        List<EmailViewModel> email = new List<EmailViewModel>();
                        foreach (DataRow row in dataTable.Rows)
                        {
                            try
                            {
                                string InvoiceNumber = row["INVOICE NO"].ToString().Trim();
                                string invoiceDateFormat = row["INVOICE DATE (YYYYMMDD)"].ToString();
                                DateTime? InvoiceDate = null;
                                if (invoiceDateFormat != "")
                                {
                                    string day = invoiceDateFormat.Substring(6, 2);
                                    string month = invoiceDateFormat.Substring(4, 2);
                                    string year = invoiceDateFormat.Substring(0, 4);
                                    string invoiceDate = year + '-' + month + '-' + day;
                                    InvoiceDate = Convert.ToDateTime(invoiceDate);
                                }
                                string PurchaseNumber = row["PO NUMBER"].ToString().Trim();
                                string PartNumber = row["PART NUMBER"].ToString().Trim();
                                string PartName = row["PART NAME"].ToString().Trim();
                                int Quantity = Convert.ToInt32(row["QUANTITY"]);
                                string Unit = row["UNIT"].ToString().Trim();
                                decimal UnitPrice = Convert.ToDecimal(row["UNIT PRICE"]);
                                decimal Amount = Convert.ToDecimal(row["AMOUNT"]);

                                var isExist = db.OutgoingPurchases.FirstOrDefault(x => x.PurchaseNumber == PurchaseNumber && x.PartNumber == PartNumber && x.Quantity == Quantity && x.AmountIDR == Amount);
                                string status = "";
                                if (isExist != null)
                                {
                                    status = "Matched";
                                }
                                else
                                {
                                    status = "N/A";
                                }

                                OutgoingInvoice invExist = db.OutgoingInvoices.FirstOrDefault(x => x.InvoiceNumber == InvoiceNumber
                                    && x.InvoiceDate == InvoiceDate
                                    && x.PartNumber == PartNumber
                                    && x.PartName == PartName
                                    && x.Quantity == Quantity
                                    && x.UnitPrice == UnitPrice
                                    && x.Amount == Amount);

                                if (invExist != null)
                                {
                                    invExist.Quantity += Quantity;
                                    db.SaveChanges();
                                }
                                else
                                {
                                    OutgoingInvoice invoice = new OutgoingInvoice();
                                    invoice.InvoiceNumber = InvoiceNumber;
                                    invoice.InvoiceDate = InvoiceDate;
                                    invoice.PurchaseNumber = PurchaseNumber;
                                    invoice.PartNumber = PartNumber;
                                    invoice.PartName = PartName;
                                    invoice.Quantity = Quantity;
                                    invoice.Unit = Unit;
                                    invoice.UnitPrice = UnitPrice;
                                    invoice.Amount = Amount;
                                    invoice.CreatedBy = Convert.ToInt32(Session["UserId"]);
                                    invoice.CreatedAt = DateTime.Now;
                                    db.OutgoingInvoices.Add(invoice);
                                    db.SaveChanges();
                                }

                                int? invTotal = db.OutgoingInvoices.Where(x => x.PurchaseNumber == PurchaseNumber && x.PartNumber == PartNumber).Sum(x => x.Quantity);
                                if (invTotal < isExist.Quantity)
                                {
                                    isExist.InvoiceStatus = 2;
                                    db.SaveChanges();
                                }
                                else if (invTotal >= isExist.Quantity)
                                {
                                    isExist.InvoiceStatus = 1;
                                    db.SaveChanges();
                                }

                                TempData["ActionMessage"] = "Success Upload";

                                EmailViewModel model = new EmailViewModel();
                                model.PartNumber = PartNumber;
                                model.Status = status;
                                email.Add(model);

                                if (System.IO.File.Exists(pathToExcelFile))
                                {
                                    System.IO.File.Delete(pathToExcelFile);
                                }
                            }
                            catch (Exception ex)
                            {
                                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                                var sw = new System.IO.StreamWriter(filename, true);
                                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                                sw.Close();
                            }
                        }

                        // preparing email body
                        var userMailLists = new[] { "KMSI_Stock", "GGI", "KMSI_Warehouse" };
                        List<User> userLists = db.Users.Where(x => userMailLists.Contains(x.Role.RoleName)).ToList();
                        ViewData["DataList"] = email;
                        //foreach (var data in userLists)
                        //{
                        //    string body = ViewRenderer.RenderRazorViewToString(this, "~/Views/Mail/ValidationPODO.cshtml", new Email()
                        //    {
                        //        To = data.FirstName + " " + data.LastName,
                        //        Date = DateTime.Now
                        //    });

                        //    EmailHelper.SendEmail(data.Email, "Data Validation PO by DO", body, "");
                        //}
                    }
                }
                else
                {
                    var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                    var sw = new System.IO.StreamWriter(filename, true);
                    sw.WriteLine(DateTime.Now.ToString() + " " + "File Not Selected!");
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();
            }

            return RedirectToAction("Purchase", "Outgoing");
        }

        // GET: OutInvoice
        public ActionResult Details(string poNumber)
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    var summary = db.OutgoingInvoices.AsEnumerable()
                        .Where(x => x.PurchaseNumber == poNumber)
                        .GroupBy(x => x.PurchaseNumber)
                        .Select(d => new
                        {
                            PurchaseNumber = d.First().PurchaseNumber,
                            Amount = d.Sum(a => a.Amount)
                        }).First();

                    decimal ppn = Decimal.Zero;
                    if (summary.Amount.HasValue)
                    {
                        ppn = (summary.Amount.Value * 10) / 100;
                    }

                    decimal total = Decimal.Zero;
                    if (summary.Amount.HasValue)
                    {
                        total = summary.Amount.Value + ppn;
                    }

                    ViewBag.PurchaseNumber = summary.PurchaseNumber;
                    ViewBag.SubTotal = summary.Amount.HasValue ? "Rp " + summary.Amount.Value.ToString("N") : "Rp 0,00";
                    ViewBag.PPn = "Rp " + ppn.ToString("N");
                    ViewBag.Total = "Rp " + total.ToString("N");
                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                TempData["ActionMessage"] = "Invoice not found";
                return RedirectToAction("Purchase", "Outgoing");
            }
        }

        public ActionResult DetailByPOList()
        {
            try
            {
                // Creating instance of DatabaseContext class
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                var number = Request.Form.GetValues("param").FirstOrDefault();

                // Paging Size
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                // Getting all data
                var invoiceData = db.OutgoingInvoices.AsEnumerable()
                    .Where(x => x.PurchaseNumber == number)
                    .Select(d => new
                    {
                        ID = d.ID,
                        InvoiceNumber = d.InvoiceNumber,
                        InvoiceDate = d.InvoiceDate.HasValue ? d.InvoiceDate.Value.ToString("yyyy-MM-dd") : "",
                        PartNumber = d.PartNumber,
                        PartName = d.PartName,
                        Quantity = d.Quantity.HasValue ? d.Quantity.Value.ToString() : "",
                        Unit = d.Unit,
                        UnitPrice = d.UnitPrice.HasValue ? "Rp " + d.UnitPrice.Value.ToString("N") : "",
                        Amount = d.Amount.HasValue ? "Rp " + d.Amount.Value.ToString("N") : ""
                    });
                // Sorting
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    invoiceData = invoiceData.OrderBy(sortColumn + " " + sortColumnDir);
                }
                // Search
                if (!string.IsNullOrEmpty(searchValue))
                {
                    invoiceData = invoiceData.Where(p => p.InvoiceNumber.ToLower().Contains(searchValue.ToLower())
                        || p.InvoiceDate.ToLower().Contains(searchValue.ToLower())
                        || p.PartNumber.ToLower().Contains(searchValue.ToLower())
                        || p.PartName.ToLower().Contains(searchValue.ToLower())
                        || p.Quantity.ToLower().Contains(searchValue.ToLower())
                        || p.Unit.ToLower().Contains(searchValue.ToLower())
                        || p.UnitPrice.ToLower().Contains(searchValue.ToLower())
                        || p.Amount.ToLower().Contains(searchValue.ToLower()));
                }

                // total number of rows count
                recordsTotal = invoiceData.Count();
                // Paging
                var data = invoiceData.Skip(skip).Take(pageSize).ToList();

                // Returning Json Data
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult Report()
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    List<MenuModels> MenuMaster = (List<MenuModels>)Session["MenuMaster"];
                    Int32 RoleID = Convert.ToInt32(Session["RoleId"]);

                    if (RoleID == 0)
                    {
                        ViewBag.IsInput = 1;
                        ViewBag.IsUpdate = 1;
                        ViewBag.IsDelete = 1;
                        ViewBag.IsUpload = 1;
                        ViewBag.IsDownload = 1;
                    }
                    else
                    {
                        var access = MenuMaster.Where(x => x.ControllerName == "Outinvoice" && x.ActionName == "Report" && x.RoleId == RoleID).FirstOrDefault();

                        if (access.IsView == 1)
                        {
                            ViewBag.IsInput = access.IsInput;
                            ViewBag.IsUpdate = access.IsUpdate;
                            ViewBag.IsDelete = access.IsDelete;
                            ViewBag.IsUpload = access.IsUpload;
                            ViewBag.IsDownload = access.IsDownload;
                        }
                        else
                        {
                            ViewBag.Message = "You don't have access to this page! Please contact administrator.";
                        }
                    }

                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult InvoiceList()
        {
            try
            {
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                var status = Request.Form.GetValues("status").FirstOrDefault();

                // Paging Size
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                Int32 IsValid = Convert.ToInt32(status);

                // Getting all Purchase data
                var invoiceData = db.OutgoingInvoices.AsEnumerable().Select(p => new{
                    InvoiceDate = p.InvoiceDate.HasValue ? p.InvoiceDate.Value.ToString("yyyy-MM-dd") : "",
                    ID = p.ID,
                    InvoiceNumber = p.InvoiceNumber,
                    PurchaseNumber = p.PurchaseNumber,
                    PartNumber = p.PartNumber,
                    PartName = p.PartName,
                    Quantity = p.Quantity,
                    Unit = p.Unit,
                    UnitPrice = p.UnitPrice,
                    Amount = p.Amount,
                    CreatedBy = p.CreatedBy,

                    CreatedAt = p.CreatedAt.HasValue ? p.CreatedAt.Value.ToString("yyyy-MM-dd") : ""
                });


                // Sorting
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    invoiceData = invoiceData.OrderBy(sortColumn + " " + sortColumnDir);
                }
                // Search
                if (!string.IsNullOrEmpty(searchValue))
                {
                    invoiceData = invoiceData.Where(p => p.InvoiceNumber.ToLower().Contains(searchValue.ToLower())
                        || p.PartNumber.ToLower().Contains(searchValue.ToLower())
                        || p.PurchaseNumber.ToLower().Contains(searchValue.ToLower())
                        || p.PartName.ToLower().Contains(searchValue.ToLower()));
                }

                // total number of rows count
                recordsTotal = invoiceData.Count();
                // Paging
                var data = invoiceData.Skip(skip).Take(pageSize).ToList();
                // Returning Json Data
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult GenerateXls(string[] ids)
        {
            try
            {
                DateTime dateTime = DateTime.Now;
                string FileName = string.Format("GGI_Invoice_" + dateTime.ToString("yyyyMMdd_HHmm") + ".xlsx");
                string handle = Guid.NewGuid().ToString();

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (ExcelPackage excel = new ExcelPackage())
                    {
                        excel.Workbook.Worksheets.Add("GGI Invoice");

                        var headerRow = new List<string[]>()
                        {
                            new string[] {
                                      "InvoiceNumber",
                                      "InvoiceDate",
                                      "PurchaseNumber",
                                      "PartNumber",
                                      "PartName",
                                      "Quantity",
                                      "Unit",
                                      "UnitPrice",
                                      "Amount",
                            }
                        };
                        string headerRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";

                        var worksheet = excel.Workbook.Worksheets["GGI Invoice"];
                        worksheet.Cells[headerRange].LoadFromArrays(headerRow);
                        worksheet.Cells[headerRange].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        worksheet.Cells[headerRange].Style.Font.Bold = true;
                        worksheet.Cells[headerRange].Style.Font.Size = 12;
                        worksheet.Cells[headerRange].AutoFitColumns();

                        var invoiceData = db.OutgoingInvoices.AsEnumerable().Select(p => new {
                            InvoiceDate = p.InvoiceDate.HasValue ? p.InvoiceDate.Value.ToString("yyyy-MM-dd") : "",
                            ID = p.ID,
                            InvoiceNumber = p.InvoiceNumber,
                            PurchaseNumber = p.PurchaseNumber,
                            PartNumber = p.PartNumber,
                            PartName = p.PartName,
                            Quantity = p.Quantity,
                            Unit = p.Unit,
                            UnitPrice = p.UnitPrice,
                            Amount = p.Amount,
                            CreatedBy = p.CreatedBy,
                            CreatedAt = p.CreatedAt.HasValue ? p.CreatedAt.Value.ToString("yyyy-MM-dd") : ""
                        }).ToList();

                        int rowNumber = 2;
                        foreach (var data in invoiceData)
                        {
                            worksheet.Cells[rowNumber, 1].Value = data.InvoiceNumber;
                            worksheet.Cells[rowNumber, 2].Value = data.InvoiceDate;
                            worksheet.Cells[rowNumber, 3].Value = data.PurchaseNumber;
                            worksheet.Cells[rowNumber, 4].Value = data.PartNumber;
                            worksheet.Cells[rowNumber, 5].Value = data.PartName;
                            worksheet.Cells[rowNumber, 6].Value = data.Quantity;
                            worksheet.Cells[rowNumber, 7].Value = data.Unit;
                            worksheet.Cells[rowNumber, 8].Value = data.UnitPrice;
                            worksheet.Cells[rowNumber, 9].Value = data.Amount;

                            rowNumber++;
                        }
                        excel.SaveAs(memoryStream);
                    }
                    memoryStream.Position = 0;
                    TempData[handle] = memoryStream.ToArray();
                }

                return new JsonResult()
                {
                    Data = new { FileGuid = handle, FileName = FileName }
                };
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [HttpGet]
        public ActionResult DownloadXls(string fileGuid, string filename)
        {
            if (TempData[fileGuid] != null)
            {
                byte[] data = TempData[fileGuid] as byte[];
                return File(data, "application/vnd.ms-excel", filename);
            }
            else
            {
                return new EmptyResult();
            }
        }

        public ActionResult GeneratePdf(string[] ids)
        {
            try
            {
                DateTime dateTime = DateTime.Now;
                string strPdfFileName = string.Format("GGI_Invoice_" + dateTime.ToString("yyyyMMdd_HHmm") + ".pdf");
                string handle = Guid.NewGuid().ToString();

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    Document pdfDoc = new Document(PageSize.A4.Rotate());
                    PdfWriter.GetInstance(pdfDoc, memoryStream).CloseStream = false;
                    pdfDoc.SetMargins(28f, 28f, 28f, 28f);

                    string strAttachment = Server.MapPath("~/PDFs/" + strPdfFileName);

                    pdfDoc.Open();
                    Phrase phrase = new Phrase();

                    PdfPTable table = new PdfPTable(1);
                    table.WidthPercentage = 100;
                    table.SpacingAfter = 10f;

                    //add a LEFT LABEL 
                    table.AddCell(new PdfPCell(new Phrase("K-PINTAR PARTS PORTAL SYSTEM", FontFactory.GetFont("Calibri", 7, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });

                    // invoke LEFT image
                    iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Content/img/logo_kmsi.png"));
                    jpg.ScaleAbsolute(120f, 40f);
                    PdfPCell imageCell = new PdfPCell(jpg);
                    imageCell.Colspan = 2; // either 1 if you need to insert one cell
                    imageCell.Border = 0;
                    imageCell.HorizontalAlignment = Element.ALIGN_LEFT;

                    //add a RIGHT LABEL 
                    table.AddCell(new PdfPCell(new Phrase("VENDOR STOCK SYSTEM", FontFactory.GetFont("Calibri", 7, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_RIGHT,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });

                    // invoke RIGHT image
                    iTextSharp.text.Image rightJpg = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Content/img/logo-komatsu.png"));
                    rightJpg.ScaleAbsolute(120f, 40f);
                    PdfPCell rightImageCell = new PdfPCell(rightJpg);
                    rightImageCell.Colspan = 2; // either 1 if you need to insert one cell
                    rightImageCell.Border = 0;
                    rightImageCell.HorizontalAlignment = Element.ALIGN_RIGHT;

                    // add a LEFT image to PdfPTables
                    table.AddCell(imageCell);
                    // add a RIGHT image to PdfPTables
                    //table.AddCell(rightImageCell);

                    table.AddCell(new PdfPCell(new Phrase("GGI INVOICE LIST", FontFactory.GetFont("Calibri", 20, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });
                    pdfDoc.Add(table);

                    var invoiceData = db.OutgoingInvoices.AsEnumerable().Select(p => new {
                        InvoiceDate = p.InvoiceDate.HasValue ? p.InvoiceDate.Value.ToString("yyyy-MM-dd") : "",
                        ID = p.ID,
                        InvoiceNumber = p.InvoiceNumber,
                        PurchaseNumber = p.PurchaseNumber,
                        PartNumber = p.PartNumber,
                        PartName = p.PartName,
                        Quantity = p.Quantity,
                        Unit = p.Unit,
                        UnitPrice = p.UnitPrice,
                        Amount = p.Amount,
                        CreatedBy = p.CreatedBy,
                        CreatedAt = p.CreatedAt.HasValue ? p.CreatedAt.Value.ToString("yyyy-MM-dd") : ""
                    }).ToList();

                    //Table
                    //float[] widths = new float[] { 70f, 50f, 100f, 100f };
                    table = new PdfPTable(9);
                    table.WidthPercentage = 100;
                    ////table.TotalWidth = 520f;
                    ////table.LockedWidth = true;
                    ////table.SetWidths(widths);
                    table.SpacingBefore = 5f;
                    table.HeaderRows = 1;

                    //header
                    string[] headers = new string[] {
                          "InvoiceNumber",
                          "InvoiceDate",
                          "PurchaseNumber",
                          "PartNumber",
                          "PartName",
                          "Quantity",
                          "Unit",
                          "UnitPrice",
                          "Amount",
                    };
                    for (int i = 0; i < headers.Count(); i++)
                    {
                        table.AddCell(new PdfPCell(new Phrase(headers[i], FontFactory.GetFont("Calibri", 10)))
                        {
                            PaddingTop = 3,
                            PaddingBottom = 8,
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                    }

                    foreach (var item in invoiceData)
                    {
                        table.AddCell(new PdfPCell(new Phrase(item.InvoiceNumber != null ? item.InvoiceNumber : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.InvoiceDate != null ? item.InvoiceDate : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.PurchaseNumber != null ? item.PurchaseNumber : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.PartNumber != null ? item.PartNumber : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.PartName != null ? item.PartName : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.Quantity != null ? item.Quantity.ToString() : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.Unit != null ? item.Unit : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.UnitPrice != null ? item.UnitPrice.ToString() : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase(item.Amount != null ? item.Amount.ToString() : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                    }
                    pdfDoc.Add(table);
                    pdfDoc.Close();

                    var bytes = memoryStream.ToArray();
                    Session[strPdfFileName] = bytes;
                }

                return Json(new { success = true, strPdfFileName }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [HttpGet]
        public virtual ActionResult DownloadPdf(string fileName)
        {
            try
            {
                var ms = Session[fileName] as byte[];
                if (ms == null)
                    return new EmptyResult();
                Session[fileName] = null;
                return File(ms, "application/octet-stream", fileName);
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return HttpNotFound();
            }
        }

    }
}