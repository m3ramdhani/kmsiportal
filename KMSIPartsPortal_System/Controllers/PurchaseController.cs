﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Web;
using System.Web.Mvc;
using KMSIPartsPortal_System.Models;

namespace KMSIPartsPortal_System.Controllers
{
    public class PurchaseController : Controller
    {
        private kmsi_portalEntities db = new kmsi_portalEntities();

        public ActionResult Details(string poNumber)
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    ViewBag.poNumber = poNumber;
                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult DetailList()
        {
            try
            {
                // Creating instance of DatabaseContext class
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                var number = Request.Form.GetValues("param").FirstOrDefault();

                // Paging Size
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                // Getting all data
                var purchaseData = db.PurchaseLists.AsEnumerable()
                                .Where(x => x.PurchaseNumber == number)
                                .Join(db.Users, p => p.CreatedBy, u => u.UserId, (p, u) => new
                                {
                                    ID = p.ID,
                                    ForecastNumber = p.ForecastNumber,
                                    PurchaseNumber = p.PurchaseNumber,
                                    PurchaseDate = p.PurchaseDate.HasValue ? p.PurchaseDate.Value.ToString("yyyy-MM-dd") : "",
                                    Item = p.ItemCode,
                                    ItemName = p.ItemName,
                                    Qty = p.OrderQuantity,
                                    Unit = p.Unit,
                                    User = u.FirstName + " " + u.LastName,
                                    UploadDate = p.CreatedAt.HasValue ? p.CreatedAt.Value.ToString("yyyy-MM-dd") : ""
                                });
                // Sorting
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    purchaseData = purchaseData.OrderBy(sortColumn + " " + sortColumnDir);
                }
                // Search
                if (!string.IsNullOrEmpty(searchValue))
                {
                    purchaseData = purchaseData.Where(p => p.ForecastNumber.ToLower().Contains(searchValue.ToLower())
                        || p.PurchaseNumber.ToLower().Contains(searchValue.ToLower())
                        || p.PurchaseDate.ToLower().Contains(searchValue.ToLower())
                        || p.Item.ToLower().Contains(searchValue.ToLower())
                        || p.ItemName.ToLower().Contains(searchValue.ToLower())
                        || p.Qty.ToString().ToLower().Contains(searchValue.ToLower())
                        || p.Unit.ToLower().Contains(searchValue.ToLower())
                        || p.User.ToLower().Contains(searchValue.ToLower())
                        || p.UploadDate.ToLower().Contains(searchValue.ToLower()));
                }

                // total number of rows count
                recordsTotal = purchaseData.Count();
                // Paging
                var data = purchaseData.Skip(skip).Take(pageSize).ToList();

                // Returning Json Data
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult Edit(string poNumber)
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    List<PurchaseList> purchases = db.PurchaseLists.Where(x => x.PurchaseNumber == poNumber).ToList();

                    ViewBag.summary = purchases.First();
                    ViewBag.lists = purchases;
                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [HttpPost]
        public ActionResult Edit(string poNumber, PurchaseList purchaseList, FormCollection form)
        {
            try
            {
                List<PurchaseList> currData = db.PurchaseLists.Where(x => x.PurchaseNumber == poNumber).ToList();

                foreach (var item in currData)
                {
                    int ID = item.ID;
                    PurchaseList purchase = db.PurchaseLists.Find(ID);
                    purchase.PurchaseNumber = purchaseList.PurchaseNumber;
                    purchase.PurchaseDate = purchaseList.PurchaseDate;
                    purchase.ItemCode = form["ItemCodes[" + ID + "]"].ToString();
                    purchase.ItemName = form["ItemNames[" + ID + "]"].ToString();
                    purchase.Unit = form["Units[" + ID + "]"].ToString();
                    purchase.OrderQuantity = (form["Quantities[" + ID + "]"] != "") ? (int?)Convert.ToInt32(form["Quantities[" + ID + "]"]) : null;
                    db.SaveChanges();
                }

                return RedirectToAction("Purchase", "Incoming");
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult Delete(string poNumber)
        {
            try
            {
                List<PurchaseList> list = db.PurchaseLists.Where(x => x.PurchaseNumber == poNumber).ToList();
                db.PurchaseLists.RemoveRange(list);
                db.SaveChanges();

                return RedirectToAction("Purchase", "Incoming");
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult PurchaseReport()
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    List<MenuModels> MenuMaster = (List<MenuModels>)Session["MenuMaster"];
                    Int32 RoleID = Convert.ToInt32(Session["RoleId"]);

                    if (RoleID == 0)
                    {
                        ViewBag.IsInput = 1;
                        ViewBag.IsUpdate = 1;
                        ViewBag.IsDelete = 1;
                        ViewBag.IsUpload = 1;
                        ViewBag.IsDownload = 1;
                    }
                    else
                    {
                        var access = MenuMaster.Where(x => x.ControllerName == "Purchase" && x.ActionName == "PurchaseReport" && x.RoleId == RoleID).FirstOrDefault();

                        if (access.IsView == 1)
                        {
                            ViewBag.IsInput = access.IsInput;
                            ViewBag.IsUpdate = access.IsUpdate;
                            ViewBag.IsDelete = access.IsDelete;
                            ViewBag.IsUpload = access.IsUpload;
                            ViewBag.IsDownload = access.IsDownload;
                        }
                        else
                        {
                            ViewBag.Message = "You don't have access to this page! Please contact administrator.";
                        }
                    }

                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }
    }
}