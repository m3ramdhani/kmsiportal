﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Web;
using System.Web.Mvc;
using KMSIPartsPortal_System.Models;

namespace KMSIPartsPortal_System.Controllers
{
    public class SidebarMenusController : Controller
    {
        private kmsi_portalEntities db = new kmsi_portalEntities();

        // GET: SidebarMenus
        public ActionResult Index(int? id)
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    ViewBag.id = id;
                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult List()
        {
            try
            {
                // Creating instance of DatabaseContext class
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();

                // Paging Size
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                // Getting all data
                var menuLists = from m in db.SidebarMenus
                                select new
                                {
                                    m.ID,
                                    m.SubMenu.MainMenu.Module.ModuleName,
                                    m.SubMenu.MainMenu.MenuName,
                                    m.SubMenu.SubMenuName,
                                    m.IsView,
                                    m.IsInput,
                                    m.IsUpdate,
                                    m.IsDelete,
                                    m.IsUpload,
                                    m.IsDownload,
                                    m.RoleID
                                };

                var id = Request.Form.GetValues("id").FirstOrDefault();
                if (id != "")
                {
                    int RoleID = Convert.ToInt32(id);
                    menuLists = menuLists.Where(s => s.RoleID == RoleID);
                }

                // Sorting
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    menuLists = menuLists.OrderBy(sortColumn + " " + sortColumnDir);
                }
                // Search
                if (!string.IsNullOrEmpty(searchValue))
                {
                    menuLists = menuLists.Where(m => m.ModuleName.ToLower().Contains(searchValue.ToLower())
                        || m.MenuName.ToLower().Contains(searchValue.ToLower())
                        || m.SubMenuName.ToLower().Contains(searchValue.ToLower())
                    );
                }

                // total number of rows count
                recordsTotal = menuLists.Count();
                // Paging
                var data = menuLists.Skip(skip).Take(pageSize).ToList();

                // Returning Json Data
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        // GET: SidebarMenus/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SidebarMenu sidebarMenu = db.SidebarMenus.Find(id);
            if (sidebarMenu == null)
            {
                return HttpNotFound();
            }
            return View(sidebarMenu);
        }

        // GET: SidebarMenus/Create
        public ActionResult Create(int? id)
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    var menuLists = from m in db.SubMenus
                                    select new
                                    {
                                        Value = m.ID,
                                        TextField = m.MainMenu.Module.ModuleName + " - " + m.SubMenuName
                                    };
                    ViewBag.MenuID = new SelectList(menuLists, "Value", "TextField");
                    ViewBag.id = id;
                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        // POST: SidebarMenus/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(SidebarMenu sidebarMenu)
        {
            if (ModelState.IsValid)
            {
                db.SidebarMenus.Add(sidebarMenu);
                db.SaveChanges();
                return RedirectToAction("Index", new
                {
                    id = sidebarMenu.RoleID
                });
            }

            ViewBag.RoleID = new SelectList(db.Roles, "RoleId", "RoleName", sidebarMenu.RoleID);
            ViewBag.MenuID = new SelectList(db.SubMenus, "ID", "SubMenuName", sidebarMenu.MenuID);
            return View(sidebarMenu);
        }

        // GET: SidebarMenus/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SidebarMenu sidebarMenu = db.SidebarMenus.Find(id);
            if (sidebarMenu == null)
            {
                return HttpNotFound();
            }
            ViewBag.RoleID = new SelectList(db.Roles, "RoleId", "RoleName", sidebarMenu.RoleID);
            ViewBag.MenuID = new SelectList(db.SubMenus, "ID", "SubMenuName", sidebarMenu.MenuID);
            return View(sidebarMenu);
        }

        // POST: SidebarMenus/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,MenuID,RoleID,IsInput,IsUpdate,IsDelete,IsView,IsUpload,IsDownload")] SidebarMenu sidebarMenu)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sidebarMenu).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.RoleID = new SelectList(db.Roles, "RoleId", "RoleName", sidebarMenu.RoleID);
            ViewBag.MenuID = new SelectList(db.SubMenus, "ID", "SubMenuName", sidebarMenu.MenuID);
            return View(sidebarMenu);
        }

        // GET: SidebarMenus/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SidebarMenu sidebarMenu = db.SidebarMenus.Find(id);
            if (sidebarMenu == null)
            {
                return HttpNotFound();
            }
            return View(sidebarMenu);
        }

        // POST: SidebarMenus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SidebarMenu sidebarMenu = db.SidebarMenus.Find(id);
            db.SidebarMenus.Remove(sidebarMenu);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
