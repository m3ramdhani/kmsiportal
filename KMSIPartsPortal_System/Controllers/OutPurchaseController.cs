﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Web;
using System.Web.Mvc;
using iTextSharp.text;
using iTextSharp.text.pdf;
using KMSIPartsPortal_System.Helpers;
using KMSIPartsPortal_System.Models;
using OfficeOpenXml;

namespace KMSIPartsPortal_System.Controllers
{
    public class OutPurchaseController : Controller
    {
        private kmsi_portalEntities db = new kmsi_portalEntities();

        public FileResult DownloadExcel()
        {
            string filename = "KMSI_PO_Outstanding_Purchase_Order";
            string path = "/Docs/" + filename + ".xlsx";
            return File(path, "application/vnd.ms-excel", filename + ".xlsx");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [HandleError]
        public ActionResult Upload(HttpPostedFileBase file)
        {
            TempData["ActionMessage"] = "Upload Failed";
            try
            {
                if (file != null)
                {
                    if (file.ContentType == "application/vnd.ms-excel" || file.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                    {
                        var fileName = file.FileName;
                        var targetPath = Server.MapPath("~/Uploads/");
                        file.SaveAs(targetPath + fileName);
                        var pathToExcelFile = targetPath + fileName;
                        string FileName = Path.GetFileName(file.FileName);
                        string Extension = Path.GetExtension(file.FileName);
                        DataTable dataTable = FileImport.ImportToGrid(pathToExcelFile, Extension, "Yes");
                        foreach (DataRow row in dataTable.Rows)
                        {
                            try
                            {
                                string PurchaseNumber = row["PO NUMBER"].ToString().Trim();
                                string poDateFormat = row["PO DATE (YYYYMMDD)"].ToString().Trim();
                                DateTime? PurchaseDate = null;
                                if (poDateFormat != "")
                                {
                                    string day = poDateFormat.Substring(6, 2);
                                    string month = poDateFormat.Substring(4, 2);
                                    string year = poDateFormat.Substring(0, 4);
                                    string purchaseDate = year + '-' + month + '-' + day;
                                    PurchaseDate = Convert.ToDateTime(purchaseDate);
                                }
                                string SupplierNumber = row["SUPPLIER CODE"].ToString().Trim();
                                string SupplierName = row["SUPPLIER NAME"].ToString().Trim();
                                int ItemNumber = Convert.ToInt32(row["ITEM NO"]);
                                string PartNumber = row["PART NUMBER"].ToString().Trim();
                                string OrgPartNumber = row["ORG PART"].ToString().Trim();
                                string PartName = row["PART NAME"].ToString().Trim();
                                int Quantity = Convert.ToInt32(row["QUANTITY"]);
                                decimal UnitPrice = Convert.ToDecimal(row["UNIT PRICE"]);
                                decimal AmountIDR = Convert.ToDecimal(row["AMOUNT"]);

                                var PartExist = db.MasterParts.FirstOrDefault(x => x.PartNumber == PartNumber);
                                if (PartExist != null)
                                {
                                    OutgoingPurchas IsExist = db.OutgoingPurchases.FirstOrDefault(x => x.PurchaseNumber == PurchaseNumber
                                        && x.PurchaseDate == PurchaseDate
                                        && x.SupplierNumber == SupplierNumber
                                        && x.SupplierName == SupplierName
                                        && x.ItemNumber == ItemNumber
                                        && x.PartNumber == PartNumber
                                        && x.OrgPartNumber == OrgPartNumber
                                        && x.PartName == PartName
                                        && x.Quantity == Quantity
                                        && x.UnitPrice == UnitPrice
                                        && x.AmountIDR == AmountIDR);

                                    OutgoingPurchas purchase = null;
                                    if (IsExist == null)
                                    {
                                        purchase = new OutgoingPurchas();
                                        purchase.PurchaseNumber = PurchaseNumber;
                                        purchase.PurchaseDate = PurchaseDate;
                                        purchase.SupplierNumber = SupplierNumber;
                                        purchase.SupplierName = SupplierName;
                                        purchase.ItemNumber = ItemNumber;
                                        purchase.PartNumber = PartNumber;
                                        purchase.OrgPartNumber = OrgPartNumber;
                                        purchase.PartName = PartName;
                                        purchase.Quantity = Quantity;
                                        purchase.UnitPrice = UnitPrice;
                                        purchase.AmountIDR = AmountIDR;
                                        purchase.AcknowledgeBy = Convert.ToInt32(Session["UserId"]);
                                        purchase.CreatedAt = DateTime.Now;
                                        db.OutgoingPurchases.Add(purchase);
                                        db.SaveChanges();

                                        TempData["ActionMessage"] = "Upload Success";
                                    }
                                    else
                                    {
                                        IsExist.Quantity += Quantity;
                                        db.SaveChanges();

                                        TempData["ActionMessage"] = "Upload Success";
                                    }
                                }

                                if (System.IO.File.Exists(pathToExcelFile))
                                {
                                    System.IO.File.Delete(pathToExcelFile);
                                }
                            }
                            catch (Exception ex)
                            {
                                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                                var sw = new System.IO.StreamWriter(filename, true);
                                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                                sw.Close();
                            }
                        }
                    }
                }
                else
                {
                    var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                    var sw = new System.IO.StreamWriter(filename, true);
                    sw.WriteLine(DateTime.Now.ToString() + " " + "File Not Selected!");
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();
            }

            return RedirectToAction("Purchase", "Outgoing");
        }

        public ActionResult Details(string poNumber, int? status)
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    ViewBag.poNumber = poNumber;
                    ViewBag.status = status.HasValue ? status : 0;
                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult DetailList()
        {
            try
            {
                // Creating instance of DatabaseContext class
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                var number = Request.Form.GetValues("param").FirstOrDefault();

                // Paging Size
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                // Getting all data
                var purchaseData = db.OutgoingPurchases.AsEnumerable()
                                .Where(x => x.PurchaseNumber == number)
                                .Join(db.Users, p => p.AcknowledgeBy, u => u.UserId, (p, u) => new
                                {
                                    ID = p.ID,
                                    PurchaseNumber = p.PurchaseNumber,
                                    PurchaseDate = p.PurchaseDate.HasValue ? p.PurchaseDate.Value.ToString("yyyy-MM-dd") : "",
                                    SupplierNumber = p.SupplierNumber,
                                    SupplierName = p.SupplierName,
                                    ItemNumber = p.ItemNumber,
                                    PartNumber = p.PartNumber,
                                    OrgPartNumber = p.OrgPartNumber,
                                    PartName = p.PartName,
                                    Quantity = p.Quantity,
                                    UnitPrice = p.UnitPrice.HasValue ? p.UnitPrice.Value.ToString("N") : "",
                                    AmountIDR = p.AmountIDR.HasValue ? p.AmountIDR.Value.ToString("C", CultureInfo.CreateSpecificCulture("id-ID")) : "",
                                    AcknowledgeBy = u.FirstName + " " + u.LastName
                                });
                // Sorting
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    purchaseData = purchaseData.OrderBy(sortColumn + " " + sortColumnDir);
                }
                // Search
                if (!string.IsNullOrEmpty(searchValue))
                {
                    purchaseData = purchaseData.Where(p => p.PurchaseNumber.ToLower().Contains(searchValue.ToLower())
                        || p.PurchaseDate.ToLower().Contains(searchValue.ToLower())
                        || p.SupplierNumber.ToLower().Contains(searchValue.ToLower())
                        || p.SupplierName.ToLower().Contains(searchValue.ToLower())
                        || p.ItemNumber.ToString().ToLower().Contains(searchValue.ToLower())
                        || p.PartNumber.ToLower().Contains(searchValue.ToLower())
                        || p.OrgPartNumber.ToLower().Contains(searchValue.ToLower())
                        || p.PartName.ToLower().Contains(searchValue.ToLower())
                        || p.OrgPartNumber.ToLower().Contains(searchValue.ToLower())
                        || p.PartName.ToLower().Contains(searchValue.ToLower())
                        || p.Quantity.ToString().ToLower().Contains(searchValue.ToLower())
                        || p.UnitPrice.ToLower().Contains(searchValue.ToLower())
                        || p.AmountIDR.ToLower().Contains(searchValue.ToLower())
                        || p.AcknowledgeBy.ToLower().Contains(searchValue.ToLower()));
                }

                // total number of rows count
                recordsTotal = purchaseData.Count();
                // Paging
                var data = purchaseData.Skip(skip).Take(pageSize).ToList();

                // Returning Json Data
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult PurchaseReport()
        {
            try
            {
                if (Session["UserId"] != null)
                {
                    List<MenuModels> MenuMaster = (List<MenuModels>)Session["MenuMaster"];
                    Int32 RoleID = Convert.ToInt32(Session["RoleId"]);

                    if (RoleID == 0)
                    {
                        ViewBag.IsView = 1;
                        ViewBag.IsInput = 1;
                        ViewBag.IsUpdate = 1;
                        ViewBag.IsDelete = 1;
                        ViewBag.IsUpload = 1;
                        ViewBag.IsDownload = 1;
                    }
                    else
                    {
                        var access = MenuMaster.Where(x => x.ControllerName == "OutPurchase" && x.ActionName == "PurchaseReport" && x.RoleId == RoleID).FirstOrDefault();

                        ViewBag.IsView = access.IsView;
                        ViewBag.IsInput = access.IsInput;
                        ViewBag.IsUpdate = access.IsUpdate;
                        ViewBag.IsDelete = access.IsDelete;
                        ViewBag.IsUpload = access.IsUpload;
                        ViewBag.IsDownload = access.IsDownload;
                    }

                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult GenerateXls(string[] ids)
        {
            try
            {
                DateTime dateTime = DateTime.Now;
                string FileName = string.Format("KMSI_Purchase_Order_" + dateTime.ToString("yyyyMMdd_HHmm") + ".xlsx");
                string handle = Guid.NewGuid().ToString();

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (ExcelPackage excel = new ExcelPackage())
                    {
                        excel.Workbook.Worksheets.Add("KMSI Incoming PO");

                        var headerRow = new List<string[]>()
                        {
                            new string[] { "PO Number", "PO Date", "Supplier Code", "Supplier Name", "Item No", "Part Number", "ORG Part", "Part Name", "Quantity", "Unit Price", "Amount (IDR)" }
                        };
                        string headerRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";

                        var worksheet = excel.Workbook.Worksheets["KMSI Incoming PO"];
                        worksheet.Cells[headerRange].LoadFromArrays(headerRow);
                        worksheet.Cells[headerRange].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        worksheet.Cells[headerRange].Style.Font.Bold = true;
                        worksheet.Cells[headerRange].Style.Font.Size = 12;
                        worksheet.Cells[headerRange].AutoFitColumns();

                        var purchaseData = db.OutgoingPurchases.AsEnumerable()
                            .Where(x => ids.Contains(x.PurchaseNumber))
                            .Select(p => new
                            {
                                PurchaseNumber = p.PurchaseNumber,
                                PurchaseDate = p.PurchaseDate.HasValue ? p.PurchaseDate.Value.ToString("yyyy-MM-dd") : "",
                                SupplierCode = p.SupplierNumber,
                                SupplierName = p.SupplierName,
                                ItemNo = p.ItemNumber,
                                PartNumber = p.PartNumber,
                                ORG = p.OrgPartNumber,
                                PartName = p.PartName,
                                Quantity = p.Quantity.ToString(),
                                Price = p.UnitPrice.HasValue ? p.UnitPrice.Value.ToString("N") : "",
                                Amount = p.AmountIDR.HasValue ? p.AmountIDR.Value.ToString("C", CultureInfo.CreateSpecificCulture("id-ID")) : ""
                            }).ToList();

                        int rowNumber = 2;
                        foreach (var data in purchaseData)
                        {
                            worksheet.Cells[rowNumber, 1].Value = data.PurchaseNumber;
                            worksheet.Cells[rowNumber, 2].Value = data.PurchaseDate;
                            worksheet.Cells[rowNumber, 3].Value = data.SupplierCode;
                            worksheet.Cells[rowNumber, 4].Value = data.SupplierName;
                            worksheet.Cells[rowNumber, 5].Value = data.ItemNo;
                            worksheet.Cells[rowNumber, 6].Value = data.PartNumber;
                            worksheet.Cells[rowNumber, 7].Value = data.ORG;
                            worksheet.Cells[rowNumber, 8].Value = data.PartName;
                            worksheet.Cells[rowNumber, 9].Value = data.Quantity;
                            worksheet.Cells[rowNumber, 10].Value = data.Price;
                            worksheet.Cells[rowNumber, 11].Value = data.Amount;

                            rowNumber++;
                        }
                        excel.SaveAs(memoryStream);
                    }
                    memoryStream.Position = 0;
                    TempData[handle] = memoryStream.ToArray();
                }

                return new JsonResult()
                {
                    Data = new { FileGuid = handle, FileName = FileName }
                };
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult GeneratePdf(string[] ids)
        {
            try
            {
                DateTime dateTime = DateTime.Now;
                string strPdfFileName = string.Format("KMSI_Purchase_Order_" + dateTime.ToString("yyyyMMdd_HHmm") + ".pdf");
                string handle = Guid.NewGuid().ToString();

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    Document pdfDoc = new Document(PageSize.A4.Rotate());
                    PdfWriter.GetInstance(pdfDoc, memoryStream).CloseStream = false;
                    pdfDoc.SetMargins(28f, 28f, 28f, 28f);

                    string strAttachment = Server.MapPath("~/PDFs/" + strPdfFileName);

                    pdfDoc.Open();
                    Phrase phrase = new Phrase();

                    PdfPTable table = new PdfPTable(1);
                    table.WidthPercentage = 100;
                    table.SpacingAfter = 10f;

                    //add a LEFT LABEL 
                    table.AddCell(new PdfPCell(new Phrase("K-PINTAR PARTS PORTAL SYSTEM", FontFactory.GetFont("Calibri", 7, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });

                    // invoke LEFT image
                    iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Content/img/logo_kmsi.png"));
                    jpg.ScaleAbsolute(120f, 33f);
                    PdfPCell imageCell = new PdfPCell(jpg);
                    imageCell.Colspan = 2; // either 1 if you need to insert one cell
                    imageCell.Border = 0;
                    imageCell.HorizontalAlignment = Element.ALIGN_LEFT;

                    //add a RIGHT LABEL 
                    table.AddCell(new PdfPCell(new Phrase("VENDOR STOCK SYSTEM", FontFactory.GetFont("Calibri", 7, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_RIGHT,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });

                    // invoke RIGHT image
                    iTextSharp.text.Image rightJpg = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Content/img/logo-komatsu.png"));
                    rightJpg.ScaleAbsolute(120f, 40f);
                    PdfPCell rightImageCell = new PdfPCell(rightJpg);
                    rightImageCell.Colspan = 2; // either 1 if you need to insert one cell
                    rightImageCell.Border = 0;
                    rightImageCell.HorizontalAlignment = Element.ALIGN_RIGHT;

                    // add a LEFT image to PdfPTables
                    table.AddCell(imageCell);
                    // add a RIGHT image to PdfPTables
                    //table.AddCell(rightImageCell);


                    table.AddCell(new PdfPCell(new Phrase("KMSI INCOMING PURCHASE ORDER", FontFactory.GetFont("Calibri", 20, BaseColor.BLACK)))
                    {
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    });
                    pdfDoc.Add(table);

                    var purchaseData = db.OutgoingPurchases.AsEnumerable()
                            .Where(x => ids.Contains(x.PurchaseNumber))
                            .Select(p => new
                            {
                                PurchaseNumber = p.PurchaseNumber,
                                PurchaseDate = p.PurchaseDate.HasValue ? p.PurchaseDate.Value.ToString("yyyy-MM-dd") : "",
                                SupplierCode = p.SupplierNumber,
                                SupplierName = p.SupplierName,
                                ItemNo = p.ItemNumber,
                                PartNumber = p.PartNumber,
                                ORG = p.OrgPartNumber,
                                PartName = p.PartName,
                                Quantity = p.Quantity.ToString(),
                                Price = p.UnitPrice.HasValue ? p.UnitPrice.Value.ToString("N") : "",
                                Amount = p.AmountIDR.HasValue ? p.AmountIDR.Value.ToString("C", CultureInfo.CreateSpecificCulture("id-ID")) : ""
                            }).ToList();

                    //Table
                    float[] widths = new float[] { 70f, 50f, 70f, 100f, 35f, 70f, 70f, 70f, 50f, 70f, 70f };
                    table = new PdfPTable(11);
                    table.TotalWidth = 725f;
                    table.LockedWidth = true;
                    table.SetWidths(widths);
                    table.SpacingBefore = 5f;
                    table.HeaderRows = 1;

                    //header
                    string[] headers = new string[] { "PO Number", "PO Date", "Supplier Code", "Supplier Name", "Item No", "Part Number", "ORG Part", "Part Name", "Quantity", "Unit Price", "Amount (IDR)" };
                    for (int i = 0; i < headers.Count(); i++)
                    {
                        table.AddCell(new PdfPCell(new Phrase(headers[i], FontFactory.GetFont("Calibri", 10)))
                        {
                            PaddingTop = 3,
                            PaddingBottom = 8,
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        });
                    }

                    foreach (var item in purchaseData)
                    {
                        table.AddCell(new PdfPCell(new Phrase((item.PurchaseNumber != null) ? item.PurchaseNumber : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.PurchaseDate != null) ? item.PurchaseDate : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.SupplierCode != null) ? item.SupplierCode : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.SupplierName != null) ? item.SupplierName : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.ItemNo != null) ? item.ItemNo.ToString() : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.PartNumber != null) ? item.PartNumber : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.ORG != null) ? item.ORG : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.PartName != null) ? item.PartName : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.Quantity != null) ? item.Quantity : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.Price != null) ? item.Price : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                        table.AddCell(new PdfPCell(new Phrase((item.Amount != null) ? item.Amount : "", FontFactory.GetFont("Calibri", 8)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                            VerticalAlignment = Element.ALIGN_TOP,
                            PaddingBottom = 8
                        });
                    }
                    pdfDoc.Add(table);
                    pdfDoc.Close();

                    var bytes = memoryStream.ToArray();
                    Session[strPdfFileName] = bytes;
                }

                return Json(new { success = true, strPdfFileName }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [HttpGet]
        public ActionResult DownloadXls(string fileGuid, string filename)
        {
            if (TempData[fileGuid] != null)
            {
                byte[] data = TempData[fileGuid] as byte[];
                return File(data, "application/vnd.ms-excel", filename);
            }
            else
            {
                return new EmptyResult();
            }
        }

        [HttpGet]
        public virtual ActionResult DownloadPdf(string fileName)
        {
            try
            {
                var ms = Session[fileName] as byte[];
                if (ms == null)
                    return new EmptyResult();
                Session[fileName] = null;
                return File(ms, "application/octet-stream", fileName);
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return HttpNotFound();
            }
        }

        public ActionResult Delete(string poNumber)
        {
            try
            {
                List<OutgoingPurchas> list = db.OutgoingPurchases.Where(x => x.PurchaseNumber == poNumber).ToList();
                db.OutgoingPurchases.RemoveRange(list);
                db.SaveChanges();

                return RedirectToAction("Purchase", "Outgoing");
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult GenerateExcel(string poNumber)
        {
            try
            {
                DateTime dateTime = DateTime.Now;
                string FileName = string.Format("KMSI_PO_" + dateTime.ToString("yyyyMMdd_HHmm") + ".xlsx");
                string handle = Guid.NewGuid().ToString();

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (ExcelPackage excel = new ExcelPackage())
                    {
                        excel.Workbook.Worksheets.Add("KMSI_PO");

                        var headerRow = new List<string[]>()
                        {
                            new string[] { "No.", "PO Number", "PO Date", "Supplier Code", "Supplier Name", "Item No", "Part Number", "ORG Part", "Part Name", "Quantity", "Unit Price", "Amount (IDR)", "Acknowledge By" }
                        };
                        string headerRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";

                        var worksheet = excel.Workbook.Worksheets["KMSI_PO"];
                        worksheet.Cells[headerRange].LoadFromArrays(headerRow);
                        worksheet.Cells[headerRange].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        worksheet.Cells[headerRange].Style.Font.Bold = true;
                        worksheet.Cells[headerRange].Style.Font.Size = 12;
                        worksheet.Cells[headerRange].AutoFitColumns();

                        var purchaseData = db.OutgoingPurchases.AsEnumerable()
                                .Where(x => x.PurchaseNumber == poNumber)
                                .Join(db.Users, p => p.AcknowledgeBy, u => u.UserId, (p, u) => new
                                {
                                    ID = p.ID,
                                    PurchaseNumber = p.PurchaseNumber,
                                    PurchaseDate = p.PurchaseDate.HasValue ? p.PurchaseDate.Value.ToString("yyyy-MM-dd") : "",
                                    SupplierNumber = p.SupplierNumber,
                                    SupplierName = p.SupplierName,
                                    ItemNumber = p.ItemNumber,
                                    PartNumber = p.PartNumber,
                                    OrgPartNumber = p.OrgPartNumber,
                                    PartName = p.PartName,
                                    Quantity = p.Quantity,
                                    UnitPrice = p.UnitPrice.HasValue ? p.UnitPrice.Value.ToString("N") : "",
                                    AmountIDR = p.AmountIDR.HasValue ? p.AmountIDR.Value.ToString("C", CultureInfo.CreateSpecificCulture("id-ID")) : "",
                                    AcknowledgeBy = u.FirstName + " " + u.LastName
                                });

                        int rowNumber = 2;
                        int number = 1;
                        foreach (var data in purchaseData)
                        {
                            worksheet.Cells[rowNumber, 1].Value = number;
                            worksheet.Cells[rowNumber, 2].Value = data.PurchaseNumber;
                            worksheet.Cells[rowNumber, 3].Value = data.PurchaseDate;
                            worksheet.Cells[rowNumber, 4].Value = data.SupplierNumber;
                            worksheet.Cells[rowNumber, 5].Value = data.SupplierName;
                            worksheet.Cells[rowNumber, 6].Value = data.ItemNumber;
                            worksheet.Cells[rowNumber, 7].Value = data.PartNumber;
                            worksheet.Cells[rowNumber, 8].Value = data.OrgPartNumber;
                            worksheet.Cells[rowNumber, 9].Value = data.PartName;
                            worksheet.Cells[rowNumber, 10].Value = data.Quantity;
                            worksheet.Cells[rowNumber, 11].Value = data.UnitPrice;
                            worksheet.Cells[rowNumber, 12].Value = data.AmountIDR;
                            worksheet.Cells[rowNumber, 13].Value = data.AcknowledgeBy;

                            rowNumber++;
                            number++;
                        }
                        excel.SaveAs(memoryStream);
                    }
                    memoryStream.Position = 0;
                    TempData[handle] = memoryStream.ToArray();
                }

                return new JsonResult()
                {
                    Data = new { FileGuid = handle, FileName = FileName }
                };
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }
    }
}