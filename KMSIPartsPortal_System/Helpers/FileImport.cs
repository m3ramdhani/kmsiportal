﻿using KMSIPartsPortal_System.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Validation;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KMSIPartsPortal_System.Helpers
{
    public static class FileImport
    {
        public static DataTable ImportToGrid(string FilePath, string Extension, string isHDR)
        {
            DataTable dt = new DataTable();
            try
            {
                string conString = string.Empty;
                switch (Extension)
                {
                    case ".xls": //Excel 97-03.
                        conString = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                        break;

                    case ".xlsx": //Excel 07 and above.
                        conString = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                        break;
                }
                conString = String.Format(conString, FilePath, isHDR);
                OleDbConnection conExcel = new OleDbConnection(conString);
                OleDbCommand cmdExcel = new OleDbCommand();
                OleDbDataAdapter oda = new OleDbDataAdapter();
                cmdExcel.Connection = conExcel;

                //Get the name of First Sheet
                conExcel.Open();
                DataTable dtExcelSchema;
                dtExcelSchema = conExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                string SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                conExcel.Close();

                //Read Data from First Sheet
                conExcel.Open();
                cmdExcel.CommandText = "SELECT * FROM [" + SheetName + "]";
                oda.SelectCommand = cmdExcel;
                oda.Fill(dt);
                conExcel.Close();
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();
            }
            return dt;
        }

        public static Boolean ImportFromDB(string remoteConnection, string localConnection)
        {
            kmsi_portalEntities db = new kmsi_portalEntities();
            DataTable dt = new DataTable();
            Boolean IsSuccess = false;
            try
            {
                using (SqlConnection conn = new SqlConnection(remoteConnection))
                {
                    conn.Open();
                    string[] SupplierCodeLs = db.Suppliers.Select(x => x.SupplierCode).ToArray();
                    var parameters = new string[SupplierCodeLs.Length];
                    var cmd = new SqlCommand();
                    for (int i = 0; i < SupplierCodeLs.Length; i++)
                    {
                        parameters[i] = string.Format("@sCode{0}", i);
                        cmd.Parameters.AddWithValue(parameters[i], SupplierCodeLs[i]);
                    }

                    //cmd.CommandText = string.Format("Select * From Ospo Where SUPPLIER_CODE IN ({0})", string.Join(", ", parameters));
                    cmd.CommandText = string.Format("Select * From Ospo");
                    cmd.Connection = conn;

                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        da.Fill(dt);
                    }

                    conn.Close();
                }

                if (dt.Rows.Count > 0)
                {
                    List<OutstandingPurchase> outstandings = db.OutstandingPurchases.Where(x => !x.STATUS.Equals("CLOSE") || !x.STATUS.Equals("PARTIAL")).ToList();
                    db.OutstandingPurchases.RemoveRange(outstandings);
                    db.SaveChanges();

                    using (SqlConnection connection = new SqlConnection(localConnection))
                    {
                        connection.Open();

                        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection, SqlBulkCopyOptions.TableLock | SqlBulkCopyOptions.FireTriggers | SqlBulkCopyOptions.UseInternalTransaction, null))
                        {
                            try
                            {
                                bulkCopy.DestinationTableName = "OutstandingPurchase";
                                bulkCopy.ColumnMappings.Add("SUPPLIER_CODE", "SUPPLIER_CODE");
                                bulkCopy.ColumnMappings.Add("PO_NUMBER", "PO_NUMBER");
                                bulkCopy.ColumnMappings.Add("PO_ITEM_NUMBER", "PO_ITEM_NUMBER");
                                bulkCopy.ColumnMappings.Add("PART_NUMBER", "PART_NUMBER");
                                bulkCopy.ColumnMappings.Add("PART_NAME", "PART_NAME");
                                bulkCopy.ColumnMappings.Add("LT_DAY", "LT_DAY");
                                bulkCopy.ColumnMappings.Add("PO_QTY", "PO_QTY");
                                bulkCopy.ColumnMappings.Add("PO_UNIT_PRICE", "PO_UNIT_PRICE");
                                bulkCopy.ColumnMappings.Add("PO_AMOUNT", "PO_AMOUNT");
                                bulkCopy.ColumnMappings.Add("CURRENCY_CODE", "CURRENCY_CODE");
                                bulkCopy.ColumnMappings.Add("CONFIRMED_QTY", "CONFIRMED_QTY");
                                bulkCopy.ColumnMappings.Add("SHIPPED_QTY", "SHIPPED_QTY");
                                bulkCopy.ColumnMappings.Add("ARRIVED_QTY", "ARRIVED_QTY");
                                bulkCopy.ColumnMappings.Add("BINNING_SLIP_QTY", "BINNING_SLIP_QTY");
                                bulkCopy.ColumnMappings.Add("BINNED_QTY", "BINNED_QTY");
                                bulkCopy.ColumnMappings.Add("FOO_QTY", "FOO_QTY");
                                bulkCopy.ColumnMappings.Add("ONORDER_ALLOC_QTY", "ONORDER_ALLOC_QTY");
                                bulkCopy.ColumnMappings.Add("PO_DATE", "PO_DATE");
                                bulkCopy.ColumnMappings.Add("SUPPLIER_CONFIRMED_DATE", "SUPPLIER_CONFIRMED_DATE");
                                bulkCopy.ColumnMappings.Add("DO_NUMBER", "DO_NUMBER");
                                bulkCopy.ColumnMappings.Add("DO_ITEM_NUMBER", "DO_ITEM_NUMBER");
                                bulkCopy.ColumnMappings.Add("DISTRIBUTOR_CODE", "DISTRIBUTOR_CODE");
                                bulkCopy.ColumnMappings.Add("BRANCH_CODE", "BRANCH_CODE");
                                bulkCopy.ColumnMappings.Add("BUYING_RATE", "BUYING_RATE");
                                bulkCopy.ColumnMappings.Add("STOCK_POINT", "STOCK_POINT");
                                bulkCopy.ColumnMappings.Add("BO_QUANTITY", "BO_QUANTITY");
                                bulkCopy.ColumnMappings.Add("OS_QTY", "OS_QTY");
                                bulkCopy.ColumnMappings.Add("SHIPPED_DATE", "SHIPPED_DATE");
                                bulkCopy.ColumnMappings.Add("ARRIVED_DATE", "ARRIVED_DATE");
                                bulkCopy.ColumnMappings.Add("BINNING_INSTRUCTION_DATE", "BINNING_INSTRUCTION_DATE");
                                bulkCopy.ColumnMappings.Add("BINNED_DATE", "BINNED_DATE");
                                bulkCopy.ColumnMappings.Add("ETA", "ETA");
                                bulkCopy.ColumnMappings.Add("MATRIX_RANK", "MATRIX_RANK");
                                bulkCopy.ColumnMappings.Add("NEW_COMMODITY_CODE", "NEW_COMMODITY_CODE");
                                bulkCopy.ColumnMappings.Add("OS_SHIPPED", "OS_SHIPPED");
                                bulkCopy.ColumnMappings.Add("OS_ARRIVED", "OS_ARRIVED");
                                bulkCopy.ColumnMappings.Add("OS_BIN", "OS_BIN");
                                bulkCopy.ColumnMappings.Add("OS_SHIPPED_AMT", "OS_SHIPPED_AMT");
                                bulkCopy.ColumnMappings.Add("OS_ARRIVED_AMT", "OS_ARRIVED_AMT");
                                bulkCopy.ColumnMappings.Add("OS_BIN_AMT", "OS_BIN_AMT");
                                bulkCopy.ColumnMappings.Add("AGING", "AGING");
                                bulkCopy.ColumnMappings.Add("REMARK", "REMARK");
                                bulkCopy.ColumnMappings.Add("PO_QTY_VS_CONFIRM_QTY", "PO_QTY_VS_CONFIRM_QTY");
                                bulkCopy.WriteToServer(dt);
                                IsSuccess = true;
                            }
                            catch (DbEntityValidationException ex)
                            {
                                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                                var sw = new System.IO.StreamWriter(filename, true);
                                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);

                                foreach (var eve in ex.EntityValidationErrors)
                                {
                                    sw.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State);
                                    foreach (var ve in eve.ValidationErrors)
                                    {
                                        sw.WriteLine("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                                    }
                                }
                                sw.Close();
                            }
                        }

                        connection.Close();
                    }
                }

                if (IsSuccess)
                {
                    OspoLog log = new OspoLog();
                    log.Activity = "Received Ospo Data";
                    log.TimeLogs = DateTime.Now;
                    log.AffectedRows = dt.Rows.Count;
                    db.OspoLogs.Add(log);
                    db.SaveChanges();
                }
            }
            catch (DbEntityValidationException ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);

                foreach (var eve in ex.EntityValidationErrors)
                {
                    sw.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sw.WriteLine("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                }
                sw.Close();
            }
            return IsSuccess;
        }

        public static Boolean ImportStockFromDB(string remoteConnection, string localConnection)
        {
            kmsi_portalEntities db = new kmsi_portalEntities();
            TimeSpan startTime = DateTime.Now.TimeOfDay;
            DataTable dt = new DataTable();
            Boolean IsSuccess = false;
            try
            {
                using (SqlConnection conn = new SqlConnection(remoteConnection))
                {
                    conn.Open();
                    var cmd = new SqlCommand();
                    cmd.CommandText = string.Format("Select * From Kpintar_Stock_Info");
                    cmd.Connection = conn;

                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        da.Fill(dt);
                    }

                    conn.Close();
                }

                if (dt.Rows.Count > 0)
                {
                    using (SqlConnection connection = new SqlConnection(localConnection))
                    {
                        connection.Open();

                        string truncateTable = "TRUNCATE TABLE Kpintar_Stock_Information";
                        SqlCommand command = new SqlCommand(truncateTable, connection);
                        command.ExecuteNonQuery();

                        SqlTransaction transaction = connection.BeginTransaction();
                        try
                        {
                            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection, SqlBulkCopyOptions.TableLock | SqlBulkCopyOptions.FireTriggers, transaction))
                            {
                                bulkCopy.BulkCopyTimeout = 0;
                                bulkCopy.BatchSize = 10000;

                                bulkCopy.DestinationTableName = "Kpintar_Stock_Information";
                                bulkCopy.ColumnMappings.Add("PART_NUMBER", "PART_NUMBER");
                                bulkCopy.ColumnMappings.Add("ITC_PART_NUMBER", "ITC_PART_NUMBER");
                                bulkCopy.ColumnMappings.Add("ITC_CODE", "ITC_CODE");
                                bulkCopy.ColumnMappings.Add("PART_NAME", "PART_NAME");
                                bulkCopy.ColumnMappings.Add("FOO", "FOO");
                                bulkCopy.ColumnMappings.Add("FOH", "FOH");
                                bulkCopy.ColumnMappings.Add("EO_RSV", "EO_RSV");
                                bulkCopy.ColumnMappings.Add("EO_RSV_DEPO", "EO_RSV_DEPO");
                                bulkCopy.ColumnMappings.Add("FREE_SHIPPED_QTY", "FREE_SHIPPED_QTY");
                                bulkCopy.ColumnMappings.Add("FREE_ARRIVED_QTY", "FREE_ARRIVED_QTY");
                                bulkCopy.ColumnMappings.Add("STOCK_POINT", "STOCK_POINT");
                                bulkCopy.WriteToServer(dt);
                                transaction.Commit();

                                IsSuccess = true;
                            }
                        }
                        catch (DbEntityValidationException ex)
                        {
                            var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                            var sw = new System.IO.StreamWriter(filename, true);
                            sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);

                            foreach (var eve in ex.EntityValidationErrors)
                            {
                                sw.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State);
                                foreach (var ve in eve.ValidationErrors)
                                {
                                    sw.WriteLine("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                                }
                            }
                            sw.Close();

                            transaction.Rollback();
                        }
                        finally
                        {
                            connection.Close();
                        }
                    }
                }

                if (IsSuccess)
                {
                    TimeSpan endTime = DateTime.Now.TimeOfDay;
                    StockInfoLog logs = new StockInfoLog();
                    logs.Activity = "Received Stock Info";
                    logs.TimeLogs = DateTime.Now;
                    logs.AffectedRows = dt.Rows.Count;
                    logs.ExecutionTime = Convert.ToDecimal(endTime.Subtract(startTime).TotalMilliseconds);
                    db.StockInfoLogs.Add(logs);
                    db.SaveChanges();
                }
            }
            catch (DbEntityValidationException ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);

                foreach (var eve in ex.EntityValidationErrors)
                {
                    sw.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sw.WriteLine("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                }
                sw.Close();
            }
            return IsSuccess;
        }

        public static Boolean ImportPartInfoFromDB(string remoteConnection, string localConnection)
        {
            kmsi_portalEntities db = new kmsi_portalEntities();
            TimeSpan startTime = DateTime.Now.TimeOfDay;
            DataTable dt = new DataTable();
            Boolean IsSuccess = false;
            try
            {
                using (SqlConnection conn = new SqlConnection(remoteConnection))
                {
                    conn.Open();
                    var cmd = new SqlCommand();
                    cmd.CommandText = string.Format("Select * From [Kpintar_Part_Info]");
                    cmd.Connection = conn;

                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        da.Fill(dt);
                    }

                    conn.Close();
                }
                
                if (dt.Rows.Count > 0)
                {
                    using (SqlConnection connection = new SqlConnection(localConnection))
                    {
                        connection.Open();

                        string truncateTable = "TRUNCATE TABLE [Kpintar_Part_Info]";
                        SqlCommand command = new SqlCommand(truncateTable, connection);
                        command.ExecuteNonQuery();

                        SqlTransaction transaction = connection.BeginTransaction();
                        try
                        {
                            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection, SqlBulkCopyOptions.TableLock | SqlBulkCopyOptions.FireTriggers, transaction))
                            {
                                bulkCopy.BulkCopyTimeout = 0;
                                bulkCopy.BatchSize = 10000;

                                bulkCopy.DestinationTableName = "Kpintar_Part_Info";
                                bulkCopy.ColumnMappings.Add("PART_NUMBER", "PART_NUMBER");
                                bulkCopy.ColumnMappings.Add("PART_NAME", "PART_NAME");
                                bulkCopy.ColumnMappings.Add("LATEST_INTERCHANGE_CODE", "LATEST_INTERCHANGE_CODE");
                                bulkCopy.ColumnMappings.Add("LATEST_INTERCHANGE_PART_NO", "LATEST_INTERCHANGE_PART_NO");
                                bulkCopy.ColumnMappings.Add("LIST_PRICE_CURRENT_IDR", "LIST_PRICE_CURRENT_IDR");
                                bulkCopy.ColumnMappings.Add("LIST_PRICE_CURRENT_USD", "LIST_PRICE_CURRENT_USD");
                                bulkCopy.ColumnMappings.Add("DEPOT_MORTALITY", "DEPOT_MORTALITY");
                                bulkCopy.ColumnMappings.Add("MATRIX_RANK", "MATRIX_RANK");
                                bulkCopy.ColumnMappings.Add("NEW_COMMODITY_CODE", "NEW_COMMODITY_CODE");
                                bulkCopy.ColumnMappings.Add("UNIT_WEIGHT", "UNIT_WEIGHT");
                                bulkCopy.ColumnMappings.Add("NET_WEIGHT", "NET_WEIGHT");
                                bulkCopy.ColumnMappings.Add("QTY_PER_MACHINE", "QTY_PER_MACHINE");
                                bulkCopy.ColumnMappings.Add("SERVICE_NEWS_NUMBER", "SERVICE_NEWS_NUMBER");
                                bulkCopy.ColumnMappings.Add("FOO", "FOO");
                                bulkCopy.ColumnMappings.Add("FOH", "FOH");
                                bulkCopy.ColumnMappings.Add("LT_PROD", "LT_PROD");
                                bulkCopy.ColumnMappings.Add("LT_DELIVERY", "LT_DELIVERY");
                                bulkCopy.WriteToServer(dt);
                                transaction.Commit();

                                IsSuccess = true;
                            }
                        }
                        catch (DbEntityValidationException ex)
                        {
                            var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                            var sw = new System.IO.StreamWriter(filename, true);
                            sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);

                            foreach (var eve in ex.EntityValidationErrors)
                            {
                                sw.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State);
                                foreach (var ve in eve.ValidationErrors)
                                {
                                    sw.WriteLine("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                                }
                            }
                            sw.Close();

                            transaction.Rollback();
                        }
                        finally
                        {
                            connection.Close();
                        }
                    }
                }
                
                if (IsSuccess)
                {
                    TimeSpan endTime = DateTime.Now.TimeOfDay;
                    PartInfoLog log = new PartInfoLog();
                    log.Activity = "Received Part Info";
                    log.TimeLogs = DateTime.Now;
                    log.AffectedRows = dt.Rows.Count;
                    log.ExecutionTime = Convert.ToDecimal(endTime.Subtract(startTime).TotalMilliseconds);
                    db.PartInfoLogs.Add(log);
                    db.SaveChanges();
                }
            }
            catch (DbEntityValidationException ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);

                foreach (var eve in ex.EntityValidationErrors)
                {
                    sw.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sw.WriteLine("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                }
                sw.Close();
            }
            return IsSuccess;
        }

        public static String UploadFileToDir(HttpPostedFileBase file, string dirPath)
        {
            try
            {
                if (!Directory.Exists(dirPath))
                    Directory.CreateDirectory(dirPath);

                string fileName = StringGenerator.RandomStringAlphanumeric(8);
                string fileExt = Path.GetExtension(file.FileName);
                string newName = fileName + fileExt;
                var saveDir = Path.Combine(dirPath + newName);
                file.SaveAs(saveDir);

                if (!System.IO.File.Exists(saveDir))
                    return String.Empty;

                return newName;
            }
            catch (Exception ex)
            {
                var filename = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logErrors.txt";
                var sw = new System.IO.StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Message + " " + ex.InnerException);
                sw.Close();

                return String.Empty;
            }
        }
    }
}