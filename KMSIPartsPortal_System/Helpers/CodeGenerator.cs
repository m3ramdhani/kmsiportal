﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace KMSIPartsPortal_System.Helpers
{
    public class CodeGenerator
    {
        public static string Generate(string Prefix, string Delimiter, string LastCode)
        {
            int length = 5;
            var lastNumber = Regex.Match(LastCode, @"\d+$").Value;
            int currentNumber = Convert.ToInt32(lastNumber) + 1;
            string dtYear = DateTime.Now.ToString("yy");
            return Prefix + Delimiter + "00" + dtYear + Delimiter + currentNumber.ToString().PadLeft(length, '0');
        }
    }
}