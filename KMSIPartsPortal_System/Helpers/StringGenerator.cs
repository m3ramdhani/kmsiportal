﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Web;

namespace KMSIPartsPortal_System.Helpers
{
    public class StringGenerator
    {
        private static Random random = new Random();

        public static string RandomString(int length)
        {
            RNGCryptoServiceProvider cryptoServiceProvider = new RNGCryptoServiceProvider();
            byte[] randomBytes = new byte[length];
            cryptoServiceProvider.GetBytes(randomBytes);
            return Convert.ToBase64String(randomBytes);
        }

        public static string RandomStringAlphanumeric(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static string GenerateAutoNumber(string Prefix, string Delimiter, string LastCode)
        {
            int length = 5;
            var lastNumber = Regex.Match(LastCode, @"\d+$").Value;
            int currentNumber = Convert.ToInt32(lastNumber) + 1;
            string dtYear = DateTime.Now.ToString("yy");
            return Prefix + Delimiter + "00" + dtYear + Delimiter + currentNumber.ToString().PadLeft(length, '0');
        }
    }
}