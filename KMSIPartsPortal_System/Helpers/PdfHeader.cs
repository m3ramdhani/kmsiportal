﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.draw;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KMSIPartsPortal_System.Helpers
{
    public class PdfHeader : PdfPageEventHelper
    {
        public override void OnStartPage(PdfWriter writer, Document document)
        {
            //Table
            PdfPTable table = new PdfPTable(2);
            table.WidthPercentage = 100;
            table.SpacingBefore = 0f;
            table.SpacingAfter = 0f;

            //Cell no 1
            Image header1 = Image.GetInstance(HttpContext.Current.Server.MapPath("~/Content/img/header-1.png"));
            header1.ScaleAbsolute(154f, 35f);
            header1.Alignment = Image.ALIGN_LEFT;
            PdfPCell cell = new PdfPCell();
            cell.Border = 0;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.AddElement(header1);
            table.AddCell(cell);

            //Cell no 2
            Image header2 = Image.GetInstance(HttpContext.Current.Server.MapPath("~/Content/img/header-2.png"));
            header2.ScaleAbsolute(110f, 21f);
            header2.Alignment = Image.ALIGN_RIGHT;
            cell = new PdfPCell();
            cell.Border = 0;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.AddElement(header2);
            table.AddCell(cell);

            //Add table to document
            document.Add(table);

            //Horizontal Line
            LineSeparator line = new LineSeparator(0.0F, 100.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1);
            document.Add(line);
        }
    }
}